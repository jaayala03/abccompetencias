socket.on('message', function(msg) {
    //    console.log(msg);
    $("#alert-no-notificaciones").remove();
    msg_amaran(msg);
    agregarelemento(msg);
    //titlenotifier.add();
});

function msg_amaran(data) {
    $.amaran({
        'theme': 'awesome' + ' ' + data.tipo,
        'content': {
            title: data.titulo,
            message: data.mensaje,
            info: '',
            icon: data.icono
        },
        closeButton: true,
        'position': 'top right',
        closeOnClick: false,
        'cssanimationIn': 'shake',
        sticky: false,
        'delay': '10000',
        beforeStart: function() {
            ion.sound.play("arpeggio");
        },
        onClick: function() {
            window.parent.location.href = data.url;
        }
    });
}

function agregarelemento(data) {
    var url_leer = laroute.url('krauff/leernotificacion', [data.codnotificacion]);
    var container = $(".media-notificaciones");
    
    var containerCont = $(".cantNotificaciones");
    var element = '<a href="'+url_leer+'" class="media note-noleida">';

    element += '<div class="media-object pull-left"><i class="' + data.icono + ' fg-success"></i></div>';
    element += '<div class="media-body">';
    element += '<span class="media-text">' + data.mensaje + '</span>';
    element += '<!-- Start meta icon -->';
    element += '<span class="media-meta">Hace un momento</span>';
    element += '<!--/ End meta icon -->';
    element += '</div><!-- /.media-body -->';
    element += '</a><!-- /.media -->';

    var cantidad_actual = $(".media-notificaciones > .media").size();
    if (cantidad_actual > 0) {
        //$(".container-notifications > .item:nth-child(0)").before(element);
        container.prepend(element);
    } else {
        container.append(element);
    }
    //var cantidad_despues = $('.container-notifications > .item').size();
    var cantidad_despues = 0;

    $(".media-notificaciones > .media").each(function(i, obj) {
        if ($(obj).hasClass("note-noleida")) {
            cantidad_despues++;
        }
    });
    containerCont.text(cantidad_despues);
}


$(document).on("click", ".note-close", function(e) {
    var parent = $(this).closest(".item");
    var objid = $(parent).attr('id');
    var id = objid.replace('notificacion', '');
    $.ajax({
        type: "POST",
        url: baseurl + 'notificaciones/eliminardeusuario',
        data: {
            "codnotificacion": id
        },
        success: function(response) {
            var data = jQuery.parseJSON(response);
            if (data.success) {
                $("#cantNotificaciones").text(data.cantidad_actual);
                $("#" + objid).slideUp(1000, function() {
                    $(this).remove();
                });
                var cantidad_actual = $('.container-notifications > .item').size();
                if (cantidad_actual === 1) {
                    $(".container-notifications").html('<li style=\"display:none\" id=\"alert-no-notificaciones\"><em>Sin nada nuevo.</em></li>');
                    $("#alert-no-notificaciones").fadeIn(2000);
                }
            }
        }
    });
});

$("#delete-all-notifications").on("click", function() {
    var data = {
        "_token": token
    };
    dialogo_confirmacion_ajax({
        title: "Advertencia",
        message: "¿Esta seguro que desea eliminar todas sus notificaciones?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_notitifaciones(data);
        },
        data: data
    });
});

function eliminar_notitifaciones(data) {
    var url = baseurl + '/krauff/eliminarnotificaciones';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            if (data.success) {
                notifier.show("Correcto!", "Sus notificaciones fueron eliminadas correctamente.", 'success', 5000);
            }
            $(".cantNotificaciones").text(data.cantidad_actual);
            $(".media-notificaciones").html('<li id=\"alert-no-notificaciones\"><em>Sin nada nuevo.</em></li>');
            
        }
    });
}