(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://abccompetencias.test',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/open","name":"debugbar.openhandler","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@handle"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/clockwork\/{id}","name":"debugbar.clockwork","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/telescope\/{id}","name":"debugbar.telescope","action":"Barryvdh\Debugbar\Controllers\TelescopeController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/stylesheets","name":"debugbar.assets.css","action":"Barryvdh\Debugbar\Controllers\AssetController@css"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/javascript","name":"debugbar.assets.js","action":"Barryvdh\Debugbar\Controllers\AssetController@js"},{"host":null,"methods":["DELETE"],"uri":"_debugbar\/cache\/{key}\/{tags?}","name":"debugbar.cache.delete","action":"Barryvdh\Debugbar\Controllers\CacheController@delete"},{"host":null,"methods":["GET","HEAD"],"uri":"auditoria\/listar","name":null,"action":"Modules\Auditoria\Http\Controllers\AuditoriaController@index"},{"host":null,"methods":["POST"],"uri":"auditoria\/getAll","name":null,"action":"Modules\Auditoria\Http\Controllers\AuditoriaController@getAll"},{"host":null,"methods":["GET","HEAD"],"uri":"auditoria\/verdetalles\/{id}","name":null,"action":"Modules\Auditoria\Http\Controllers\AuditoriaController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"auditoria\/exportar","name":"auditoria.exportar","action":"Modules\Auditoria\Http\Controllers\AuditoriaController@exportar"},{"host":null,"methods":["GET","HEAD"],"uri":"auditoria\/accesos\/listar","name":"auditoria.accesos.listar","action":"Modules\Auditoria\Http\Controllers\AccesosController@index"},{"host":null,"methods":["POST"],"uri":"auditoria\/accesos\/getAll","name":null,"action":"Modules\Auditoria\Http\Controllers\AccesosController@getAll"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/verperfil","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@verperfil"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/getimage\/{id}","name":"krauff.getimage","action":"Modules\Krauff\Http\Controllers\KrauffController@getimage"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/getfile\/{id}","name":"krauff.getfile","action":"Modules\Krauff\Http\Controllers\KrauffController@getfile"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/verpdf","name":"krauff.verpdf","action":"Modules\Krauff\Http\Controllers\KrauffController@verpdf"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/editarperfil","name":"krauff.editarperfil","action":"Modules\Krauff\Http\Controllers\UsuariosController@editarperfil"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/leernotificacion\/{id}","name":"krauff.leernotificacion","action":"Modules\Krauff\Http\Controllers\UsuariosController@leernotificacion"},{"host":null,"methods":["POST"],"uri":"krauff\/updateperfil","name":"krauff.updateperfil","action":"Modules\Krauff\Http\Controllers\UsuariosController@updateperfil"},{"host":null,"methods":["POST"],"uri":"krauff\/eliminarnotificaciones","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@eliminarnotificaciones"},{"host":null,"methods":["POST"],"uri":"krauff\/validarcontrasena","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@validarcontrasena"},{"host":null,"methods":["POST"],"uri":"krauff\/editarperfilajax","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@editarperfilajax"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/usuarios","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@listar"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/TodosDatos","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@TodosDatos"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/crear","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@store"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/eliminar","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/usuarios\/editar\/{id}","name":"krauff.usuarios.editar","action":"Modules\Krauff\Http\Controllers\UsuariosController@editar"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/update","name":"krauff.usuarios.update","action":"Modules\Krauff\Http\Controllers\UsuariosController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/usuarios\/funcionalidades\/{id}","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@ShowFunc"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/cargarfunc","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@LoadFunc"},{"host":null,"methods":["POST"],"uri":"krauff\/usuarios\/guardarfunc","name":null,"action":"Modules\Krauff\Http\Controllers\UsuariosController@SaveFunc"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/perfiles","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@listar"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/TodosDatos","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@TodosDatos"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/crear","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@store"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/eliminar","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@destroy"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/editar","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/perfiles\/funcionalidades\/{id}","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@ShowFunc"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/cargarfunc","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@LoadFunc"},{"host":null,"methods":["POST"],"uri":"krauff\/perfiles\/guardarfunc","name":null,"action":"Modules\Krauff\Http\Controllers\PerfilesController@SaveFunc"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/perfiles\/cargarusuariosperfil\/{codperfilcifrado}","name":"krauff.perfiles.cargarusuariosperfil","action":"Modules\Krauff\Http\Controllers\PerfilesController@cargarusuariosperfil"},{"host":null,"methods":["GET","HEAD"],"uri":"krauff\/ubicaciones\/buscarUbicacion","name":null,"action":"Modules\Krauff\Http\Controllers\UbicacionesController@buscarUbicacion"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/inicio","name":"outside.inicio","action":"Modules\Outside\Http\Controllers\OutsideController@inicio"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/quienes_somos","name":"outside.quienes_somos","action":"Modules\Outside\Http\Controllers\OutsideController@quienes_somos"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/servicios","name":"outside.servicios","action":"Modules\Outside\Http\Controllers\OutsideController@servicios"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/acerca_de","name":"outside.acerca_de","action":"Modules\Outside\Http\Controllers\OutsideController@acerca_de"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/consultas","name":"outside.consultas","action":"Modules\Outside\Http\Controllers\OutsideController@consultas"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/descargas","name":"outside.descargas","action":"Modules\Outside\Http\Controllers\OutsideController@descargas"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/ingresar","name":"outside.ingresar","action":"Modules\Outside\Http\Controllers\OutsideController@ingresar"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/getimage\/{id}","name":"outside.getimage","action":"Modules\Outside\Http\Controllers\OutsideController@getimage"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/getfile\/{id}","name":"outside.getfile","action":"Modules\Outside\Http\Controllers\OutsideController@getfile"},{"host":null,"methods":["POST"],"uri":"outside\/usuarios\/registrarme","name":"krauff.registrarme","action":"Modules\Outside\Http\Controllers\UsuariosController@registrarme"},{"host":null,"methods":["POST"],"uri":"outside\/usuarios\/validar_documento","name":"outside.usuarios.validar_documento","action":"Modules\Outside\Http\Controllers\UsuariosController@validar_documento"},{"host":null,"methods":["POST"],"uri":"outside\/usuarios\/validar_email","name":"outside.usuarios.validar_email","action":"Modules\Outside\Http\Controllers\UsuariosController@validar_email"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/usuarios\/activar_usuario\/{codusuario_cifrado}","name":"outside.usuarios.activar_usuario","action":"Modules\Outside\Http\Controllers\UsuariosController@activar_usuario"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/imagenes_fachada","name":"outside.imagenes_fachada.listar","action":"Modules\Outside\Http\Controllers\Imagenes_fachadaController@listar"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/imagenes_fachada\/cargarcrear","name":"outside.imagenes_fachada.cargarcrear","action":"Modules\Outside\Http\Controllers\Imagenes_fachadaController@cargarcrear"},{"host":null,"methods":["POST"],"uri":"outside\/imagenes_fachada\/crear","name":"outside.imagenes_fachada.crear","action":"Modules\Outside\Http\Controllers\Imagenes_fachadaController@crear"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/noticias\/{slug}","name":"outside.vernoticia","action":"Modules\Outside\Http\Controllers\OutsideController@vernoticia"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/noticias\/admin\/listar","name":"outside.noticias.listar","action":"Modules\Outside\Http\Controllers\NoticiasController@listar"},{"host":null,"methods":["POST"],"uri":"outside\/noticias\/admin\/datatable","name":"outside.noticias.datatable","action":"Modules\Outside\Http\Controllers\NoticiasController@datatable"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/noticias\/admin\/cargarcrear","name":"outside.noticias.cargarcrear","action":"Modules\Outside\Http\Controllers\NoticiasController@cargarcrear"},{"host":null,"methods":["POST"],"uri":"outside\/noticias\/admin\/crear","name":"outside.noticias.crear","action":"Modules\Outside\Http\Controllers\NoticiasController@crear"},{"host":null,"methods":["GET","HEAD"],"uri":"outside\/noticias\/admin\/cargareditar\/{codnoticia}","name":"outside.noticias.cargareditar","action":"Modules\Outside\Http\Controllers\NoticiasController@cargareditar"},{"host":null,"methods":["POST"],"uri":"outside\/noticias\/admin\/editar","name":"outside.noticias.editar","action":"Modules\Outside\Http\Controllers\NoticiasController@editar"},{"host":null,"methods":["POST"],"uri":"outside\/noticias\/admin\/eliminar","name":"outside.noticias.eliminar","action":"Modules\Outside\Http\Controllers\NoticiasController@eliminar"},{"host":null,"methods":["POST"],"uri":"outside\/solicitudes\/consultar","name":"outside.solicitudes.consultar","action":"Modules\Outside\Http\Controllers\SolicitudesController@consultar"},{"host":null,"methods":["POST"],"uri":"outside\/normas\/todosdatosNormas","name":"outside.normas.todosdatosNormas","action":"Modules\Outside\Http\Controllers\NormasController@todosdatosNormas"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/importar","name":"parametros.importar","action":"Modules\Parametros\Http\Controllers\ParametrosController@importar"},{"host":null,"methods":["POST"],"uri":"parametros\/subir_importacion","name":"parametros.subir_importacion","action":"Modules\Parametros\Http\Controllers\ParametrosController@subir_importacion"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/preguntas","name":"parametros.preguntas.listar","action":"Modules\Parametros\Http\Controllers\PreguntasController@listar"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/preguntas\/cargarcrear","name":"parametros.preguntas.cargarcrear","action":"Modules\Parametros\Http\Controllers\PreguntasController@cargarcrear"},{"host":null,"methods":["POST"],"uri":"parametros\/preguntas\/crear","name":"parametros.preguntas.crear","action":"Modules\Parametros\Http\Controllers\PreguntasController@crear"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/preguntas\/cargareditar\/{codpregunta}","name":"parametros.preguntas.cargareditar","action":"Modules\Parametros\Http\Controllers\PreguntasController@cargareditar"},{"host":null,"methods":["POST"],"uri":"parametros\/preguntas\/editar","name":"parametros.preguntas.editar","action":"Modules\Parametros\Http\Controllers\PreguntasController@editar"},{"host":null,"methods":["POST"],"uri":"parametros\/preguntas\/datatable","name":"parametros.preguntas.datatable","action":"Modules\Parametros\Http\Controllers\PreguntasController@datatable"},{"host":null,"methods":["POST"],"uri":"parametros\/preguntas\/eliminar","name":"parametros.preguntas.eliminar","action":"Modules\Parametros\Http\Controllers\PreguntasController@eliminar"},{"host":null,"methods":["POST"],"uri":"parametros\/preguntas\/obtenerpreguntas_aleatorio","name":"parametros.preguntas.obtenerpreguntas_aleatorio","action":"Modules\Parametros\Http\Controllers\PreguntasController@obtenerpreguntas_aleatorio"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/cuestionarios","name":"parametros.cuestionarios.listar","action":"Modules\Parametros\Http\Controllers\CuestionariosController@listar"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/cuestionarios\/cargarcrear","name":"parametros.cuestionarios.cargarcrear","action":"Modules\Parametros\Http\Controllers\CuestionariosController@cargarcrear"},{"host":null,"methods":["POST"],"uri":"parametros\/cuestionarios\/crear","name":"parametros.cuestionarios.crear","action":"Modules\Parametros\Http\Controllers\CuestionariosController@crear"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/cuestionarios\/cargareditar\/{codpregunta}","name":"parametros.cuestionarios.cargareditar","action":"Modules\Parametros\Http\Controllers\CuestionariosController@cargareditar"},{"host":null,"methods":["POST"],"uri":"parametros\/cuestionarios\/editar","name":"parametros.cuestionarios.editar","action":"Modules\Parametros\Http\Controllers\CuestionariosController@editar"},{"host":null,"methods":["POST"],"uri":"parametros\/cuestionarios\/datatable","name":"parametros.cuestionarios.datatable","action":"Modules\Parametros\Http\Controllers\CuestionariosController@datatable"},{"host":null,"methods":["POST"],"uri":"parametros\/cuestionarios\/eliminar","name":"parametros.cuestionarios.eliminar","action":"Modules\Parametros\Http\Controllers\CuestionariosController@eliminar"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/normas","name":"parametros.normas.listar","action":"Modules\Parametros\Http\Controllers\NormasController@listar"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/normas\/cargarcrear","name":"parametros.normas.cargarcrear","action":"Modules\Parametros\Http\Controllers\NormasController@cargarcrear"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/crear","name":"parametros.normas.crear","action":"Modules\Parametros\Http\Controllers\NormasController@crear"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/normas\/cargareditar\/{codpregunta}","name":"parametros.normas.cargareditar","action":"Modules\Parametros\Http\Controllers\NormasController@cargareditar"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/editar","name":"parametros.normas.editar","action":"Modules\Parametros\Http\Controllers\NormasController@editar"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/datatable","name":"parametros.normas.datatable","action":"Modules\Parametros\Http\Controllers\NormasController@datatable"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/eliminar","name":"parametros.normas.eliminar","action":"Modules\Parametros\Http\Controllers\NormasController@eliminar"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/obtener_actividades","name":"parametros.normas.obtener_actividades","action":"Modules\Parametros\Http\Controllers\NormasController@obtener_actividades"},{"host":null,"methods":["GET","HEAD"],"uri":"parametros\/normas\/indicadores\/listar\/{codnorma}","name":"parametros.normas.indicadores.listar","action":"Modules\Parametros\Http\Controllers\IndicadoresController@listar"},{"host":null,"methods":["POST"],"uri":"parametros\/normas\/indicadores\/guardar","name":"parametros.normas.indicadores.guardar","action":"Modules\Parametros\Http\Controllers\IndicadoresController@guardar"},{"host":null,"methods":["POST"],"uri":"reportes\/buscarNorma","name":"reportes.buscarNorma","action":"Modules\Reportes\Http\Controllers\ReportesController@buscarNorma"},{"host":null,"methods":["POST"],"uri":"reportes\/buscarUsuario","name":"reportes.buscarUsuario","action":"Modules\Reportes\Http\Controllers\ReportesController@buscarUsuario"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/historialsolicitudes","name":"reportes.historialsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@historialsolicitudes"},{"host":null,"methods":["POST"],"uri":"reportes\/todosdatosHistorialsolicitudes","name":"reportes.todosdatosHistorialsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@todosdatosHistorialsolicitudes"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/exportar_historialsolicitudes","name":"reportes.exportar_historialsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@exportar_historialsolicitudes"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/cantidadsolicitudes","name":"reportes.cantidadsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@cantidadsolicitudes"},{"host":null,"methods":["POST"],"uri":"reportes\/todosdatosCantidadsolicitudes","name":"reportes.todosdatosCantidadsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@todosdatosCantidadsolicitudes"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/exportar_cantidadsolicitudes","name":"reportes.exportar_cantidadsolicitudes","action":"Modules\Reportes\Http\Controllers\ReportesController@exportar_cantidadsolicitudes"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/pagos","name":"reportes.pagos","action":"Modules\Reportes\Http\Controllers\ReportesController@pagos"},{"host":null,"methods":["POST"],"uri":"reportes\/todosdatosPagos","name":"reportes.todosdatosPagos","action":"Modules\Reportes\Http\Controllers\ReportesController@todosdatosPagos"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/exportar_pagos","name":"reportes.exportar_pagos","action":"Modules\Reportes\Http\Controllers\ReportesController@exportar_pagos"},{"host":null,"methods":["GET","HEAD"],"uri":"reportes\/calificaciones","name":"reportes.calificaciones","action":"Modules\Reportes\Http\Controllers\ReportesController@calificaciones"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/registrar","name":"solicitudes.registrar","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@registrar"},{"host":null,"methods":["POST"],"uri":"solicitudes\/registrar_titulaciones","name":"solicitudes.registrar_titulaciones","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@registrar_titulaciones"},{"host":null,"methods":["POST"],"uri":"solicitudes\/registrar_normas","name":"solicitudes.registrar_normas","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@registrar_normas"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/missolicitudes","name":"solicitudes.missolicitudes","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@missolicitudes"},{"host":null,"methods":["POST"],"uri":"solicitudes\/todosdatosmissolicitudes","name":"solicitudes.todosdatosmissolicitudes","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@todosdatosmissolicitudes"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/todas","name":"solicitudes.todas","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@todas"},{"host":null,"methods":["POST"],"uri":"solicitudes\/datatable_todas","name":"solicitudes.datatable_todas","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@datatable_todas"},{"host":null,"methods":["POST"],"uri":"solicitudes\/cambiaretapa","name":"solicitudes.cambiaretapa","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@cambiaretapa"},{"host":null,"methods":["POST"],"uri":"solicitudes\/agendar","name":"solicitudes.agendar","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@agendar"},{"host":null,"methods":["POST"],"uri":"solicitudes\/getSolicitud","name":"solicitudes.getSolicitud","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@getSolicitud"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/evaluacion\/{codevaluacionsolicitudcifrado}","name":"solicitudes.evaluacion","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@evaluacion"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/generarpdfevaluacion\/{codevaluacionsolicitudcifrado}","name":"solicitudes.generarpdfevaluacion","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@generarpdfevaluacion"},{"host":null,"methods":["POST"],"uri":"solicitudes\/registrarvaloracion","name":"solicitudes.registrarvaloracion","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@registrarvaloracion"},{"host":null,"methods":["POST"],"uri":"solicitudes\/registrarevaluacionsolicitud","name":"solicitudes.registrarevaluacionsolicitud","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@registrarevaluacionsolicitud"},{"host":null,"methods":["POST"],"uri":"solicitudes\/evaluar","name":"solicitudes.evaluar","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@evaluar"},{"host":null,"methods":["POST"],"uri":"solicitudes\/habilitar","name":"solicitudes.habilitar","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@habilitar"},{"host":null,"methods":["POST"],"uri":"solicitudes\/enlacepago","name":"solicitudes.enlacepago","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@enlacepago"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/pagos\/respuesta","name":"solicitudes.pagos.respuesta_pago","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@respuesta_pago"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/pagos\/confirmacion","name":"solicitudes.pagos.confirmacion_pago","action":"Modules\Solicitudes\Http\Controllers\SolicitudesController@confirmacion_pago"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/pagos\/detalle\/{codpagosolicitud}","name":"solicitudes.pagos.detalle","action":"Modules\Solicitudes\Http\Controllers\PagosSolicitudesController@detalle"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/responder\/{codcuestionario_solicitud}","name":"solicitudes.cuestionarios.responder","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@responder"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/respondercorto\/{codcuestionario_solicitud}","name":"solicitudes.cuestionarios.respondercorto","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@respondercorto"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/tiempo_finalizado\/{codcuestionariocifrado}","name":"solicitudes.cuestionarios.tiempo_finalizado","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@tiempo_finalizado"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/finalizar\/{codcuestionariocifrado}","name":"solicitudes.cuestionarios.finalizar","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@finalizar"},{"host":null,"methods":["POST"],"uri":"solicitudes\/cuestionarios\/rep_tiempo","name":"solicitudes.cuestionarios.rep_tiempo","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@rep_tiempo"},{"host":null,"methods":["POST"],"uri":"solicitudes\/cuestionarios\/preguntas\/responder","name":"solicitudes.cuestionarios.preguntas.responder","action":"Modules\Solicitudes\Http\Controllers\PreguntasController@responder"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/resultados\/{codsolicitudcifrado}","name":"solicitudes.cuestionarios.resultados","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@resultados"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/imprimircuestionario\/{codsolicitudcifrado}","name":"solicitudes.cuestionarios.imprimircuestionario","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@imprimircuestionario"},{"host":null,"methods":["GET","HEAD"],"uri":"solicitudes\/cuestionarios\/verpdf\/{codsolicitudcifrado}","name":"solicitudes.cuestionarios.verpdf","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@verpdf"},{"host":null,"methods":["POST"],"uri":"solicitudes\/cuestionarios\/generarnuevo","name":"solicitudes.cuestionarios.generarnuevo","action":"Modules\Solicitudes\Http\Controllers\CuestionariosController@generarnuevo"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"inactivo","name":"usuario.inactivo","action":"ABCcomptencias\Http\Controllers\HomeController@usuarioinactivo"},{"host":null,"methods":["POST"],"uri":"registrarse","name":"register.create","action":"ABCcomptencias\Http\Controllers\Auth\RegisterController@create"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"ABCcomptencias\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"ABCcomptencias\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"ABCcomptencias\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"ABCcomptencias\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"ABCcomptencias\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"ABCcomptencias\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"ABCcomptencias\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"ABCcomptencias\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":"password.update","action":"ABCcomptencias\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":null,"action":"ABCcomptencias\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"logs","name":null,"action":"\Rap2hpoutre\LaravelLogViewer\LogViewerController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"home#{hash?}","name":"home","action":"ABCcomptencias\Http\Controllers\HomeController@index"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

