$(document).ready(function() {
    tbl_historial = $("#tbl_historial").DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: baseurl + "/reportes/todosdatosHistorialsolicitudes",
            type: "POST",
            data: function(d) {
                d.fecha_inicio = app.fecha_inicio;
                d.fecha_fin = app.fecha_fin;
                d.etapa = app.etapa;
                d.codnorma = app.codnorma;
                d.codusuario = app.codusuario;
            }
        },
        fnDrawCallback: function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $(".image-lightbox").featherlight({
                type: "image"
            });
        },
        columns: [{
                data: "documento",
                name: "usuarios.documento"
            },
            {
                data: "candidato",
                name: "usuarios.nombres"
            },
            {
                data: "candidato",
                name: "usuarios.primerapellido",
                visible: false
            },
            {
                data: "candidato",
                name: "usuarios.segundoapellido",
                visible: false
            },
            {
                data: "codigonorma",
                name: "normas.codigo"
            },
            {
                className: "text-center",
                data: "etapa",
                name: "etapas_solicitudes.etapa"
            },
            {
                className: "text-center",
                data: "fechaetapa",
                name: "etapas_solicitudes.created_at"
            },
            {
                className: "text-center",
                data: "opciones",
                searchable: false,
                orderable: false
            }
        ]
    });
});

// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        fecha_inicio: "",
        fecha_fin: "",
        etapas: window.etapas,
        etapa: "",
        normas: [],
        codnorma: [],
        codusuario: [],
        usuarios: [],
        loading: false
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        buscarNormas(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;

                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/reportes/buscarNorma", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.normas = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.normas = [];
            }
        },
        buscarUsuario(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;

                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/reportes/buscarUsuario", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.usuarios = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.usuarios = [];
            }
        },
        actualizardatatable() {
            tbl_historial.ajax.reload(null, false);
        },
        limpiardatatable() {
            this.fecha_inicio = "";
            this.fecha_fin = "";
            this.etapa = "";
            this.usuarios = [];
            this.codusuario = [];
            this.codnorma = [];
            this.normas = [];
            this.actualizardatatable();
        },
        exportar() {
            var route = "reportes.exportar_historialsolicitudes";
            if (route === "") {
                swal("Error!", "No se ha podido exportar!", "warning");
                return false;
            }
            var url =
                baseurl +
                laroute.route(route, {
                    fecha_inicio: this.fecha_inicio,
                    fecha_fin: this.fecha_fin,
                    etapa: this.etapa,
                    codnorma: this.codnorma,
                    codusuario: this.codusuario
                });
            var win = window.open(url, "_blank");
            win.focus();
        }
    }
});