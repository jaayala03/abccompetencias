// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        cuestionarios_solicitud: [],
        // cuestionario: {},
        solicitud: {},
        preguntas: {
            currentpage: 1,
            perpage: 10,
            items: [],
            datapaginate: []
        },
        finalizado: false,
        chart: null,
        agendar: {
            tipo: 1,
            fecha_agendada: "",
            hora: "",
            codevaluador: "",
        },
        evaluadores: window.evaluadores,
    },
    components: {},
    computed: {
        cuestionarios_aprobados(){
            return this.cuestionarios_solicitud.filter(function (a) {
                return a.porcentaje && a.porcentaje >= 85;
            });
        },
        cuestionarios_secundarios() {
            return this.cuestionarios_solicitud.filter(function (a) {
                return a.cuestionario.tipo == 2;
            });
        },
        cuestionarios_solicitud_pendientes() {
            return this.cuestionarios_solicitud.filter(o => !o.terminado);
        }
    },
    mounted: function () {
        // this.preguntas.items = window.preguntas.map((ob, index) => {
        //     ob.numero = index + 1;
        //     return ob;
        // });

        // if (this.preguntas.items.length > 0) {
        //     this.preguntas.datapaginate = this.preguntas.items.slice(0, this.preguntas.items.length / (this.preguntas.items.length / this.preguntas.perpage))
        // }
        // this.cuestionario = window.cuestionario;
        this.solicitud = window.solicitud;
        
        this.cuestionarios_solicitud = window.CuestionariosSolicitud;

        setTimeout(() => {
            this.cuestionarios_solicitud.forEach(element => {
                if(element.chart) {
                    element.chart.data.datasets.forEach((dataset) => {
                        dataset.data = newValue;
                    });
                    element.chart.update();
                } else {
                    var id = 'chart-'+ element.codcuestionario_solicitud;
                    element.chart = new Chart(id, {
                        type: 'bar',
                        data: {
                            labels: ["Correctas", "Incorrectas", "No respondidas"],
                            datasets: [{
                                label: "Resultados",
                                backgroundColor: ["#9CC96C", "#F76C52","#EBAA4C"],
                                data: this.chartData(element)
                            }],
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: false,
                            tooltips: {
                                enabled: true
                            },
                            title: {
                                display: true,
                                text: 'Gráfica'
                            }
                        }
                    });
                }
            });
         }, 1000);
    },
    watch: {

    },
    methods: {
        /**
         * metodo abrir un modal
         */
        showmodal(name) {
            this.$refs[name].open();
        },
        /**
         * metodo cerrar un modal
         */
        hidemodal(name) {
            this.$refs[name].close();
        },
        chartData(cuestionario_solicitud) {
            return [this.correctas(cuestionario_solicitud).length, this.incorrectas(cuestionario_solicitud).length, this.no_respondidas(cuestionario_solicitud).length]
        },
        aprobado(cuestionario_solicitud) {
            return cuestionario_solicitud.porcentaje && cuestionario_solicitud.porcentaje >= 85;
        },
        respondidas(cuestionario_solicitud) {
            return cuestionario_solicitud.preguntas.filter(function (a) {
                return a.value;
            });
        },
        no_respondidas(cuestionario_solicitud) {
            return cuestionario_solicitud.preguntas.filter(function (a) {
                return !a.value;
            });
        },
        correctas(cuestionario_solicitud) {
            return cuestionario_solicitud.preguntas.filter(function (a) {
                var correcta = a.opciones.find(x => x.correcta);
                if(correcta) {
                    if(a.value == correcta.codopcion){
                        return true;
                    }
                }
                return false;
            });
        },
        incorrectas(cuestionario_solicitud) {
            return cuestionario_solicitud.preguntas.filter(function (a) {
                var correcta = a.opciones.find(x => x.correcta);
                if(correcta) {
                    if(a.value && a.value != correcta.codopcion){
                        return true;
                    }
                }
                return false;
            });
        },
        resetagendar(){
            this.agendar = {
                tipo: 1,
                fecha_agendada: "",
                hora: "",
                codevaluador: "",
            }
        },
        confirmar_generar_nuevo() {
            var self = this;
            this.$validator.validateAll("form_agendar").then(result => {
                if (result) {
                    self.$confirm('¿Está seguro que desea generar un nuevo cuestionario?', 'Atención', {
                        confirmButtonText: 'Si / generar',
                        cancelButtonText: 'Cancelar',
                        type: 'warning',
                        center: true
                    }).then(() => {
                        self.generar_nuevo();
                    }).catch(() => {
                    });
                }
            });
        },
        generar_nuevo() {
            var self = this;
            swal({
                showCloseButton: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                focusConfirm: false,
                showCancelButton: false,
                allowOutsideClick: false,
                title: 'Generando el nuevo cuestionario',
                html: 'Por favor espere...',
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: function () {
                    axios.post(baseurl + '/solicitudes/cuestionarios/generarnuevo', {
                        codcuestionario_solicitudcifrado: self.cuestionarios_solicitud[0].codcuestionario_solicitudcifrado,
                        agendar: self.agendar
                    })
                    .then(function (response) {
                        var data =  response.data;
                        self.cuestionarios_solicitud = data.cuestionarios_solicitud;
                        self.hidemodal('modal_agendar')
                        swal.close();
                        swal('Éxito', 'Nuevo cuestionario generado correctamente.', 'success')
                    })
                    .catch(function (error) {
                        console.log(error)
                        swal('Error', 'No se ha podido generar el nuevo cuestionario.', 'error')
                    });
                },
                onClose: () => {
    
                }
            })
        },
        verpdf() {
            var url = baseurl + laroute.url('/solicitudes/cuestionarios/verpdf', [this.cuestionarios_solicitud[0].solicitud.codsolicitudcifrado]);
            var win = window.open(url, '_blank');
            win.focus();
        },
        checked_op(pregunta, opcion){
            respondida = pregunta.value;
            if(respondida == opcion.codopcion){
                return true;
            }
            return false;
        },
        onCurrentPageP(page) {
            this.preguntas.datapaginate = this.preguntas.items.slice((page * this.preguntas.perpage) - this.preguntas.perpage, page * this.preguntas.perpage);
        }
    }
});