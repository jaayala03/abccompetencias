var visible_agendada = false;
$(document).ready(function () {
    // console.log(app.seleccionados, 'en el document');

    $.each(app.etapas_solicitud, function (index, etapa) {
        if (etapa.key === '3') { //agendados
            visible_agendada = true;
        } else {
            visible_agendada = false;
        }
        var tabla_solicitud = $("#tbl_todas_" + etapa.key).DataTable({
            aLengthMenu: [
                [10, 25, 50, 100, 200, -1],
                [10, 25, 50, 100, 200, "Todos"]
            ],
            orderClasses: false,
            processing: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url: baseurl + "/solicitudes/datatable_todas",
                type: "POST",
                data: function (d) {
                    d.etapa = etapa.key;
                    d.fecha_inicio = app.fecha_inicio;
                    d.fecha_fin = app.fecha_fin;
                    d.norma_filtros = app.norma_filtros;
                },
                dataFilter: function (data) {
                    var json = jQuery.parseJSON(data);
                    var total = json.recordsFiltered;
                    $("#cont_" + etapa.key).text(total);
                    return data;
                }
            },
            fnDrawCallback: function (oSettings, json) {
                $(".tooltip-opcion").tooltip();
                $(".image-lightbox").featherlight({
                    type: "image"
                });
            },
            columns: [{
                className: "text-center",
                data: "codsolicitud",
                render: function (data, type, row) {
                    if (type === "display") {
                        return (
                            '<div class="ckbox ckbox-success"><input name="codssolicitudes" value="' +
                            row.codsolicitud +
                            '" type="checkbox" id="' +
                            row.codsolicitud +
                            '" class="arraySolicitudes arraySolicitudes_' + index + ' editor-active"><label for="' +
                            row.codsolicitud +
                            '"><span></span></label></div>'
                        );
                    }
                    return data;
                },
                searchable: false,
                orderable: false
            },
            {
                data: "documento",
                name: "documento"
            },
            {
                className: "text-center",
                data: "usuario",
                searchable: false,
                orderable: false
            },
            {
                data: "codigo",
                name: "codigo"
            },
            {
                data: "nombre",
                name: "nombre"
            },
            {
                data: "fecha",
                name: "fecha"
            },
            {
                className: "text-center",
                data: "opciones",
                searchable: false,
                orderable: false
            }
            ]
        });
        app.tablas_solicitudes.push(tabla_solicitud);
        app.etapas_solicitud[index].table = tabla_solicitud;
    });

    $(".tablas_solicitud").on("click", ".veragendamientos", function (e) {
        e.preventDefault();
        var codsolicitudcifrado = $(this).data("id");
        app.modalveragendamientos(codsolicitudcifrado);
    });
});
$(document).ready(function () {
    $(document).on('change', '.arraySolicitudes', function () {
        if (this.checked) {
            app.seleccionados.push(this.value);
        } else {
            var pos = app.seleccionados.indexOf(this.value);
            app.seleccionados.splice(pos, 1);
        }
        const dataArr = new Set(app.seleccionados);
        let result = [...dataArr];
        app.seleccionados = result;

        // console.log(app.seleccionados, 'al checkear');
    });
});


// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        etapas_solicitud: window.etapas_solicitud,
        cuestionarios: window.cuestionarios,
        evaluadores: window.evaluadores,
        normas: window.normas,
        tablas_solicitudes: [],
        activetab: 0,
        oldtab: 0,
        codssolicitudes: [],
        codcuestionario: "",
        etapa: "",
        fecha_inicio: "",
        fecha_fin: "",
        fecha_agendar: "",
        hora_agendar: "",
        codevaluador: "",
        norma_filtros: "",
        solicitud: [],
        seleccionados: []
    },
    components: {},
    computed: {},
    mounted: function () {
        this.activetab = 1;
        $('a[data-toggle="tab"]').on("shown.bs.tab", this.settab);
        this.seleccionados = [];

        // console.log(this.seleccionados, 'pille loca');
    },
    watch: {
        activetab: function (val, old) {
            if (old <= 0) {
                this.oldtab = 0;
            } else {
                this.oldtab = old - 1;
            }
        }
    },
    methods: {
        /**
         * metodo abrir un modal
         */
        showmodal(name) {
            this.$refs[name].open();
        },
        /**
         * metodo cerrar un modal
         */
        hidemodal(name) {
            this.$refs[name].close();
        },
        settab(e) {
            var self = this;
            var target = $(e.target).attr("href");
            $.each(this.etapas_solicitud, function (index, etapa) {
                if (target == "#tab_" + etapa.key) {
                    self.activetab = parseInt(etapa.key);
                }
            });
            this.seleccionados = [];
            this.tablas_solicitudes[parseInt(this.oldtab)].ajax.reload();
        },
        modalveragendamientos(codsolicitudcifrado) {
            this.solicitud = [];
            var datos = {
                codsolicitudcifrado: codsolicitudcifrado,
            };
            var self = this;
            axios
                .post(baseurl + "/solicitudes/getSolicitud", datos)
                .then(function (response) {
                    var data = response.data;
                    self.solicitud = data;
                    self.showmodal("modal_veragendamientos");

                })
                .catch(function (error) {
                    self.solicitud = [];
                    console.log(error);
                });
        },
        format_date(fecha) {
            return moment(fecha).format('YYYY/M/DD');
        },
        pago_registrado(ids) {
            var self = this;
            this.$confirm('¿Está seguro que desea continuar?', 'Confirmación', {
                confirmButtonText: 'Si',
                cancelButtonText: 'Cancelar',
                type: 'warning',
                center: true
            }).then(() => {
                axios
                    .post(baseurl + "/solicitudes/pagoregistrado", {
                        solicitudes: ids
                    })
                    .then(function (response) {
                        var data = response.data;
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type_optional
                        });
                        self.seleccionados = [];

                        self.reset_datatables();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }).catch(() => { });
        },
        agendar() {
            var self = this;
            this.$validator.validateAll("form_agendar").then(result => {
                if (result) {
                    var datos = {
                        codssolicitudes: self.seleccionados,
                        fecha_agendar: self.fecha_agendar,
                        hora_agendar: self.hora_agendar,
                        codevaluador: self.codevaluador,
                    };
                    $("#btn_agendar").attr('disabled', 'disabled');
                    axios
                        .post(baseurl + "/solicitudes/agendar", datos)
                        .then(function (response) {
                            $("#btn_agendar").removeAttr('disabled');
                            var data = response.data;
                            if (data.type == "error") {
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            } else {
                                self.reset_datatables();
                                self.hidemodal("modal_agendar");
                                self.seleccionados = [];
                                self.fecha_agendar = "";
                                self.hora_agendar = "";
                                self.codevaluador = "";
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                } else {
                    self.$message({
                        showClose: true,
                        message: "Campos incompletos.",
                        type: "error"
                    });
                }
            });
        },
        habilitar() {
            var self = this;
            this.$validator.validateAll("form_habilitar").then(result => {
                if (result) {
                    var datos = {
                        codssolicitudes: self.seleccionados,
                        codcuestionario: self.codcuestionario,
                    };
                    $("#btn_habilitar").attr('disabled', 'disabled');
                    axios
                        .post(baseurl + "/solicitudes/habilitar", datos)
                        .then(function (response) {
                            $("#btn_habilitar").removeAttr('disabled');
                            var data = response.data;
                            if (data.type == "error") {
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            } else {
                                self.reset_datatables();
                                self.hidemodal("modal_habilitar");
                                self.seleccionados = [];
                                self.codcuestionario = "";
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                } else {
                    self.$message({
                        showClose: true,
                        message: "Campos incompletos.",
                        type: "error"
                    });
                }
            });
        },
        // seleccionados() {
        //     var self = this;
        //     var cods = [];
        //     $('input[name="codssolicitudes"]:checked').each(function () {
        //         cods.push($(this).val());
        //     });
        //     this.codssolicitudes = cods;
        // },
        reset_datatables() {
            var self = this;
            for (var i = 0; i < this.tablas_solicitudes.length; i++) {
                self.tablas_solicitudes[i].ajax.reload();
            }
        },
    }
});

$('.seleccionartodo').on('click', function () {
    // console.log("seleccionartodo", app.etapas_solicitud, app.etapas_solicitud[$(this).data('etapa')]);
    var self = this;
    var etapa = $(this).data('etapa');
    app.seleccionados = [];
    var table = app.etapas_solicitud[etapa].table;
    var rows = table.rows({
        'search': 'applied'
    }).nodes();
    _.forEach(rows, function (fila, key) {
        //console.log($('.arraySolicitudes', fila).val())
        if (self.checked) {
            app.seleccionados.push($('.arraySolicitudes_' + etapa, fila).val());
        } else {
            var pos = app.seleccionados.indexOf($('.arraySolicitudes_' + etapa, fila).val());
            app.seleccionados.splice(pos, 1);
        }
    });
    $('.arraySolicitudes_' + etapa, rows).prop('checked', this.checked);
    // console.log(app.seleccionados, app.activetab)
});

$('.tablas_solicitud').on('click', '.btn-sig-etapa', function () {
    var codsolicitud = $(this).data('codsolicitud');
    app.pago_registrado([codsolicitud]);
});