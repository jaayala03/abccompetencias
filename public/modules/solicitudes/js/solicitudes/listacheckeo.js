// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        evaluacionsolicitud: window.evaluacionsolicitud,
        fecha_resultados: "",
        indicadores: window.indicadores,
        evaluadores: window.evaluadores,
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        registrarvaloracion(indicador) {
            var self = this;
            axios
                .post(baseurl + "/solicitudes/registrarvaloracion", indicador)
                .then(function(response) {
                    var data = response.data;
                    if (data.type == "success") {
                        self.indicadores = data.indicadores;
                    } else {
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        registrarevaluacionsolicitud() {
            var self = this;
            axios
                .post(
                    baseurl + "/solicitudes/registrarevaluacionsolicitud",
                    self.evaluacionsolicitud
                )
                .then(function(response) {
                    var data = response.data;
                    if (data.type == "success") {
                        self.evaluacionsolicitud = data.evaluacionsolicitud;
                    } else {
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        formatfecha(fecha) {
            return moment(fecha).format('DD-MM-YYYY');
        }
    }
});