// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        tipo_solicitud: '',
        titulaciones: {},
        normas: {},
        tipos_titulacion: {},
        seleccionadas: [],
    },
    components: {},
    computed: {},
    mounted: function () {
        this.titulaciones = window.titulaciones;
        this.normas = window.normas;
        this.tipos_titulacion = window.tipos_titulacion;
        // console.log(this.tipos_titulacion);
    },
    watch: {},
    methods: {
        toggleSelection(rows) {
            if (rows) {
                rows.forEach(row => {
                    this.$refs.multipleTable.toggleRowSelection(row);
                });
            } else {
                this.$refs.multipleTable.clearSelection();
            }
        },
        handleSelectionChange(val) {
            this.seleccionadas = val;
        },
        confirm_registrar_titulaciones() {
            var self = this;
            var data = {
                "solicitud": this.seleccionadas
            };
            dialogo_confirmacion_ajax({
                title: "Registrar Solicitud",
                message: "¿Está seguro que desea registrar la solicitud con los datos seleccionados?",
                labelconfirm: "Si / Registrar",
                callbackfunction: function (data) {
                    self.registrar_titulaciones(data);
                },
                data: data
            });
        },
        registrar_titulaciones(data) {
            var self = this;
            var url = baseurl + '/solicitudes/registrar_titulaciones';
            call_ajax({
                url: url,
                data: data,
                callbackfunctionsuccess: function (data) {
                    swal({
                        type: 'success',
                        title: 'Éxito',
                        text: 'Su solicitud fue registrada, por favor dirijase a su correo electronico para mas información',
                    });
                    self.reset();
                }
            });
        },
        confirm_registrar_normas() {
            var self = this;
            var data = {
                "solicitud": this.seleccionadas
            };
            dialogo_confirmacion_ajax({
                title: "Registrar Solicitud",
                message: "¿Está seguro que desea registrar la solicitud con los datos seleccionados?",
                labelconfirm: "Si / Registrar",
                callbackfunction: function (data) {
                    self.registrar_normas(data);
                },
                data: data
            });
        },
        registrar_normas(data) {
            $('#btn_registrar').attr('disabled', 'disabled');
            var self = this;
            swal({
                showCloseButton: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                focusConfirm: false,
                showCancelButton: false,
                allowOutsideClick: false,
                title: 'Generando la Solicitud',
                html: '<span class="label label-success rounded">Por favor espere...</span>',
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: function () {
                    var url = baseurl + '/solicitudes/registrar_normas';
                    axios.post(url, data)
                        .then(({ data }) => {
                            swal.close();
                            swal({
                                type: 'success',
                                title: data.title || 'Éxito',
                                text: data.message || 'Su solicitud fue registrada, por favor dirijase a su correo electronico para mas información',
                            });
                            $('#btn_registrar').removeAttr('disabled');
                            self.reset();
                        })
                        .catch(({ response: { data } }) => {
                            swal.close();
                            swal({
                                type: 'error',
                                title: data.title || 'Error',
                                text: data.message || 'Ha ocurrido un error mientras se registraba su solicitud, por favor intente nuevamente.',
                            });
                            $('#btn_registrar').removeAttr('disabled');
                        });

                },
                onClose: () => {

                }
            });
        },
        reset() {
            this.tipo_solicitud = '';
            this.seleccionadas = [];
        }
    }
});