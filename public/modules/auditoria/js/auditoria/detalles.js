// =========================================================================
// Instancia Vue
// =========================================================================
var vm = new Vue({
    el: "#app",
    data: {
        activity: window.activity,
        propiedades: window.propiedades
    },
    computed: {},
    mounted: function() {
        console.log(this.propiedades)
    },
    watch: {},
    methods: {

    }
});
