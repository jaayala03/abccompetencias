//******************************************************************************
//Datatable perfiles
//******************************************************************************
$(document).ready(function() {
    tablaPerfiles = $('#perfiles').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/krauff/perfiles/TodosDatos',
            "type": "POST",
            "data": function(d) {}
        },
        fnDrawCallback: function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
        },
        columns: [{
            data: 'nombreperfil',
            name: 'nombreperfil'
        }, {
            className: "text-center",
            data: 'horario',
        }, {
            className: "text-center",
            data: 'restriccion_ip',
        }, {
            className: "text-center",
            data: 'usuarios'
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false,
        }]
    });
    //******************************************************************************
    //******************************************************************************
    //EVENTO AJAX PARA CREAR PERFIL
    //******************************************************************************
    $('#btn-guardar').click(function() {
        var url = baseurl + '/krauff/perfiles/crear';
        call_ajax({
            url: url,
            data: $("#frm_crear_perfil").serialize(),
            callbackfunctionsuccess: function(data) {
                notifier.show(data.title, data.message, 'success', 5000);
                tablaPerfiles.ajax.reload();
                $('#PerfilesCrear').modal('hide');
                $('#PerfilesCrear').find('form').trigger('reset');
            }
        });
    });
    //******************************************************************************
    //******************************************************************************
    //EVENTO AJAX PARA ELIMINAR PERFIL
    //******************************************************************************
    $('#perfiles').on("click", ".btn-eliminarperfil", function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var data = {
            "id": id
        };
        dialogo_confirmacion_ajax({
            title: "Eliminar perfil",
            message: "¿Está seguro que desea eliminar?",
            labelconfirm: "Si / Eliminar",
            callbackfunction: function(data) {
                eliminar_perfil(data);
            },
            data: data
        });
    });

    function eliminar_perfil(data) {
        var url = baseurl + '/krauff/perfiles/eliminar';
        call_ajax({
            url: url,
            data: data,
            callbackfunctionsuccess: function(data) {
                notifier.show(data.title, data.message, 'success', 5000);
                tablaPerfiles.ajax.reload();
            }
        });
    }
    //******************************************************************************
    //******************************************************************************
    //VENTANA FLOTANTE PARA EDITAR
    //******************************************************************************
    $('#PerfilesEditar').on('shown.bs.modal', function(e) {
        if (typeof e.relatedTarget !== 'undefined') {
            var perfil = e.relatedTarget.dataset['perfil'];
            var codperfil = e.relatedTarget.dataset['id'];
            var codpaginainicio = e.relatedTarget.dataset['codpaginainicio'];
            var hinicio = e.relatedTarget.dataset['hinicio'];
            var hfin = e.relatedTarget.dataset['hfin'];
            var restriccion_ip = e.relatedTarget.dataset['restriccion_ip'];
            $("#frm_editar_perfil input[name=nombreperfil]").val(perfil);
            $("#frm_editar_perfil input[name=codperfil]").val(codperfil);
            $("#frm_editar_perfil input[name=hinicio]").val(hinicio);
            $("#frm_editar_perfil input[name=hfin]").val(hfin);
            $("#codpaginainicio_edit").val(codpaginainicio);
            if (restriccion_ip == 1) {
                $('.switch_restriccion_ip').bootstrapSwitch('state', true);
            } else {
                $('.switch_restriccion_ip').bootstrapSwitch('state', false);
            }
        }
    });
    $('#PerfilesEditar').on('hidden.bs.modal', function(e) {});
    //******************************************************************************
    //******************************************************************************
    //EVENTO AJAX PARA EDITAR PERFIL
    //******************************************************************************
    $('#btn-guardar-editar').click(function() {
        $('#PerfilesEditar').modal('hide');
        var data = $("#frm_editar_perfil").serializeArray();
        var r_ip = $('.switch_restriccion_ip').bootstrapSwitch('state');
        if (r_ip) {
            //alert("1")
            data.push({
                name: 'restriccion_ip',
                value: 1
            });
        } else {
            //alert("2")
            data.push({
                name: 'restriccion_ip',
                value: 2
            });
        }
        dialogo_confirmacion_ajax({
            title: "Editar perfil",
            message: "¿Está seguro que desea editar?",
            labelconfirm: "Si / Editar",
            callbackfunction: function(data) {
                editar_perfil(data);
            },
            callbackfunctioncancel: function(data) {
                $('#PerfilesEditar').modal('show');
            },
            data: data
        });
    });

    function editar_perfil(data) {
        var url = baseurl + '/krauff/perfiles/editar';
        call_ajax({
            url: url,
            data: data,
            callbackfunctionsuccess: function(data) {
                notifier.show(data.title, data.message, 'success', 5000);
                tablaPerfiles.ajax.reload();
                $('#PerfilesEditar').modal('hide');
            }
        });
    }
    //******************************************************************************
    //******************************************************************************
    //VENTANA FLOTANTE PARA FUNCIONALDADES
    //******************************************************************************
    $(document).ready(function() {
        $('#VentanaFlotanteFunc').on('shown.bs.modal', function(e) {
            var title = e.relatedTarget.dataset['title'];
            var url = e.relatedTarget.dataset['id'];
            var src = $('#contenedorIframeFunc').attr('data-iframe-src');
            src = baseurl + "/krauff/perfiles/funcionalidades/" + url;
            $('#contenedorIframeFunc').attr('src', src);
            $("#VentanaFlotanteFunc").find("#titulo-modal").html(title);
        });
        $('#VentanaFlotanteFunc').on('hidden.bs.modal', function(e) {
            $('#contenedorIframeFunc').attr('src', '');
        });
    });
    //******************************************************************************
    $('.inputmask_hora').inputmask({
        mask: '99:99'
    });
    $('.switch_restriccion_ip').bootstrapSwitch();
});