var AdminDashboard = function() {
    return {
        init: function() {
            if (window.location.hash === '') {
                AdminDashboard.goapp();
            }
        },
        goapp: function() {
            new Vue({
                el: '#dashboard',
                data: {
                    dashboard: {
                        'section1': {
                            'tramites': {
                                'count': 0,
                                'decrease': 0
                            },
                            'impuestos': {
                                'count': 0,
                                'decrease': 0
                            },
                            'parqueaderos': {
                                'count': 0,
                                'decrease': 0
                            },
                            'comparendos': {
                                'count': 0,
                                'decrease': 0
                            },
                        },
                        'section2': {
                            'diario': {
                                'count': 0,
                                'decrease': 0
                            },
                            'diario_old': {
                                'count': 0
                            },
                            'semanal': {
                                'count': 0,
                                'decrease': 0
                            },
                            'semanal_old': {
                                'count': 0
                            },
                            'tramites': {
                                'count': 0
                            },
                            'multas': {
                                'count': 0
                            },
                            'otros_pagos': {
                                'count': 0
                            },
                        },
                        'section3': {
                            'emitidas': {
                                'count': 0
                            },
                            'pagadas': {
                                'count': 0
                            },
                            'utilizadas': {
                                'count': 0
                            },
                            'anuladas': {
                                'count': 0
                            },
                        },
                        'section4': {
                            'particular': {
                                'count': 0
                            },
                            'publico': {
                                'count': 0
                            },
                            'oficial': {
                                'count': 0
                            },
                            'otro': {
                                'count': 0
                            },
                        },
                    }
                },
                mounted: function() {
                    this.fetchDashboard();
                    this.handleTooltip();
                },
                methods: {
                    handleTooltip: function() {
                        if ($('[data-toggle=tooltip]').length) {
                            $('[data-toggle=tooltip]').tooltip({
                                animation: 'fade'
                            });
                        }
                    },
                    handleCounter() {
                        if ($('.counter').length) {
                            $('.counter').counterUp({
                                delay: 10,
                                time: 4000
                            });
                        }
                    },
                    fetchDashboard() {
                        var self = this;
                        this.$http.post(baseurl + '/getdashboard', null, {
                            headers: {
                                Accept: "application/json"
                            }
                        }).then(response => {
                            self.dashboard = response.data;
                            self.handleCounter();
                            this.someData = response.body;
                        }, response => {
                            console.log(response)
                        });
                    },
                },
            })
        }
    };
}();
AdminDashboard.init();