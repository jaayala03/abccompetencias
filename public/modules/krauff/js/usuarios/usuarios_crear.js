// =========================================================================
// datepicker
// =========================================================================
$(document).ready(function() {
    // Default datepicker (options)
    $('#fechanacimiento').flatpickr({
        "locale": "es",
    });
});
// =========================================================================
// recortar foto perfil
// =========================================================================
$(document).ready(function() {
    var $image = $(".image-crop > img");
    $($image).cropper({
        aspectRatio: '1',
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });
    var $inputImage = $("#FileInput");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                files = this.files,
                file;
            if (!files.length) {
                return;
            }
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }
    $("#zoomIn").click(function() {
        $image.cropper("zoom", 0.1);
    });
    $("#zoomOut").click(function() {
        $image.cropper("zoom", -0.1);
    });
    $("#rotateLeft").click(function() {
        $image.cropper("rotate", 45);
    });
    $("#rotateRight").click(function() {
        $image.cropper("rotate", -45);
    });
    $("#setDrag").click(function() {
        $image.cropper("setDragMode", "crop");
    });
});
//==========================================================
//validation
//==========================================================
var BlankonFormWizard = function() {
    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function() {
            BlankonFormWizard.twitterBootstrapWizard();
        },
        // =========================================================================
        // TWITTER BOOTSTRAP WIZARD
        // =========================================================================
        twitterBootstrapWizard: function() {
            if ($('#validacion').length) {
                var $validator = $("#frm_crear_usuario").validate({
                    rules: {
                        tipodoc: {
                            required: true
                        },
                        documento: {
                            required: true
                        },
                        nombres: {
                            required: true
                        },
                        primerapellido: {
                            required: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        codperfil: {
                            required: true
                        },
                        estado: {
                            required: true
                        },
                        nombreusuario: {
                            required: true
                        },
                        password: {
                            required: true,
                            minlength: 8,
                            maxlength: 20
                        },
                        password2: {
                            required: true,
                            equalTo: "#password"
                        }
                    },
                    messages: {
                        tipodoc: {
                            required: "Seleccione un tipo de documento."
                        },
                        documento: {
                            required: "Este campo es requerido."
                        },
                        nombres: {
                            required: "Este campo es requerido."
                        },
                        primerapellido: {
                            required: "Este campo es requerido."
                        },
                        email: {
                            required: "Este campo es requerido.",
                            email: "Ingrese un correo valido."
                        },
                        codperfil: {
                            required: "Seleccione un perfil."
                        },
                        estado: {
                            required: "Seleccione un estado."
                        },
                        nombreusuario: {
                            required: "Este campo es requerido."
                        },
                        password: {
                            required: "Este campo es requerido.",
                            minlength: "La contraseña debe tener minimo 8 carcteres.",
                            maxlength: "La contraseña debe tener maximo 20 carcteres."
                        },
                        password2: {
                            required: "Este campo es requerido.",
                            equalTo: "Las contraseñas no coinciden."
                        }
                    },
                    highlight: function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    },
                    submitHandler: function() {
                        alert("submitted!");
                    }
                });
                $('#validacion').bootstrapWizard({
                    'nextSelector': '.next',
                    'previousSelector': '.previous',
                    'onNext': function(tab, navigation, index) {
                        var $valid = $("#frm_crear_usuario").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false
                        }
                        var $active = navigation.find('li.active');
                        $active.addClass('success-step');
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacion').find('.progress-bar').css('width', $percent + '%');
                    },
                    onPrevious: function(tab, navigation, index) {
                        if (index == -1) {
                            return false;
                        }
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacion').find('.progress-bar').css('width', $percent + '%');
                    },
                    onTabClick: function(tab, navigation, index) {
                        return false;
                    },
                    onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacion').find('.progress-bar').css('width', $percent + '%');
                    }
                });
            }
        }
    };
}();
// Call main app init
BlankonFormWizard.init();


//******************************************************************************
//Busqueda de ubicaciones residencia
//******************************************************************************
$(document).ready(function() {
    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('nombre'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: baseurl + '/krauff/ubicaciones/buscarUbicacion?q=%QUERY'
        }
    });
    engine.initialize();
    $('#codubicacion').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        items: 5
    }, {
        name: 'codubicacion',
        display: function(item) {
            return item.nombre + ' - ' + item.padre.nombre
        },
        source: engine.ttAdapter(),
        templates: {
            empty: ['<div class="list-group search-results-dropdown"><div class="list-group-item">sin resultados</div></div>'],
            suggestion: function(data) {
                return '<div>' + data.nombre + ' - ' + data.padre.nombre + '</a>';
            }
        }
    }).on('typeahead:selected', function(event, data) {
        $("#codubicacion_value").val(data.codubicacion);
        $("#codubicacion").prop("disabled", "disabled");
    });
    $(".remove-ubicacion").click(function(e) {
        $('#codubicacion').typeahead('val', '');
        $("#codubicacion_value").val('');
        $("#codubicacion").removeAttr("disabled");
    });
});
//******************************************************************************


//******************************************************************************
//Busqueda de ubicaciones nacimiento
//******************************************************************************
$(document).ready(function() {
    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('nombre'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: baseurl + '/krauff/ubicaciones/buscarUbicacion?q=%QUERY'
        }
    });
    engine.initialize();
    $('#codubicacionnacimiento').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        items: 5
    }, {
        name: 'codubicacion',
        display: function(item) {
            return item.nombre + ' - ' + item.padre.nombre
        },
        source: engine.ttAdapter(),
        templates: {
            empty: ['<div class="list-group search-results-dropdown"><div class="list-group-item">sin resultados</div></div>'],
            suggestion: function(data) {
                return '<div>' + data.nombre + ' - ' + data.padre.nombre + '</a>';
            }
        }
    }).on('typeahead:selected', function(event, data) {
        $("#codubicacionnacimiento_value").val(data.codubicacion);
        $("#codubicacionnacimiento").prop("disabled", "disabled");
    });
    $(".remove-nacimiento").click(function(e) {
        $('#codubicacionnacimiento').typeahead('val', '');
        $("#codubicacionnacimiento_value").val('');
        $("#codubicacionnacimiento").removeAttr("disabled");
    });
});
//******************************************************************************