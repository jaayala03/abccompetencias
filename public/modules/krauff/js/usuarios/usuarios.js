//******************************************************************************
//Datatable usuarios
//******************************************************************************
$(document).ready(function() {
    tablaUsuariosActivos = $('#tbl_usuariosactivos').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/krauff/usuarios/TodosDatos',
            "type": "POST",
            "data": function(d) {
                d.estado = 1;
                if (typeof codperfilcifrado !== 'undefined') {
                    d.codperfilcifrado = codperfilcifrado;
                }
            },
            dataFilter: function(data) {
                var json = jQuery.parseJSON(data);
                var total = json.recordsFiltered;
                $('#contador-usuariosactivos').text(total);
                return (data);
            }
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $("#usuarios_filter").css({
                "display": "none"
            });
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'documento',
            name: 'documento',
            visible: false,
        }, {
            data: 'primerapellido',
            name: 'primerapellido',
            visible: false,
        }, {
            data: 'segundoapellido',
            name: 'segundoapellido',
            visible: false,
        }, {
            data: 'codperfil',
            name: 'codperfil',
        }, {
            className: "text-center",
            data: 'foto'
        }, {
            data: 'nombres',
        }, {
            data: 'email',
            name: 'email'
        }, {
            data: 'telefono',
            name: 'telefono'
        }, {
            data: 'celular',
            name: 'celular',
            visible: false
        }, {
            data: 'created_at',
            name: 'created_at'
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
    tablaUsuariosInactivos = $('#tbl_usuariosinactivos').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/krauff/usuarios/TodosDatos',
            "type": "POST",
            "data": function(d) {
                d.estado = 2;
                if (typeof codperfilcifrado !== 'undefined') {
                    d.codperfilcifrado = codperfilcifrado;
                }
            },
            dataFilter: function(data) {
                var json = jQuery.parseJSON(data);
                var total = json.recordsFiltered;
                $('#contador-usuariosinactivos').text(total);
                return (data);
            }
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $("#usuarios_filter").css({
                "display": "none"
            });
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'documento',
            name: 'documento',
            visible: false,
        }, {
            data: 'primerapellido',
            name: 'primerapellido',
            visible: false,
        }, {
            data: 'segundoapellido',
            name: 'segundoapellido',
            visible: false,
        }, {
            data: 'codperfil',
            name: 'codperfil',
        }, {
            className: "text-center",
            data: 'foto'
        }, {
            data: 'nombres',
        }, {
            data: 'email',
            name: 'email'
        }, {
            data: 'telefono',
            name: 'telefono'
        }, {
            data: 'celular',
            name: 'celular',
            visible: false
        }, {
            data: 'created_at',
            name: 'created_at'
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
    $("#busquedadatatable").keypress(function(e) {
        if (e.which == 13) {
            tablaUsuariosActivos.search(this.value).draw();
            tablaUsuariosInactivos.search(this.value).draw();
        }
    });
    $("#busquedadatatable").keyup(function(e) {
        if ($(this).val() == "") {
            tablaUsuariosActivos.search(this.value).draw();
            tablaUsuariosInactivos.search(this.value).draw();
        }
    });
    $('#borrarBusqueda').click(function() {
        $('#busquedadatatable').val("");
        tablaUsuariosActivos.search($("#busquedadatatable").val()).draw();
        tablaUsuariosInactivos.search($("#busquedadatatable").val()).draw();
    });
});
//******************************************************************************
//EVENTO AJAX PARA CREAR PERFIL
//******************************************************************************
$('#btn-guardar').click(function(e) {
    e.preventDefault();
    procesar_imagen({
        image: $(".image-crop > img"),
        form: $("#frm_crear_usuario"),
        hiddenfield: $("#imagenAdjunta")
    });
    var url = baseurl + '/krauff/usuarios/crear';
    call_ajax({
        url: url,
        data: $("#frm_crear_usuario").serialize(),
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tablaUsuariosActivos.ajax.reload();
            tablaUsuariosInactivos.ajax.reload();
            $('#UsuariosCrear').modal('hide');
            $('#UsuariosCrear').find('form').trigger('reset');
        }
    });
});
//******************************************************************************
//******************************************************************************
//EVENTO AJAX PARA ELIMINAR USUARIO
//******************************************************************************
$('#usuarios').on("click", ".btn-eliminar-usuario", function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var data = {
        "id": id
    };
    dialogo_confirmacion_ajax({
        title: "Eliminar Usuario",
        message: "¿Está seguro que desea eliminar?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_usuario(data);
        },
        data: data
    });
});

function eliminar_usuario(data) {
    var url = baseurl + '/krauff/usuarios/eliminar';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tablaUsuariosActivos.ajax.reload();
            tablaUsuariosInactivos.ajax.reload();
        }
    });
}
//******************************************************************************
//******************************************************************************
//VENTANA FLOTANTE PARA FUNCIONALDADES
//******************************************************************************
$(document).ready(function() {
    $('#VentanaFlotanteFunc').on('shown.bs.modal', function(e) {
        var title = e.relatedTarget.dataset['title'];
        var url = e.relatedTarget.dataset['id'];
        var src = $('#contenedorIframeFunc').attr('data-iframe-src');
        src = baseurl + "/krauff/usuarios/funcionalidades/" + url;
        $('#contenedorIframeFunc').attr('src', src);
        $("#VentanaFlotanteFunc").find("#titulo-modal").html(title);
    });
    $('#VentanaFlotanteFunc').on('hidden.bs.modal', function(e) {
        $('#contenedorIframeFunc').attr('src', '');
    });
});
//******************************************************************************
$("#UsuariosCrear").on('hidden.bs.modal', function() {
    $(this).data('bs.modal', null);
});