$(document).ready(function() {
    tabla_normas = $('#tbl_normas').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/parametros/normas/datatable',
            "type": "POST",
            "data": function(d) {}
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'codigo',
            name: 'codigo',
        },{
            data: 'nombre',
            name: 'nombre',
        },{
            data: 'descripcion',
            name: 'descripcion',
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
});

//******************************************************************************
//EVENTO AJAX PARA ELIMINAR NOTICIA
//******************************************************************************
$("#tbl_normas").on("click", ".btn-eliminar-norma", function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var data = {
        "codnormacifrado": id
    };
    dialogo_confirmacion_ajax({
        title: "Eliminar Norma",
        message: "¿Está seguro que desea eliminar?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_norma(data);
        },
        data: data
    });
});

function eliminar_norma(data) {
    var url = baseurl + '/parametros/normas/eliminar';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tabla_normas.ajax.reload();
        }
    });
}