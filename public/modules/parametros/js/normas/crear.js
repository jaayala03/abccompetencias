// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        money: {
            decimal: ",",
            thousands: ".",
            prefix: "$ ",
            suffix: "",
            precision: 0,
            masked: false
        },
        codigo: "",
        nombre: "",
        descripcion: "",
        adjunto: "",
        valor: "",
        conocimientos: [{
            nombre: ""
        }],
        creando: false,
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        agregarconocimiento() {
            this.conocimientos.push({
                nombre: ""
            });
        },
        eliminarconocimiento(index) {
            this.conocimientos.splice(index, 1);
        },
        crear_norma() {
            var self = this;
            this.creando = true;
            this.$validator.validateAll("form_crear").then(result => {
                if (result) {
                    var form = new FormData();
                    form.append("file", self.adjunto);
                    form.append("codigo", self.codigo);
                    form.append("nombre", self.nombre);
                    form.append("descripcion", self.descripcion);
                    form.append("valor", self.valor);
                    $.each(self.conocimientos, function(index, val) {
                        form.append("conocimientos[]", val.nombre);
                    });

                    axios
                        .post(baseurl + "/parametros/normas/crear", form)
                        .then(function(response) {
                            console.log(response.data);
                            var data = response.data;
                            if (data.type == "error") {
                                self.creando = false;
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            } else {
                                self.creando = false;
                                routie(data.url);
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            }
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                } else {
                    self.creando = false;
                    // self.$message({
                    //   showClose: true,
                    //   message: "Campos incompletos.",
                    //   type: "error"
                    // });
                }
            });
        }
    }
});
$(function() {
    $(document).on('change', '#adjunto', function() {
        app.adjunto = this.files[0];
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready(function() {
        $('#adjunto').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });
    });
});