// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        preguntas: [],
        conocimientos: [],
        normas: [],
        codnorma: "",
        codcuestionario: window.cuestionario.codcuestionario,
        nombre: window.cuestionario.nombre,
        instrucciones: window.cuestionario.instrucciones,
        segundos_x_pregunta: window.cuestionario.segundos_x_pregunta,
        preguntas_cuestionario: [],
        preguntas_aleatorias: [],
        search: "",
        search2: "",
        totalaleatorias: 0,
        editando: false,
    },
    components: {},
    computed: {},
    mounted: function() {
        this.preguntas_aleatorias = window.cuestionario.preguntas;
        this.totalaleatorias = window.cuestionario.preguntas.length;
        this.normas = window.normas;
        this.codnorma = window.codnorma;
    },
    watch: {
        codnorma: function(val) {
            this.obtener_conocimientos(val);
        }
    },
    methods: {
        obtener_conocimientos(codnorma) {
            if (codnorma) {
                this.conocimientos = _.find(this.normas, { codnorma: codnorma }).conocimientos;
            } else {
                this.conocimientos = [];
                this.preguntas = [];
                this.preguntas_cuestionario = [];
                this.preguntas_aleatorias = [];
                this.totalaleatorias = 0;
                this.nombre = "";
            }
        },
        cambio(conocimiento) {
            var self = this;
            if (conocimiento.porcentaje === "") {
                conocimiento.porcentaje = 0;
            }
            cant_preg_act = Math.round(
                (conocimiento.cantpreguntas * parseFloat(conocimiento.porcentaje)) / 100
            ); //cantidad de preguntas por conocimiento
            conocimiento.preguntasaleatorias = cant_preg_act;
            self.obtener_preguntas_conocimiento(conocimiento.codconocimiento, cant_preg_act);
        },
        obtener_preguntas_conocimiento(codconocimiento, cant_preguntas) {
            var self = this;
            console.log(codconocimiento, cant_preguntas);
            var data = {
                codconocimiento: codconocimiento,
                cant_preguntas: cant_preguntas
            };
            axios
                .post(
                    baseurl + "/parametros/preguntas/obtenerpreguntas_aleatorio",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.preguntas_cuestionario[codconocimiento] = data;

                    self.preguntas_aleatorias = self.preguntas_cuestionario.flat(1);

                    var cant = 0;
                    $.each(self.conocimientos, function(index, value) {
                        cant = cant + value.preguntasaleatorias;
                    });
                    self.totalaleatorias = cant;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        agregar_pregunta(index, row) {
            this.preguntas.splice(index, 1);
            this.preguntas_aleatorias.push(row);
        },
        quitar_pregunta(index, row) {
            this.preguntas_aleatorias.splice(index, 1);
            this.preguntas.push(row);
        },
        editar_cuestionario() {
            var self = this;
            this.editando = true;
            this.$validator.validateAll("form_editar").then(result => {
                if (result) {
                    var data = {
                        codcuestionario: self.codcuestionario,
                        preguntas_cuestionario_eliminar: self.preguntas,
                        preguntas_cuestionario: self.preguntas_aleatorias,
                        nombre: self.nombre,
                        segundos_x_pregunta: self.segundos_x_pregunta,
                        instrucciones: self.instrucciones
                    };
                    axios
                        .post(baseurl + "/parametros/cuestionarios/editar", data)
                        .then(function(response) {
                            console.log(response.data);
                            var data = response.data;
                            if (data.type == "error") {
                                self.editando = false;
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            } else {
                                self.editando = false;
                                routie(data.url);
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            }
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                } else {
                    self.editando = false;
                    // self.$message({
                    //   showClose: true,
                    //   message: "Campos incompletos.",
                    //   type: "error"
                    // });
                }
            });
        }
    }
});