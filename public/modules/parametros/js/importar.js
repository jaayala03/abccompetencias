// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        files: [],
        accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        extensions: 'xlsx',
        minSize: 1,
        size: 1024 * 1024 * 10,
        multiple: true,
        directory: false,
        drop: true,
        dropDirectory: true,
        addIndex: false,
        thread: 3,
        name: 'file',
        postAction: 'parametros/subir_importacion',
        //data: {
        //    'param': 'xxxxxx',
        //},
        autoCompress: 1024 * 1024,
        uploadAuto: false,
        isOption: false,
        file_selected: {
            name: '',
            count_success: 0,
            count_failed: 0,
            logs: []
        },
    },
    computed: {
        headersupload: function() {
            return {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            };
        },
    },
    mounted: function() {},
    watch: {},
    methods: {
        inputFilter(newFile, oldFile, prevent) {
            if (newFile && !oldFile) {
                if (!/\.(xlsx)$/i.test(newFile.name)) {
                    swal('Oops...', 'Solo se permiten archivos .xlsx!', 'error');
                    return prevent()
                }
                
                newFile.response = {
                    items: [],
                    success: true,
                }
            }
        },
        inputFile(newFile, oldFile) {
            if (newFile && oldFile && !newFile.active && oldFile.active) {
                //console.log('response', newFile.response)
                var error = _.find(newFile.response.items, {
                    'success': false
                });
                if (error) {
                    this.$refs.upload.update(newFile, {
                        error: 'Revisar errores'
                    })
                }
                if (newFile.xhr) {
                    //console.log('status', newFile.xhr.status)
                }
            }
            if (newFile && oldFile) {
                if (newFile.active && !oldFile.active) {
                    if (newFile.size >= 0 && this.minSize > 0 && newFile.size < this.minSize) {
                        this.$refs.upload.update(newFile, {
                            error: 'Tamaño del archivo'
                        })
                    }
                }
                if (newFile.progress !== oldFile.progress) {
                    // progress
                }
                if (newFile.error && !oldFile.error) {
                    // error
                }
                if (newFile.success && !oldFile.success) {
                    // success
                }
            }
            if (!newFile && oldFile) {
                // remove
                if (oldFile.success && oldFile.response.id) {
                    // $.ajax({
                    //   type: 'DELETE',
                    //   url: '/upload/delete?id=' + oldFile.response.id,
                    // })
                }
            }
            if (Boolean(newFile) !== Boolean(oldFile) || oldFile.error !== newFile.error) {
                if (this.uploadAuto && !this.$refs.upload.active) {
                    this.$refs.upload.active = true
                }
            }
        },
        onAddFolader() {
            if (!this.$refs.upload.features.directory) {
                alert('El navegador no soporta esta funcionalidad')
                return
            }
            let input = this.$refs.upload.$el.querySelector('input')
            input.directory = true
            input.webkitdirectory = true
            this.directory = true
            input.onclick = null
            input.click()
            input.onclick = (e) => {
                this.directory = false
                input.directory = false
                input.webkitdirectory = false
            }
        },
        vererrores(file) {
            var self = this;
            self.file_selected.count_success = _.filter(file.response.items, {
                'success': true
            }).length;
            self.file_selected.count_failed = _.filter(file.response.items, {
                'success': false
            }).length;
            self.file_selected.name = file.name;
            self.file_selected.logs = [];
            _.forEach(file.response.items, function(value) {
                self.file_selected.logs.push(value);
            });
        },
        eliminararchivo(file) {
            this.$refs.upload.remove(file);
            this.reset_fileselected();
        },
        reset_fileselected() {
            this.file_selected = {
                name: '',
                count_success: 0,
                count_failed: 0,
                logs: []
            };
        }
    }
});