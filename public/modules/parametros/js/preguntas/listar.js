$(document).ready(function() {
    tabla_preguntas = $("#tbl_preguntas").DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: baseurl + "/parametros/preguntas/datatable",
            type: "POST",
            data: function(d) {
                d.codnorma = app.codnorma;
                d.codconocimiento = app.codconocimiento;
                d.codactividad = app.codactividad;
            }
        },
        fnDrawCallback: function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $(".image-lightbox").featherlight({
                type: "image"
            });
        },
        columns: [{
                data: "pregunta",
                name: "pregunta"
            },
            {
                data: "actividad",
                name: "actividades.nombre"
            },
            {
                data: "conocimiento",
                name: "conocimientos.nombre"
            },
            {
                data: "norma",
                name: "normas.codigo"
            },
            {
                className: "text-center",
                data: "opciones",
                searchable: false,
                orderable: false
            }
        ]
    });
});

//******************************************************************************
//EVENTO AJAX PARA ELIMINAR NOTICIA
//******************************************************************************
$("#tbl_preguntas").on("click", ".btn-eliminar-pregunta", function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var data = {
        "codpreguntacifrado": id
    };
    dialogo_confirmacion_ajax({
        title: "Eliminar Pregunta",
        message: "¿Está seguro que desea eliminar?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_pregunta(data);
        },
        data: data
    });
});

function eliminar_pregunta(data) {
    var url = baseurl + '/parametros/preguntas/eliminar';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tabla_preguntas.ajax.reload();
        }
    });
}

// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        normas: [],
        codnorma: '',
        conocimientos: [],
        codconocimiento: '',
        actividades: [],
        codactividad: '',
        loading: false
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {
        'codnorma': function(nw, od) {
            this.buscarConocimientosNorma();
        },
        'codconocimiento': function(nw, od) {
            this.buscarActividadesConocimiento();
        }
    },
    methods: {
        buscarNormas(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;
                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/parametros/normas/buscarNorma", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.normas = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.normas = [];
            }
        },
        buscarConocimientosNorma() {
            var data = {
                codnorma: this.codnorma
            };
            var self = this;
            axios
                .post(
                    baseurl + "/parametros/conocimientos/buscarConocimientosNorma",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.conocimientos = data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        buscarActividadesConocimiento() {
            var data = {
                codconocimiento: this.codconocimiento
            };
            var self = this;
            axios
                .post(
                    baseurl + "/parametros/actividades/buscarActividadesConocimiento",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.actividades = data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        actualizardatatable() {
            tabla_preguntas.ajax.reload(null, false);
        },
        limpiardatatable() {
            this.codnorma = '';
            this.normas = [];

            this.codconocimiento = '';
            this.conocimientos = [];

            this.codactividad = '';
            this.actividades = [];

            this.actualizardatatable();
        }
    }
});