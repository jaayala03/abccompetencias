// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        normas: [],
        codnorma: '',
        conocimientos: [],
        codconocimiento: '',
        actividades: [],
        codactividad: '',
        loading: false,
        codpregunta: window.datos.pregunta.codpregunta,
        pregunta: window.datos.pregunta.pregunta,
        opciones: window.datos.pregunta.opciones,
        creando: false,
        opciones_eliminar: [],
    },
    components: {},
    computed: {},
    mounted: function() {
        this.buscarNormas(window.datos.norma.nombre);
        this.codnorma = window.datos.norma.codnorma;
        this.codconocimiento = window.datos.conocimiento.codconocimiento;
        this.codactividad = window.datos.actividad.codactividad;
    },
    watch: {
        'codnorma': function(nw, od) {
            this.buscarConocimientosNorma();
        },
        'codconocimiento': function(nw, od) {
            this.buscarActividadesConocimiento();
        }
    },
    methods: {
        buscarNormas(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;
                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/parametros/normas/buscarNorma", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.normas = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.normas = [];
            }
        },
        buscarConocimientosNorma() {
            var data = {
                codnorma: this.codnorma
            };
            var self = this;
            axios
                .post(
                    baseurl + "/parametros/conocimientos/buscarConocimientosNorma",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.conocimientos = data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        buscarActividadesConocimiento() {
            var data = {
                codconocimiento: this.codconocimiento
            };
            var self = this;
            axios
                .post(
                    baseurl + "/parametros/actividades/buscarActividadesConocimiento",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.actividades = data;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        agregaropcion() {
            this.opciones.push({
                opcion: '',
                correcta: false,
            });
        },
        eliminaropcion(index) {
            this.opciones_eliminar.push(this.opciones[index]);
            this.opciones.splice(index, 1);
        },
        editar_pregunta() {
            var self = this;
            this.creando = true;
            this.$validator.validateAll("form_editar").then(result => {
                if (result) {
                    var data = {
                        codpregunta: self.codpregunta,
                        pregunta: self.pregunta,
                        codactividad: self.codactividad,
                        opciones: self.opciones,
                        opciones_eliminar: self.opciones_eliminar,
                    };
                    axios.post(baseurl + "/parametros/preguntas/editar", data).then(function(response) {
                        var data = response.data;
                        if (data.type == 'error') {
                            self.$notify({
                                title: data.title,
                                message: data.message,
                                type: data.type
                            });
                            this.creando = false;
                        } else {
                            routie(data.url);
                            self.$notify({
                                title: data.title,
                                message: data.message,
                                type: data.type
                            });
                            this.creando = false;
                        }
                    }).catch(function(error) {
                        console.log(error);
                    });
                } else {
                    this.creando = false;
                    self.$message({
                        showClose: true,
                        message: "Campos incompletos.",
                        type: "error"
                    });
                }
            });
        },
    }
});