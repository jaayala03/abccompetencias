// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        norma: [],
        desempeno: "",
        producto: "",
        indicadores: [{
            codindicador: "",
            codnorma: "",
            nombre: "",
            tipo: ""
        }],
        tipos_indicador: [],
        indicadores_eliminar: [],
        ind_desempeno: [],
        ind_producto: []
    },
    components: {},
    computed: {},
    mounted: function() {
        this.norma = window.norma;
        this.indicadores = window.norma.indicadores.reverse();
        this.tipos_indicador = window.tipos_indicador;
    },
    watch: {
        indicadores: function(val) {
            this.ind_desempeno = this.indicadores.filter(
                indicador => indicador.tipo == 1
            );
            this.ind_producto = this.indicadores.filter(
                indicador => indicador.tipo == 2
            );
        }
    },
    methods: {
        agregar_desempeno() {
            if (this.desempeno == '') {
                this.$message({
                    showClose: true,
                    message: "Ingrese el indicador",
                    type: "warning"
                });
            } else {
                const result = this.ind_desempeno.filter(
                    indicador => indicador.nombre == this.desempeno
                );
                if (result.length > 0) {
                    this.$message({
                        showClose: true,
                        message: "El indicador ya existe",
                        type: "error"
                    });
                } else {
                    this.indicadores.push({
                        codindicador: "",
                        codnorma: norma.codnorma,
                        nombre: this.desempeno,
                        tipo: 1
                    });
                    this.desempeno = "";
                    this.$message({
                        showClose: true,
                        message: "El indicador agregado",
                        type: "success"
                    });
                }
            }
        },
        agregar_producto() {
            if (this.producto == '') {
                this.$message({
                    showClose: true,
                    message: "Ingrese el indicador",
                    type: "warning"
                });
            } else {
                const result = this.ind_producto.filter(
                    indicador => indicador.nombre == this.producto
                );
                if (result.length > 0) {
                    this.$message({
                        showClose: true,
                        message: "El indicador ya existe",
                        type: "error"
                    });
                } else {
                    this.indicadores.push({
                        codindicador: "",
                        codnorma: norma.codnorma,
                        nombre: this.producto,
                        tipo: 2
                    });
                    this.producto = "";
                    this.$message({
                        showClose: true,
                        message: "El indicador agregado",
                        type: "success"
                    });
                }
            }
        },
        eliminarindicador_des(index) {
            var self = this;
            this.indicadores_eliminar.push(this.ind_desempeno[index]);

            $.each(this.indicadores, function(i, value) {
                if (value.nombre == self.ind_desempeno[index].nombre) {
                    self.indicadores.splice(i, 1);
                }
            });
        },
        eliminarindicador_prod(index) {
            var self = this;
            this.indicadores_eliminar.push(this.ind_producto[index]);
            $.each(this.indicadores, function(i1, value1) {
                if (value1.nombre == self.ind_producto[index].nombre) {
                    self.indicadores.splice(i1, 1);
                }
            });
        },
        guardar() {
            var self = this;

            var data = {
                codnorma: self.norma.codnorma,
                ind_desempeno: self.ind_desempeno,
                ind_producto: self.ind_producto,
                indicadores_eliminar: self.indicadores_eliminar
            };
            axios
                .post(baseurl + "/parametros/normas/indicadores/guardar", data)
                .then(function(response) {
                    var data = response.data;
                    if (data.type == "error") {
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    } else {
                        routie(data.url);
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
    }
});