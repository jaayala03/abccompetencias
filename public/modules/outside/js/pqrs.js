$(document).ready(function() {
    $("#frm_pqrs").validate({
        submitHandler: function(form) {
            var url = baseurl + '/outside/registrar_pqrs';
            $('#registrar_pqrs').attr('disabled', 'true');
            $('#registrar_pqrs').html('Registrando...');
            call_ajax({
                url: url,
                data: $("#frm_pqrs").serialize(),
                callbackfunctionsuccess: function(response) {
                    if (response.type == 'success') {
                        $('#registrar_pqrs').removeAttr('disabled');
                        $('#registrar_pqrs').html('<i class="fa fa-send"></i> Enviar');
                        $("#frm_pqrs").trigger("reset");
                        grecaptcha.reset();
                        swal({
                            type: 'success',
                            title: 'Registro Exitoso!',
                            text: 'Su mensaje fue enviado con exito, pronto nos pondremos en contacto',
                        });
                        // routie('/outside/ingresar/');
                    } else {
                        notifier.show(response.title, response.message, response.type, 5000);
                        $('#registrar_pqrs').removeAttr('disabled');
                        $('#registrar_pqrs').html('<i class="fa fa-send"></i> Enviar');
                    }
                }
            });
        },
        rules: {
            nombre: {
                required: true
            },
            email: {
                required: true
            },
            tipo: {
                required: true
            },
            mensaje: {
                required: true
            },
        },
        messages: {
        },
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error has-feedback');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        }
    });

    $(document).on('click', '#registrar_pqrs', function() {
        $("#frm_pqrs").submit();
    });
});