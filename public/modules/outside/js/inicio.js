jQuery(function() {
    if (Modernizr.mq('only all and (max-width: 768px)')) {
        jQuery('#camera_wrap_1').camera({
            height: '100px',
            pagination: false,
            thumbnails: false
        });
        $(".desc_mainslider").css("font-size", "6px");
    } else {
        jQuery('#camera_wrap_1').camera({
            height: '350px',
            pagination: false,
            thumbnails: false
        });
    }
});