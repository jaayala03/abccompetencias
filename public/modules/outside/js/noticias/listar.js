$(document).ready(function() {
    tabla_noticias = $('#tbl_noticias').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/outside/noticias/admin/datatable',
            "type": "POST",
            "data": function(d) {}
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'titulo',
            name: 'titulo',
        }, {
            className: "text-center",
            data: 'imagen',
            searchable: false,
        }, {
            data: 'descripcion',
            name: 'descripcion',
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
});
//******************************************************************************
//EVENTO AJAX PARA ELIMINAR NOTICIA
//******************************************************************************
$("#tbl_noticias").on("click", ".btn-eliminar-noticia", function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var data = {
        "codnoticiacifrado": id
    };
    dialogo_confirmacion_ajax({
        title: "Eliminar Noticia",
        message: "¿Está seguro que desea eliminar?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_noticia(data);
        },
        data: data
    });
});

function eliminar_noticia(data) {
    var url = baseurl + '/outside/noticias/admin/eliminar';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tabla_noticias.ajax.reload();
        }
    });
}