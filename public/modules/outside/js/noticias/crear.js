$(document).ready(function() {
    $('#contenido').summernote();
    var $image = $(".image-crop > img");
    $($image).cropper({
        aspectRatio: 16 / 9,
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });
    var $inputImage = $("#FileInput");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                files = this.files,
                file;
            if (!files.length) {
                return;
            }
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }
    ///////////////////////////////////////////////////////////////
    $("#frm_agregar_noticia").validate({
        submitHandler: function(form) {
            procesar_imagen({
                image: $(".image-crop > img"),
                form: $("#frm_agregar_noticia"),
                hiddenfield: $("#imagenAdjunta")
            });
            var url = baseurl + '/outside/noticias/admin/crear';
            call_ajax({
                url: url,
                data: $("#frm_agregar_noticia").serialize(),
                callbackfunctionsuccess: function(response) {
                    if (response.type == 'success') {
                        routie('/outside/noticias/admin/listar');
                    }
                    notifier.show(response.title, response.message, response.type, 5000);
                }
            });
        },
        rules: {
            titulo: {
                required: true
            },
            descripcion: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error has-feedback');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        }
    });
});
$(document).on('click', '#btn_agregar_noticia', function() {
    $("#frm_agregar_noticia").submit();
});