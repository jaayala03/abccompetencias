// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        busy: false,
        form_consulta: {
            documento: ''
        },
        solicitudes:[],
        consultado: false
    },
    components: {},
    computed: {
        
    },
    mounted: function () {
        
    },
    watch: {
        'form_consulta.documento': {
            handler: function(newValue) {
                this.consultado = false;
            },
            deep: true
        }
    },
    methods: {
        submit() {
            var self = this;
            this.$validator.validateAll('formC').then((result) => {
                if (result) {
                    self.busy = true;
                    var data = {
                        'documento': self.form_consulta.documento,
                        'g-recaptcha-response': grecaptcha.getResponse()
                    };
                    axios.post(baseurl + '/outside/solicitudes/consultar', data).then(function(response) {
                        if(response.data.solicitudes){
                            self.busy = false;
                            self.solicitudes = response.data.solicitudes;
                            self.consultado = true;
                            grecaptcha.reset();
                        }else{
                            self.consultado = false;
                            self.busy = false;
                            notifier.show(response.data.title, response.data.message, response.data.type, 5000);
                        }
                    }).catch(function(error) {
                        console.log(error)
                        self.busy = false;
                        self.$setLaravelValidationErrorsFromResponse(error.response.data);
                        //swal.fire('Oops...', 'Ha ocurrido un error!', 'error')
                    });
                }
            });
        }
    }
});