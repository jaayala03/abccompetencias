$(document).ready(function() {
    tabla_noticias = $('#table_descargas').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/outside/normas/todosdatosNormas',
            "type": "POST",
            "data": function (d) { }
        },
        "fnDrawCallback": function (oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'codigo',
            name: 'codigo',
        }, {
            data: 'nombre',
            name: 'nombre'
        },{
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
});