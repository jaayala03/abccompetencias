/*
 jQuery.ganttView v.0.8.8
 Copyright (c) 2010 JC Grubbs - jc.grubbs@devmynd.com
 MIT License Applies
 */

/*
 Options
 -----------------
 showWeekends: boolean
 data: object
 cellWidth: number
 cellHeight: number
 slideWidth: number
 dataUrl: string
 behavior: {
 clickable: boolean,
 draggable: boolean,
 resizable: boolean,
 navigate:'scroll' o 'otro',
 onClick: function,
 onDrag: function,
 onResize: function
 }
 */

(function (jQuery) {

    jQuery.fn.ganttView = function () {

        var args = Array.prototype.slice.call(arguments);

        if (args.length == 1 && typeof (args[0]) == "object") {
            build.call(this, args[0]);
        }

        if (args.length == 2 && typeof (args[0]) == "string") {
            handleMethod.call(this, args[0], args[1]);
        }
    };

    function build(options) {

        var els = this;
        var defaults = {
            fullWidth: false,
            Width: 990,
            showWeekends: true,
            cellWidth: 15,
            cellHeight: 31,
            slideWidth: 400,
            vHeaderWidth: 100,
            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                lineas: true,
                navigate: 'scroll'
            },
            scrollNavigation: {
                panelMouseDown: false,
                scrollerMouseDown: false,
                mouseX: null,
                panelMargin: 0,
                repositionDelay: 0,
                panelMaxPos: 0,
                canScroll: true
            },
            slideWidthContainer: 0
        };

        var opts = jQuery.extend(true, defaults, options);
        if (opts.data) {
            build();
        } else if (opts.dataUrl) {
            jQuery.getJSON(opts.dataUrl, function (data) {
                opts.data = data;
                build();
            });
        }

        function build() {

            var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 5);
//            var startEnd = DateUtils.getBoundaryDatesFromData(opts.data, minDays);
            opts.start = new Date(opts.data[0].start);
            opts.start.setDate(opts.start.getDate() + 1);
            opts.start.setHours(0);

            opts.end = new Date(opts.data[0].end);
            opts.end.setHours(24);
            opts.end.setMinutes(00);
            els.each(function () {
                var container = jQuery(this);
                var div = jQuery("<div>", {"class": "ganttview"});
                new Chart(div, opts).render();
                container.append(div);


                var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
                        jQuery("div.ganttview-slide-container", container).outerWidth();
                
                checkWidth();
                
                new Behavior(container, opts).apply();

            });

            //Agregado Nuevo
            $('.ganttview-slide-container').css("width", opts.slideWidthContainer + "px")
            repositionLabel(opts);
            lineas(opts);
            $('.ganttview').append(navigation(opts));
            var $rightPanel = $('.rightPanel');
            var $dataPanel = $('.ganttview-slide-container');
            opts.scrollNavigation.panelMargin = parseInt($dataPanel.css('margin-left').replace('px', ''));
            opts.scrollNavigation.panelMaxPos = ($dataPanel.width() - $rightPanel.width());
            opts.scrollNavigation.canScroll = ($dataPanel.width() > $rightPanel.width());

            moverpanel(opts);

        }
    }

    function checkWidth() {
        var container = $('.ganttview').parent();
        wcontainer = container.parent().width();
        var $rightPanel = $('.rightPanel');
        
        if(wcontainer !== 0){
            container.css("width", wcontainer + "px");
            $rightPanel.css("width",(wcontainer- (wcontainer * 0.10))+"px");
        }else{
            setTimeout(function(){
                container.css("width", container.parent().width() + "px");
                $rightPanel.css("width",(container.parent().width()- (container.parent().width() * 0.11))+"px");
            }, 2000);
        }
        
    }


    function moverpanel(opts) {
        var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
        var panel = document.getElementsByClassName('ganttview-slide-container');
        if (document.attachEvent)
            panel[0].attachEvent("on" + mousewheelevt, function (e) {
                wheelScroll(opts, e);
            });
        else if (document.addEventListener)
            panel[0].addEventListener(mousewheelevt, function (e) {
                wheelScroll(opts, e);
            }, false);
    }
    function wheelScroll(opts, e){
        var delta = e.detail ? e.detail * (-50) : e.wheelDelta / 120 * 50;

        scrollPanel(opts, delta);

        clearTimeout(opts.scrollNavigation.repositionDelay);
        opts.scrollNavigation.repositionDelay = setTimeout(repositionLabel, 50, opts);

        if (e.preventDefault)
            e.preventDefault();
        else
            return false;

    }
    function scrollPanel(opts, delta) {
        if (!opts.scrollNavigation.canScroll)
            return false;
        var _panelMargin = parseInt(opts.scrollNavigation.panelMargin) + delta;
        if (_panelMargin > 0)
        {
            opts.scrollNavigation.panelMargin = 0;
            $('.ganttview-slide-container').css('margin-left', opts.scrollNavigation.panelMargin + 'px');
        } else if (_panelMargin < opts.scrollNavigation.panelMaxPos * -1) {
            opts.scrollNavigation.panelMargin = opts.scrollNavigation.panelMaxPos * -1;
            $('.ganttview-slide-container').css('margin-left', opts.scrollNavigation.panelMargin + 'px');
        } else {
            opts.scrollNavigation.panelMargin = _panelMargin;
            $('.ganttview-slide-container').css('margin-left', opts.scrollNavigation.panelMargin + 'px');
        }
    }

    // Nuevos metodos
    // Lineas para la dependencia
    function lineas(opts) {
        if(!opts.behavior.lineas){
            return;
        }
            
        var contador = 0;
        for (var i = 0; i < opts.data.length; i++) {
            for (var j = 0; j < opts.data[i].series.length; j++) {
                for (var h = 0; h < opts.data[i].series[j].ordenesproduccion.length; h++) {
                    var series = opts.data[i].series[j];

                    if (series.ordenesproduccion[h].codordenproduccion_pt && series.ordenesproduccion[h].dep)
                    {

                        var elemA = $("#" + series.ordenesproduccion[h].codordenproduccion_pt);

                        var elemB = $("#" + series.ordenesproduccion[h].dep);
                        if (elemA.length > 0 && elemB.length <= 0) {

                            var depS = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "Start' class='depStart'></div>");
                            var depE = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "End' class='depEnd'></div>");

                            depS.css('background-color', "#888");
                            depE.css('background-color', elemA.css("background-color"));
                            var pa = elemA.position();
                            var ppa = elemA.parent().position();
                            var pA = {
                                left: ppa.left + elemA.outerWidth() + 3,
                                top: ppa.top + Math.round(elemA.outerHeight() / 2) + 2
                            };
                            var sizeS = DateUtils.daysBetween(series.ordenesproduccion[h].start, series.ordenesproduccion[h].end, opts.end);
                            var offsetS = DateUtils.daysBetweenOffset(opts.start, series.ordenesproduccion[h].start, opts.cellWidth);
                            var sizeE = DateUtils.daysBetween(elemB.data('start'), elemB.data('end'), opts.end);
                            var offsetE = DateUtils.daysBetweenOffset(opts.start, elemB.data('start'), opts.cellWidth);
                            
                            depS.css('left', pA.left - 5 + "px");
                            depS.css('margin-left', offsetS + "px");
                            depS.css('top', pA.top - 4 + "px");
                            depE.css('left', pA.left + 16 + "px");
                            depE.css('margin-left', offsetE + "px");
                            depE.css('top', pA.top - 4 + "px");
                            
                            var dep = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "Top' class='depLine'></div>");
                            dep.css('left', pA.left - 5 + "px");
                            dep.css('top', pA.top + 1 + "px");
                            dep.css('width', "25px");
                            dep.css('height', "5px");
                            dep.css('border-top', "1px solid #B6B5B5");

                            var el = null;
                            for (var f = 0; f < opts.data.length; f++)
                                for (var k = 0; k < opts.data[f].series.length; k++)
                                    for (var t = 0; t < opts.data[f].series[k].ordenesproduccion.length; t++)
                                        if (opts.data[f].series[k].ordenesproduccion[t].codordenproduccion_pt == series.ordenesproduccion[h].dep)
                                        {
                                            el = opts.data[f].series[k].ordenesproduccion[k];
                                            break;
                                        }
                            var hColor = "#FF0D00";
                            var mouseover = function (e) {
                                depS.css("border-color", hColor);
                                depE.css("border-color", hColor);
                                depS.css("z-index", "10002");
                                depE.css("z-index", "10002");
                                dep.css("z-index", "10000");
                                dep.css("border-color", hColor);
                            };
                            var mouseout = function (e) {
                                depS.css("border-color", "#fff");
                                depE.css("border-color", "#fff");
                                depS.css("z-index", "10001");
                                depE.css("z-index", "10001");
                                dep.css("z-index", "9999");
                                dep.css("border-color", "#B6B5B5");
                            };
                            if (el) {
                                depE.mouseover(function (e) {
                                    var hint = $("<div class='fn-gantt-hint' />").html(el.desc);
                                    $("body").append(hint);
                                    hint.css('left', e.pageX);
                                    hint.css('top', e.pageY);
                                    hint.show();
                                })
                                        .mouseout(function () {
                                            $(".fn-gantt-hint").remove();
                                        })
                                        .mousemove(function (e) {
                                            $('.fn-gantt-hint').css('left', e.pageX);
                                            $('.fn-gantt-hint').css('top', e.pageY + 15);
                                        });
                            }
                            depS.mouseover(mouseover);
                            depS.mouseout(mouseout);
                            depE.mouseover(mouseover);
                            depE.mouseout(mouseout);

                            
                            var $dataPanel = $('.ganttview-blocks');
                            $dataPanel.append(dep);                              
                            $dataPanel.append(depS);
                            $dataPanel.append(depE);

                        } else if (elemA.length > 0 && elemB.length > 0) {
                            var depS = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "Start' class='depStart'></div>");
                            var depE = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "End' class='depEnd'></div>");

                            depS.css('background-color', elemB.css("background-color"));
                            depE.css('background-color', elemA.css("background-color"));

                            var dep = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "Top' class='depLine'></div>");
                            var dep2 = $("<div id='" + series.ordenesproduccion[h].codordenproduccion_pt + "-" + series.ordenesproduccion[h].dep + "Bottom' class='depLine'></div>");

                            var sizeS = DateUtils.daysBetween(series.ordenesproduccion[h].start, series.ordenesproduccion[h].end, opts.end);
                            var offsetS = DateUtils.daysBetweenOffset(opts.start, series.ordenesproduccion[h].start, opts.cellWidth);
                            var sizeE = DateUtils.daysBetween(elemB.data('start'), elemB.data('end'), opts.end);
                            var offsetE = DateUtils.daysBetweenOffset(opts.start, elemB.data('start'), opts.cellWidth);

                            var pa = elemA.position();
                            var pb = elemB.position();

                            var ppa = elemA.parent().position();
                            var ppb = elemB.parent().position();

                            var pA = {
                                left: ppa.left + elemA.outerWidth() + 3,
                                top: ppa.top + Math.round(elemA.outerHeight() / 2) + 2
                            };

                            var pB = {
                                left: pb.left + 1,
                                top: ppb.top + Math.round(elemB.outerHeight() / 2) + 2
                            };

                            depS.css('left', pA.left - 5 + "px");
                            depS.css('margin-left', offsetS + "px");
                            depS.css('top', pA.top - 4 + "px");
                            depE.css('left', pB.left - 4 + "px");
                            depE.css('margin-left', offsetE + "px");
                            depE.css('top', pB.top - 4 + "px");

                            var depLW = function (obj, left, width, borders, offset) {
                                obj.css('left', left + "px");
                                obj.css('margin-left', offset + "px");
                                obj.css('width', width + "px");
                                var bdStyle = "1px solid #B6B5B5";
                                for (k = 0; k < borders.length; k++) {
                                    obj.css('border-' + borders[k], bdStyle);
                                }
                            };
                            if (pA.top < pB.top) {

                                var hW = Math.round(Math.abs(offsetS + sizeS - offsetE) / 2);
                                if (offsetS < offsetE) {
                                    depLW(dep, pA.left, hW, ["top", "right"], offsetS);
                                    if ((offsetS + sizeS) == offsetE) {
                                        depLW(dep2, pA.left + hW, hW, ["left", "bottom"], offsetS);
                                    } else {
                                        depLW(dep2, pA.left + hW, hW, ["left", "bottom"], offsetS - 1);
                                    }
                                } else {
                                    depLW(dep, pB.left + hW, hW + 5, ["right", "bottom"], offsetE);
                                    depLW(dep2, pB.left, hW + 5, ["top", "left"], offsetE);
                                }

                                dep.css('top', pA.top + 1 + "px");
                                dep.css('height', Math.floor((pB.top - pA.top) / 2) + "px");
                                dep2.css('top', pA.top + Math.floor((pB.top - pA.top) / 2) + 1 + "px");
                                dep2.css('height', Math.floor((pB.top - pA.top) / 2) + "px");
                            } else {
                                var hW = Math.round(Math.abs(offsetS + sizeS - offsetE) / 2);
                                if (offsetS < offsetE) {
                                    depLW(dep2, pA.left, hW + 1, ["bottom", "right"], offsetS);
                                    depLW(dep, pA.left + hW, hW, ["left", "top"], offsetS);
                                } else {
                                    depLW(dep2, pB.left + hW, hW + 5, ["top", "right"], offsetE);
                                    depLW(dep, pB.left, hW + 6, ["left", "bottom"], offsetE);
                                }

                                dep.css('top', pB.top + 1 + "px");
                                dep.css('height', Math.floor((pA.top - pB.top) / 2) + "px");
                                dep2.css('top', pB.top + Math.floor((pA.top - pB.top) / 2) + 1 + "px");
                                dep2.css('height', Math.floor((pA.top - pB.top) / 2) + "px");
                            }
                            depS.addClass('depo' + contador);
                            depE.addClass('depo' + contador);
                            dep.addClass('line' + contador);
                            dep2.addClass('line' + contador);
                            depS.attr('data-classover', '.depo' + contador);
                            depE.attr('data-classover', '.depo' + contador);
                            depS.attr('data-classoverline', '.line' + contador);
                            depE.attr('data-classoverline', '.line' + contador);
                            contador++;
                            var $dataPanel = $('.ganttview-blocks');
                            $dataPanel.append(dep);
                            $dataPanel.append(dep2);                               
                            $dataPanel.append(depS);
                            $dataPanel.append(depE);
                        }

                    }
                }
            }
        }
        var hColor = "#FF0D00";
        $(document).on('mouseover', '.depStart, .depEnd', function (e) {
            var clase = $(this).data('classover');
            $(clase).css("border-color", hColor);
            $(clase).css("z-index", "10002");
            var clase2 = $(this).data('classoverline');
            $(clase2).css("z-index", "10000");
            $(clase2).css("border-color", hColor);
        });
        $(document).on('mouseout', '.depStart, .depEnd', function (e) {
            var clase = $(this).data('classover');
            $(clase).css("border-color", "#fff");
            $(clase).css("z-index", "10001");
            var clase2 = $(this).data('classoverline');
            $(clase2).css("z-index", "9999");
            $(clase2).css("border-color", "#B6B5B5");
        });
    }
    //Navegacion
    function navigation(opts) {
        var ganttNavigate = null;
        if (opts.behavior.navigate == 'scroll')
        {
            ganttNavigate = $('<div class="navigate" />')
                    .append($('<div class="nav-slider" />')
                            .append($('<div class="nav-slider-left" />')
                                    .append($('<a href="javascript:///" class="nav-link nav-prev-day"/>')
                                            .html('&lt;')
                                            .click(function () {
                                                navigateTo(opts, getCellSize());
                                            })))
                            .append($('<div class="nav-slider-content" />')
                                    .append($('<div class="nav-slider-bar" />')
                                            .append($('<a class="nav-slider-button" />')
                                                    )
                                            .mousedown(function (e) {
                                                if (e.preventDefault)
                                                    e.preventDefault();
                                                opts.scrollNavigation.scrollerMouseDown = true;
                                                sliderScroll(opts, e);
                                            })
                                            .mousemove(function (e) {
                                                if (opts.scrollNavigation.scrollerMouseDown)
                                                {
                                                    sliderScroll(opts, e);
                                                }
                                            })
                                            )
                                    )
                            .append($('<div class="nav-slider-right" />')
                                    .append($('<a href="javascript:///" class="nav-link nav-next-day"/>')
                                            .html('&gt;')
                                            .click(function () {
                                                navigateTo(opts, getCellSize() * -1);
                                            }))
//                                    .append($('<a href="javascript:///" class="nav-link nav-zoomIn"/>')
//                                            .html('&#43;')
//                                            .click(function () {
//                                                core.zoomInOut(element, -1)
//                                            }))
//                                    .append($('<a href="javascript:///" class="nav-link nav-zoomOut"/>')
//                                            .html('&#45;')
//                                            .click(function () {
//                                                core.zoomInOut(element, 1);
//                                            }))
                                    )
                            );
            $(document).mouseup(function () {
                opts.scrollNavigation.scrollerMouseDown = false;
            });
        } else {
            /* Navigation panel */
            ganttNavigate = $('<div class="navigate" />')
                    .append($('<a href="javascript:///" class="nav-link nav-begin"/>')
                            .html('&#124;&lt;')
                            .click(function () {
                                navigateTo(opts, 'begin');
                            }))
                    .append($('<a href="javascript:///" class="nav-link nav-prev-week"/>')
                            .html('&lt;&lt;')
                            .click(function () {
                                navigateTo(opts, getCellSize() * 7);
                            }))
                    .append($('<a href="javascript:///" class="nav-link nav-prev-day"/>')
                            .html('&lt;')
                            .click(function () {
                                navigateTo(opts, getCellSize());
                            }))
                    .append($('<a href="javascript:///" class="nav-link nav-next-day"/>')
                            .html('&gt;')
                            .click(function () {
                                navigateTo(opts, getCellSize() * -1);
                            }))
                    .append($('<a href="javascript:///" class="nav-link nav-next-week"/>')
                            .html('&gt;&gt;')
                            .click(function () {
                                navigateTo(opts, getCellSize() * -7);
                            }))
                    .append($('<a href="javascript:///" class="nav-link nav-end"/>')
                            .html('&gt;&#124;')
                            .click(function () {
                                navigateTo(opts, 'end');
                            }));
//                    .append($('<a href="javascript:///" class="nav-link nav-zoomIn"/>')
//                            .html('&#43;')
//                            .click(function () {
//                                core.zoomInOut(element, -1)
//                            }))
//                    .append($('<a href="javascript:///" class="nav-link nav-zoomOut"/>')
//                            .html('&#45;')
//                            .click(function () {
//                                core.zoomInOut(element, 1);
//                            }));
        }
        return $('<div class="bottom"/>').append(ganttNavigate);
    }
    function navigateTo(opts, val) {
        var $rightPanel = $('.rightPanel');
        var $dataPanel = $('.ganttview-slide-container');
        var rightPanelWidth = $rightPanel.width();
        var dataPanelWidth = $dataPanel.width();

        switch (val) {
            case 'begin':
                $dataPanel.animate({
                    'margin-left': '0px'
                }, 'fast', function () {
                    repositionLabel(opts);
                });
                opts.scrollNavigation.panelMargin = 0;
                break;
            case 'end':
                var mLeft = dataPanelWidth - rightPanelWidth;
                opts.scrollNavigation.panelMargin = mLeft * -1;
                $dataPanel.animate({
                    'margin-left': '-' + mLeft + 'px'
                }, 'fast', function () {
                    repositionLabel(opts);
                });
                break;
            case 'now':
                if (!opts.scrollNavigation.canScroll)
                    return false;
                var max_left = (dataPanelWidth - rightPanelWidth) * -1;
                var cur_marg = $dataPanel.css('margin-left').replace('px', '');
                var val = $dataPanel.find('.today').offset().left - $dataPanel.offset().left;
                val *= -1;
                if (val > 0)
                    val = 0;
                else if (val < max_left)
                    val = max_left;

                $dataPanel.animate({
                    'margin-left': val + 'px'
                }, 'fast', repositionLabel(opts));
                opts.scrollNavigation.panelMargin = val;
                break;
            default:
                var max_left = (dataPanelWidth - rightPanelWidth) * -1;
                var cur_marg = $dataPanel.css('margin-left').replace('px', '');
                var val = parseInt(cur_marg) + val;
                if (val <= 0 && val >= max_left)
                    $dataPanel.animate({
                        'margin-left': val + 'px'
                    }, 'fast', repositionLabel(opts));
                opts.scrollNavigation.panelMargin = val;
                break;
        }
    }
    function repositionLabel(opts) {
        var $rightPanel = $('.rightPanel');
        var $dataPanel = $('.ganttview-slide-container');
        $dataPanel.stop();

        var wrapper = {offset: $rightPanel.offset(),
            width: $rightPanel.width(),
            height: $rightPanel.height()};

        $(".ganttview .rightPanel .ganttview-hzheader-month, .ganttview .rightPanel .ganttview-hzheader-day").each(function (i, obj) {
            var objDim = {offset: $(obj).offset(),
                width: $(obj).width(),
                height: $(obj).height()};
            if (objDim.offset.left + objDim.width > wrapper.offset.left
                    && objDim.offset.left < wrapper.offset.left + wrapper.width)
            {
                var viewArea = {
                    left: objDim.offset.left > wrapper.offset.left ? objDim.offset.left : wrapper.offset.left,
                    right: objDim.offset.left + objDim.width < wrapper.offset.left + wrapper.width ? objDim.offset.left + objDim.width : wrapper.offset.left + wrapper.width
                };
                $(obj).children(".labela").css("float", "left");
                var labelWidth = $(obj).children(".labela").width();
                var objMarg = objDim.offset.left < wrapper.offset.left ? wrapper.offset.left - objDim.offset.left : 0;
                if (viewArea.right - viewArea.left > labelWidth) {
                    $(obj).children(".labela")
                            .css("margin-left", objMarg + (viewArea.right - viewArea.left) / 2 - labelWidth / 2 + "px");
                }
            }
        });
        synchronizeScroller(opts);
    }
    function synchronizeScroller(opts) {
        if (opts.behavior.navigate == 'scroll')
        {
            var $rightPanel = $('.rightPanel');
            var $dataPanel = $('.ganttview-slide-container');
            var $sliderBar = $('.nav-slider-bar');
            var $sliderBtn = $sliderBar.find('.nav-slider-button');

            var bWidth = $sliderBar.width();
            var wButton = $sliderBtn.width();

            var mLeft = $dataPanel.width() - $rightPanel.width();
            var hPos = 0;
            if ($dataPanel.css('margin-left'))
                hPos = $dataPanel.css('margin-left').replace('px', '');
            var pos = hPos * bWidth / mLeft - $sliderBtn.width() * 0.25;
            pos = pos > 0 ? 0 : (pos * -1 >= bWidth - (wButton * 0.75)) ? (bWidth - (wButton * 1.25)) * -1 : pos;
            $sliderBtn.css('left', pos * -1);
        }
    }
    var _getCellSize = null;
    function getCellSize() {
        if (!_getCellSize)
        {
            $("body").append(
                    $("<div style='display: none; position: absolute;' class='ganttview' id='measureCellWidth'><div class='ganttview-grid-row-cell'></div></div>")
                    );
            _getCellSize = $('#measureCellWidth .ganttview-grid-row-cell').height();
            $('#measureCellWidth').empty().remove();
        }
        return _getCellSize;
    }
    function sliderScroll(opts, e) {

        var $sliderBar = $('.nav-slider-bar');
        var $sliderBarBtn = $sliderBar.find('.nav-slider-button');
        var $rightPanel = $('.rightPanel');
        var $dataPanel = $('.ganttview-slide-container');

        var bPos = $sliderBar.offset();
        var bWidth = $sliderBar.width();
        var wButton = $sliderBarBtn.width();

        if ((e.pageX >= bPos.left) && (e.pageX <= bPos.left + bWidth))
        {
            var pos = e.pageX - bPos.left;
            var pos = pos - wButton / 2;
            $sliderBarBtn.css('left', pos);

            var mLeft = $dataPanel.width() - $rightPanel.width();

            var pPos = pos * mLeft / bWidth * -1;
            if (pPos >= 0)
            {
                $dataPanel.css('margin-left', '0px');
                opts.scrollNavigation.panelMargin = 0;
            } else if (pos >= bWidth - (wButton * 1))
            {
                $dataPanel.css('margin-left', mLeft * -1 + 'px');
                opts.scrollNavigation.panelMargin = mLeft * -1;
            } else
            {
                $dataPanel.css('margin-left', pPos + 'px');
                opts.scrollNavigation.panelMargin = pPos;
            }
            clearTimeout(opts.scrollNavigation.repositionDelay);
            opts.scrollNavigation.repositionDelay = setTimeout(repositionLabel, 50, opts);
        }
    }

    //Metodos que traia el Plugin
    function handleMethod(method, value) {

        if (method == "setSlideWidth") {
            var div = $("div.ganttview", this);
            div.each(function () {
                var vtWidth = $("div.ganttview-vtheader", div).outerWidth();
                $(div).width(vtWidth + value + 1);
                $("div.ganttview-slide-container", this).width(value);
            });
        }
    }

    var Chart = function (div, opts) {

        function render() {
            addVtHeader(div, opts.data, opts.cellHeight);

            var slideDiv = jQuery("<div>", {
                "class": "ganttview-slide-container"
            });
            dates = getDates(opts.start, opts.end);
            addHzHeader(slideDiv, dates, opts.cellWidth);
            addGrid(slideDiv, opts.data, dates, opts.cellWidth, opts.showWeekends);
            addBlockContainers(slideDiv, opts.data);
            addBlocks(slideDiv, opts.data, opts.cellWidth, opts.start, opts.end);
            var rightPanel = jQuery("<div>", {
                "class": "rightPanel"
            });
            rightPanel.append(slideDiv);
            div.append(rightPanel);

            applyLastClass(div.parent());
        }

        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        // Creates a 3 dimensional array [year][month][day] of every day 
        // between the given start and end dates
        function getDates(start, end) {
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start]
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) {
                    dates[next.getFullYear()] = [];
                }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }
            return dates;
        }

        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", {"class": "ganttview-vtheader"});
            for (var i = 0; i < data.length; i++) {
                var itemDiv = jQuery("<div>", {"class": "ganttview-vtheader-item"});
                itemDiv.append(jQuery("<div>", {
                    "class": "ganttview-vtheader-item-name",
                    "css": {"height": (data[i].series.length * cellHeight) + "px"}
                }).append(data[i].name));
                var seriesDiv = jQuery("<div>", {"class": "ganttview-vtheader-series"});
                for (var j = 0; j < data[i].series.length; j++) {
                    seriesDiv.append(jQuery("<div>", {"class": "ganttview-vtheader-series-name"})
                            .append(data[i].series[j].name));
                }
                //itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }

        function addHzHeader(div, dates, cellWidth) {
            var headerDiv = jQuery("<div>", {"class": "ganttview-hzheader"});
            var monthsDiv = jQuery("<div>", {"class": "ganttview-hzheader-months"});
            var daysDiv = jQuery("<div>", {"class": "ganttview-hzheader-days"});
            var HorasDiv = jQuery("<div>", {"class": "ganttview-hzheader-daysHours"});
            var totalW = 0;
            for (var y in dates) {
                for (var m in dates[y]) {
                    var w = dates[y][m].length * cellWidth;
                    totalW = totalW + w * 96;
                    var MesLabela = jQuery("<div>", {"class": "labela"});
                    MesLabela.append(monthNames[m] + "/" + y);
                    monthsDiv.append(jQuery("<div>", {
                        "class": "ganttview-hzheader-month",
                        "css": {"width": ((w * 96)) + "px"}
                    }).append(MesLabela));
                    for (var d in dates[y][m]) {
                        var DiaLabela = jQuery("<div>", {"class": "labela"});
                        DiaLabela.append(dates[y][m][d].getDate());
                        daysDiv.append(jQuery("<div>", {
                            "class": "ganttview-hzheader-day",
                            "css": {"width": ((cellWidth * 96)) + "px"}
                        }).append(DiaLabela));
                        var fechita = new Date(2017, 00, 01, 00, 00);
                        for (var i = 1, max = 97; i < max; i++) {
                            var HorasDivDias = jQuery("<div>", {"class": "continer"});
                            HorasDiv.append(HorasDivDias);
                            HorasDivDias.append(jQuery("<div>", {"class": "ganttview-hzheader-daysHour"})
                                    .append(fechita.getHours() + ":" + fechita.getMinutes()));
                            fechita.addMinutes(15);
                        }


                    }
                }
            }
            monthsDiv.css("width", totalW + "px");
            daysDiv.css("width", totalW + "px");
            HorasDiv.css("width", totalW + "px");

            headerDiv.append(monthsDiv).append(daysDiv).append(HorasDiv);
            div.append(headerDiv);
            opts.slideWidthContainer = totalW;

        }

        function addGrid(div, data, dates, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", {"class": "ganttview-grid"});
            var rowDiv = jQuery("<div>", {"class": "ganttview-grid-row"});
            for (var y in dates) {
                for (var m in dates[y]) {
                    for (var d in dates[y][m]) {
                        for (var i = 1, max = 97; i < max; i++) {
                            var cellDiv = jQuery("<div>", {"class": "ganttview-grid-row-cell"});
                            if (DateUtils.isWeekend(dates[y][m][d]) && showWeekends) {
                                cellDiv.addClass("ganttview-weekend");
                            }
                            rowDiv.append(cellDiv);
                        }
                    }
                }
            }
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", w + "px");
            gridDiv.css("width", w + "px");
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    gridDiv.append(rowDiv.clone());
                }
            }
            div.append(gridDiv);
        }

        function addBlockContainers(div, data) {
            var blocksDiv = jQuery("<div>", {"class": "ganttview-blocks"});
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
//                    for (var h = 0; h < data[i].series[j].ordenesproduccion.length; h++) {
                    blocksDiv.append(jQuery("<div>", {"class": "ganttview-block-container"}));
//                    }
                }
            }
            div.append(blocksDiv);
        }

        function addBlocks(div, data, cellWidth, start, end) {
            end.setHours(24);
            end.setMinutes(00);
            var rows = jQuery("div.ganttview-blocks div.ganttview-block-container", div);
            var rowIdx = 0;
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    for (var h = 0; h < data[i].series[j].ordenesproduccion.length; h++) {
                        var series = data[i].series[j];
                        var size = DateUtils.daysBetween(series.ordenesproduccion[h].start, series.ordenesproduccion[h].end, end);
                        var offset = DateUtils.daysBetweenOffset(start, series.ordenesproduccion[h].start, cellWidth);
                        var block = jQuery("<div>", {
                            "id": series.ordenesproduccion[h].codordenproduccion_pt,
                            "class": "ganttview-block",
                            "data-op": series.ordenesproduccion[h].op,
                            "data-start": series.ordenesproduccion[h].start,
                            "data-end": series.ordenesproduccion[h].end,
                            "css": {
                                "width": ((size) - 2) + "px",
                                "margin-left": ((offset)) + "px"
                            }
                        });
                        addBlockData(block, data[i], series.ordenesproduccion[h]);
                        if (data[i].series[j].ordenesproduccion[h].color) {
                            block.css("background-color", data[i].series[j].ordenesproduccion[h].color);
                        }
                        block.append(jQuery("<div>", {"class": "ganttview-block-text"}).text(""));
                        jQuery(rows[rowIdx]).append(block);


                    }
                    rowIdx = rowIdx + 1;
                }
            }
            //Ver Titulo de la Orden de Produccion
            $(document).on('mouseover', '.ganttview-block', function (e) {
                $(this).data('op')
                var divpop = $("<div>").addClass("details-container");
                divpop.append($("<h5>").html("Orden de Producion: " + $(this).data('op')));
                divpop.append($("<p>").html("Fecha Inicio: " + $(this).data('start')));
                divpop.append($("<p>").html("Fecha Fin: " + $(this).data('end')));
                var hint = $("<div class='fn-gantt-hint' />").html(divpop);

                $("body").append(hint);
                hint.css('left', e.pageX);
                hint.css('top', e.pageY);
                hint.show();
            });

            $(document).on('mouseout', '.ganttview-block', function () {
                $(".fn-gantt-hint").remove();
            });
            $(document).on('mousemove', '.ganttview-block', function (e) {
                $('.fn-gantt-hint').css('left', e.pageX);
                $('.fn-gantt-hint').css('top', e.pageY + 15);
            });



        }

        function addBlockData(block, data, series) {
            // This allows custom attributes to be added to the series data objects
            // and makes them available to the 'data' argument of click, resize, and drag handlers
            var blockData = {id: data.id, name: data.name};
            //jQuery.extend(blockData, series);
            block.data("block-data", series);
        }

        function applyLastClass(div) {
            jQuery("div.ganttview-grid-row div.ganttview-grid-row-cell:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-days div.ganttview-hzheader-day:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-months div.ganttview-hzheader-month:last-child", div).addClass("last");
        }

        return {
            render: render
        };
    }

    var Behavior = function (div, opts) {
        function apply() {
            if (opts.behavior.clickable) {
                bindBlockClick(div, opts.behavior.onClick);
            }
            if (opts.behavior.resizable) {
                bindBlockResize(div, opts.cellWidth, opts.start, opts.behavior.onResize);
            }
            if (opts.behavior.draggable) {
                bindBlockDrag(div, opts.cellWidth, opts.start, opts.behavior.onDrag);
            }
        }

        function bindBlockClick(div, callback) {
            $(".ganttview-block").on("click", function () {
                if (callback) {
                    callback(jQuery(this).data("block-data"));
                }
            });
        }

        function bindBlockResize(div, cellWidth, startDate, callback) {
            $(".ganttview-block").resizable({
                grid: cellWidth,
                handles: "e,w",
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startDate);
                    if (callback) {
                        callback(block.data("block-data"));
                    }
                }
            });
        }

        function bindBlockDrag(div, cellWidth, startDate, callback) {
            jQuery("div.ganttview-block", div).draggable({
                axis: "x",
                grid: [cellWidth, cellWidth],
                stop: function () {
                    var block = jQuery(this);
                    updateDataAndPosition(div, block, cellWidth, startDate);
                    if (callback) {
                        callback(block.data("block-data"));
                    }
                }
            });
        }

        function updateDataAndPosition(div, block, cellWidth, startDate) {
            var container = jQuery("div.ganttview-slide-container", div);
            var scroll = container.scrollLeft();
            var offset = block.offset().left - container.offset().left - 1 + scroll;

            // Set new start date
            var daysFromStart = Math.round(offset / cellWidth);
            var newStart = startDate.clone().addDays(daysFromStart);
            block.data("block-data").start = newStart;

            // Set new end date
            var width = block.outerWidth();
            var numberOfDays = Math.round(width / cellWidth) - 1;
            block.data("block-data").end = newStart.clone().addDays(numberOfDays);
            jQuery("div.ganttview-block-text", block).text(numberOfDays + 1);

            // Remove top and left properties to avoid incorrect block positioning,
            // set position to relative to keep blocks relative to scrollbar when scrolling
            block.css("top", "").css("left", "")
                    .css("position", "absolute").css("margin-left", offset + "px");
        }

        return {
            apply: apply
        };
    }

    var ArrayUtils = {

        contains: function (arr, obj) {
            var has = false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj) {
                    has = true;
                }
            }
            return has;
        }
    };

    var DateUtils = {

        daysBetween: function (start, end, endinicial) {

            if (!start || !end) {
                return 0;
            }

            start = Date.parse(start);
            end = Date.parse(end);
            endinicial = Date.parse(endinicial);

            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }
            var count2 = 0, date2 = start.clone();
            if (end > endinicial) {
                while (date2.compareTo(endinicial) == -1) {
                    count2 = count2 + 1;
                    date2.addMinutes(1);
                }
                return count2;
                console.log('verdad')
            }

            var count = 0, date = start.clone();
            while (date.compareTo(end) == -1) {
                count = count + 1;
                date.addMinutes(1);

            }


            return count;
        },
        daysBetweenOffset: function (start, end, cellWidth) {

            if (!start || !end) {
                return 0;
            }

            start = Date.parse(start);
            end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }

            var count = 0, date = start.clone();
            if (date.compareTo(end) == 0) {
                var hora = date.getHours() * 4;
                var minuto = date.getMinutes();
                if (hora == 0) {
                    return hora + minuto + cellWidth - 15;
                } else {
                    return (hora * cellWidth) + minuto;
                }

            }
            while (date.compareTo(end) == -1) {
                count = count + 1;
                date.addMinutes(1);
            }
            var hora = start.getHours() * 4;
            var minuto = start.getMinutes();

            if (hora == 0) {
                return hora + minuto + cellWidth - 15 + count;
            } else {
                return (hora * cellWidth) + minuto + count;
            }
        },

        isWeekend: function (date) {
            return date.getDay() % 6 == 0;
        },

        getBoundaryDatesFromData: function (data, minDays) {
            var minStart = new Date();
            maxEnd = new Date();
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    for (var h = 0; h < data[i].series[j].ordenesproduccion.length; h++) {
                        var start = Date.parse(data[i].series[j].ordenesproduccion[h].start);
                        var end = Date.parse(data[i].series[j].ordenesproduccion[h].end)
                        if (i == 0 && j == 0 && h == 0) {
                            minStart = start;
                            maxEnd = end;
                        }
                        if (minStart.compareTo(start) == 1) {
                            minStart = start;
                        }
                        if (maxEnd.compareTo(end) == -1) {
                            maxEnd = end;
                        }
                    }
                }
            }

            // Insure that the width of the chart is at least the slide width to avoid empty
            // whitespace to the right of the grid
            if (DateUtils.daysBetween(minStart, maxEnd, data[0].end) < minDays) {
                maxEnd = minStart.clone().addDays(minDays);
            }

            return [minStart, maxEnd];
        }
    };

})(jQuery);