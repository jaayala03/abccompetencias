<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//**********************************************************************
//Ruta inicio de la aplicacion
//**********************************************************************
Route::get('/', function () {
    return redirect(route('outside.inicio'));
    // return redirect('/home');
});

Route::get('inactivo', [
    'uses' => 'HomeController@usuarioinactivo',
    'as'   => 'usuario.inactivo',
]);

Route::post('registrarse', [
    'uses' => 'Auth\RegisterController@create',
    'as'   => 'register.create',
]);

//**********************************************************************
//Rutas Auth
//**********************************************************************
Auth::routes();

//**********************************************************************
//Ruta para redireccionar a la pagina principal
//**********************************************************************
Route::get('/home', 'HomeController@index');

// Route::get('/inicio', '\Modules\Outside\Http\Controllers\OutsideController@inicio');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware(['auth', 'funcionalidades:AUDIT_SIS_LOGS']);

Route::get('/home#{hash?}', 'HomeController@index')->name('home');