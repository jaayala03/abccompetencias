<?php

use Illuminate\Database\Seeder;

class PlacasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Modules\Tramites\Entities\Placa::class, 10000)->create();
    }
}
