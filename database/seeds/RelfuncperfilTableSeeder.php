<?php

use Illuminate\Database\Seeder;

class RelfuncperfilTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run() {

        \DB::table('relfuncperfil')->delete();

        \DB::table('relfuncperfil')->insert(array(
            0 =>
            array(
                'codperfil' => 1,
                'codfunc' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array(
                'codperfil' => 1,
                'codfunc' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 =>
            array(
                'codperfil' => 1,
                'codfunc' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 =>
            array(
                'codperfil' => 1,
                'codfunc' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 =>
            array(
                'codperfil' => 1,
                'codfunc' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 =>
            array(
                'codperfil' => 1,
                'codfunc' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 =>
            array(
                'codperfil' => 1,
                'codfunc' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 =>
            array(
                'codperfil' => 1,
                'codfunc' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 =>
            array(
                'codperfil' => 1,
                'codfunc' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 =>
            array(
                'codperfil' => 1,
                'codfunc' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 =>
            array(
                'codperfil' => 1,
                'codfunc' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 =>
            array(
                'codperfil' => 1,
                'codfunc' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 =>
            array(
                'codperfil' => 1,
                'codfunc' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 =>
            array(
                'codperfil' => 1,
                'codfunc' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 =>
            array(
                'codperfil' => 1,
                'codfunc' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }

}
