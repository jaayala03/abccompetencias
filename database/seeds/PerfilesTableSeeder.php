<?php

use Illuminate\Database\Seeder;

class PerfilesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('perfiles')->insert(
                array(
                    0 =>
                    array(
                        'codperfil' => '1',
                        'nombreperfil' => 'Super Administrador',
                        'codpaginainicio' => '1',
                    ),
                    1 =>
                    array(
                        'codperfil' => '2',
                        'nombreperfil' => 'Admin',
                        'codpaginainicio' => '1',
                    ),
                    2 =>
                    array(
                        'codperfil' => '3',
                        'nombreperfil' => 'Director',
                        'codpaginainicio' => '1',
                    ),
                    3 =>
                    array(
                        'codperfil' => '4',
                        'nombreperfil' => 'Tramites',
                        'codpaginainicio' => '1',
                    )
        ));
    }

}
