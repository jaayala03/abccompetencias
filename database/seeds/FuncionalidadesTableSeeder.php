<?php

use Illuminate\Database\Seeder;

class FuncionalidadesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('funcionalidades')->delete();
        
        \DB::table('funcionalidades')->insert(array (
            0 => 
            array (
                'codfunc' => 1,
                'codpadre' => NULL,
                'nombre' => 'MENU_SYSTEM',
                'identificador' => 'SYS',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'URLPAGES',
                'tipo' => 'MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'codfunc' => 2,
                'codpadre' => 1,
                'nombre' => 'Auth',
                'identificador' => 'AUTH',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-user',
                'tipo' => 'MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'codfunc' => 3,
                'codpadre' => 2,
                'nombre' => 'Sistema',
                'identificador' => 'SISTEMA',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-bank',
                'tipo' => 'MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'codfunc' => 4,
                'codpadre' => 3,
                'nombre' => 'Usuarios',
                'identificador' => 'MNTO_USU',
                'orden' => 1,
                'urlpagina' => '/krauff/usuarios',
                'target' => '_parent',
                'icono' => 'fa fa-user',
                'tipo' => 'MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'codfunc' => 5,
                'codpadre' => 4,
                'nombre' => 'Listar',
                'identificador' => 'MNTO_USU_LIST',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-list-alt',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'codfunc' => 6,
                'codpadre' => 4,
                'nombre' => 'Crear',
                'identificador' => 'MNTO_USU_ADD',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-plus',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'codfunc' => 7,
                'codpadre' => 4,
                'nombre' => 'Editar',
                'identificador' => 'MNTO_USU_EDI',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-pencil-square-o',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'codfunc' => 8,
                'codpadre' => 4,
                'nombre' => 'Eliminar',
                'identificador' => 'MNTO_USU_ELI',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-trash-o',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'codfunc' => 9,
                'codpadre' => 4,
                'nombre' => 'Asignar funcionalidades',
                'identificador' => 'MNTO_USU_FUNC',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-users',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'codfunc' => 10,
                'codpadre' => 3,
                'nombre' => 'Perfiles',
                'identificador' => 'MNTO_PER',
                'orden' => 2,
                'urlpagina' => '/krauff/perfiles',
                'target' => '_parent',
                'icono' => 'fa fa-user',
                'tipo' => 'MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'codfunc' => 11,
                'codpadre' => 10,
                'nombre' => 'Listar',
                'identificador' => 'MNTO_PER_LIST',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-list-alt',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'codfunc' => 12,
                'codpadre' => 10,
                'nombre' => 'Crear',
                'identificador' => 'MNTO_PER_ADD',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-plus',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'codfunc' => 13,
                'codpadre' => 10,
                'nombre' => 'Editar',
                'identificador' => 'MNTO_PER_EDI',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-pencil-square-o',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'codfunc' => 14,
                'codpadre' => 10,
                'nombre' => 'Eliminar',
                'identificador' => 'MNTO_PER_ELI',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-trash-o',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'codfunc' => 15,
                'codpadre' => 10,
                'nombre' => 'Asignar funcionalidades',
                'identificador' => 'MNTO_PER_FUNC',
                'orden' => 1,
                'urlpagina' => 'URLPAGES',
                'target' => '_parent',
                'icono' => 'fa fa-users',
                'tipo' => 'PERMISO',
                'created_at' => NULL,
                'updated_at' => NULL,
            )
        ));
        
        
    }
}