<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #inserts Paginas inicio
        $this->call(PaginasinicioTableSeeder::class);
        #inserts perfiles
        $this->call(PerfilesTableSeeder::class);
        #inserts Usuarios
        $this->call(UsersTableSeeder::class);
        #inserts Funcionalidades
        $this->call(FuncionalidadesTableSeeder::class);
        #inserts Funcionalidades para los perfiles
        $this->call(RelfuncperfilTableSeeder::class);
        #inserts ubicaciones
        $this->call(UbicacionesTableSeeder::class);
    }
}
