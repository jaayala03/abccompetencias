<?php

use Illuminate\Database\Seeder;
use ABCcomptencias\User;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //factory(User::class, 1)->create();

        DB::table('usuarios')->insert(array(
            0 =>
            array(
                'codperfil' => '1',
                'tipodoc' => 'CC',
                'documento' => '1',
                'nombres' => 'Admin',
                'primerapellido' => 'Admin',
                'segundoapellido' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('secret'),
                'estado' => '1',
                'remember_token' => str_random(10),
            )
        ));

    }

}
