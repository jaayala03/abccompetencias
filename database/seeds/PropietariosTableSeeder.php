<?php

use Illuminate\Database\Seeder;

class PropietariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Modules\Tramites\Entities\Propietario::class, 1000)->create();
    }
}
