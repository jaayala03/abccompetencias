<?php

use Illuminate\Database\Seeder;

class PaginasinicioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginasinicio')->insert(array (
            0 => 
            array (
                'codpaginainicio' => 1,
                'nombre' => 'Admin',
                'nombrearchivo' => 'admin'
            ),
            1 => 
            array (
                'codpaginainicio' => 2,
                'nombre' => 'Tramites',
                'nombrearchivo' => 'tramites'
            )
        ));

    }
}
