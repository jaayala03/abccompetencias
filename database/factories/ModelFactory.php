<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(ABCcomptencias\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'nombres' => $faker->name,
        'correo' => $faker->unique()->safeEmail,
        'clave' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Modules\Tramites\Entities\Placa::class, function (Faker\Generator $faker) {

    return [
        'placa' => $faker->bothify('?#??###?#'),
    ];

});


$factory->define(Modules\Tramites\Entities\Vehiculo::class, function (Faker\Generator $faker) {

    return [
    	'codplaca' => factory(Modules\Tramites\Entities\Placa::class)->create()->codplaca,
        'codlinea' => $faker->numberBetween(1, 900),
        'codtipo_combustible' => $faker->numberBetween(1, 4),
        'codtipo_servicio' => $faker->numberBetween(1, 4),
        'codmodelo' => $faker->numberBetween(1, 1000),
        'codcolor' => $faker->numberBetween(1, 400),
        'fecha_matricula' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'nro_motor' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'nro_serie' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'nro_chasis' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'nro_importacion' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'fecha_importacion' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'ciudad_importacion' => $faker->numberBetween(2, 999),
        'codtipo_carroceria' => $faker->numberBetween(1, 155),
        'origen' => $faker->numberBetween(1, 2),
        'nro_factura_sirev' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'cilindraje' => $faker->numberBetween(100, 5000),
        'nro_licencia_transito' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'cantidad_pasajeros' => $faker->numberBetween(1, 500),
        'cantidad_puertas' => $faker->numberBetween(1, 6),
        'estado' => $faker->numberBetween(1, 3),
        'toneladas' => $faker->numberBetween(1, 50),
        'aduana' => $faker->regexify('[A-Z0-9._%+-]+[A-Z0-9.-]+\.[A-Z]{2,4}'),
    ];

});

$factory->define(Modules\Tramites\Entities\Persona::class, function (Faker\Generator $faker) {

    return [
        'tipo_documento' => $faker->numberBetween(1, 2),
        'nro_documento'=> $faker->numberBetween(1, 99999999999999999),
        'apellido1'=> $faker->lastName,
        'apellido2'=> $faker->lastName,
        'nombre1'=> $faker->firstName,
        'nombre2'=> $faker->firstName,
        'direccion'=> $faker->address,
        'telefono'=> $faker->tollFreePhoneNumber,
        'celular'=> $faker->e164PhoneNumber     ,
        'genero'=> $faker->numberBetween(1, 2),
        'fecha_nacimiento'=> $faker->date($format = 'Y-m-d', $max = 'now'),
        'lugar_expedicion'=> $faker->numberBetween(2, 200),
    ];

});


$factory->define(Modules\Tramites\Entities\Propietario::class, function (Faker\Generator $faker) {

    return [
        'codpersona' => factory(Modules\Tramites\Entities\Persona::class)->create()->codpersona,
        'codvehiculo'=> factory(Modules\Tramites\Entities\Vehiculo::class)->create()->codvehiculo,
        'fecha_propiedad'=> $faker->date($format = 'Y-m-d', $max = 'now'),
    ];

});