<?php

namespace ABCcomptencias\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Hash;

class MyResetPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        $txtAleatorio         = str_random(8);
        $newPassword          = Hash::make($txtAleatorio);
        $notifiable->password = $newPassword;
        $notifiable->save();

        return (new MailMessage)
            ->subject('Recuperar contraseña')
            ->greeting('Hola, ' . $notifiable->nombrecompleto)
            ->line('Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.')
            ->line('Tu usuario es: ' . $notifiable->email)
            ->line('Tu nueva contraseña es: ' . $txtAleatorio)
            ->line('Para mayor seguridad, ¡No olvide cambiar su contraseña al ingresar nuevamente!')
            ->action('Clic para aqui para ingresar!', url('/login'))
            ->line('Por favor no responda este mensaje.')
            ->salutation('Saludos, ' . config('app.name'));
    }
}
