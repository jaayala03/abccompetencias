<?php

namespace ABCcomptencias;

use Illuminate\Database\Eloquent\Model;
use ABCcomptencias\Traits\Transformers;
use Spatie\Activitylog\Traits\LogsActivity;

class Parametro extends Model
{
    use LogsActivity;
    use Transformers;

    protected $table      = 'parametros';
    protected $primaryKey = 'codparametro';
    protected $fillable   = [
        'codusuario',
        'nombre_empresa',
        'nit',
        'direccion',
        'telefono',
        'correo',
        'porc_iva',
        'ip',
    ];
    protected $guarded = [];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty         = true;
    protected static $logAttributes = [
        'codparametro',
        'codusuario',
        'nombre_empresa',
        'nit',
        'direccion',
        'telefono',
        'correo',
        'porc_iva',
        'ip',
    ];

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'modelo';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó los datos basicos de: ' . $this->nombre_empresa;
                break;
            case 'updated':
                return 'Editó los datos basicos de: ' . $this->nombre_empresa;
                break;
            case 'deleted':
                return 'Eliminó los datos basicos de: ' . $this->nombre_empresa;
                break;
        }
        return '';
    }

    //**********************************************************************
}
