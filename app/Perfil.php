<?php

namespace ABCcomptencias;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{

    protected $table      = 'perfiles';
    protected $primaryKey = 'codperfil';
    protected $fillable   = [
        'nombreperfil',
        'codpaginainicio',
        'hinicio',
        'hfin',
        'restriccion_ip',
    ];

    public function usuarios()
    {
        return $this->hasMany('ABCcomptencias\User', 'codperfil', 'codperfil');
    }

    public function paginainicio()
    {
        return $this->belongsTo('ABCcomptencias\Paginainicio', 'codpaginainicio');
    }

}
