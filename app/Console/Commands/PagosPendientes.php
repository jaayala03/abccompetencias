<?php

namespace ABCcomptencias\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Modules\Solicitudes\Entities\PagoSolicitud;
use Modules\Solicitudes\Entities\EtapaSolicitud;

class PagosPendientes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pagos:pendientes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try {
            $epayco = new \Epayco\Epayco(array(
                "apiKey" => config('pay.public_key'),
                "privateKey" => config('pay.private_key'),
                "lenguage" => "ES",
                "test" => config('pay.test')
            ));
            $efectivos = ['BA', 'EF', 'GA', 'PR', 'RS'];
            $credit_cards = ['VS', 'MC', 'AM', 'DC', 'CR', 'SP'];
            $pse = ['PSE'];

            $PagosSolicitudes = PagoSolicitud::where('state', 3)->get();
            foreach ($PagosSolicitudes as $PagoSolicitud) {
                $this->info('Verificando pago: '.$PagoSolicitud->codpagosolicitud);
                $Solicitud = $PagoSolicitud->solicitud;
                DB::beginTransaction();
                try {
                    if(in_array($PagoSolicitud->franchise, $efectivos)) {
                        $pay = $epayco->cash->transaction($PagoSolicitud->payu_order_id);
                    } else if(in_array($PagoSolicitud->franchise, $credit_cards)) {
                        $pay = $epayco->charge->transaction($PagoSolicitud->payu_order_id);
                    } else {
                        $pay = $epayco->bank->get($PagoSolicitud->payu_order_id);
                    }

                    if($pay->success) {
                        $data = $pay->data;
                        $this->info('El estado del pago es: '.$data->x_response_reason_text);
                        $PagoSolicitud->transaction_id = $data->x_ref_payco;
                        $PagoSolicitud->payu_order_id = $data->x_ref_payco;
                        $PagoSolicitud->state = $data->x_cod_response;
                        $PagoSolicitud->value = $data->x_amount;
                        $PagoSolicitud->franchise = $data->x_franchise;
                        $PagoSolicitud->currency = $data->x_currency_code;
                        $PagoSolicitud->description = $data->x_description;
                        $PagoSolicitud->user_id = \Auth::id();
                        $PagoSolicitud->save();

                        if($data->x_cod_response == 1) {
                            $Solicitud->etapassolicitud()->update(['ultima' => false]);
                            $EtapaSolicitud = EtapaSolicitud::create([
                                'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.pago_registrado', 2),
                                'codsolicitud' => $Solicitud->codsolicitud,
                                'ultima' => true
                            ]);
                            $paid = true;
                        }
                    } else {
                        $this->error('Error al consultar');
                    }
                    DB::commit();
                } catch(\Exception $e) {
                    DB::rollback();
                    $this->error('Error al verificar: '.$e->getMessage());
                }

            }

            $this->info('Terminado');
        } catch (\Exception $e) {
            //dd($e);
            $this->info('Error');
        }
    }
}
