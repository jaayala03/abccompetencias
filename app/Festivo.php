<?php

namespace ABCcomptencias;

use Illuminate\Database\Eloquent\Model;

class Festivo extends Model {

    protected $table = 'festivos';
    protected $primaryKey = 'codfestivo';
    protected $fillable   = [
        'fecha',
    ];

}
