<?php

namespace ABCcomptencias;

use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use ABCcomptencias\Notifications\MyResetPassword;

class User extends Authenticatable
{

    use Notifiable;

    protected $table      = 'usuarios';
    protected $primaryKey = 'codusuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $appends = ['nombrecompleto'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNombrecompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primerapellido . ' ' . $this->segundoapellido;
    }

    public function perfil()
    {
        return $this->belongsTo('ABCcomptencias\Perfil', 'codperfil');
    }

    public function ubicacion()
    {
        return $this->hasOne('ABCcomptencias\Ubicacion', 'codubicacion', 'codubicacion');
    }

    public function ubicacionnacimiento()
    {
        return $this->hasOne('ABCcomptencias\Ubicacion', 'codubicacion', 'codubicacionnacimiento');
    }

    public function tiene_func($funcionalidad)
    {
        $stop = true;
        if (!is_array($funcionalidad) && $funcionalidad == "*") {
            $stop = false;
        } else {
            foreach (session('funcionalidades') as $arr) {
                if (in_array($arr->identificador, $funcionalidad)) {
                    $stop = false;
                    break;
                }
            }
        }
        return $stop;
    }

    public function UserFunc($codusuario, $codperfil, $tipo)
    {
        $subquery1 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc");
        $subquery2 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc")->whereRaw("tipo = 'MENU'");

        $funcionalidades = DB::table('funcionalidades as f')
            ->select('f.codfunc', 'f.codpadre', 'f.nombre', 'f.identificador', 'f.orden', 'f.urlpagina', 'f.target', 'f.icono', 'f.tipo', DB::raw("({$subquery1->toSql()}) as hijos"), DB::raw("({$subquery2->toSql()}) as hijostipomenu"))
            ->orderBy('f.orden')
            ->get();

        $UserfuncArray = collect(DB::table('relfuncusuarios')
                ->where('codusuario', $codusuario)->get())->keyBy('codfunc');
        $PerfilfuncArray = collect(DB::table('relfuncperfil')
                ->where('codperfil', $codperfil)->get())->keyBy('codfunc');

        $finalArray = array();
        foreach ($funcionalidades as $vector) {
            if (array_key_exists($vector->codfunc, $UserfuncArray->toArray())) {
                $finalArray[] = $vector;
            }

            if (array_key_exists($vector->codfunc, $PerfilfuncArray->toArray())) {
                $finalArray[] = $vector;
            }
        }

        $final = array_map("unserialize", array_unique(array_map("serialize", $finalArray)));
        //$in= array_unique($finalArray, SORT_REGULAR);
        //array_unique($finalArray);
        return $final;
    }

    public function UserPermisos($codusuario, $codperfil, $tipo)
    {
        $subquery1 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc");
        $subquery2 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc")->whereRaw("tipo = 'MENU'");

        $funcionalidades = DB::table('funcionalidades as f')
            ->select('f.codfunc', 'f.codpadre', 'f.nombre', 'f.identificador', 'f.orden', 'f.urlpagina', 'f.target', 'f.icono', 'f.tipo', DB::raw("({$subquery1->toSql()}) as hijos"), DB::raw("({$subquery2->toSql()}) as hijostipomenu"))
            ->where('tipo', $tipo)
            ->orderBy('f.orden')
            ->get();

        $UserfuncArray = collect(DB::table('relfuncusuarios')->join('funcionalidades', 'relfuncusuarios.codfunc', '=', 'funcionalidades.codfunc')
                ->where('relfuncusuarios.codusuario', $codusuario)->get())->keyBy('codfunc');
        $PerfilfuncArray = collect(DB::table('relfuncperfil')->join('funcionalidades', 'relfuncperfil.codfunc', '=', 'funcionalidades.codfunc')
                ->where('codperfil', $codperfil)->get())->keyBy('codfunc');

        $finalArray = array();
        foreach ($funcionalidades as $vector) {
            if (array_key_exists($vector->codfunc, $UserfuncArray->toArray())) {
                $finalArray[$vector->identificador] = $vector;
            }

            if (array_key_exists($vector->codfunc, $PerfilfuncArray->toArray())) {
                $finalArray[$vector->identificador] = $vector;
            }
        }

        $final = array_map("unserialize", array_unique(array_map("serialize", $finalArray)));
        //$in= array_unique($finalArray, SORT_REGULAR);
        //array_unique($finalArray);
        return $final;
    }

    public function scopePermiso($query, $identificador)
    {
        return false;
    }

    //enviar correo de recuperacion de contraseña
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

}
