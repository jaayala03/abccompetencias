<?php

namespace ABCcomptencias;

use Illuminate\Database\Eloquent\Model;

class Paginainicio extends Model
{
    protected $table = 'paginasinicio';
    protected $primaryKey = 'codpaginainicio';
    protected $fillable = [
        'codpaginainicio',
        'nombre',
        'nombrearchivo'
    ];

    function perfiles() {
        return $this->hasMany('ABCcomptencias\Perfil', 'codpaginainicio', 'codpaginainicio');
    }
}
