<?php

namespace ABCcomptencias\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Model;

class CustomModel
{
    use SerializesModels;


    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Model $model)
    {

    }


}