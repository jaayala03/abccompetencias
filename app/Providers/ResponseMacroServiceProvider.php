<?php

namespace ABCcomptencias\Providers;

use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Response::macro('custom', function ($status, $data = [], $custommessage = '', $customtitle = '') {
            $data["success"] = $status;
            $http_code = 200;
            switch ($status) {
                case true:
                    $status          = 'success';
                    $status_optional = 'success';
                    $title           = ($customtitle !== '' && $customtitle !== null) ? $customtitle : 'Éxito';
                    $msg             = ($custommessage !== '' && $custommessage !== null) ? $custommessage : __('messages.success');
                    break;
                case false:
                    $status          = 'danger';
                    $status_optional = 'error';
                    $title           = ($customtitle !== '' && $customtitle !== null) ? $customtitle : 'Error';
                    $msg             = ($custommessage !== '' && $custommessage !== null) ? $custommessage : __('messages.error');
                    break;
                default:
                    $status          = 'warning';
                    $status_optional = 'warning';
                    $title           = ($customtitle !== '' && $customtitle !== null) ? $customtitle : 'Advertencia';
                    $msg             = ($custommessage !== '' && $custommessage !== null) ? $custommessage : __('messages.warning');
                    break;
            }

            return response()->json([
                'message'       => $msg,
                'title'         => $title,
                'type'          => $status,
                'type_optional' => $status_optional,
                'data'          => $data,
            ]);
        });

        \Response::macro('status', function ($status, $http_status_code = 200) {
            $data["success"] = $status;
            switch ($status) {
                case true:
                    $status          = 'success';
                    $status_optional = 'success';
                    $title           = 'Éxito';
                    $msg             = __('messages.success');;
                    break;
                case false:
                    $status          = 'danger';
                    $status_optional = 'error';
                    $title           = 'Error';
                    $msg             = __('messages.error');;
                    break;
                default:
                    $status          = 'success';
                    $status_optional = 'success';
                    $title           = 'Éxito';
                    $msg             = __('messages.success');;
                    break;
            }

            return response()->json([
                'message'       => $msg,
                'title'         => $title,
                'type'          => $status,
                'type_optional' => $status_optional,
                'data'          => $data,
            ], $http_status_code);
        });


        \Response::macro('error', function ($http_status_code = 500, $msg = null, $title = 'Error') {
            $data["success"] = false;
            $status          = 'danger';
            $status_optional = 'error';
            $msg = $msg ?? __('messages.error');

            return response()->json([
                'message'       => $msg,
                'title'         => $title,
                'type'          => $status,
                'type_optional' => $status_optional,
                'data'          => $data,
            ], $http_status_code);
        });

        \Response::macro('success', function ($http_status_code = 200, $msg = null, $title = 'Éxito') {
            $data["success"] = true;
            $status          = 'success';
            $status_optional = 'success';
            $msg = $msg ?? __('messages.success');

            return response()->json([
                'message'       => $msg,
                'title'         => $title,
                'type'          => $status,
                'type_optional' => $status_optional,
                'data'          => $data,
            ], $http_status_code);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
