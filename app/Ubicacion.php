<?php

namespace ABCcomptencias;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use ABCcomptencias\Ubicacion;

class Ubicacion extends Model {

    use SearchableTrait;

    protected $table = 'ubicaciones';
    protected $primaryKey = 'codubicacion';
    protected $fillable = [
        'codubicacion',
        'codpadre',
        'identificador',
        'nombre',
        'tipo'
    ];
    protected $searchable = [
        'columns' => [
            'ubicaciones.nombre' => 10,
        ]
    ];
    
    public function padre() {
        return $this->hasOne(Ubicacion::class, 'codubicacion', 'codpadre');
    }    
    
    public function todosPadres() {
        return $this->padre()->with('todosPadres');
    }
       
    public function hijos() {
        return $this->hasMany(Ubicacion::class, "codpadre", "codubicacion");
    }

    public function todosHijos() {
        return $this->hijos()->with('todosHijos');
    }

    public function scopeSinHijos($q) {
        return $q->has('todosHijos', '=', 0);
    }

    public function scopeMunicipios($query) {
        return $query->where('tipo', '=', 'CM');
    }

}