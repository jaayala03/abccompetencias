<?php

namespace ABCcomptencias\Http\Middleware;

use Auth;
use Closure;
use Crypt;
use ABCcomptencias\User;

class CheckSession
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && !$request->session()->has('funcionalidades')) {
            $codusuario  = Auth::user()->codusuario;
            $codperfil   = Auth::user()->codperfil;
            $img_usuario = route('krauff.getimage', ['id' => Crypt::encrypt($codusuario), 'option' => Crypt::encrypt(1)]);

            $Usuario       = User::findOrFail($codusuario);
            $perfilusuario = $Usuario->perfil->nombreperfil;

            $funcionalidades = $this->user->UserFunc($codusuario, $codperfil, 'MENU');
            $permisos        = $this->user->UserPermisos($codusuario, $codperfil, 'PERMISO');

            session([
                'funcionalidades' => $funcionalidades,
                'permisos'        => $permisos,
                'perfil'          => $perfilusuario,
                'imagen_perfil'   => $img_usuario,
            ]);
        }

        return $next($request);
    }
}
