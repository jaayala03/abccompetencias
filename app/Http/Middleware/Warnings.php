<?php

namespace ABCcomptencias\Http\Middleware;

use Closure;
use ABCcomptencias\ConceptoTramite;
use ABCcomptencias\Interes;
use ABCcomptencias\Tarifa;
use ABCcomptencias\User;

class Warnings
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $warnings           = [];
        // $interes            = Interes::fecha(Date('Y-m-d'))->first();
        // $tarifas            = Tarifa::tarifaVigente()->first();
        // $conceptos_tramites = ConceptoTramite::periodo(Date('Y'))->count();

        // if (empty($interes)) {
        //     array_push($warnings, [
        //         'title' => __('app.warning_title'),
        //         'text'  => __('app.intereses_warning'),
        //         'type'  => 1,
        //     ]);
        // }
        // if (empty($tarifas)) {
        //     array_push($warnings, [
        //         'title' => __('app.warning_title'),
        //         'text'  => __('app.tarifa_warning'),
        //         'type'  => 2,
        //     ]);
        // }
        // if ($conceptos_tramites == 0) {
        //     array_push($warnings, [
        //         'title' => __('app.warning_title'),
        //         'text'  => __('app.contra_warning'),
        //         'type'  => 3,
        //     ]);
        // }
        // session(['warnings' => $warnings]);
        return $next($request);
    }
}
