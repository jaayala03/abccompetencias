<?php

namespace ABCcomptencias\Http\Controllers\Auth;

use ABCcomptencias\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */

use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    protected function resetPassword($user, $password) {
        $user->password = bcrypt($password);
        $user->save();
        //alert()->info(trans('app.reset_password_success'))->persistent(trans('app.close_sweet'));
        return redirect($this->redirectPath());
    }

}
