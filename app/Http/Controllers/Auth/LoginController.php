<?php

namespace ABCcomptencias\Http\Controllers\Auth;

use Auth;
use Crypt;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Modules\Krauff\Entities\Acceso;
use ABCcomptencias\Http\Controllers\Controller;
use ABCcomptencias\Libraries\Festivos;
use ABCcomptencias\Parametro;
use ABCcomptencias\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->user = $user;
    }

    public function username()
    {
        return 'email';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            //Instancia de festivos
            $festivos = Festivos::getInstance();

            if (Auth::user()->estado == \Config::get('dominios.ESTADOUSU.VALORES.INACTIVO', 2)) {
                return redirect()->route('usuario.inactivo');
            }

            //******************************************************************
            //Verificacion de horario y dia habil
            //******************************************************************
            if (!empty(Auth::user()->perfil->hinicio) && !empty(Auth::user()->perfil->hfin)) {
                $hora    = date('H:i');
                $eshabil = $festivos->es_habil(Date('Y-m-d'));
                if (Auth::user()->perfil->hinicio < Auth::user()->perfil->hfin) {
                    if (!$eshabil) {
                        \Auth::logout();
                        \Session::flush();
                        abort(403, __('auth.user_day'));
                    }

                    if ($hora < Auth::user()->perfil->hinicio or $hora > Auth::user()->perfil->hfin) {
                        \Auth::logout();
                        \Session::flush();
                        abort(403, __('auth.user_hour'));
                    }
                }
            }

            //******************************************************************
            //Verificacion de restricción de IP
            //******************************************************************
            if (!empty(Auth::user()->perfil->restriccion_ip) && (int) Auth::user()->perfil->restriccion_ip == \Config::get('dominios.DOMINIO_SI_NO.VALORES.SI', 1)) {
                $ip        = \Request::ip();
                $parametro = Parametro::all()->first();
                if (!empty($parametro)) {
                    $ip_param = $parametro->ip;
                    if ($ip !== $ip_param) {
                        \Auth::logout();
                        \Session::flush();
                        abort(403, __('auth.ip_not_allowed'));
                    }
                }
            }

            //******************************************************************
            //Guardar el acceso en la BD
            //******************************************************************
            $user_agent = $_SERVER['HTTP_USER_AGENT'];

            $os_platform = "Desconocido";

            $os_array = [
                '/windows nt 10/i'      => 'Windows 10',
                '/windows nt 6.3/i'     => 'Windows 8.1',
                '/windows nt 6.2/i'     => 'Windows 8',
                '/windows nt 6.1/i'     => 'Windows 7',
                '/windows nt 6.0/i'     => 'Windows Vista',
                '/windows nt 5.2/i'     => 'Windows Server 2003/XP x64',
                '/windows nt 5.1/i'     => 'Windows XP',
                '/windows xp/i'         => 'Windows XP',
                '/windows nt 5.0/i'     => 'Windows 2000',
                '/windows me/i'         => 'Windows ME',
                '/win98/i'              => 'Windows 98',
                '/win95/i'              => 'Windows 95',
                '/win16/i'              => 'Windows 3.11',
                '/macintosh|mac os x/i' => 'Mac OS X',
                '/mac_powerpc/i'        => 'Mac OS 9',
                '/linux/i'              => 'Linux',
                '/ubuntu/i'             => 'Ubuntu',
                '/iphone/i'             => 'iPhone',
                '/ipod/i'               => 'iPod',
                '/ipad/i'               => 'iPad',
                '/android/i'            => 'Android',
                '/blackberry/i'         => 'BlackBerry',
                '/webos/i'              => 'Mobile',
            ];

            foreach ($os_array as $regex => $value) {
                if (preg_match($regex, $user_agent)) {
                    $os_platform = $value;
                }
            }

            $browser       = "Desconocido";
            $browser_array = [
                '/msie/i'      => 'Internet Explorer',
                '/firefox/i'   => 'Firefox',
                '/safari/i'    => 'Safari',
                '/chrome/i'    => 'Chrome',
                '/edge/i'      => 'Edge',
                '/opera/i'     => 'Opera',
                '/netscape/i'  => 'Netscape',
                '/maxthon/i'   => 'Maxthon',
                '/konqueror/i' => 'Konqueror',
                '/mobile/i'    => 'Handheld Browser',
            ];

            foreach ($browser_array as $regex => $value) {
                if (preg_match($regex, $user_agent)) {
                    $browser = $value;
                }
            }

            $acceso = new Acceso();
            $acceso->fill([
                'codusuario'   => Auth::user()->codusuario,
                'fechaingreso' => date("Y-m-d"),
                'horaingreso'  => date("H:i:s"),
                'ipoculta'     => \Request::ip(),
                'ipvisible'    => \Request::ip(),
                'so'           => $os_platform,
                'navegador'    => $browser,
            ]);
            $acceso->save();
            //******************************************************************

            $codusuario  = Auth::user()->codusuario;
            $codperfil   = Auth::user()->codperfil;
            $img_usuario = route('krauff.getimage', ['id' => Crypt::encrypt($codusuario), 'option' => Crypt::encrypt(1)]);

            $Usuario       = User::findOrFail($codusuario);
            $perfilusuario = $Usuario->perfil->nombreperfil;

            $funcionalidades = $this->user->UserFunc($codusuario, $codperfil, 'MENU');
            $permisos        = $this->user->UserPermisos($codusuario, $codperfil, 'PERMISO');

            session([
                'funcionalidades' => $funcionalidades,
                'permisos'        => $permisos,
                'perfil'          => $perfilusuario,
                'imagen_perfil'   => $img_usuario,
            ]);

            \Session::flash('welcome', Auth::user()->nombres . ' / ' . Auth::user()->perfil->nombreperfil);

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('outside/inicio');
    }

}
