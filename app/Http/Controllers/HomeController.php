<?php

namespace ABCcomptencias\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\View;
use ABCcomptencias\Libraries\Festivos;
use ABCcomptencias\Perfil;
use Session;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $epayco = new \Epayco\Epayco(array(
        //     "apiKey" => config('pay.public_key'),
        //     "privateKey" => config('pay.private_key'),
        //     "lenguage" => "ES",
        //     "test" => config('pay.test')
        // ));
        // // $pay = $epayco->cash->transaction("11566837");
        // dd($epayco);
        // exit;

        $codperfil = Auth::user()->codperfil;
        $perfil    = Perfil::findOrFail($codperfil);

        View::share('parametros', [
            'meta_title' => 'Inicio',
            'componente' => 'Inicio',
        ]);

        View::share('breadcrumbs');

        // $hora_actual = \Carbon::now();
        // $hora_fin_estimado = $hora_actual->copy()->addMinutes(2);

        // dd($hora_actual->format('Y-m-d H:i:s'), $hora_fin_estimado->format('Y-m-d H:i:s'), $hora_actual->diffInSeconds($hora_fin_estimado));

        return view('krauff::inicio.' . $perfil->paginainicio->nombrearchivo);
    }

    public function usuarioinactivo()
    {
        if (Auth::check()) {
            \Auth::logout();
            \Session::flush();
            return response()->view('errors.custom', [], 403);
        }

        return response()->view('errors.404_out', [], 404);

    }

}
