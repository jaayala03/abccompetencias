<?php

namespace ABCcomptencias\Traits;

trait Reportesdat
{

    /**
     * [generate_file_dat  download a .dat file]
     * @param  array  $params  [array with the report $params]
     * @param  Collection  $results [query results]
     * @param  String $name    [$file name to to download]
     * @return [void]          []
     */
    public function generate_file_dat(array $params, \Illuminate\Database\Eloquent\Collection $results, string $name)
    {
        $name = $name . "." . 'dat';
        $file = fopen($name, "w") or die('Error creating dat file');
        foreach ($results as $key => $value) {
            $fila    = [];
            $linea   = '';
            $difTemp = 0;
            foreach ($params as $key2 => $value2) {
                $longitud = (int) $value2['longitud'] - 1;
                $inicio   = strlen($linea) + 1;
                $sum1     = $inicio + $longitud;

                $propiedad = trim($value->{$value2['campo']});
                if ($value2['tipo'] == 'numerico' or is_numeric($propiedad)) {
                    $propiedad = sprintf('%0' . $value2['longitud'] . 'd', $propiedad);
                }

                $cantidad_caracteres   = strlen((string) $propiedad);
                $diferencia_caracteres = (int) $value2['longitud'] - $cantidad_caracteres;
                $difTemp               = $diferencia_caracteres;

                if ($diferencia_caracteres < 0) {
                    if (is_string($propiedad)) {
                        $propiedad = substr($propiedad, 0, (int) $value2['longitud']);
                        $linea     = $linea . $propiedad;
                    } else {
                        $espacios = str_repeat(' ', $cantidad_caracteres);
                        $linea    = $linea . $espacios;
                    }
                } else if ($diferencia_caracteres > 0) {
                    $espacios = str_repeat(' ', $diferencia_caracteres);
                    $linea    = $linea . $propiedad . $espacios;
                } else {
                    $linea = $linea . $propiedad;
                }

                $fila[$key2] = $propiedad;
            }

            $str = $linea . "\r\n";
            fputs($file, $str);
        }
        fclose($file);

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($name));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($name));
        ob_clean();
        flush();
        readfile($name);
        unlink($name);
    }

}
