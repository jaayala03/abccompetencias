<?php

namespace ABCcomptencias\Traits;

trait Transformers
{

    /**
     * Propiedades a definir en el modelo para no aplicarse uppercase
     * protected $no_uppercase  = ['campo', ...];
     */

    /**
     * Propiedades a definir en el modelo para los campos de fecha con formato d/m/Y
     * protected $format_date  = ['campo', ...]
     */

    protected static function boot()
    {
        parent::boot();
        static::eventsCustomTransformers()->each(function ($eventName) {
            static::$eventName(function ($model) {
                foreach ($model->attributes as $key => $value) {

                    /**
                     * Uppercase
                     */
                    $noupper = empty($model->no_uppercase) ? [] : $model->no_uppercase;
                    if (is_string($value)) {
                        if (!in_array($key, $noupper)) {
                            $model->attributes[$key] = trim(mb_strtoupper($value));
                        }
                    }

                    /**
                     * Fechas
                     */
                    $format_date = empty($model->format_date) ? [] : $model->format_date;
                    if (in_array($key, $format_date)) {
                        $datetime = new \DateTime();
                        $newDate  = $datetime->createFromFormat('d/m/Y', $value);
                        if ($newDate) {
                            $model->attributes[$key] = $newDate->format('Y-m-d');
                        }
                    }

                }
            });
        });
    }

    /**
     * Eventos del modelo para los transformers
     */
    protected static function eventsCustomTransformers()
    {
        $events = collect([
            'saving',
            'updating',
        ]);
        return $events;
    }

}
