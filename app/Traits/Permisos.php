<?php

namespace ABCcomptencias\Traits;

use Modules\Krauff\Entities\Funcionalidad;

trait Permisos
{

    public function verificarpermiso($identificador)
    {
        if (array_key_exists($identificador, session("permisos"))):
            return true;
        endif;
        return false;
    }

    public function componente($componente)
    {
        if (is_array($componente)):
            $ArrStr = [];
            foreach ($componente as $value) {
                $funcionalidad = Funcionalidad::Identificador($value)->firstOrFail();
                $ArrStr[]      = $funcionalidad->identificador;
            }
            $str = implode("-", $ArrStr);
            return $str;
        else:
            $funcionalidad = Funcionalidad::Identificador($componente)->firstOrFail();
            return $funcionalidad->identificador;
        endif;
    }

}
