<?php

namespace ABCcomptencias\Traits;

trait Notificaciones {

	public function enviarnotificacion($data)
    {
        //******************************************************************
        //Conectar con redis
        //******************************************************************
        $redis = Redis::connection();
        //******************************************************************
        //Manejo de los datos
        //******************************************************************
        $cantidad_mensaje    = strlen($data["parametros"]["mensaje"]);
        $mensaje             = ($cantidad_mensaje > 100) ? substr($data["parametros"]["mensaje"], 0, 100) . "..." : $data["parametros"]["mensaje"];
        $data_msg["mensaje"] = $mensaje;


        //******************************************************************
        //Guardar la notificacion en la BD
        //******************************************************************
        $notificacion = new Notificaciones;
        $notificacion->fill([
            'titulo'  => $data["parametros"]["titulo"],
            'mensaje' => $data["parametros"]["mensaje"],
            'icono'   => $data["parametros"]["icono"],
            'url'     => $data["parametros"]["url"],
            'tipo'    => $data["parametros"]["tipo"],
        ]);
        $notificacion->save();
        $data["parametros"]['codnotificacion'] = Crypt::encrypt($notificacion->codnotificacion);
        $data["parametros"]['url']   = route('krauff.leernotificacion', Crypt::encrypt($notificacion->codnotificacion));
        //******************************************************************
        //Enviar y asignar las notificaciones a los usuarios
        //******************************************************************
        foreach ($data["usuarios"] as $usuario) {
            if (Auth::user()->id != $usuario->id) {
                //******************************************************************
                //Asignar en la tabla intermedia
                //******************************************************************
                $usuario->notificaciones()->attach($notificacion->codnotificacion);
                //******************************************************************
                //validar si el usuario esta en linea
                //******************************************************************
                //if ($redis->exists("usuario:" . $usuario->codusuario) == 1) {
                    //1 = true, 0 = false
                    //******************************************************************
                    //Emitir la notiicacion
                    //******************************************************************
                    $redis->publish($usuario->id, json_encode($data["parametros"]));

                //}
            }
        }
    }

}
