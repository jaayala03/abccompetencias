<?php

namespace ABCcomptencias\Libraries;

use ABCcomptencias\Libraries\FPDF_CellFit;

//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Fpdf_formato extends FPDF_CellFit
{
    private $paramsheader;
    private $paramsfooter;
    private $titulo;
    private $titulo2;
    private $codigo;
    private $version;
    private $fecha;
    public function __construct() {
        parent::__construct();
    }
    
    function getParamsheader() {
        return $this->paramsheader;
    }
    function getParamsfooter() {
        return $this->paramsfooter;
    }

    function getTitulo() {
        return $this->titulo;
    }
    
    function getTitulo2() {
        return $this->titulo2;
    }
    function getCodigo() {
        return $this->codigo;
    }
    function getVersion() {
        return $this->version;
    }
    function getFecha() {
        return $this->fecha;
    }

    function setParamsheader($paramsheader) {
        $this->paramsheader = $paramsheader;
    }
    function setParamsfooter($paramsfooter) {
        $this->paramsfooter = $paramsfooter;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setTitulo2($titulo2) {
        $this->titulo2 = $titulo2;
    }       
    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }       
    function setVersion($version) {
        $this->version = $version;
    }       
    function setFecha($fecha) {
        $this->fecha = $fecha;
    }       
          
    // El encabezado del PDF
    public function Header() {

        $this->SetLeftMargin(20);
        $this->SetFont('Arial', 'B', 12);
        $this->SetTextColor(0,0,0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(255, 255, 255);

        if ($this->getParamsheader()["imagen1"]["x"] > 0) {
            $this->Image('../public/images/logo2.png', $this->getParamsheader()["imagen1"]["x"], $this->getParamsheader()["imagen1"]["y"], $this->getParamsheader()["imagen1"]["ancho"], $this->getParamsheader()["imagen1"]["alto"]);
            $this->Cell($this->getParamsheader()["celda1"]["ancho"], $this->getParamsheader()["celda1"]["alto"], '', 1, 0);    
        }
        
        $this->SetFont('Arial', 'B', 12);
        $this->CellFitScale($this->getParamsheader()["celda2"]["ancho"], $this->paramsheader["celda2"]["alto"], $this->getTitulo(), 1, 0, 'C'); 
        $x= $this->GetX();
        $y= $this->GetY();
        $this->Cell($this->getParamsheader()["celda3"]["ancho"], $this->paramsheader["celda3"]["alto"], '', 1, 1, 'C'); 
        $this->SetFont('Helvetica', 'B', 9);
        $this->Text($x + $this->paramsheader["codigo"]["x"], $y + $this->paramsheader["codigo"]["y"], $this->getCodigo());                   
        $this->Text($x + $this->paramsheader["version"]["x"], $y + $this->paramsheader["version"]["y"], $this->getVersion());                   
        $this->Text($x + $this->paramsheader["fecha"]["x"], $y + $this->paramsheader["fecha"]["y"], $this->getFecha());                         

        $this->Ln(5);
    }

    // El pie del pdf
    public function Footer() {
        $this->SetY(-20);
        $this->SetTextColor(107,107,107);
        $this->SetDrawColor(178, 178, 178);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'I', 7);
        if($this->getParamsfooter()){
            $this->Cell($this->getParamsfooter()["paginador"]["ancho"], 3, utf8_decode('Página '.$this->PageNo().' de {nb}'), 0, 1, 'C');
            $this->Cell($this->getParamsfooter()["fecha"]["ancho"], 3, 'ABC competencias: '.date('d/m/Y'), 0, 1, 'C');
        }
    }

}
