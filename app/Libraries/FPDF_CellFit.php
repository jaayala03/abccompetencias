<?php

namespace ABCcomptencias\Libraries;

use Codedge\Fpdf\Fpdf\Fpdf;
//use setasign\Fpdi\Fpdi;

class FPDF_CellFit extends Fpdf
{

    protected $legends;
    protected $wLegend;
    protected $sum;
    protected $NbVal;

    protected $T128; // Tableau des codes 128
    protected $ABCset = ""; // jeu des caractères éligibles au C128
    protected $Aset   = ""; // Set A du jeu des caractères éligibles
    protected $Bset   = ""; // Set B du jeu des caractères éligibles
    protected $Cset   = ""; // Set C du jeu des caractères éligibles
    protected $SetFrom; // Convertisseur source des jeux vers le tableau
    protected $SetTo; // Convertisseur destination des jeux vers le tableau
    protected $JStart     = array("A" => 103, "B" => 104, "C" => 105); // Caractères de sélection de jeu au début du C128
    protected $JSwap      = array("A" => 101, "B" => 100, "C" => 99); // Caractères de changement de jeu
    protected $extgstates = array();
    public $flowingBlockAttr;
    protected $encrypted = false; //whether document is protected
    protected $Uvalue; //U entry in pdf document
    protected $Ovalue; //O entry in pdf document
    protected $Pvalue; //P entry in pdf document
    protected $enc_obj_id; //encryption object id

//____________________________ Extension du constructeur _______________________

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4')
    {

        parent::__construct($orientation, $unit, $format);

        $this->T128[] = array(2, 1, 2, 2, 2, 2); //0 : [ ]               // composition des caractères
        $this->T128[] = array(2, 2, 2, 1, 2, 2); //1 : [!]
        $this->T128[] = array(2, 2, 2, 2, 2, 1); //2 : ["]
        $this->T128[] = array(1, 2, 1, 2, 2, 3); //3 : [#]
        $this->T128[] = array(1, 2, 1, 3, 2, 2); //4 : [$]
        $this->T128[] = array(1, 3, 1, 2, 2, 2); //5 : [%]
        $this->T128[] = array(1, 2, 2, 2, 1, 3); //6 : [&]
        $this->T128[] = array(1, 2, 2, 3, 1, 2); //7 : [']
        $this->T128[] = array(1, 3, 2, 2, 1, 2); //8 : [(]
        $this->T128[] = array(2, 2, 1, 2, 1, 3); //9 : [)]
        $this->T128[] = array(2, 2, 1, 3, 1, 2); //10 : [*]
        $this->T128[] = array(2, 3, 1, 2, 1, 2); //11 : [+]
        $this->T128[] = array(1, 1, 2, 2, 3, 2); //12 : [,]
        $this->T128[] = array(1, 2, 2, 1, 3, 2); //13 : [-]
        $this->T128[] = array(1, 2, 2, 2, 3, 1); //14 : [.]
        $this->T128[] = array(1, 1, 3, 2, 2, 2); //15 : [/]
        $this->T128[] = array(1, 2, 3, 1, 2, 2); //16 : [0]
        $this->T128[] = array(1, 2, 3, 2, 2, 1); //17 : [1]
        $this->T128[] = array(2, 2, 3, 2, 1, 1); //18 : [2]
        $this->T128[] = array(2, 2, 1, 1, 3, 2); //19 : [3]
        $this->T128[] = array(2, 2, 1, 2, 3, 1); //20 : [4]
        $this->T128[] = array(2, 1, 3, 2, 1, 2); //21 : [5]
        $this->T128[] = array(2, 2, 3, 1, 1, 2); //22 : [6]
        $this->T128[] = array(3, 1, 2, 1, 3, 1); //23 : [7]
        $this->T128[] = array(3, 1, 1, 2, 2, 2); //24 : [8]
        $this->T128[] = array(3, 2, 1, 1, 2, 2); //25 : [9]
        $this->T128[] = array(3, 2, 1, 2, 2, 1); //26 : [:]
        $this->T128[] = array(3, 1, 2, 2, 1, 2); //27 : [;]
        $this->T128[] = array(3, 2, 2, 1, 1, 2); //28 : [<]
        $this->T128[] = array(3, 2, 2, 2, 1, 1); //29 : [=]
        $this->T128[] = array(2, 1, 2, 1, 2, 3); //30 : [>]
        $this->T128[] = array(2, 1, 2, 3, 2, 1); //31 : [?]
        $this->T128[] = array(2, 3, 2, 1, 2, 1); //32 : [@]
        $this->T128[] = array(1, 1, 1, 3, 2, 3); //33 : [A]
        $this->T128[] = array(1, 3, 1, 1, 2, 3); //34 : [B]
        $this->T128[] = array(1, 3, 1, 3, 2, 1); //35 : [C]
        $this->T128[] = array(1, 1, 2, 3, 1, 3); //36 : [D]
        $this->T128[] = array(1, 3, 2, 1, 1, 3); //37 : [E]
        $this->T128[] = array(1, 3, 2, 3, 1, 1); //38 : [F]
        $this->T128[] = array(2, 1, 1, 3, 1, 3); //39 : [G]
        $this->T128[] = array(2, 3, 1, 1, 1, 3); //40 : [H]
        $this->T128[] = array(2, 3, 1, 3, 1, 1); //41 : [I]
        $this->T128[] = array(1, 1, 2, 1, 3, 3); //42 : [J]
        $this->T128[] = array(1, 1, 2, 3, 3, 1); //43 : [K]
        $this->T128[] = array(1, 3, 2, 1, 3, 1); //44 : [L]
        $this->T128[] = array(1, 1, 3, 1, 2, 3); //45 : [M]
        $this->T128[] = array(1, 1, 3, 3, 2, 1); //46 : [N]
        $this->T128[] = array(1, 3, 3, 1, 2, 1); //47 : [O]
        $this->T128[] = array(3, 1, 3, 1, 2, 1); //48 : [P]
        $this->T128[] = array(2, 1, 1, 3, 3, 1); //49 : [Q]
        $this->T128[] = array(2, 3, 1, 1, 3, 1); //50 : [R]
        $this->T128[] = array(2, 1, 3, 1, 1, 3); //51 : [S]
        $this->T128[] = array(2, 1, 3, 3, 1, 1); //52 : [T]
        $this->T128[] = array(2, 1, 3, 1, 3, 1); //53 : [U]
        $this->T128[] = array(3, 1, 1, 1, 2, 3); //54 : [V]
        $this->T128[] = array(3, 1, 1, 3, 2, 1); //55 : [W]
        $this->T128[] = array(3, 3, 1, 1, 2, 1); //56 : [X]
        $this->T128[] = array(3, 1, 2, 1, 1, 3); //57 : [Y]
        $this->T128[] = array(3, 1, 2, 3, 1, 1); //58 : [Z]
        $this->T128[] = array(3, 3, 2, 1, 1, 1); //59 : [[]
        $this->T128[] = array(3, 1, 4, 1, 1, 1); //60 : [\]
        $this->T128[] = array(2, 2, 1, 4, 1, 1); //61 : []]
        $this->T128[] = array(4, 3, 1, 1, 1, 1); //62 : [^]
        $this->T128[] = array(1, 1, 1, 2, 2, 4); //63 : [_]
        $this->T128[] = array(1, 1, 1, 4, 2, 2); //64 : [`]
        $this->T128[] = array(1, 2, 1, 1, 2, 4); //65 : [a]
        $this->T128[] = array(1, 2, 1, 4, 2, 1); //66 : [b]
        $this->T128[] = array(1, 4, 1, 1, 2, 2); //67 : [c]
        $this->T128[] = array(1, 4, 1, 2, 2, 1); //68 : [d]
        $this->T128[] = array(1, 1, 2, 2, 1, 4); //69 : [e]
        $this->T128[] = array(1, 1, 2, 4, 1, 2); //70 : [f]
        $this->T128[] = array(1, 2, 2, 1, 1, 4); //71 : [g]
        $this->T128[] = array(1, 2, 2, 4, 1, 1); //72 : [h]
        $this->T128[] = array(1, 4, 2, 1, 1, 2); //73 : [i]
        $this->T128[] = array(1, 4, 2, 2, 1, 1); //74 : [j]
        $this->T128[] = array(2, 4, 1, 2, 1, 1); //75 : [k]
        $this->T128[] = array(2, 2, 1, 1, 1, 4); //76 : [l]
        $this->T128[] = array(4, 1, 3, 1, 1, 1); //77 : [m]
        $this->T128[] = array(2, 4, 1, 1, 1, 2); //78 : [n]
        $this->T128[] = array(1, 3, 4, 1, 1, 1); //79 : [o]
        $this->T128[] = array(1, 1, 1, 2, 4, 2); //80 : [p]
        $this->T128[] = array(1, 2, 1, 1, 4, 2); //81 : [q]
        $this->T128[] = array(1, 2, 1, 2, 4, 1); //82 : [r]
        $this->T128[] = array(1, 1, 4, 2, 1, 2); //83 : [s]
        $this->T128[] = array(1, 2, 4, 1, 1, 2); //84 : [t]
        $this->T128[] = array(1, 2, 4, 2, 1, 1); //85 : [u]
        $this->T128[] = array(4, 1, 1, 2, 1, 2); //86 : [v]
        $this->T128[] = array(4, 2, 1, 1, 1, 2); //87 : [w]
        $this->T128[] = array(4, 2, 1, 2, 1, 1); //88 : [x]
        $this->T128[] = array(2, 1, 2, 1, 4, 1); //89 : [y]
        $this->T128[] = array(2, 1, 4, 1, 2, 1); //90 : [z]
        $this->T128[] = array(4, 1, 2, 1, 2, 1); //91 : [{]
        $this->T128[] = array(1, 1, 1, 1, 4, 3); //92 : [|]
        $this->T128[] = array(1, 1, 1, 3, 4, 1); //93 : [}]
        $this->T128[] = array(1, 3, 1, 1, 4, 1); //94 : [~]
        $this->T128[] = array(1, 1, 4, 1, 1, 3); //95 : [DEL]
        $this->T128[] = array(1, 1, 4, 3, 1, 1); //96 : [FNC3]
        $this->T128[] = array(4, 1, 1, 1, 1, 3); //97 : [FNC2]
        $this->T128[] = array(4, 1, 1, 3, 1, 1); //98 : [SHIFT]
        $this->T128[] = array(1, 1, 3, 1, 4, 1); //99 : [Cswap]
        $this->T128[] = array(1, 1, 4, 1, 3, 1); //100 : [Bswap]
        $this->T128[] = array(3, 1, 1, 1, 4, 1); //101 : [Aswap]
        $this->T128[] = array(4, 1, 1, 1, 3, 1); //102 : [FNC1]
        $this->T128[] = array(2, 1, 1, 4, 1, 2); //103 : [Astart]
        $this->T128[] = array(2, 1, 1, 2, 1, 4); //104 : [Bstart]
        $this->T128[] = array(2, 1, 1, 2, 3, 2); //105 : [Cstart]
        $this->T128[] = array(2, 3, 3, 1, 1, 1); //106 : [STOP]
        $this->T128[] = array(2, 1); //107 : [END BAR]

        for ($i = 32; $i <= 95; $i++) {
            // jeux de caractères
            $this->ABCset .= chr($i);
        }
        $this->Aset = $this->ABCset;
        $this->Bset = $this->ABCset;

        for ($i = 0; $i <= 31; $i++) {
            $this->ABCset .= chr($i);
            $this->Aset .= chr($i);
        }
        for ($i = 96; $i <= 127; $i++) {
            $this->ABCset .= chr($i);
            $this->Bset .= chr($i);
        }
        for ($i = 200; $i <= 210; $i++) {
            // controle 128
            $this->ABCset .= chr($i);
            $this->Aset .= chr($i);
            $this->Bset .= chr($i);
        }
        $this->Cset = "0123456789" . chr(206);

        for ($i = 0; $i < 96; $i++) {
            // convertisseurs des jeux A & B
            @$this->SetFrom["A"] .= chr($i);
            @$this->SetFrom["B"] .= chr($i + 32);
            @$this->SetTo["A"] .= chr(($i < 32) ? $i + 64 : $i - 32);
            @$this->SetTo["B"] .= chr($i);
        }
        for ($i = 96; $i < 107; $i++) {
            // contrôle des jeux A & B
            @$this->SetFrom["A"] .= chr($i + 104);
            @$this->SetFrom["B"] .= chr($i + 104);
            @$this->SetTo["A"] .= chr($i);
            @$this->SetTo["B"] .= chr($i);
        }

        if (function_exists('openssl_encrypt')) {
            function RC4($key, $data)
            {
                return openssl_encrypt($data, 'RC4-40', $key, OPENSSL_RAW_DATA);
            }
        } elseif (function_exists('mcrypt_encrypt')) {
            function RC4($key, $data)
            {
                return @mcrypt_encrypt(MCRYPT_ARCFOUR, $key, $data, MCRYPT_MODE_STREAM, '');
            }
        } else {
            function RC4($key, $data)
            {
                static $last_key, $last_state;

                if ($key != $last_key) {
                    $k     = str_repeat($key, 256 / strlen($key) + 1);
                    $state = range(0, 255);
                    $j     = 0;
                    for ($i = 0; $i < 256; $i++) {
                        $t         = $state[$i];
                        $j         = ($j + $t + ord($k[$i])) % 256;
                        $state[$i] = $state[$j];
                        $state[$j] = $t;
                    }
                    $last_key   = $key;
                    $last_state = $state;
                } else {
                    $state = $last_state;
                }

                $len = strlen($data);
                $a   = 0;
                $b   = 0;
                $out = '';
                for ($i = 0; $i < $len; $i++) {
                    $a         = ($a + 1) % 256;
                    $t         = $state[$a];
                    $b         = ($b + $t) % 256;
                    $state[$a] = $state[$b];
                    $state[$b] = $t;
                    $k         = $state[($state[$a] + $state[$b]) % 256];
                    $out .= chr(ord($data[$i]) ^ $k);
                }
                return $out;
            }
        }
    }

    //Cell with horizontal scaling if text is too wide
    public function CellFit($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $scale = false, $force = true)
    {
        //Get string width
        $str_width = $this->GetStringWidth($txt);

        //Calculate ratio to fit cell
        if ($w == 0) {
            $w = $this->w - $this->rMargin - $this->x;
        }

        if ($str_width == 0) {
            $str_width = 1;
        }

        $ratio = ($w - $this->cMargin * 2) / $str_width;

        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit) {
            if ($scale) {
                //Calculate horizontal scaling
                $horiz_scale = $ratio * 100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET', $horiz_scale));
            } else {
                //Calculate character spacing in points
                $char_space = ($w - $this->cMargin * 2 - $str_width) / max($this->MBGetStringLength($txt) - 1, 1) * $this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET', $char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align = '';
        }

        //Pass on to Cell method
        $this->Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);

        //Reset character spacing/horizontal scaling
        if ($fit) {
            $this->_out('BT ' . ($scale ? '100 Tz' : '0 Tc') . ' ET');
        }

    }

    //Cell with horizontal scaling only if necessary
    public function CellFitScale($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        $this->CellFit($w, $h, $txt, $border, $ln, $align, $fill, $link, true, false);
    }

    //Cell with horizontal scaling always
    public function CellFitScaleForce($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        $this->CellFit($w, $h, $txt, $border, $ln, $align, $fill, $link, true, true);
    }

    //Cell with character spacing only if necessary
    public function CellFitSpace($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        $this->CellFit($w, $h, $txt, $border, $ln, $align, $fill, $link, false, false);
    }

    //Cell with character spacing always
    public function CellFitSpaceForce($w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '')
    {
        //Same as calling CellFit directly
        $this->CellFit($w, $h, $txt, $border, $ln, $align, $fill, $link, false, true);
    }

    //Patch to also work with CJK double-byte text
    public function MBGetStringLength($s)
    {
        if ($this->CurrentFont['type'] == 'Type0') {
            $len     = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++) {
                if (ord($s[$i]) < 128) {
                    $len++;
                } else {
                    $len++;
                    $i++;
                }
            }
            return $len;
        } else {
            return strlen($s);
        }

    }

    public function AddPage($orientation = '', $size = '', $rotation = 0)
    {
        $this->AliasNbPages('{totalPages}');
        // Start a new page
        if ($this->state == 3) {
            $this->Error('The document is closed');
        }

        $family   = $this->FontFamily;
        $style    = $this->FontStyle . ($this->underline ? 'U' : '');
        $fontsize = $this->FontSizePt;
        $lw       = $this->LineWidth;
        $dc       = $this->DrawColor;
        $fc       = $this->FillColor;
        $tc       = $this->TextColor;
        $cf       = $this->ColorFlag;
        if ($this->page > 0) {
            // Page footer
            $this->InFooter = true;
            $this->Footer();
            $this->InFooter = false;
            // Close page
            $this->_endpage();
        }
        // Start new page
        $this->_beginpage($orientation, $size, $rotation);
        // Set line cap style to square
        $this->_out('2 J');
        // Set line width
        $this->LineWidth = $lw;
        $this->_out(sprintf('%.2F w', $lw * $this->k));
        // Set font
        if ($family) {
            $this->SetFont($family, $style, $fontsize);
        }

        // Set colors
        $this->DrawColor = $dc;
        if ($dc != '0 G') {
            $this->_out($dc);
        }

        $this->FillColor = $fc;
        if ($fc != '0 g') {
            $this->_out($fc);
        }

        $this->TextColor = $tc;
        $this->ColorFlag = $cf;
        // Page header
        $this->InHeader = true;
        $this->Header();
        $this->InHeader = false;
        // Restore line width
        if ($this->LineWidth != $lw) {
            $this->LineWidth = $lw;
            $this->_out(sprintf('%.2F w', $lw * $this->k));
        }
        // Restore font
        if ($family) {
            $this->SetFont($family, $style, $fontsize);
        }

        // Restore colors
        if ($this->DrawColor != $dc) {
            $this->DrawColor = $dc;
            $this->_out($dc);
        }
        if ($this->FillColor != $fc) {
            $this->FillColor = $fc;
            $this->_out($fc);
        }
        $this->TextColor = $tc;
        $this->ColorFlag = $cf;
    }

    // alpha: real value from 0 (transparent) to 1 (opaque)
    // bm:    blend mode, one of the following:
    //          Normal, Multiply, Screen, Overlay, Darken, Lighten, ColorDodge, ColorBurn,
    //          HardLight, SoftLight, Difference, Exclusion, Hue, Saturation, Color, Luminosity
    public function SetAlpha($alpha, $bm = 'Normal')
    {
        // set alpha for stroking (CA) and non-stroking (ca) operations
        $gs = $this->AddExtGState(array('ca' => $alpha, 'CA' => $alpha, 'BM' => '/' . $bm));
        $this->SetExtGState($gs);
    }

    public function AddExtGState($parms)
    {
        $n                             = count($this->extgstates) + 1;
        $this->extgstates[$n]['parms'] = $parms;
        return $n;
    }

    public function SetExtGState($gs)
    {
        $this->_out(sprintf('/GS%d gs', $gs));
    }

    public function _enddoc()
    {
        if (!empty($this->extgstates) && $this->PDFVersion < '1.4') {
            $this->PDFVersion = '1.4';
        }

        parent::_enddoc();
    }

    public function _putextgstates()
    {
        for ($i = 1; $i <= count($this->extgstates); $i++) {
            $this->_newobj();
            $this->extgstates[$i]['n'] = $this->n;
            $this->_out('<</Type /ExtGState');
            $parms = $this->extgstates[$i]['parms'];
            $this->_out(sprintf('/ca %.3F', $parms['ca']));
            $this->_out(sprintf('/CA %.3F', $parms['CA']));
            $this->_out('/BM ' . $parms['BM']);
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    public function _putresourcedict()
    {
        parent::_putresourcedict();
        $this->_out('/ExtGState <<');
        foreach ($this->extgstates as $k => $extgstate) {
            $this->_out('/GS' . $k . ' ' . $extgstate['n'] . ' 0 R');
        }

        $this->_out('>>');
    }

    public function _putresources()
    {
        $this->_putextgstates();
        parent::_putresources();

        if ($this->encrypted) {
            $this->_newobj();
            $this->enc_obj_id = $this->n;
            $this->_put('<<');
            $this->_putencryption();
            $this->_put('>>');
            $this->_put('endobj');
        }
    }

    public function PageNo()
    {
        // Get current page number
        return $this->page;
    }

    public function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k  = $this->k;
        $hp = $this->h;
        if ($style == 'F') {
            $op = 'f';
        } elseif ($style == 'FD' || $style == 'DF') {
            $op = 'B';
        } else {
            $op = 'S';
        }

        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));

        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));
        if (strpos($corners, '2') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $y) * $k));
        } else {
            $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        }

        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '3') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - ($y + $h)) * $k));
        } else {
            $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        }

        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        if (strpos($corners, '4') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - ($y + $h)) * $k));
        } else {
            $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        }

        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        if (strpos($corners, '1') === false) {
            $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $y) * $k));
            $this->_out(sprintf('%.2F %.2F l', ($x + $r) * $k, ($hp - $y) * $k));
        } else {
            $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        }

        $this->_out($op);
    }

    public function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

    public function Code128($x, $y, $code, $w, $h)
    {
        $Aguid = ""; // Création des guides de choix ABC
        $Bguid = "";
        $Cguid = "";
        for ($i = 0; $i < strlen($code); $i++) {
            $needle = substr($code, $i, 1);
            $Aguid .= ((strpos($this->Aset, $needle) === false) ? "N" : "O");
            $Bguid .= ((strpos($this->Bset, $needle) === false) ? "N" : "O");
            $Cguid .= ((strpos($this->Cset, $needle) === false) ? "N" : "O");
        }

        $SminiC = "OOOO";
        $IminiC = 4;

        $crypt = "";
        while ($code > "") {
            // BOUCLE PRINCIPALE DE CODAGE
            $i = strpos($Cguid, $SminiC); // forçage du jeu C, si possible
            if ($i !== false) {
                $Aguid[$i] = "N";
                $Bguid[$i] = "N";
            }

            if (substr($Cguid, 0, $IminiC) == $SminiC) {
                // jeu C
                $crypt .= chr(($crypt > "") ? $this->JSwap["C"] : $this->JStart["C"]); // début Cstart, sinon Cswap
                $made = strpos($Cguid, "N"); // étendu du set C
                if ($made === false) {
                    $made = strlen($Cguid);
                }
                if (fmod($made, 2) == 1) {
                    $made--; // seulement un nombre pair
                }
                for ($i = 0; $i < $made; $i += 2) {
                    $crypt .= chr(strval(substr($code, $i, 2))); // conversion 2 par 2
                }
                $jeu = "C";
            } else {
                $madeA = strpos($Aguid, "N"); // étendu du set A
                if ($madeA === false) {
                    $madeA = strlen($Aguid);
                }
                $madeB = strpos($Bguid, "N"); // étendu du set B
                if ($madeB === false) {
                    $madeB = strlen($Bguid);
                }
                $made = (($madeA < $madeB) ? $madeB : $madeA); // étendu traitée
                $jeu  = (($madeA < $madeB) ? "B" : "A"); // Jeu en cours

                $crypt .= chr(($crypt > "") ? $this->JSwap[$jeu] : $this->JStart[$jeu]); // début start, sinon swap

                $crypt .= strtr(substr($code, 0, $made), $this->SetFrom[$jeu], $this->SetTo[$jeu]); // conversion selon jeu
            }
            $code  = substr($code, $made); // raccourcir légende et guides de la zone traitée
            $Aguid = substr($Aguid, $made);
            $Bguid = substr($Bguid, $made);
            $Cguid = substr($Cguid, $made);
        } // FIN BOUCLE PRINCIPALE

        $check = ord($crypt[0]); // calcul de la somme de contrôle
        for ($i = 0; $i < strlen($crypt); $i++) {
            $check += (ord($crypt[$i]) * $i);
        }
        $check %= 103;

        $crypt .= chr($check) . chr(106) . chr(107); // Chaine cryptée complète

        $i     = (strlen($crypt) * 11) - 8; // calcul de la largeur du module
        $modul = $w / $i;

        for ($i = 0; $i < strlen($crypt); $i++) {
            // BOUCLE D'IMPRESSION
            $c = $this->T128[ord($crypt[$i])];
            for ($j = 0; $j < count($c); $j++) {
                $this->Rect($x, $y, $c[$j] * $modul, $h, "F");
                $x += ($c[$j++] + $c[$j]) * $modul;
            }
        }
    }

    public $angle = 0;
    public function Rotate($angle, $x = -1, $y = -1)
    {
        if ($x == -1) {
            $x = $this->x;
        }

        if ($y == -1) {
            $y = $this->y;
        }

        if ($this->angle != 0) {
            $this->_out('Q');
        }

        $this->angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c  = cos($angle);
            $s  = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    public function RotatedText($x, $y, $txt, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    public $B     = 0;
    public $I     = 0;
    public $U     = 0;
    public $HREF  = '';
    public $ALIGN = '';

    public function WriteHTML($html)
    {
        //HTML parser
        $html = str_replace("\n", ' ', $html);
        $a    = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i % 2 == 0) {
                //Text
                if ($this->HREF) {
                    $this->PutLink($this->HREF, $e);
                } elseif ($this->ALIGN == 'center') {
                    $this->Cell(0, 5, $e, 0, 1, 'C');
                } else {
                    $this->Write(5, $e);
                }

            } else {
                //Tag
                if ($e[0] == '/') {
                    $this->CloseTag(strtoupper(substr($e, 1)));
                } else {
                    //Extract properties
                    $a2   = explode(' ', $e);
                    $tag  = strtoupper(array_shift($a2));
                    $prop = array();
                    foreach ($a2 as $v) {
                        if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3)) {
                            $prop[strtoupper($a3[1])] = $a3[2];
                        }

                    }
                    $this->OpenTag($tag, $prop);
                }
            }
        }
    }

    public function OpenTag($tag, $prop)
    {
        //Opening tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U') {
            $this->SetStyle($tag, true);
        }

        if ($tag == 'A') {
            $this->HREF = $prop['HREF'];
        }

        if ($tag == 'BR') {
            $this->Ln(5);
        }

        if ($tag == 'P') {
            $this->ALIGN = $prop['ALIGN'];
        }

        if ($tag == 'HR') {
            if (!empty($prop['WIDTH'])) {
                $Width = $prop['WIDTH'];
            } else {
                $Width = $this->w - $this->lMargin - $this->rMargin;
            }

            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x, $y, $x + $Width, $y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    public function CloseTag($tag)
    {
        //Closing tag
        if ($tag == 'B' || $tag == 'I' || $tag == 'U') {
            $this->SetStyle($tag, false);
        }

        if ($tag == 'A') {
            $this->HREF = '';
        }

        if ($tag == 'P') {
            $this->ALIGN = '';
        }

    }

    public function SetStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach (array('B', 'I', 'U') as $s) {
            if ($this->$s > 0) {
                $style .= $s;
            }
        }

        $this->SetFont('', $style);
    }

    public function PutLink($URL, $txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }

    /**
     * Draws text within a box defined by width = w, height = h, and aligns
     * the text vertically within the box ($valign = M/B/T for middle, bottom, or top)
     * Also, aligns the text horizontally ($align = L/C/R/J for left, centered, right or justified)
     * drawTextBox uses drawRows
     *
     * This function is provided by TUFaT.com
     */
    public function drawTextBox($strText, $w, $h, $align = 'L', $valign = 'T', $border = true)
    {
        $xi = $this->GetX();
        $yi = $this->GetY();

        $hrow     = $this->FontSize;
        $textrows = $this->drawRows($w, $hrow, $strText, 0, $align, 0, 0, 0);
        $maxrows  = floor($h / $this->FontSize);
        $rows     = min($textrows, $maxrows);

        $dy = 0;
        if (strtoupper($valign) == 'M') {
            $dy = ($h - $rows * $this->FontSize) / 2;
        }

        if (strtoupper($valign) == 'B') {
            $dy = $h - $rows * $this->FontSize;
        }

        $this->SetY($yi + $dy);
        $this->SetX($xi);

        $this->drawRows($w, $hrow, $strText, 0, $align, false, $rows, 1);

        if ($border) {
            $this->Rect($xi, $yi, $w, $h);
        }

    }

    public function drawRows($w, $h, $txt, $border = 0, $align = 'J', $fill = false, $maxline = 0, $prn = 0)
    {
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0) {
            $w = $this->w - $this->rMargin - $this->x;
        }

        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s    = str_replace("\r", '', $txt);
        $nb   = strlen($s);
        if ($nb > 0 && $s[$nb - 1] == "\n") {
            $nb--;
        }

        $b = 0;
        if ($border) {
            if ($border == 1) {
                $border = 'LTRB';
                $b      = 'LRT';
                $b2     = 'LR';
            } else {
                $b2 = '';
                if (is_int(strpos($border, 'L'))) {
                    $b2 .= 'L';
                }

                if (is_int(strpos($border, 'R'))) {
                    $b2 .= 'R';
                }

                $b = is_int(strpos($border, 'T')) ? $b2 . 'T' : $b2;
            }
        }
        $sep = -1;
        $i   = 0;
        $j   = 0;
        $l   = 0;
        $ns  = 0;
        $nl  = 1;
        while ($i < $nb) {
            //Get next character
            $c = $s[$i];
            if ($c == "\n") {
                //Explicit line break
                if ($this->ws > 0) {
                    $this->ws = 0;
                    if ($prn == 1) {
                        $this->_out('0 Tw');
                    }

                }
                if ($prn == 1) {
                    $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
                }
                $i++;
                $sep = -1;
                $j   = $i;
                $l   = 0;
                $ns  = 0;
                $nl++;
                if ($border && $nl == 2) {
                    $b = $b2;
                }

                if ($maxline && $nl > $maxline) {
                    return substr($s, $i);
                }

                continue;
            }
            if ($c == ' ') {
                $sep = $i;
                $ls  = $l;
                $ns++;
            }
            $l += $cw[$c];
            if ($l > $wmax) {
                //Automatic line break
                if ($sep == -1) {
                    if ($i == $j) {
                        $i++;
                    }

                    if ($this->ws > 0) {
                        $this->ws = 0;
                        if ($prn == 1) {
                            $this->_out('0 Tw');
                        }

                    }
                    if ($prn == 1) {
                        $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
                    }
                } else {
                    if ($align == 'J') {
                        $this->ws = ($ns > 1) ? ($wmax - $ls) / 1000 * $this->FontSize / ($ns - 1) : 0;
                        if ($prn == 1) {
                            $this->_out(sprintf('%.3F Tw', $this->ws * $this->k));
                        }

                    }
                    if ($prn == 1) {
                        $this->Cell($w, $h, substr($s, $j, $sep - $j), $b, 2, $align, $fill);
                    }
                    $i = $sep + 1;
                }
                $sep = -1;
                $j   = $i;
                $l   = 0;
                $ns  = 0;
                $nl++;
                if ($border && $nl == 2) {
                    $b = $b2;
                }

                if ($maxline && $nl > $maxline) {
                    return substr($s, $i);
                }

            } else {
                $i++;
            }

        }
        //Last chunk
        if ($this->ws > 0) {
            $this->ws = 0;
            if ($prn == 1) {
                $this->_out('0 Tw');
            }

        }
        if ($border && is_int(strpos($border, 'B'))) {
            $b .= 'B';
        }

        if ($prn == 1) {
            $this->Cell($w, $h, substr($s, $j, $i - $j), $b, 2, $align, $fill);
        }
        $this->x = $this->lMargin;
        return $nl;
    }

    public function saveFont()
    {

        $saved = array();

        $saved['family'] = $this->FontFamily;
        $saved['style']  = $this->FontStyle;
        $saved['sizePt'] = $this->FontSizePt;
        $saved['size']   = $this->FontSize;
        $saved['curr']   = &$this->CurrentFont;

        return $saved;

    }

    public function restoreFont($saved)
    {

        $this->FontFamily  = $saved['family'];
        $this->FontStyle   = $saved['style'];
        $this->FontSizePt  = $saved['sizePt'];
        $this->FontSize    = $saved['size'];
        $this->CurrentFont = &$saved['curr'];

        if ($this->page > 0) {
            $this->_out(sprintf('BT /F%d %.2F Tf ET', $this->CurrentFont['i'], $this->FontSizePt));
        }

    }

    public function newFlowingBlock($w, $h, $b = 0, $a = 'J', $f = 0)
    {

        // cell width in points
        $this->flowingBlockAttr['width'] = $w * $this->k;

        // line height in user units
        $this->flowingBlockAttr['height'] = $h;

        $this->flowingBlockAttr['lineCount'] = 0;

        $this->flowingBlockAttr['border'] = $b;
        $this->flowingBlockAttr['align']  = $a;
        $this->flowingBlockAttr['fill']   = $f;

        $this->flowingBlockAttr['font']         = array();
        $this->flowingBlockAttr['content']      = array();
        $this->flowingBlockAttr['contentWidth'] = 0;

    }

    public function finishFlowingBlock()
    {

        $maxWidth = &$this->flowingBlockAttr['width'];

        $lineHeight = &$this->flowingBlockAttr['height'];

        $border = &$this->flowingBlockAttr['border'];
        $align  = &$this->flowingBlockAttr['align'];
        $fill   = &$this->flowingBlockAttr['fill'];

        $content = &$this->flowingBlockAttr['content'];
        $font    = &$this->flowingBlockAttr['font'];

        // set normal spacing
        $this->_out(sprintf('%.3F Tw', 0));

        // print out each chunk

        // the amount of space taken up so far in user units
        $usedWidth = 0;

        foreach ($content as $k => $chunk) {

            $b = '';

            if (is_int(strpos($border, 'B'))) {
                $b .= 'B';
            }

            if ($k == 0 && is_int(strpos($border, 'L'))) {
                $b .= 'L';
            }

            if ($k == count($content) - 1 && is_int(strpos($border, 'R'))) {
                $b .= 'R';
            }

            $this->restoreFont($font[$k]);

            // if it's the last chunk of this line, move to the next line after
            if ($k == count($content) - 1) {
                $this->Cell(($maxWidth / $this->k) - $usedWidth + 2 * $this->cMargin, $lineHeight, $chunk, $b, 1, $align, $fill);
            } else {
                $this->Cell($this->GetStringWidth($chunk), $lineHeight, $chunk, $b, 0, $align, $fill);
            }

            $usedWidth += $this->GetStringWidth($chunk);

        }

    }

    public function WriteFlowingBlock($s)
    {

        // width of all the content so far in points
        $contentWidth = &$this->flowingBlockAttr['contentWidth'];

        // cell width in points
        $maxWidth = &$this->flowingBlockAttr['width'];

        $lineCount = &$this->flowingBlockAttr['lineCount'];

        // line height in user units
        $lineHeight = &$this->flowingBlockAttr['height'];

        $border = &$this->flowingBlockAttr['border'];
        $align  = &$this->flowingBlockAttr['align'];
        $fill   = &$this->flowingBlockAttr['fill'];

        $content = &$this->flowingBlockAttr['content'];
        $font    = &$this->flowingBlockAttr['font'];

        $font[]    = $this->saveFont();
        $content[] = '';

        $currContent = &$content[count($content) - 1];

        // where the line should be cutoff if it is to be justified
        $cutoffWidth = $contentWidth;

        // for every character in the string
        for ($i = 0; $i < strlen($s); $i++) {

            // extract the current character
            $c = $s[$i];

            // get the width of the character in points
            $cw = $this->CurrentFont['cw'][$c] * ($this->FontSizePt / 1000);

            if ($c == ' ') {

                $currContent .= ' ';
                $cutoffWidth = $contentWidth;

                $contentWidth += $cw;

                continue;

            }

            // try adding another char
            if ($contentWidth + $cw > $maxWidth) {

                // won't fit, output what we have
                $lineCount++;

                // contains any content that didn't make it into this print
                $savedContent = '';
                $savedFont    = array();

                // first, cut off and save any partial words at the end of the string
                $words = explode(' ', $currContent);

                // if it looks like we didn't finish any words for this chunk
                if (count($words) == 1) {

                    // save and crop off the content currently on the stack
                    $savedContent = array_pop($content);
                    $savedFont    = array_pop($font);

                    // trim any trailing spaces off the last bit of content
                    $currContent = &$content[count($content) - 1];

                    $currContent = rtrim($currContent);

                }

                // otherwise, we need to find which bit to cut off
                else {

                    $lastContent = '';

                    for ($w = 0; $w < count($words) - 1; $w++) {
                        $lastContent .= "{$words[$w]} ";
                    }

                    $savedContent = $words[count($words) - 1];
                    $savedFont    = $this->saveFont();

                    // replace the current content with the cropped version
                    $currContent = rtrim($lastContent);

                }

                // update $contentWidth and $cutoffWidth since they changed with cropping
                $contentWidth = 0;

                foreach ($content as $k => $chunk) {

                    $this->restoreFont($font[$k]);

                    $contentWidth += $this->GetStringWidth($chunk) * $this->k;

                }

                $cutoffWidth = $contentWidth;

                // if it's justified, we need to find the char spacing
                if ($align == 'J') {

                    // count how many spaces there are in the entire content string
                    $numSpaces = 0;

                    foreach ($content as $chunk) {
                        $numSpaces += substr_count($chunk, ' ');
                    }

                    // if there's more than one space, find word spacing in points
                    if ($numSpaces > 0) {
                        $this->ws = ($maxWidth - $cutoffWidth) / $numSpaces;
                    } else {
                        $this->ws = 0;
                    }

                    $this->_out(sprintf('%.3F Tw', $this->ws));

                }

                // otherwise, we want normal spacing
                else {
                    $this->_out(sprintf('%.3F Tw', 0));
                }

                // print out each chunk
                $usedWidth = 0;

                foreach ($content as $k => $chunk) {

                    $this->restoreFont($font[$k]);

                    $stringWidth = $this->GetStringWidth($chunk) + ($this->ws * substr_count($chunk, ' ') / $this->k);

                    // determine which borders should be used
                    $b = '';

                    if ($lineCount == 1 && is_int(strpos($border, 'T'))) {
                        $b .= 'T';
                    }

                    if ($k == 0 && is_int(strpos($border, 'L'))) {
                        $b .= 'L';
                    }

                    if ($k == count($content) - 1 && is_int(strpos($border, 'R'))) {
                        $b .= 'R';
                    }

                    // if it's the last chunk of this line, move to the next line after
                    if ($k == count($content) - 1) {
                        $this->Cell(($maxWidth / $this->k) - $usedWidth + 2 * $this->cMargin, $lineHeight, $chunk, $b, 1, $align, $fill);
                    } else {

                        $this->Cell($stringWidth + 2 * $this->cMargin, $lineHeight, $chunk, $b, 0, $align, $fill);
                        $this->x -= 2 * $this->cMargin;

                    }

                    $usedWidth += $stringWidth;

                }

                // move on to the next line, reset variables, tack on saved content and current char
                $this->restoreFont($savedFont);

                $font    = array($savedFont);
                $content = array($savedContent . $s[$i]);

                $currContent = &$content[0];

                $contentWidth = $this->GetStringWidth($currContent) * $this->k;
                $cutoffWidth  = $contentWidth;

            }

            // another character will fit, so add it on
            else {

                $contentWidth += $cw;
                $currContent .= $s[$i];

            }

        }

    }

    /**
     * Function to set permissions as well as user and owner passwords
     *
     * - permissions is an array with values taken from the following list:
     *   copy, print, modify, annot-forms
     *   If a value is present it means that the permission is granted
     * - If a user password is set, user will be prompted before document is opened
     * - If an owner password is set, document can be opened in privilege mode with no
     *   restriction if that password is entered
     */
    public function SetProtection($permissions = array(), $user_pass = '', $owner_pass = null)
    {
        $options    = array('print' => 4, 'modify' => 8, 'copy' => 16, 'annot-forms' => 32);
        $protection = 192;
        foreach ($permissions as $permission) {
            if (!isset($options[$permission])) {
                $this->Error('Incorrect permission: ' . $permission);
            }

            $protection += $options[$permission];
        }
        if ($owner_pass === null) {
            $owner_pass = uniqid(rand());
        }

        $this->encrypted = true;
        $this->padding   = "\x28\xBF\x4E\x5E\x4E\x75\x8A\x41\x64\x00\x4E\x56\xFF\xFA\x01\x08" .
            "\x2E\x2E\x00\xB6\xD0\x68\x3E\x80\x2F\x0C\xA9\xFE\x64\x53\x69\x7A";
        $this->_generateencryptionkey($user_pass, $owner_pass, $protection);
    }

/****************************************************************************
 *                                                                           *
 *                              Private methods                              *
 *                                                                           *
 ****************************************************************************/

    public function _putstream($s)
    {
        if ($this->encrypted) {
            $s = RC4($this->_objectkey($this->n), $s);
        }

        parent::_putstream($s);
    }

    public function _textstring($s)
    {
        if (!$this->_isascii($s)) {
            $s = $this->_UTF8toUTF16($s);
        }

        if ($this->encrypted) {
            $s = RC4($this->_objectkey($this->n), $s);
        }

        return '(' . $this->_escape($s) . ')';
    }

    /**
     * Compute key depending on object number where the encrypted data is stored
     */
    public function _objectkey($n)
    {
        return substr($this->_md5_16($this->encryption_key . pack('VXxx', $n)), 0, 10);
    }

    public function _putencryption()
    {
        $this->_put('/Filter /Standard');
        $this->_put('/V 1');
        $this->_put('/R 2');
        $this->_put('/O (' . $this->_escape($this->Ovalue) . ')');
        $this->_put('/U (' . $this->_escape($this->Uvalue) . ')');
        $this->_put('/P ' . $this->Pvalue);
    }

    public function _puttrailer()
    {
        parent::_puttrailer();
        if ($this->encrypted) {
            $this->_put('/Encrypt ' . $this->enc_obj_id . ' 0 R');
            $this->_put('/ID [()()]');
        }
    }

    /**
     * Get MD5 as binary string
     */
    public function _md5_16($string)
    {
        return md5($string, true);
    }

    /**
     * Compute O value
     */
    public function _Ovalue($user_pass, $owner_pass)
    {
        $tmp           = $this->_md5_16($owner_pass);
        $owner_RC4_key = substr($tmp, 0, 5);
        return RC4($owner_RC4_key, $user_pass);
    }

    /**
     * Compute U value
     */
    public function _Uvalue()
    {
        return RC4($this->encryption_key, $this->padding);
    }

    /**
     * Compute encryption key
     */
    public function _generateencryptionkey($user_pass, $owner_pass, $protection)
    {
        // Pad passwords
        $user_pass  = substr($user_pass . $this->padding, 0, 32);
        $owner_pass = substr($owner_pass . $this->padding, 0, 32);
        // Compute O value
        $this->Ovalue = $this->_Ovalue($user_pass, $owner_pass);
        // Compute encyption key
        $tmp                  = $this->_md5_16($user_pass . $this->Ovalue . chr($protection) . "\xFF\xFF\xFF");
        $this->encryption_key = substr($tmp, 0, 5);
        // Compute U value
        $this->Uvalue = $this->_Uvalue();
        // Compute P value
        $this->Pvalue = -(($protection ^ 255) + 1);
    }

    public function Code39($xpos, $ypos, $code, $baseline = 0.5, $height = 5)
    {

        $wide   = $baseline;
        $narrow = $baseline / 3;
        $gap    = $narrow;

        $barChar['0'] = 'nnnwwnwnn';
        $barChar['1'] = 'wnnwnnnnw';
        $barChar['2'] = 'nnwwnnnnw';
        $barChar['3'] = 'wnwwnnnnn';
        $barChar['4'] = 'nnnwwnnnw';
        $barChar['5'] = 'wnnwwnnnn';
        $barChar['6'] = 'nnwwwnnnn';
        $barChar['7'] = 'nnnwnnwnw';
        $barChar['8'] = 'wnnwnnwnn';
        $barChar['9'] = 'nnwwnnwnn';
        $barChar['A'] = 'wnnnnwnnw';
        $barChar['B'] = 'nnwnnwnnw';
        $barChar['C'] = 'wnwnnwnnn';
        $barChar['D'] = 'nnnnwwnnw';
        $barChar['E'] = 'wnnnwwnnn';
        $barChar['F'] = 'nnwnwwnnn';
        $barChar['G'] = 'nnnnnwwnw';
        $barChar['H'] = 'wnnnnwwnn';
        $barChar['I'] = 'nnwnnwwnn';
        $barChar['J'] = 'nnnnwwwnn';
        $barChar['K'] = 'wnnnnnnww';
        $barChar['L'] = 'nnwnnnnww';
        $barChar['M'] = 'wnwnnnnwn';
        $barChar['N'] = 'nnnnwnnww';
        $barChar['O'] = 'wnnnwnnwn';
        $barChar['P'] = 'nnwnwnnwn';
        $barChar['Q'] = 'nnnnnnwww';
        $barChar['R'] = 'wnnnnnwwn';
        $barChar['S'] = 'nnwnnnwwn';
        $barChar['T'] = 'nnnnwnwwn';
        $barChar['U'] = 'wwnnnnnnw';
        $barChar['V'] = 'nwwnnnnnw';
        $barChar['W'] = 'wwwnnnnnn';
        $barChar['X'] = 'nwnnwnnnw';
        $barChar['Y'] = 'wwnnwnnnn';
        $barChar['Z'] = 'nwwnwnnnn';
        $barChar['-'] = 'nwnnnnwnw';
        $barChar['.'] = 'wwnnnnwnn';
        $barChar[' '] = 'nwwnnnwnn';
        $barChar['*'] = 'nwnnwnwnn';
        $barChar['$'] = 'nwnwnwnnn';
        $barChar['/'] = 'nwnwnnnwn';
        $barChar['+'] = 'nwnnnwnwn';
        $barChar['%'] = 'nnnwnwnwn';

        $this->SetFont('Arial', '', 10);
        $this->Text($xpos, $ypos + $height + 4, $code);
        $this->SetFillColor(0);

        $code = '*' . strtoupper($code) . '*';
        for ($i = 0; $i < strlen($code); $i++) {
            $char = $code[$i];
            if (!isset($barChar[$char])) {
                $this->Error('Invalid character in barcode: ' . $char);
            }
            $seq = $barChar[$char];
            for ($bar = 0; $bar < 9; $bar++) {
                if ($seq[$bar] == 'n') {
                    $lineWidth = $narrow;
                } else {
                    $lineWidth = $wide;
                }
                if ($bar % 2 == 0) {
                    $this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
                }
                $xpos += $lineWidth;
            }
            $xpos += $gap;
        }
    }

    function Sector($xc, $yc, $r, $a, $b, $style='FD', $cw=true, $o=90)
    {
        $d0 = $a - $b;
        if($cw){
            $d = $b;
            $b = $o - $a;
            $a = $o - $d;
        }else{
            $b += $o;
            $a += $o;
        }
        while($a<0)
            $a += 360;
        while($a>360)
            $a -= 360;
        while($b<0)
            $b += 360;
        while($b>360)
            $b -= 360;
        if ($a > $b)
            $b += 360;
        $b = $b/360*2*M_PI;
        $a = $a/360*2*M_PI;
        $d = $b - $a;
        if ($d == 0 && $d0 != 0)
            $d = 2*M_PI;
        $k = $this->k;
        $hp = $this->h;
        if (sin($d/2))
            $MyArc = 4/3*(1-cos($d/2))/sin($d/2)*$r;
        else
            $MyArc = 0;
        //first put the center
        $this->_out(sprintf('%.2F %.2F m',($xc)*$k,($hp-$yc)*$k));
        //put the first point
        $this->_out(sprintf('%.2F %.2F l',($xc+$r*cos($a))*$k,(($hp-($yc-$r*sin($a)))*$k)));
        //draw the arc
        if ($d < M_PI/2){
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }else{
            $b = $a + $d/4;
            $MyArc = 4/3*(1-cos($d/8))/sin($d/8)*$r;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }
        //terminate drawing
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='b';
        else
            $op='s';
        $this->_out($op);
    }

    function PieChart($w, $h, $data, $format, $colors=null)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $hLegend = 5;
        $radius = min($w - $margin * 4 - $hLegend - $this->wLegend, $h - $margin * 2);
        $radius = floor($radius / 2);
        $XDiag = $XPage + $margin + $radius;
        $YDiag = $YPage + $margin + $radius;
        if($colors == null) {
            for($i = 0; $i < $this->NbVal; $i++) {
                $gray = $i * intval(255 / $this->NbVal);
                $colors[$i] = array($gray,$gray,$gray);
            }
        }

        //Sectors
        $this->SetLineWidth(0.2);
        $angleStart = 0;
        $angleEnd = 0;
        $i = 0;
        foreach($data as $val) {
            $angle = ($val * 360) / doubleval($this->sum);
            if ($angle != 0) {
                $angleEnd = $angleStart + $angle;
                $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
                $this->Sector($XDiag, $YDiag, $radius, $angleStart, $angleEnd);
                $angleStart += $angle;
            }
            $i++;
        }

        //Legends
        $this->SetFont('Courier', '', 10);
        $x1 = $XPage + 2 * $radius + 4 * $margin;
        $x2 = $x1 + $hLegend + $margin;
        $y1 = $YDiag - $radius + (2 * $radius - $this->NbVal*($hLegend + $margin)) / 2;
        for($i=0; $i<$this->NbVal; $i++) {
            $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
            $this->Rect($x1, $y1, $hLegend, $hLegend, 'DF');
            $this->SetXY($x2,$y1);
            $this->Cell(0,$hLegend,$this->legends[$i]);
            $y1+=$hLegend + $margin;
        }
    }

    function BarDiagram($w, $h, $data, $format, $color=null, $maxVal=0, $nbDiv=4)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $YDiag = $YPage + $margin;
        $hDiag = floor($h - $margin * 2);
        $XDiag = $XPage + $margin * 2 + $this->wLegend;
        $lDiag = floor($w - $margin * 3 - $this->wLegend);
        if($color == null)
            $color=array(155,155,155);
        if ($maxVal == 0) {
            $maxVal = max($data);
        }
        $valIndRepere = ceil($maxVal / $nbDiv);
        $maxVal = $valIndRepere * $nbDiv;
        $lRepere = floor($lDiag / $nbDiv);
        $lDiag = $lRepere * $nbDiv;
        $unit = $lDiag / $maxVal;
        $hBar = floor($hDiag / ($this->NbVal + 1));
        $hDiag = $hBar * ($this->NbVal + 1);
        $eBaton = floor($hBar * 80 / 100);

        $this->SetLineWidth(0.2);
        $this->Rect($XDiag, $YDiag, $lDiag, $hDiag);

        $this->SetFont('Courier', '', 10);
        $this->SetFillColor($color[0],$color[1],$color[2]);
        $i=0;
        foreach($data as $val) {
            //Bar
            $xval = $XDiag;
            $lval = (int)($val * $unit);
            $yval = $YDiag + ($i + 1) * $hBar - $eBaton / 2;
            $hval = $eBaton;
            $this->Rect($xval, $yval, $lval, $hval, 'DF');
            //Legend
            $this->SetXY(0, $yval);
            $this->Cell($xval - $margin, $hval, $this->legends[$i],0,0,'R');
            $i++;
        }

        //Scales
        for ($i = 0; $i <= $nbDiv; $i++) {
            $xpos = $XDiag + $lRepere * $i;
            $this->Line($xpos, $YDiag, $xpos, $YDiag + $hDiag);
            $val = $i * $valIndRepere;
            $xpos = $XDiag + $lRepere * $i - $this->GetStringWidth($val) / 2;
            $ypos = $YDiag + $hDiag - $margin;
            $this->Text($xpos, $ypos, $val);
        }
    }

    function SetLegends($data, $format)
    {
        $this->legends=array();
        $this->wLegend=0;
        $this->sum=array_sum($data);
        $this->NbVal=count($data);
        foreach($data as $l=>$val)
        {
            $p=sprintf('%.2f',$val/$this->sum*100).'%';
            $legend=str_replace(array('%l','%v','%p'),array($l,$val,$p),$format);
            $this->legends[]=$legend;
            $this->wLegend=max($this->GetStringWidth($legend),$this->wLegend);
        }
    }

    function Circle($x, $y, $r, $style = 'D')
    {
        $this->Ellipse($x, $y, $r, $r, $style);
    }

    function Ellipse($x, $y, $rx, $ry, $style = 'D')
    {
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $lx = 4 / 3 * (M_SQRT2 - 1) * $rx;
        $ly = 4 / 3 * (M_SQRT2 - 1) * $ry;
        $k = $this->k;
        $h = $this->h;
        $this->_out(sprintf(
            '%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x + $rx) * $k,
            ($h - $y) * $k,
            ($x + $rx) * $k,
            ($h - ($y - $ly)) * $k,
            ($x + $lx) * $k,
            ($h - ($y - $ry)) * $k,
            $x * $k,
            ($h - ($y - $ry)) * $k
        ));
        $this->_out(sprintf(
            '%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x - $lx) * $k,
            ($h - ($y - $ry)) * $k,
            ($x - $rx) * $k,
            ($h - ($y - $ly)) * $k,
            ($x - $rx) * $k,
            ($h - $y) * $k
        ));
        $this->_out(sprintf(
            '%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x - $rx) * $k,
            ($h - ($y + $ly)) * $k,
            ($x - $lx) * $k,
            ($h - ($y + $ry)) * $k,
            $x * $k,
            ($h - ($y + $ry)) * $k
        ));
        $this->_out(sprintf(
            '%.2F %.2F %.2F %.2F %.2F %.2F c %s',
            ($x + $lx) * $k,
            ($h - ($y + $ry)) * $k,
            ($x + $rx) * $k,
            ($h - ($y + $ly)) * $k,
            ($x + $rx) * $k,
            ($h - $y) * $k,
            $op
        ));
    }

    function SetDash($black=false, $white=false)
    {
        if($black and $white)
            $s=sprintf('[%.3f %.3f] 0 d', $black*$this->k, $white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
  

}
