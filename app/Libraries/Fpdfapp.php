<?php

namespace ABCcomptencias\Libraries;

use ABCcomptencias\Libraries\FPDF_CellFit;

//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Fpdfapp extends FPDF_CellFit
{
    private $paramsheader;
    private $paramsfooter;
    private $titulo;
    private $titulo2;
    public function __construct() {
        parent::__construct();
    }
    
    function getParamsheader() {
        return $this->paramsheader;
    }
    function getParamsfooter() {
        return $this->paramsfooter;
    }

    function getTitulo() {
        return $this->titulo;
    }
    
    function getTitulo2() {
        return $this->titulo2;
    }

    function setParamsheader($paramsheader) {
        $this->paramsheader = $paramsheader;
    }
    function setParamsfooter($paramsfooter) {
        $this->paramsfooter = $paramsfooter;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setTitulo2($titulo2) {
        $this->titulo2 = $titulo2;
    }       
          
    // El encabezado del PDF
    public function Header() {

        $this->SetLeftMargin(25);
        $this->SetFont('Helvetica', 'B', 12);
        $this->SetTextColor(107,107,107);
        $this->SetDrawColor(178, 178, 178);
        $this->SetFillColor(255, 255, 255);
//        var_dump($this->getParamsheader());        
        // if ($this->getParamsheader()["imagen1"]["x"] > 0) {
        //     $this->Image('../public/images/brochure.png', $this->getParamsheader()["imagen1"]["x"], $this->getParamsheader()["imagen1"]["y"], $this->getParamsheader()["imagen1"]["ancho"], $this->getParamsheader()["imagen1"]["alto"]);
        //     $this->Cell($this->getParamsheader()["celda1"]["ancho"], $this->getParamsheader()["celda1"]["alto"], '', 1, 0);    
        //     $x=$this->GetX();
        //     $y=$this->GetY();
        // }
        
        $this->SetFont('Helvetica', 'B', 12);
        $this->Cell($this->getParamsheader()["celda2"]["ancho"], $this->paramsheader["celda2"]["alto"], $this->getTitulo(), 'LTR', 1, 'C'); 
        $this->Cell($this->getParamsheader()["celda3"]["ancho"], $this->paramsheader["celda3"]["alto"], $this->getTitulo2(), 'LRB', 0, 'C');                      
        $this->SetFont('Helvetica', '', 9);             
        // $this->Cell($this->getParamsheader()["celda1"]["ancho"], $this->getParamsheader()["celda3"]["alto"], '', 0, 0);
        $this->SetFont('Helvetica', 'B', 12);
        $this->Ln(15);
    }

    // El pie del pdf
    public function Footer() {
        $this->SetY(-20);
        $this->SetTextColor(107,107,107);
        $this->SetDrawColor(178, 178, 178);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell($this->getParamsfooter()["paginador"]["ancho"], 5, utf8_decode('Página '.$this->PageNo().' de {nb}'), 'LRT', 1, 'C');
        $this->Cell($this->getParamsfooter()["fecha"]["ancho"], 5, 'Generado el: '.date('d/m/Y'), 'LRB', 1, 'C');
    }

}
