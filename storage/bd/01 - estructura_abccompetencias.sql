/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     7/04/2020 3:30:25 p. m.                      */
/*==============================================================*/


/*==============================================================*/
/* Table: accesos                                               */
/*==============================================================*/
create table accesos (
   codacceso            SERIAL               not null,
   codusuario           int4                 null,
   fechaingreso         date                 not null,
   horaingreso          time                 null,
   ipoculta             varchar(50)          null,
   ipvisible            varchar(50)          null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   so                   varchar(300)         null,
   navegador            varchar(300)         null,
   constraint accesos_pkey primary key (codacceso)
);

-- set table ownership
alter table accesos owner to user_abccompetencias
;
/*==============================================================*/
/* Table: actividades                                           */
/*==============================================================*/
create table actividades (
   codactividad         SERIAL               not null,
   codconocimiento      INT4                 null,
   nombre               varchar(255)         null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint actividades_pkey primary key (codactividad)
);

-- set table ownership
alter table actividades owner to user_abccompetencias
;
/*==============================================================*/
/* Table: activity_log                                          */
/*==============================================================*/
create table activity_log (
   id                   SERIAL               not null,
   log_name             varchar(255)         null,
   description          varchar(255)         null,
   subject_id           int8                 null,
   subject_type         varchar(255)         null,
   causer_id            int8                 null,
   causer_type          varchar(255)         null,
   properties           text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null
);

-- set table ownership
alter table activity_log owner to user_abccompetencias
;
/*==============================================================*/
/* Index: activity_log_log_name_index                           */
/*==============================================================*/
create  index activity_log_log_name_index on activity_log using BTREE (
( log_name )
);

/*==============================================================*/
/* Index: index_log_name                                        */
/*==============================================================*/
create  index index_log_name on activity_log using BTREE (
( log_name )
);

/*==============================================================*/
/* Table: agendadas                                             */
/*==============================================================*/
create table agendadas (
   codagendada          SERIAL               not null,
   codsolicitud         int4                 not null,
   codevaluador         int4                 null,
   fecha_agendada       date                 not null,
   hora                 time                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint agendadas_pkey primary key (codagendada)
);

-- set table ownership
alter table agendadas owner to user_abccompetencias
;
/*==============================================================*/
/* Table: cache                                                 */
/*==============================================================*/
create table cache (
   key                  text                 null,
   value                text                 null,
   expiration           int4                 null,
   constraint cache_key_key unique (key)
);

-- set table ownership
alter table cache owner to user_abccompetencias
;
/*==============================================================*/
/* Table: certificados                                          */
/*==============================================================*/
create table certificados (
   codcertificado       SERIAL               not null,
   codsolicitud         int4                 null,
   codigo               text                 null,
   fecha                date                 null,
   fecha_vencimiento    date                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint certificados_pkey primary key (codcertificado)
);

-- set table ownership
alter table certificados owner to user_abccompetencias
;
/*==============================================================*/
/* Table: conocimientos                                         */
/*==============================================================*/
create table conocimientos (
   codconocimiento      SERIAL               not null,
   codnorma             INT4                 null,
   nombre               varchar(255)         null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint PK_CONOCIMIENTOS primary key (codconocimiento)
);

-- set table ownership
alter table conocimientos owner to user_abccompetencias
;
/*==============================================================*/
/* Table: cuestionarios                                         */
/*==============================================================*/
create table cuestionarios (
   codcuestionario      SERIAL               not null,
   nombre               varchar(255)         null,
   segundos_x_pregunta  int8                 not null,
   tipo                 int8                 null default 1,
   instrucciones        text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint cuestionarios_pkey primary key (codcuestionario)
);

-- set table ownership
alter table cuestionarios owner to user_abccompetencias
;
/*==============================================================*/
/* Table: cuestionarios_solicitud                               */
/*==============================================================*/
create table cuestionarios_solicitud (
   codcuestionario_solicitud SERIAL               not null,
   codsolicitud         int4                 null,
   codcuestionario      int4                 null,
   codevaluacion_desempeno int4                 null,
   codevaluacion_producto int4                 null,
   terminado            bool                 null,
   hora_inicio          timestamp            null,
   hora_fin             timestamp            null,
   hora_fin_estimado    timestamp            null,
   aprobo               bool                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint cuestionarios_solicitud_pkey primary key (codcuestionario_solicitud),
   constraint cuestionarios_solicitud_un unique (codsolicitud, codcuestionario)
);

-- set table ownership
alter table cuestionarios_solicitud owner to user_abccompetencias
;
/*==============================================================*/
/* Table: etapas_solicitudes                                    */
/*==============================================================*/
create table etapas_solicitudes (
   codetapasolicitud    SERIAL               not null,
   codsolicitud         int8                 not null,
   etapa                int4                 not null,
   ultima               bool                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint estapas_solicitudes_pk primary key (codetapasolicitud)
);

-- set table ownership
alter table etapas_solicitudes owner to user_abccompetencias
;
/*==============================================================*/
/* Table: evaluaciones                                          */
/*==============================================================*/
create table evaluaciones (
   codevaluacion        SERIAL               not null,
   nombre               varchar(255)         null,
   descripcion          text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint evaluaciones_pkey primary key (codevaluacion)
);

-- set table ownership
alter table evaluaciones owner to user_abccompetencias
;
/*==============================================================*/
/* Table: evaluaciones_solicitudes                              */
/*==============================================================*/
create table evaluaciones_solicitudes (
   codevaluacionsolicitud SERIAL               not null,
   codsolicitud         int4                 not null,
   codevaluacion        int4                 not null,
   codevaluador         int4                 null,
   aprobo               bool                 not null,
   fecha_aplicacion     date                 null,
   fecha_resultados     date                 null,
   lugar                text                 null,
   ciudad               text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint evaluaciones_solicitudes_pkey primary key (codevaluacionsolicitud)
);

-- set table ownership
alter table evaluaciones_solicitudes owner to user_abccompetencias
;
/*==============================================================*/
/* Table: festivos                                              */
/*==============================================================*/
create table festivos (
   codfestivo           SERIAL               not null,
   fecha                date                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint festivos_pkey primary key (codfestivo),
   constraint festivos_fecha_key unique (fecha)
);

-- set table ownership
alter table festivos owner to user_abccompetencias
;
/*==============================================================*/
/* Table: funcionalidades                                       */
/*==============================================================*/
create table funcionalidades (
   codfunc              SERIAL               not null,
   codpadre             int4                 null,
   nombre               varchar(100)         null,
   identificador        varchar(200)         null,
   orden                int4                 not null,
   urlpagina            varchar(100)         null,
   target               varchar(50)          null,
   icono                varchar(100)         null,
   tipo                 varchar(10)          null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint funcionalidades_pkey primary key (codfunc),
   constraint func_identificador_key unique (identificador)
);

-- set table ownership
alter table funcionalidades owner to user_abccompetencias
;
/*==============================================================*/
/* Table: imagenes_fachada                                      */
/*==============================================================*/
create table imagenes_fachada (
   codimagenfachada     SERIAL               not null,
   nombre               varchar(255)         null,
   descripcion          text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint imagenes_fachada_pkey primary key (codimagenfachada),
   constraint uq_imagenes_fachada_nombre unique (nombre)
);

-- set table ownership
alter table imagenes_fachada owner to user_abccompetencias
;
/*==============================================================*/
/* Table: indicadores                                           */
/*==============================================================*/
create table indicadores (
   codindicador         SERIAL               not null,
   codnorma             int4                 not null,
   nombre               text                 null,
   tipo                 int4                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint indicadores_pkey primary key (codindicador)
);

-- set table ownership
alter table indicadores owner to user_abccompetencias
;
/*==============================================================*/
/* Table: indicadores_evaluacion                                */
/*==============================================================*/
create table indicadores_evaluacion (
   codindicadorevaluacion SERIAL               not null,
   codindicador         int4                 null,
   codevaluacionsolicitud int4                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint indicadores_evaluacion_pkey primary key (codindicadorevaluacion)
);

-- set table ownership
alter table indicadores_evaluacion owner to user_abccompetencias
;
/*==============================================================*/
/* Table: migrations                                            */
/*==============================================================*/
create table migrations (
   id                   SERIAL               not null,
   migration            varchar(255)         null,
   batch                int4                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint migrations_pkey primary key (id)
);

-- set table ownership
alter table migrations owner to user_abccompetencias
;
/*==============================================================*/
/* Table: normas                                                */
/*==============================================================*/
create table normas (
   codnorma             SERIAL               not null,
   codtipo_titulacion   int4                 null,
   nombre               text                 null,
   descripcion          text                 null,
   codigo               varchar(50)          null,
   archivo              varchar(255)         null,
   valor                numeric              null,
   estado               int2                 not null default 1,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint normas_pkey primary key (codnorma)
);

-- set table ownership
alter table normas owner to user_abccompetencias
;
/*==============================================================*/
/* Table: noticias                                              */
/*==============================================================*/
create table noticias (
   codnoticia           SERIAL               not null,
   titulo               varchar(100)         null,
   contenido            text                 null,
   descripcion          text                 null,
   imagen               varchar(255)         null,
   estado               int2                 not null default 1,
   slug                 text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint noticias_pkey primary key (codnoticia)
);

-- set table ownership
alter table noticias owner to user_abccompetencias
;
/*==============================================================*/
/* Table: notificaciones                                        */
/*==============================================================*/
create table notificaciones (
   codnotificacion      SERIAL               not null,
   titulo               varchar(200)         null,
   mensaje              varchar(600)         null,
   icono                varchar(200)         null,
   url                  varchar(300)         null,
   tipo                 varchar(50)          null,
   fecha                timestamp            null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint notificaciones_pkey primary key (codnotificacion)
);

-- set table ownership
alter table notificaciones owner to user_abccompetencias
;
/*==============================================================*/
/* Table: notificacionesusuarios                                */
/*==============================================================*/
create table notificacionesusuarios (
   codnotificacion      int4                 not null,
   codusuario           int4                 null,
   leida                int4                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint notificacionesusuarios_pkey primary key (codnotificacion)
);

-- set table ownership
alter table notificacionesusuarios owner to user_abccompetencias
;
/*==============================================================*/
/* Index: ak_key_1_notifica                                     */
/*==============================================================*/
create unique index ak_key_1_notifica on notificacionesusuarios using BTREE (
( codnotificacion )
);

/*==============================================================*/
/* Table: opciones                                              */
/*==============================================================*/
create table opciones (
   codopcion            SERIAL               not null,
   codpregunta          int4                 null,
   opcion               varchar(255)         null,
   correcta             bool                 null,
   updated_at           timestamp            null,
   created_at           timestamp            null,
   constraint opciones_pkey primary key (codopcion)
);

-- set table ownership
alter table opciones owner to user_abccompetencias
;
/*==============================================================*/
/* Table: paginasinicio                                         */
/*==============================================================*/
create table paginasinicio (
   codpaginainicio      SERIAL               not null,
   nombre               varchar(200)         null,
   nombrearchivo        varchar(200)         null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint paginasinicio_pkey primary key (codpaginainicio)
);

-- set table ownership
alter table paginasinicio owner to user_abccompetencias
;
/*==============================================================*/
/* Table: pagos_solicitudes                                     */
/*==============================================================*/
create table pagos_solicitudes (
   codpagosolicitud     SERIAL               not null,
   codsolicitud         int4                 not null,
   reference            varchar(255)         null,
   payu_order_id        varchar(255)         null,
   transaction_id       varchar(255)         null,
   state                varchar(50)          null,
   value                varchar(255)         null,
   user_id              int8                 null,
   respuesta            json                 null,
   franchise            varchar(3)           null,
   currency             varchar(10)          null,
   description          text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint PK_PAGOS_SOLICITUDES primary key (codpagosolicitud)
);

-- set table ownership
alter table pagos_solicitudes owner to user_abccompetencias
;
/*==============================================================*/
/* Table: parametros                                            */
/*==============================================================*/
create table parametros (
   codparametro         SERIAL               not null,
   codusuario           int4                 null,
   nombre_empresa       varchar(100)         null,
   nit                  varchar(50)          null,
   direccion            varchar(100)         null,
   telefono             varchar(10)          null,
   correo               varchar(50)          null,
   ip                   varchar(200)         null,
   conocimientos        numeric              null,
   desempeno            numeric              null,
   producto             numeric              null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint parametros_pkey primary key (codparametro)
);

-- set table ownership
alter table parametros owner to user_abccompetencias
;
/*==============================================================*/
/* Table: password_resets                                       */
/*==============================================================*/
create table password_resets (
   email                varchar(300)         null,
   token                varchar(500)         null,
   created_at           timestamp            null
);

-- set table ownership
alter table password_resets owner to user_abccompetencias
;
/*==============================================================*/
/* Index: index_email_pr                                        */
/*==============================================================*/
create  index index_email_pr on password_resets using BTREE (
( email )
);

/*==============================================================*/
/* Index: index_token_pr                                        */
/*==============================================================*/
create  index index_token_pr on password_resets using BTREE (
( token )
);

/*==============================================================*/
/* Table: perfiles                                              */
/*==============================================================*/
create table perfiles (
   codperfil            SERIAL               not null,
   codpaginainicio      int4                 not null,
   nombreperfil         varchar(200)         null,
   hinicio              time                 null,
   hfin                 time                 null,
   restriccion_ip       int2                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint perfiles_pkey primary key (codperfil)
);

-- set table ownership
alter table perfiles owner to user_abccompetencias
;
/*==============================================================*/
/* Table: preguntas                                             */
/*==============================================================*/
create table preguntas (
   codpregunta          SERIAL               not null,
   codactividad         int4                 null,
   pregunta             text                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint preguntas_pkey primary key (codpregunta)
);

-- set table ownership
alter table preguntas owner to user_abccompetencias
;
/*==============================================================*/
/* Table: preguntas_cuestionarios                               */
/*==============================================================*/
create table preguntas_cuestionarios (
   codpregunta_cuestionario SERIAL               not null,
   codcuestionario      int4                 null,
   codpregunta          int4                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint preguntas_cuestionarios_pkey primary key (codpregunta_cuestionario)
);

-- set table ownership
alter table preguntas_cuestionarios owner to user_abccompetencias
;
/*==============================================================*/
/* Table: relfuncperfil                                         */
/*==============================================================*/
create table relfuncperfil (
   codfunc              int4                 not null,
   codperfil            int4                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint relfuncperfil_pkey primary key (codfunc, codperfil)
);

-- set table ownership
alter table relfuncperfil owner to user_abccompetencias
;
/*==============================================================*/
/* Table: relfuncusuarios                                       */
/*==============================================================*/
create table relfuncusuarios (
   codusuario           int4                 not null,
   codfunc              int4                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint relfuncusuarios_pkey primary key (codusuario, codfunc)
);

-- set table ownership
alter table relfuncusuarios owner to user_abccompetencias
;
/*==============================================================*/
/* Table: respuestas                                            */
/*==============================================================*/
create table respuestas (
   codrespuesta         SERIAL               not null,
   codopcion            int4                 not null,
   codpregunta_cuestionario int4                 null,
   codcuestionario_solicitud int4                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint respuestas_pkey primary key (codrespuesta)
);

-- set table ownership
alter table respuestas owner to user_abccompetencias
;
/*==============================================================*/
/* Table: sessions                                              */
/*==============================================================*/
create table sessions (
   id                   varchar(255)         null,
   user_id              int4                 null,
   ip_address           varchar(45)          null,
   user_agent           text                 null,
   payload              text                 null,
   last_activity        int4                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null
);

-- set table ownership
alter table sessions owner to user_abccompetencias
;
/*==============================================================*/
/* Table: solicitudes                                           */
/*==============================================================*/
create table solicitudes (
   codsolicitud         SERIAL               not null,
   codnorma             int4                 null,
   codusuario           int4                 not null,
   codtitulacion        int4                 null,
   fecha                date                 not null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint solicitudes_pkey primary key (codsolicitud)
);

-- set table ownership
alter table solicitudes owner to user_abccompetencias
;
/*==============================================================*/
/* Table: tipos_titulacion                                      */
/*==============================================================*/
create table tipos_titulacion (
   codtipo_titulacion   SERIAL               not null,
   nombre               varchar(255)         null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint tipos_titulacion_pkey primary key (codtipo_titulacion)
);

-- set table ownership
alter table tipos_titulacion owner to user_abccompetencias
;
/*==============================================================*/
/* Table: titulaciones                                          */
/*==============================================================*/
create table titulaciones (
   codtitulacion        SERIAL               not null,
   nombre               text                 null,
   descripcion          text                 null,
   codigo               varchar(50)          null,
   archivo              varchar(255)         null,
   estado               int2                 not null default 1,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint titulaciones_pkey primary key (codtitulacion)
);

-- set table ownership
alter table titulaciones owner to user_abccompetencias
;
/*==============================================================*/
/* Table: ubicaciones                                           */
/*==============================================================*/
create table ubicaciones (
   codubicacion         SERIAL               not null,
   codpadre             int4                 null,
   identificador        varchar(30)          null,
   nombre               varchar(200)         null,
   tipo                 varchar(10)          null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint ubicaciones_pkey primary key (codubicacion),
   constraint ubicaciones_identificador_key unique (identificador)
);

-- set table ownership
alter table ubicaciones owner to user_abccompetencias
;
/*==============================================================*/
/* Table: usuarios                                              */
/*==============================================================*/
create table usuarios (
   codusuario           SERIAL               not null,
   codperfil            int4                 null,
   codubicacion         int4                 null,
   codubicacionnacimiento int4                 null,
   nombres              varchar(100)         null,
   primerapellido       varchar(100)         null,
   segundoapellido      varchar(100)         null,
   email                varchar(200)         null,
   direccion            varchar(100)         null,
   telefono             varchar(30)          null,
   celular              varchar(30)          null,
   password             varchar(500)         null,
   imagencodificada     varchar(200)         null,
   mime                 varchar(200)         null,
   tamanno              varchar(200)         null,
   nombreimagen         varchar(200)         null,
   estado               int2                 null default 1,
   tipodoc              varchar(3)           null,
   documento            varchar(30)          null,
   remember_token       varchar(100)         null,
   fechanacimiento      date                 null,
   genero               int2                 null default 1,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint usuarios_pkey primary key (codusuario)
);

-- set table ownership
alter table usuarios owner to user_abccompetencias
;
/*==============================================================*/
/* Index: ak_usuarios_documento_usuarios                        */
/*==============================================================*/
create unique index ak_usuarios_documento_usuarios on usuarios using BTREE (
( documento )
);

/*==============================================================*/
/* Table: valoracion_indicadores                                */
/*==============================================================*/
create table valoracion_indicadores (
   codvaloracionindicador SERIAL               not null,
   codindicadorevaluacion int4                 null,
   observacion          text                 null,
   cumple               bool                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint valoracion_indicadores_pkey primary key (codvaloracionindicador)
);

-- set table ownership
alter table valoracion_indicadores owner to user_abccompetencias
;
/*==============================================================*/
/* Table: valoraciones                                          */
/*==============================================================*/
create table valoraciones (
   codvaloracion        SERIAL               not null,
   codsolicitud         int4                 null,
   cuestionario1        numeric              null,
   cuestionario2        numeric              null,
   producto             numeric              null,
   desempeno            numeric              null,
   aprobacion           bool                 null,
   created_at           timestamp            null,
   updated_at           timestamp            null,
   constraint valoraciones_pkey primary key (codvaloracion)
);

comment on column valoraciones.cuestionario1 is
'porcentaje de primer cuestionario';

comment on column valoraciones.cuestionario2 is
'porcentaje segunda oportunidad cuestionario';

comment on column valoraciones.producto is
'porcentaje de porducto';

comment on column valoraciones.desempeno is
'porcentaje de desempeÃ±o';

-- set table ownership
alter table valoraciones owner to user_abccompetencias
;
alter table accesos
   add constraint accesos_codusuario_fkey foreign key (codusuario)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table actividades
   add constraint fk_actividades_conocimiento foreign key (codconocimiento)
      references conocimientos (codconocimiento)
      on delete restrict on update restrict;

alter table agendadas
   add constraint fk_agendadas_evaluador foreign key (codevaluador)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table agendadas
   add constraint fk_solicitud_agendada foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table certificados
   add constraint fk_certificados_solicitud foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table conocimientos
   add constraint fk_conocimientos_norma foreign key (codnorma)
      references normas (codnorma)
      on delete restrict on update restrict;

alter table cuestionarios_solicitud
   add constraint fk_cuessol_evalprod foreign key (codevaluacion_producto)
      references evaluaciones (codevaluacion)
      on delete restrict on update restrict;

alter table cuestionarios_solicitud
   add constraint fk_cuest_sol_evaluaciones foreign key (codevaluacion_desempeno)
      references evaluaciones (codevaluacion)
      on delete restrict on update restrict;

alter table cuestionarios_solicitud
   add constraint fk_cuest_solic_cuestionario foreign key (codcuestionario)
      references cuestionarios (codcuestionario)
      on delete restrict on update restrict;

alter table cuestionarios_solicitud
   add constraint fk_cuest_solic_solicitud foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table etapas_solicitudes
   add constraint estapas_solicitudes_fk foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table evaluaciones_solicitudes
   add constraint fk_eval_sol_evaluaciones foreign key (codevaluacion)
      references evaluaciones (codevaluacion)
      on delete restrict on update restrict;

alter table evaluaciones_solicitudes
   add constraint fk_eval_solicit_evaluador foreign key (codevaluador)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table evaluaciones_solicitudes
   add constraint fk_evaluaciones_sol_solicitudes foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table funcionalidades
   add constraint fk_func_func_padre foreign key (codpadre)
      references funcionalidades (codfunc)
      on delete restrict on update restrict;

alter table indicadores
   add constraint fk_indicadores_norma foreign key (codnorma)
      references normas (codnorma)
      on delete restrict on update restrict;

alter table indicadores_evaluacion
   add constraint fk_indica_eval_evaluacion_sol foreign key (codevaluacionsolicitud)
      references evaluaciones_solicitudes (codevaluacionsolicitud)
      on delete restrict on update restrict;

alter table indicadores_evaluacion
   add constraint fk_indicadores_eval_indicadores foreign key (codindicador)
      references indicadores (codindicador)
      on delete restrict on update restrict;

alter table normas
   add constraint fk_normas_tipo_titulacion foreign key (codtipo_titulacion)
      references tipos_titulacion (codtipo_titulacion)
      on delete restrict on update restrict;

alter table notificacionesusuarios
   add constraint fk_notific_usu_notificacion foreign key (codnotificacion)
      references notificaciones (codnotificacion)
      on delete restrict on update restrict;

alter table notificacionesusuarios
   add constraint fk_notificaciones_usu_usuario foreign key (codusuario)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table opciones
   add constraint fk_opciones_pregunta foreign key (codpregunta)
      references preguntas (codpregunta)
      on delete restrict on update restrict;

alter table pagos_solicitudes
   add constraint fk_pagos_solic_solicitud foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

alter table parametros
   add constraint fk_parametros_usuario foreign key (codusuario)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table perfiles
   add constraint perfiles_codpaginainicio_fkey foreign key (codpaginainicio)
      references paginasinicio (codpaginainicio)
      on delete restrict on update restrict;

alter table preguntas
   add constraint fk_preguntas_actividad foreign key (codactividad)
      references actividades (codactividad)
      on delete restrict on update restrict;

alter table preguntas_cuestionarios
   add constraint fk_preguntas_cuest_cuestionario foreign key (codcuestionario)
      references cuestionarios (codcuestionario)
      on delete restrict on update restrict;

alter table preguntas_cuestionarios
   add constraint fk_preguntas_cuest_pregunta foreign key (codpregunta)
      references preguntas (codpregunta)
      on delete restrict on update restrict;

alter table relfuncperfil
   add constraint fk_relfuncperfil_funcionalidad foreign key (codfunc)
      references funcionalidades (codfunc)
      on delete restrict on update restrict;

alter table relfuncperfil
   add constraint fk_relfuncperfil_perfil foreign key (codperfil)
      references perfiles (codperfil)
      on delete restrict on update restrict;

alter table relfuncusuarios
   add constraint fk_relfuncusuarios_func foreign key (codfunc)
      references funcionalidades (codfunc)
      on delete restrict on update restrict;

alter table relfuncusuarios
   add constraint fk_relfuncusuarios_usuario foreign key (codusuario)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table respuestas
   add constraint fk_resp_cuestionarios_solicit foreign key (codcuestionario_solicitud)
      references cuestionarios_solicitud (codcuestionario_solicitud)
      on delete restrict on update restrict;

alter table respuestas
   add constraint fk_resp_preguntas_cuest foreign key (codpregunta_cuestionario)
      references preguntas_cuestionarios (codpregunta_cuestionario)
      on delete restrict on update restrict;

alter table respuestas
   add constraint fk_respuestas_opcion foreign key (codopcion)
      references opciones (codopcion)
      on delete restrict on update restrict;

alter table solicitudes
   add constraint fk_solicitudes_norma foreign key (codnorma)
      references normas (codnorma)
      on delete restrict on update restrict;

alter table solicitudes
   add constraint fk_solicitudes_titulacion foreign key (codtitulacion)
      references titulaciones (codtitulacion)
      on delete restrict on update restrict;

alter table solicitudes
   add constraint fk_solicitudes_usuario foreign key (codusuario)
      references usuarios (codusuario)
      on delete restrict on update restrict;

alter table ubicaciones
   add constraint fk_ubicaciones_ubicacion_padre foreign key (codpadre)
      references ubicaciones (codubicacion)
      on delete restrict on update restrict;

alter table usuarios
   add constraint fk_usuarios_perfil foreign key (codperfil)
      references perfiles (codperfil)
      on delete restrict on update restrict;

alter table usuarios
   add constraint fk_usuarios_ubicacion foreign key (codubicacion)
      references ubicaciones (codubicacion)
      on delete restrict on update restrict;

alter table usuarios
   add constraint fk_usuarios_ubicacion_nacim foreign key (codubicacionnacimiento)
      references ubicaciones (codubicacion)
      on delete restrict on update restrict;

alter table valoracion_indicadores
   add constraint fk_valor_indica_indicador_eval foreign key (codindicadorevaluacion)
      references indicadores_evaluacion (codindicadorevaluacion)
      on delete restrict on update restrict;

alter table valoraciones
   add constraint fk_valoraciones_solicitud foreign key (codsolicitud)
      references solicitudes (codsolicitud)
      on delete restrict on update restrict;

