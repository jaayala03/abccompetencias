-- paginas de incio
INSERT INTO "paginasinicio"
    ("codpaginainicio", "nombre", "nombrearchivo", "created_at", "updated_at")
VALUES
    ('1', 'Admin', 'admin', NULL, NULL);
INSERT INTO "paginasinicio"
    ("codpaginainicio", "nombre", "nombrearchivo", "created_at", "updated_at")
VALUES
    ('2', 'En construccion', 'enconstruccion', NULL, NULL);


-- evaluaciones
INSERT INTO "evaluaciones"
    ("codevaluacion", "nombre", "created_at", "updated_at", "descripcion")
VALUES
    ('1', 'Desempeño', NULL, NULL, 'Lista de checkeo');
INSERT INTO "evaluaciones"
    ("codevaluacion", "nombre", "created_at", "updated_at", "descripcion")
VALUES
    ('2', 'Producto', NULL, NULL, 'Lista de verificación');


-- perfiles
INSERT INTO "perfiles"
    ("codperfil", "codpaginainicio", "nombreperfil", "created_at", "updated_at", "hinicio", "hfin", "restriccion_ip")
VALUES
    ('1', '2', 'Super Administrador', NULL, NULL, NULL, NULL, '2');
INSERT INTO "perfiles"
    ("codperfil", "codpaginainicio", "nombreperfil", "created_at", "updated_at", "hinicio", "hfin", "restriccion_ip")
VALUES
    ('2', '2', 'Persona', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "perfiles"
    ("codperfil", "codpaginainicio", "nombreperfil", "created_at", "updated_at", "hinicio", "hfin", "restriccion_ip")
VALUES
    ('3', '2', 'Empresa', NULL, NULL, NULL, NULL, NULL);
INSERT INTO "perfiles"
    ("codperfil", "codpaginainicio", "nombreperfil", "created_at", "updated_at", "hinicio", "hfin", "restriccion_ip")
VALUES
    ('4', '2', 'Evaluador', NULL, NULL, NULL, NULL, NULL);


-- funcionalidades (menus y permisos)
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('1', NULL, 'MENU_SYSTEM', 'SYS', '1', 'URLPAGES', '_parent', 'URLPAGES', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('2', '1', 'Seguridad', 'AUTH', '1', 'URLPAGES', '_parent', 'fa fa-user', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('3', '2', 'Sistema', 'GENERAL', '1', 'URLPAGES', '_parent', 'fa fa-bank', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('4', '3', 'Usuarios', 'MNTO_USU', '1', '/krauff/usuarios', '_parent', 'fa fa-user', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('5', '4', 'Listar', 'MNTO_USU_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('6', '4', 'Crear', 'MNTO_USU_ADD', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('7', '4', 'Editar', 'MNTO_USU_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('8', '4', 'Eliminar', 'MNTO_USU_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('9', '4', 'Asignar funcionalidades', 'MNTO_USU_FUNC', '5', 'URLPAGES', '_parent', 'fa fa-users', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('10', '3', 'Perfiles', 'MNTO_PER', '2', '/krauff/perfiles', '_parent', 'fa fa-user', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('11', '10', 'Listar', 'MNTO_PER_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('12', '10', 'Crear', 'MNTO_PER_ADD', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('13', '10', 'Editar', 'MNTO_PER_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('14', '10', 'Eliminar', 'MNTO_PER_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('15', '10', 'Asignar funcionalidades', 'MNTO_PER_FUNC', '5', 'URLPAGES', '_parent', 'fa fa-users', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('50', '1', 'Parametros', 'PARAM', '2', 'URLPAGES', '_parent', 'fa fa-cog', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('51', '50', 'Fachada', 'PARAM_FACH', '1', 'URLPAGES', '_parent', 'fa fa-square', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('52', '51', 'Imágenes Slider', 'PARAM_FACH_IMG', '1', '/outside/imagenes_fachada', '_parent', 'fa fa-image', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('53', '51', 'Listar', 'PARAM_FACH_IMG_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('54', '51', 'Crear', 'PARAM_FACH_IMG_CRE', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('55', '51', 'Editar', 'PARAM_FACH_IMG_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('56', '51', 'Eliminar', 'PARAM_FACH_IMG_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('57', '51', 'Noticias', 'PARAM_FACH_NOT', '1', '/outside/noticias/admin/listar', '_parent', 'fa fa-image', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('58', '57', 'Listar', 'PARAM_FACH_NOT_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('59', '57', 'Crear', 'PARAM_FACH_NOT_CRE', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('60', '57', 'Editar', 'PARAM_FACH_NOT_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('61', '57', 'Eliminar', 'PARAM_FACH_NOT_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('62', '50', 'Cuestionarios', 'PARAM_CUEST', '2', 'URLPAGES', '_parent', 'fa fa-square', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('63', '62', 'Preguntas', 'PARAM_CUEST_PREG', '2', '/parametros/preguntas', '_parent', 'fa fa-list-alt', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('64', '63', 'Listar', 'PARAM_CUEST_PREG_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('65', '63', 'Crear', 'PARAM_CUEST_PREG_CRE', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('66', '63', 'Editar', 'PARAM_CUEST_PREG_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('67', '63', 'Eliminar', 'PARAM_CUEST_PREG_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('68', '62', 'Cuestionarios', 'PARAM_CUEST_CUES', '2', '/parametros/cuestionarios', '_parent', 'fa fa-list-alt', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('69', '68', 'Listar', 'PARAM_CUEST_CUES_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('70', '68', 'Crear', 'PARAM_CUEST_CUES_CRE', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('71', '68', 'Editar', 'PARAM_CUEST_CUES_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('72', '68', 'Eliminar', 'PARAM_CUEST_CUES_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('73', '50', 'Normas', 'PARAM_NOR', '1', '/parametros/normas', '_parent', 'fa fa-image', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('74', '73', 'Listar', 'PARAM_NOR_LIST', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('75', '73', 'Crear', 'PARAM_NOR_CRE', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('76', '73', 'Editar', 'PARAM_NOR_EDI', '3', 'URLPAGES', '_parent', 'fa fa-pencil-square-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('77', '73', 'Eliminar', 'PARAM_NOR_ELI', '4', 'URLPAGES', '_parent', 'fa fa-trash-o', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('78', '50', 'Importar', 'PARAM_IMPOR', '1', '/parametros/importar', '_parent', 'fa fa-upload', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('100', '1', 'Solicitudes', 'SOLIC', '2', 'URLPAGES', '_parent', 'fa fa-check', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('101', '100', 'Registrar', 'SOLIC_REG', '1', '/solicitudes/registrar', '_parent', 'fa fa-plus', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('102', '100', 'Mis Solicitudes', 'SOLIC_MIS', '2', '/solicitudes/missolicitudes', '_parent', 'fa fa-envelope', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('103', '102', 'Listar', 'SOLIC_LIS', '1', 'URLPAGES', '_parent', 'fa fa-list-alt', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('104', '102', 'Eliminar', 'SOLIC_ELI', '2', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('105', '102', 'Cambiar Estado', 'SOLIC_CAES', '3', 'URLPAGES', '_parent', 'fa fa-exchange', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('106', '100', 'Todas', 'SOLIC_TODAS', '1', '/solicitudes/todas', '_parent', 'fa fa-list-alt', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('150', '1', 'Cuestionarios', 'CUEST', '2', 'URLPAGES', '_parent', 'fa fa-question', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('152', '150', 'Ver detalle resultados', 'CUEST_DET_CUES', '2', 'URLPAGES', '_parent', 'fa fa-eye', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('153', '150', 'Generar cuestionario secundario', 'CUEST_GEN_SEC', '3', 'URLPAGES', '_parent', 'fa fa-plus', 'PERMISO', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('200', '1', 'Reportes', 'REP', '5', 'URLPAGES', '_parent', 'fa fa-file-pdf-o', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('201', '200', 'Historial solicitudes', 'REP_HIST_SOL', '1', '/reportes/historialsolicitudes', '_parent', 'fa fa-file-pdf-o', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('202', '200', 'Cantidad Solicitudes', 'REP_CANT_SOL', '2', '/reportes/cantidadsolicitudes', '_parent', 'fa fa-file-pdf-o', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('203', '200', 'Pagos', 'REP_PAG', '3', '/reportes/pagos', '_parent', 'fa fa-file-pdf-o', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('204', '200', 'Calificaciones', 'REP_CALIF', '4', '/reportes/calificaciones', '_parent', 'fa fa-file-pdf-o', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('1400', '1', 'Auditoria', 'AUDIT', '11', 'URLPAGES', '_parent', 'fa fa-history', 'MENU', NULL, NULL);
INSERT INTO "funcionalidades"
    ("codfunc", "codpadre", "nombre", "identificador", "orden", "urlpagina", "target", "icono", "tipo", "created_at", "updated_at")
VALUES
    ('1407', '1400', 'Logs', 'AUDIT_SIS_LOGS', '2', 'URLPAGES', '_parent', 'fa fa-file-text-o', 'PERMISO', NULL, NULL);

--permisos del perfil super administrador para mostrar los menus y los permisos
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('1', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('2', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('3', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('4', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('5', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('6', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('7', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('8', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('9', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('10', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('11', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('12', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('13', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('14', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('15', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('50', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('51', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('52', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('53', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('54', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('55', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('56', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('57', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('58', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('59', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('60', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('61', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('62', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('63', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('64', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('65', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('66', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('67', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('68', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('69', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('70', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('71', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('72', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('73', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('74', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('75', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('76', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('77', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('78', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('100', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('101', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('102', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('103', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('104', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('105', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('106', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('150', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('152', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('153', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('200', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('201', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('202', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('203', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('204', '1', '2019-09-27 22:37:08', '2019-09-27 22:37:08');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('1400', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');
INSERT INTO "relfuncperfil"
    ("codfunc", "codperfil", "created_at", "updated_at")
VALUES
    ('1407', '1', '2019-09-27 22:37:07', '2019-09-27 22:37:07');

-- usuario super administrador (pass: 12345678)
INSERT INTO "public"."usuarios"
    ("codusuario", "codperfil", "codubicacion", "codubicacionnacimiento", "nombres", "primerapellido", "segundoapellido", "email", "direccion", "telefono", "celular", "password", "imagencodificada", "mime", "tamanno", "nombreimagen", "estado", "tipodoc", "documento", "remember_token", "fechanacimiento", "genero", "created_at", "updated_at")
VALUES
    ('1', '1', NULL, NULL, 'Administrador', 'ACB', 'Competencias', 'abccompetencias@gmail.com', NULL, NULL, NULL, '$2y$10$U8Yyf1CcWlFyEAH0Wd5D3uOW6iiZr3sHjMNIPhMSOeA8DeippN1ze', NULL, NULL, NULL, NULL, '1', '1', '10101010', 'GNJkbpk7zpCz7QS0tbyjJrv2HAzppJfVbdivc1ZSJ4dnJBFQG2ZJP5BcaUC2', '1994-02-18', '1', '2018-01-06 00:00:00', '2019-12-12 20:00:39');
