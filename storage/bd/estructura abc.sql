CREATE TABLE "accesos" (

"codacceso" serial,

"codusuario" int4,

"fechaingreso" date NOT NULL,

"horaingreso" time(6) NOT NULL,

"ipoculta" varchar(50) COLLATE "default",

"ipvisible" varchar(50) COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

"so" varchar(300) COLLATE "default",

"navegador" varchar(300) COLLATE "default",

CONSTRAINT "accesos_pkey" PRIMARY KEY ("codacceso") 

);



CREATE TABLE "actividades" (

"codactividad" serial,

"nombre" varchar(255) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codnorma" int4,

CONSTRAINT "actividades_pkey" PRIMARY KEY ("codactividad") 

);



CREATE TABLE "activity_log" (

"id" serial,

"log_name" varchar(255) COLLATE "default",

"description" varchar(255) COLLATE "default" NOT NULL,

"subject_id" int8,

"subject_type" varchar(255) COLLATE "default",

"causer_id" int8,

"causer_type" varchar(255) COLLATE "default",

"properties" text COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6)

);



CREATE INDEX "activity_log_log_name_index" ON "activity_log" ("log_name" ASC);

CREATE INDEX "index_log_name" ON "activity_log" ("log_name" ASC);



CREATE TABLE "agendadas" (

"codagendada" serial,

"fecha_agendada" date NOT NULL,

"hora" time(6) NOT NULL,

"codsolicitud" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codevaluador" int4,

CONSTRAINT "agendadas_pkey" PRIMARY KEY ("codagendada") 

);



CREATE TABLE "cache" (

"key" text COLLATE "default",

"value" text COLLATE "default",

"expiration" int4,

CONSTRAINT "cache_key_key" UNIQUE ("key")

);



CREATE TABLE "certificados" (

"codcertificado" serial,

"fecha" date,

"fecha_vencimiento" date,

"codsolicitud" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codigo" text COLLATE "default",

CONSTRAINT "certificados_pkey" PRIMARY KEY ("codcertificado") 

);



CREATE TABLE "cuestionarios" (

"codcuestionario" serial,

"nombre" varchar(255) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"segundos_x_pregunta" int8 NOT NULL,

"tipo" int8 DEFAULT 1,

"instrucciones" text COLLATE "default",

CONSTRAINT "cuestionarios_pkey" PRIMARY KEY ("codcuestionario") 

);



CREATE TABLE "cuestionarios_solicitud" (

"codcuestionario_solicitud" serial,

"codsolicitud" int4,

"codcuestionario" int4,

"terminado" bool DEFAULT false,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"hora_inicio" timestamp(6),

"hora_fin" timestamp(6),

"hora_fin_estimado" timestamp(6),

"codevaluacion_desempeno" int4,

"codevaluacion_producto" int4,

"aprobo" bool,

CONSTRAINT "cuestionarios_solicitud_pkey" PRIMARY KEY ("codcuestionario_solicitud") ,

CONSTRAINT "cuestionarios_solicitud_un" UNIQUE ("codsolicitud", "codcuestionario")

);



CREATE TABLE "etapas_solicitudes" (

"codetapasolicitud" serial,

"etapa" int4 NOT NULL,

"ultima" bool NOT NULL DEFAULT true,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codsolicitud" int8 NOT NULL,

CONSTRAINT "estapas_solicitudes_pk" PRIMARY KEY ("codetapasolicitud") 

);



CREATE TABLE "evaluaciones" (

"codevaluacion" serial,

"nombre" varchar(255) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"descripcion" text COLLATE "default",

CONSTRAINT "evaluaciones_pkey" PRIMARY KEY ("codevaluacion") 

);



CREATE TABLE "evaluaciones_solicitudes" (

"codevaluacionsolicitud" serial,

"codsolicitud" int4 NOT NULL,

"codevaluacion" int4 NOT NULL,

"aprobo" bool NOT NULL DEFAULT false,

"codevaluador" int4,

"fecha_aplicacion" date,

"fecha_resultados" date,

"lugar" text COLLATE "default",

"ciudad" text COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "evaluaciones_solicitudes_pkey" PRIMARY KEY ("codevaluacionsolicitud") 

);



CREATE TABLE "festivos" (

"codfestivo" serial,

"fecha" date NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "festivos_pkey" PRIMARY KEY ("codfestivo") ,

CONSTRAINT "festivos_fecha_key" UNIQUE ("fecha")

);



CREATE TABLE "funcionalidades" (

"codfunc" serial,

"codpadre" int4,

"nombre" varchar(100) COLLATE "default" NOT NULL,

"identificador" varchar(200) COLLATE "default" NOT NULL,

"orden" int4 NOT NULL,

"urlpagina" varchar(100) COLLATE "default",

"target" varchar(50) COLLATE "default" NOT NULL DEFAULT '_parent'::character varying,

"icono" varchar(100) COLLATE "default",

"tipo" varchar(10) COLLATE "default" NOT NULL DEFAULT 'text'::character varying,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "funcionalidades_pkey" PRIMARY KEY ("codfunc") ,

CONSTRAINT "funcionalidades_identificador_key" UNIQUE ("identificador")

);



CREATE TABLE "imagenes_fachada" (

"codimagenfachada" serial,

"nombre" varchar(255) COLLATE "default" NOT NULL,

"descripcion" text COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "imagenes_fachada_pkey" PRIMARY KEY ("codimagenfachada") ,

CONSTRAINT "uq_imagenes_fachada_nombre" UNIQUE ("nombre")

);



CREATE TABLE "indicadores" (

"codindicador" serial,

"nombre" text COLLATE "default" NOT NULL,

"codnorma" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"tipo" int4,

CONSTRAINT "indicadores_pkey" PRIMARY KEY ("codindicador") 

);



CREATE TABLE "indicadores_evaluacion" (

"codindicadorevaluacion" serial,

"codindicador" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codevaluacionsolicitud" int4,

CONSTRAINT "indicadores_evaluacion_pkey" PRIMARY KEY ("codindicadorevaluacion") 

);



CREATE TABLE "migrations" (

"id" serial,

"migration" varchar(255) COLLATE "default",

"batch" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "migrations_pkey" PRIMARY KEY ("id") 

);



CREATE TABLE "normas" (

"codnorma" serial,

"nombre" text COLLATE "default" NOT NULL,

"descripcion" text COLLATE "default",

"codigo" varchar(50) COLLATE "default" NOT NULL,

"archivo" varchar(255) COLLATE "default",

"estado" int2 NOT NULL DEFAULT 1,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codtipo_titulacion" int4,

"valor" numeric,

CONSTRAINT "normas_pkey" PRIMARY KEY ("codnorma") 

);



CREATE TABLE "noticias" (

"codnoticia" serial,

"titulo" varchar(100) COLLATE "default" NOT NULL,

"contenido" text COLLATE "default" NOT NULL,

"descripcion" text COLLATE "default",

"imagen" varchar(255) COLLATE "default",

"estado" int2 NOT NULL DEFAULT 1,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"slug" text COLLATE "default",

CONSTRAINT "noticias_pkey" PRIMARY KEY ("codnoticia") 

);



CREATE TABLE "notificaciones" (

"codnotificacion" serial,

"titulo" varchar(200) COLLATE "default" NOT NULL,

"mensaje" varchar(600) COLLATE "default" NOT NULL,

"icono" varchar(200) COLLATE "default" NOT NULL,

"url" varchar(300) COLLATE "default" NOT NULL,

"tipo" varchar(50) COLLATE "default" NOT NULL,

"fecha" timestamp(6) DEFAULT now(),

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "notificaciones_pkey" PRIMARY KEY ("codnotificacion") 

);



CREATE TABLE "notificacionesusuarios" (

"codnotificacion" int4 NOT NULL,

"codusuario" int4,

"leida" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "notificacionesusuarios_pkey" PRIMARY KEY ("codnotificacion") 

);



CREATE UNIQUE INDEX "ak_key_1_notifica" ON "notificacionesusuarios" ("codnotificacion" ASC);



CREATE TABLE "opciones" (

"codopcion" serial,

"opcion" varchar(255) COLLATE "default" NOT NULL,

"correcta" bool DEFAULT false,

"codpregunta" int4,

"updated_at" timestamp(6),

"created_at" timestamp(6),

CONSTRAINT "opciones_pkey" PRIMARY KEY ("codopcion") 

);



CREATE TABLE "paginasinicio" (

"codpaginainicio" serial,

"nombre" varchar(200) COLLATE "default",

"nombrearchivo" varchar(200) COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "paginasinicio_pkey" PRIMARY KEY ("codpaginainicio") 

);



CREATE TABLE "pagos_solicitudes" (

"codpagosolicitud" serial,

"reference" varchar(255) COLLATE "default" NOT NULL,

"payu_order_id" varchar(255) COLLATE "default",

"transaction_id" varchar(255) COLLATE "default",

"state" varchar(50) COLLATE "default",

"value" varchar(255) COLLATE "default",

"user_id" int8,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codsolicitud" int8 NOT NULL,

"respuesta" json,

"franchise" varchar(3) COLLATE "default",

"currency" varchar(10) COLLATE "default",

"description" text COLLATE "default"

);



CREATE TABLE "parametros" (

"codparametro" serial,

"codusuario" int4,

"nombre_empresa" varchar(100) COLLATE "default" NOT NULL,

"nit" varchar(50) COLLATE "default" NOT NULL,

"direccion" varchar(100) COLLATE "default",

"telefono" varchar(10) COLLATE "default",

"correo" varchar(50) COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

"ip" varchar(200) COLLATE "default",

"conocimientos" numeric,

"desempeno" numeric,

"producto" numeric,

CONSTRAINT "parametros_pkey" PRIMARY KEY ("codparametro") 

);



CREATE TABLE "password_resets" (

"email" varchar(300) COLLATE "default" NOT NULL,

"token" varchar(500) COLLATE "default" NOT NULL,

"created_at" timestamp(6)

);



CREATE INDEX "index_email_pr" ON "password_resets" ("email" ASC);

CREATE INDEX "index_token_pr" ON "password_resets" ("token" ASC);



CREATE TABLE "perfiles" (

"codperfil" serial,

"codpaginainicio" int4 NOT NULL,

"nombreperfil" varchar(200) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"hinicio" time(6),

"hfin" time(6),

"restriccion_ip" int2,

CONSTRAINT "perfiles_pkey" PRIMARY KEY ("codperfil") 

);



CREATE TABLE "preguntas" (

"codpregunta" serial,

"pregunta" text COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codactividad" int4,

CONSTRAINT "preguntas_pkey" PRIMARY KEY ("codpregunta") 

);



CREATE TABLE "preguntas_cuestionarios" (

"codpregunta_cuestionario" serial,

"codcuestionario" int4,

"codpregunta" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "preguntas_cuestionarios_pkey" PRIMARY KEY ("codpregunta_cuestionario") 

);



CREATE TABLE "relfuncperfil" (

"codfunc" int4 NOT NULL,

"codperfil" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "relfuncperfil_pkey" PRIMARY KEY ("codfunc", "codperfil") 

);



CREATE TABLE "relfuncusuarios" (

"codusuario" int4 NOT NULL,

"codfunc" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "relfuncusuarios_pkey" PRIMARY KEY ("codusuario", "codfunc") 

);



CREATE TABLE "respuestas" (

"codrespuesta" serial,

"codopcion" int4 NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codpregunta_cuestionario" int4,

"codcuestionario_solicitud" int8 NOT NULL,

CONSTRAINT "respuestas_pkey" PRIMARY KEY ("codrespuesta") 

);



CREATE TABLE "sessions" (

"id" varchar(255) COLLATE "default",

"user_id" int4,

"ip_address" varchar(45) COLLATE "default",

"user_agent" text COLLATE "default",

"payload" text COLLATE "default",

"last_activity" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6)

);



CREATE TABLE "solicitudes" (

"codsolicitud" serial,

"codusuario" int4 NOT NULL,

"codtitulacion" int4,

"fecha" date NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

"codnorma" int4,

CONSTRAINT "solicitudes_pkey" PRIMARY KEY ("codsolicitud") 

);



CREATE TABLE "tipos_titulacion" (

"codtipo_titulacion" serial,

"nombre" varchar(255) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "tipos_titulacion_pkey" PRIMARY KEY ("codtipo_titulacion") 

);



CREATE TABLE "titulaciones" (

"codtitulacion" serial,

"nombre" text COLLATE "default" NOT NULL,

"descripcion" text COLLATE "default",

"codigo" varchar(50) COLLATE "default" NOT NULL,

"archivo" varchar(255) COLLATE "default",

"estado" int2 NOT NULL DEFAULT 1,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "titulaciones_pkey" PRIMARY KEY ("codtitulacion") 

);



CREATE TABLE "ubicaciones" (

"codubicacion" serial,

"codpadre" int4,

"identificador" varchar(30) COLLATE "default",

"nombre" varchar(200) COLLATE "default" NOT NULL,

"tipo" varchar(10) COLLATE "default" NOT NULL,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "ubicaciones_pkey" PRIMARY KEY ("codubicacion") ,

CONSTRAINT "ubicaciones_identificador_key" UNIQUE ("identificador")

);



CREATE TABLE "usuarios" (

"codusuario" serial,

"codperfil" int4,

"codubicacion" int4,

"codubicacionnacimiento" int4,

"nombres" varchar(100) COLLATE "default",

"primerapellido" varchar(100) COLLATE "default",

"segundoapellido" varchar(100) COLLATE "default",

"email" varchar(200) COLLATE "default" NOT NULL,

"direccion" varchar(100) COLLATE "default",

"telefono" varchar(30) COLLATE "default",

"celular" varchar(30) COLLATE "default",

"password" varchar(500) COLLATE "default" NOT NULL,

"imagencodificada" varchar(200) COLLATE "default",

"mime" varchar(200) COLLATE "default",

"tamanno" varchar(200) COLLATE "default",

"nombreimagen" varchar(200) COLLATE "default",

"estado" int2 DEFAULT 1,

"tipodoc" varchar(3) COLLATE "default" DEFAULT 'CC'::character varying,

"documento" varchar(30) COLLATE "default" NOT NULL,

"remember_token" varchar(100) COLLATE "default",

"fechanacimiento" date,

"genero" int2 DEFAULT 1,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "usuarios_pkey" PRIMARY KEY ("codusuario") 

);



CREATE UNIQUE INDEX "ak_usuarios_documento_usuarios" ON "usuarios" ("documento" ASC);



CREATE TABLE "valoracion_indicadores" (

"codvaloracionindicador" serial,

"codindicadorevaluacion" int4,

"observacion" text COLLATE "default",

"created_at" timestamp(6),

"updated_at" timestamp(6),

"cumple" bool DEFAULT false,

CONSTRAINT "valoracion_indicadores_pkey" PRIMARY KEY ("codvaloracionindicador") 

);



CREATE TABLE "valoraciones" (

"codvaloracion" serial,

"cuestionario1" numeric,

"cuestionario2" numeric,

"producto" numeric,

"desempeno" numeric,

"aprobacion" bool,

"codsolicitud" int4,

"created_at" timestamp(6),

"updated_at" timestamp(6),

CONSTRAINT "valoraciones_pkey" PRIMARY KEY ("codvaloracion") 

);



COMMENT ON COLUMN "valoraciones"."cuestionario1" IS 'porcentaje de primer cuestionario';

COMMENT ON COLUMN "valoraciones"."cuestionario2" IS 'porcentaje segunda oportunidad cuestionario';

COMMENT ON COLUMN "valoraciones"."producto" IS 'porcentaje de porducto';

COMMENT ON COLUMN "valoraciones"."desempeno" IS 'porcentaje de desempeño';





ALTER TABLE "accesos" ADD CONSTRAINT "accesos_codusuario_fkey" FOREIGN KEY ("codusuario") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "actividades" ADD CONSTRAINT "fk_act_norm" FOREIGN KEY ("codnorma") REFERENCES "normas" ("codnorma");

ALTER TABLE "agendadas" ADD CONSTRAINT "fk_solicitud_agendada" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "agendadas" ADD CONSTRAINT "fk_agendadas_evaluador" FOREIGN KEY ("codevaluador") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "certificados" ADD CONSTRAINT "fk_cert_solici" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "cuestionarios_solicitud" ADD CONSTRAINT "fk_solic_cuest" FOREIGN KEY ("codcuestionario") REFERENCES "cuestionarios" ("codcuestionario");

ALTER TABLE "cuestionarios_solicitud" ADD CONSTRAINT "fk_soli_solic" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "cuestionarios_solicitud" ADD CONSTRAINT "fk_cuessol_evalprod" FOREIGN KEY ("codevaluacion_producto") REFERENCES "evaluaciones" ("codevaluacion");

ALTER TABLE "cuestionarios_solicitud" ADD CONSTRAINT "fk_cuessol_evaldes" FOREIGN KEY ("codevaluacion_desempeno") REFERENCES "evaluaciones" ("codevaluacion");

ALTER TABLE "etapas_solicitudes" ADD CONSTRAINT "estapas_solicitudes_fk" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "evaluaciones_solicitudes" ADD CONSTRAINT "fk_evasol_eva" FOREIGN KEY ("codevaluacion") REFERENCES "evaluaciones" ("codevaluacion");

ALTER TABLE "evaluaciones_solicitudes" ADD CONSTRAINT "fk_evasol_sol" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "evaluaciones_solicitudes" ADD CONSTRAINT "fk_evaluaciones_solicitudes_evaluador" FOREIGN KEY ("codevaluador") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "funcionalidades" ADD CONSTRAINT "funcionalidades_codpadre_fkey" FOREIGN KEY ("codpadre") REFERENCES "funcionalidades" ("codfunc");

ALTER TABLE "indicadores" ADD CONSTRAINT "fk_indica_norma" FOREIGN KEY ("codnorma") REFERENCES "normas" ("codnorma");

ALTER TABLE "indicadores_evaluacion" ADD CONSTRAINT "fk_indeva_indicadores" FOREIGN KEY ("codindicador") REFERENCES "indicadores" ("codindicador");

ALTER TABLE "indicadores_evaluacion" ADD CONSTRAINT "fk_indeva_evaluacion" FOREIGN KEY ("codevaluacionsolicitud") REFERENCES "evaluaciones_solicitudes" ("codevaluacionsolicitud");

ALTER TABLE "normas" ADD CONSTRAINT "pk_tip_titu_normas" FOREIGN KEY ("codtipo_titulacion") REFERENCES "tipos_titulacion" ("codtipo_titulacion");

ALTER TABLE "notificacionesusuarios" ADD CONSTRAINT "notificacionesusuarios_codusuario_fkey" FOREIGN KEY ("codusuario") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "notificacionesusuarios" ADD CONSTRAINT "notificacionesusuarios_codnotificacion_fkey" FOREIGN KEY ("codnotificacion") REFERENCES "notificaciones" ("codnotificacion");

ALTER TABLE "opciones" ADD CONSTRAINT "fk_opciones_pregunta" FOREIGN KEY ("codpregunta") REFERENCES "preguntas" ("codpregunta");

ALTER TABLE "pagos_solicitudes" ADD CONSTRAINT "pagos_solicitudes_fk" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE "parametros" ADD CONSTRAINT "parametros_codusuario_fkey" FOREIGN KEY ("codusuario") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "perfiles" ADD CONSTRAINT "perfiles_codpaginainicio_fkey" FOREIGN KEY ("codpaginainicio") REFERENCES "paginasinicio" ("codpaginainicio");

ALTER TABLE "preguntas" ADD CONSTRAINT "fk_preg_act" FOREIGN KEY ("codactividad") REFERENCES "actividades" ("codactividad");

ALTER TABLE "preguntas_cuestionarios" ADD CONSTRAINT "fk_preg_cuest" FOREIGN KEY ("codcuestionario") REFERENCES "cuestionarios" ("codcuestionario");

ALTER TABLE "preguntas_cuestionarios" ADD CONSTRAINT "fk_preg_preg" FOREIGN KEY ("codpregunta") REFERENCES "preguntas" ("codpregunta");

ALTER TABLE "relfuncperfil" ADD CONSTRAINT "relfuncperfil_codperfil_fkey" FOREIGN KEY ("codperfil") REFERENCES "perfiles" ("codperfil");

ALTER TABLE "relfuncperfil" ADD CONSTRAINT "relfuncperfil_codfunc_fkey" FOREIGN KEY ("codfunc") REFERENCES "funcionalidades" ("codfunc");

ALTER TABLE "relfuncusuarios" ADD CONSTRAINT "relfuncusuarios_codfunc_fkey" FOREIGN KEY ("codfunc") REFERENCES "funcionalidades" ("codfunc");

ALTER TABLE "relfuncusuarios" ADD CONSTRAINT "relfuncusuarios_codusuario_fkey" FOREIGN KEY ("codusuario") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "respuestas" ADD CONSTRAINT "fk_respuestas_preguntas_cuestionarios" FOREIGN KEY ("codpregunta_cuestionario") REFERENCES "preguntas_cuestionarios" ("codpregunta_cuestionario");

ALTER TABLE "respuestas" ADD CONSTRAINT "fk_respuestas_cuestionarios_solicitud" FOREIGN KEY ("codcuestionario_solicitud") REFERENCES "cuestionarios_solicitud" ("codcuestionario_solicitud");

ALTER TABLE "respuestas" ADD CONSTRAINT "fk_respuestas_opcion" FOREIGN KEY ("codopcion") REFERENCES "opciones" ("codopcion");

ALTER TABLE "solicitudes" ADD CONSTRAINT "rel_soli_usu" FOREIGN KEY ("codusuario") REFERENCES "usuarios" ("codusuario");

ALTER TABLE "solicitudes" ADD CONSTRAINT "rel_soli_norm" FOREIGN KEY ("codnorma") REFERENCES "normas" ("codnorma");

ALTER TABLE "solicitudes" ADD CONSTRAINT "rel_soli_titu" FOREIGN KEY ("codtitulacion") REFERENCES "titulaciones" ("codtitulacion");

ALTER TABLE "ubicaciones" ADD CONSTRAINT "ubicaciones_codpadre_fkey" FOREIGN KEY ("codpadre") REFERENCES "ubicaciones" ("codubicacion");

ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_codubicacion_fkey" FOREIGN KEY ("codubicacion") REFERENCES "ubicaciones" ("codubicacion");

ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_codubicacionnacimiento_fkey" FOREIGN KEY ("codubicacionnacimiento") REFERENCES "ubicaciones" ("codubicacion");

ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_codperfil_fkey" FOREIGN KEY ("codperfil") REFERENCES "perfiles" ("codperfil");

ALTER TABLE "valoracion_indicadores" ADD CONSTRAINT "fk_valorindica_indicaeval" FOREIGN KEY ("codindicadorevaluacion") REFERENCES "indicadores_evaluacion" ("codindicadorevaluacion");

ALTER TABLE "valoraciones" ADD CONSTRAINT "fk_valora_solici" FOREIGN KEY ("codsolicitud") REFERENCES "solicitudes" ("codsolicitud");

ALTER TABLE accesos OWNER TO user_abccompetencias;
ALTER TABLE actividades OWNER TO user_abccompetencias;
ALTER TABLE activity_log OWNER TO user_abccompetencias;
ALTER TABLE agendadas OWNER TO user_abccompetencias;
ALTER TABLE cache OWNER TO user_abccompetencias;
ALTER TABLE certificados OWNER TO user_abccompetencias;
ALTER TABLE cuestionarios OWNER TO user_abccompetencias;
ALTER TABLE cuestionarios_solicitud OWNER TO user_abccompetencias;
ALTER TABLE etapas_solicitudes OWNER TO user_abccompetencias;
ALTER TABLE evaluaciones OWNER TO user_abccompetencias;
ALTER TABLE evaluaciones_solicitudes OWNER TO user_abccompetencias;
ALTER TABLE festivos OWNER TO user_abccompetencias;
ALTER TABLE funcionalidades OWNER TO user_abccompetencias;
ALTER TABLE imagenes_fachada OWNER TO user_abccompetencias;
ALTER TABLE indicadores OWNER TO user_abccompetencias;
ALTER TABLE indicadores_evaluacion OWNER TO user_abccompetencias;
ALTER TABLE migrations OWNER TO user_abccompetencias;
ALTER TABLE normas OWNER TO user_abccompetencias;
ALTER TABLE noticias OWNER TO user_abccompetencias;
ALTER TABLE notificaciones OWNER TO user_abccompetencias;
ALTER TABLE notificacionesusuarios OWNER TO user_abccompetencias;
ALTER TABLE opciones OWNER TO user_abccompetencias;
ALTER TABLE paginasinicio OWNER TO user_abccompetencias;
ALTER TABLE pagos_solicitudes OWNER TO user_abccompetencias;
ALTER TABLE parametros OWNER TO user_abccompetencias;
ALTER TABLE password_resets OWNER TO user_abccompetencias;
ALTER TABLE perfiles OWNER TO user_abccompetencias;
ALTER TABLE preguntas OWNER TO user_abccompetencias;
ALTER TABLE preguntas_cuestionarios OWNER TO user_abccompetencias;
ALTER TABLE relfuncperfil OWNER TO user_abccompetencias;
ALTER TABLE relfuncusuarios OWNER TO user_abccompetencias;
ALTER TABLE respuestas OWNER TO user_abccompetencias;
ALTER TABLE sessions OWNER TO user_abccompetencias;
ALTER TABLE solicitudes OWNER TO user_abccompetencias;
ALTER TABLE tipos_titulacion OWNER TO user_abccompetencias;
ALTER TABLE titulaciones OWNER TO user_abccompetencias;
ALTER TABLE ubicaciones OWNER TO user_abccompetencias;
ALTER TABLE usuarios OWNER TO user_abccompetencias;
ALTER TABLE valoracion_indicadores OWNER TO user_abccompetencias;
ALTER TABLE valoraciones OWNER TO user_abccompetencias;



