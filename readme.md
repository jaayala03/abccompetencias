## Ejecutar

**Crear base de datos:**
```
CREATE DATABASE bd_abccompetencias;
CREATE USER user_abccompetencias WITH CREATEDB CREATEROLE PASSWORD 'abccompetencias';
GRANT ALL PRIVILEGES ON DATABASE bd_abccompetencias to user_abccompetencias WITH GRANT OPTION;
```

**Adjuntar la estructura de la bd:**
```
Ubicada en el directorio storage/bd/01 - estructura_abccompetencias.sql
```

**Adjuntar los datos iniciales de la bd:**
```
Ubicada en el directorio storage/bd/02 - datos_abccompetencias.sql
```

**Crear el archivo .env:**
```
APP_NAME=abccompetencias
APP_ENV=local
APP_KEY=base64:bTQzk5gTsF7ioCBMyOnrv8/Qr0G67EjjHIXAPpQRgKc=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://abccompetencias.test
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=bd_abccompetencias
DB_USERNAME=user_abccompetencias
DB_PASSWORD=abccompetencias
BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=database
QUEUE_DRIVER=sync
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=abccompetencias.contacto@gmail.com
MAIL_PASSWORD=abccompetencias2019
MAIL_ENCRYPTION=ssl
MAIL_FROM_NAME=abccompetencias
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=0


PAYU_TESTING_ENV=true
PAYU_URL=https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/
PAYU_API_KEY=4Vj8eK4rloUd272L48hsrarnUA
PAYU_API_LOGIN=pRRXKOl8ikMmt9u
PAYU_MERCHANT_ID=508029
PAYU_ACCOUNT_ID=512321
PAYU_COUNTRY=CO
PSE_REDIRECT_URL=http://abccompetencias.test

RECAPTCHAV3_SITEKEY=6LcZqKwUAAAAADEe1PKsdI4UT7211-IFFKt69fPu
RECAPTCHAV3_SECRET=6LcZqKwUAAAAAOERG2Ye7y4SbpD54d_YamIB9Cdq

PAY_P_CUST_ID_CLIENTE=53322
PAY_P_KEY=0b0cdb4b6fc3848ceb29a8d10004c7945f9d95cf
PAY_PUBLIC_KEY=de0953d04630c2697a6af61cac28fa4b
PAY_PRIVATE_KEY=ee680982cec2874954c932ff06dfd71d
PAY_TEST=true 
```