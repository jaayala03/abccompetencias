<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Solicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'solicitudes';
    protected $primaryKey = 'codsolicitud';
    protected $fillable   = [
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];
    //**********************************************************************

      // protected $appends = ['rutaimagen', 'rutaimagenpublica'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codsolicitud',
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la solicitud: ' . $this->codsolicitud;
                break;
            case 'updated':
                return 'Editó la solicitud: ' . $this->codsolicitud;
                break;
            case 'deleted':
                return 'Eliminó la solicitud: ' . $this->codsolicitud;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }
    // //**********************************************************************
    // public function getRutaImagenAttribute()
    // {
    //     return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }    

    // public function getRutaImagenPublicaAttribute()
    // {
    //     return route('outside.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }



}
