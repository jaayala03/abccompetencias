<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Conocimiento extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'conocimientos';
    protected $primaryKey = 'codconocimiento';
    protected $fillable   = [
        'codnorma',
        'nombre',
    ];
    //**********************************************************************
    protected $appends = ['actividades', 'preguntasaleatorias', 'porcentaje'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codconocimiento',
        'codnorma',
        'nombre',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el Conocimiento: ' . $this->codconocimiento;
                break;
            case 'updated':
                return 'Editó el Conocimiento: ' . $this->codconocimiento;
                break;
            case 'deleted':
                return 'Eliminó el Conocimiento: ' . $this->codconocimiento;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function getActividadesAttribute($value)
    {
        return $this->actividades()->get();
    }

    public function getPreguntasaleatoriasAttribute($value)
    {
        return 0;
    }

    public function getPorcentajeAttribute($value)
    {
        return 0;
    }


    //Relacion uno a muchos con actividades
    public function actividades()
    {
        return $this->hasMany('Modules\Parametros\Entities\Actividad', 'codconocimiento', 'codconocimiento');
    }

    public function ScopeCantpreguntas($query, $codconocimiento){
        $query->select(\DB::raw('count(preguntas.codpregunta)'))
        ->join('actividades', 'conocimientos.codconocimiento', '=', 'actividades.codconocimiento')
        ->join('preguntas', 'actividades.codactividad', '=', 'preguntas.codactividad')
        ->where('conocimientos.codconocimiento', $codconocimiento);

        return $query;
    }

}
