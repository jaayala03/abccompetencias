<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Nicolaslopezj\Searchable\SearchableTrait;


class Norma extends Model
{

    use LogsActivity;
    use SearchableTrait;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'normas';
    protected $primaryKey = 'codnorma';
    protected $fillable   = [
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion',
        'valor'
    ];
    //**********************************************************************

    protected $appends = ['conocimientos', 'indicadores', 'rutaarchivo'];

    protected $searchable = [
        'columns' => [
            'normas.nombre'       => 5,
            'normas.codigo'       => 10,
        ],
    ];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codnorma',
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion',
        'valor'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la norma: ' . $this->codigo;
                break;
            case 'updated':
                return 'Editó la norma: ' . $this->codigo;
                break;
            case 'deleted':
                return 'Eliminó la norma: ' . $this->codigo;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function getRutaArchivoAttribute()
    {
        return route('krauff.getfile', ['id' => \Crypt::encrypt($this->codnorma), 'option' => \Crypt::encrypt(1), 'type' => \Crypt::encrypt(1)]);
    }
    

    public function getConocimientosAttribute($value)
    {
        return $this->conocimientos()->get();
    }

    public function getIndicadoresAttribute($value)
    {
        return $this->indicadores()->get();
    }


    //relaciones********************************************************************
    //relacion de muchos a uno con tipos de titulacion
    
    public function tipotitulacion()
    {
        return $this->belongsTo('Modules\Parametros\Entities\Tipo_titulacion', 'codtipo_titulacion');
    }

    //Relacion uno a muchos con conocimientos
    public function conocimientos()
    {
        return $this->hasMany('Modules\Parametros\Entities\Conocimiento', 'codnorma', 'codnorma');
    }    
    //Relacion uno a muchos con indicadores
    public function indicadores()
    {
        return $this->hasMany('Modules\Parametros\Entities\Indicador', 'codnorma', 'codnorma');
    }    

    //Relacion uno a muchos con solicitudes
    public function solicitudes()
    {
        return $this->hasMany('Modules\Parametros\Entities\Solicitud', 'codnorma', 'codnorma');
    }

    public function ScopeNormacuestionario($query, $codcuestionario){

        $query->select('normas.*')
        ->JOIN('conocimientos', 'normas.codnorma', '=', 'conocimientos.codnorma')
        ->JOIN('actividades', 'conocimientos.codconocimiento', '=', 'actividades.codconocimiento')
        ->JOIN('preguntas', 'actividades.codactividad', '=', 'preguntas.codactividad')
        ->JOIN('preguntas_cuestionarios', 'preguntas.codpregunta', '=', 'preguntas_cuestionarios.codpregunta')
        ->WHERE('preguntas_cuestionarios.codcuestionario', $codcuestionario)
        ->groupBy('normas.codnorma');

        return $query;
    }
}
