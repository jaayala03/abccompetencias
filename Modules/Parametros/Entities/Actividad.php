<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Actividad extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'actividades';
    protected $primaryKey = 'codactividad';
    protected $fillable   = [
        'codconocimiento',
        'nombre',
    ];
    //**********************************************************************
    protected $appends = ['preguntas', 'cantpreguntas', 'preguntasaleatorias'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codactividad',
        'codconocimiento',
        'nombre',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la Actividad: ' . $this->codactividad;
                break;
            case 'updated':
                return 'Editó la Actividad: ' . $this->codactividad;
                break;
            case 'deleted':
                return 'Eliminó la Actividad: ' . $this->codactividad;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function getCantpreguntasAttribute($value)
    {
        return $this->preguntas()->count();
    }

    public function getPreguntasAttribute($value)
    {
        return $this->preguntas()->get();
    }

    public function getPreguntasaleatoriasAttribute($value)
    {
        return 0;
    }

    //Relacion uno a muchos con preguntas
    public function preguntas()
    {
        return $this->hasMany('Modules\Parametros\Entities\Pregunta', 'codactividad', 'codactividad');
    }

}
