<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pregunta_cuestionario extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'preguntas_cuestionarios';
    protected $primaryKey = 'codpregunta_cuestionario';
    protected $fillable   = [
        'codcuestionario',
        'codpregunta',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codpregunta_cuestionario',
        'codcuestionario',
        'codpregunta',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la pregunta_cuestionario: ' . $this->codpregunta_cuestionario;
                break;
            case 'updated':
                return 'Editó la pregunta_cuestionario: ' . $this->codpregunta_cuestionario;
                break;
            case 'deleted':
                return 'Eliminó la pregunta_cuestionario: ' . $this->codpregunta_cuestionario;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }


}
