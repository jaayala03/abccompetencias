<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pregunta extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'preguntas';
    protected $primaryKey = 'codpregunta';
    protected $fillable   = [
        'pregunta',
        'codactividad',
    ];
    //**********************************************************************

      protected $appends = ['opciones'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codpregunta',
        'pregunta',
        'codactividad',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la pregunta: ' . $this->codpregunta;
                break;
            case 'updated':
                return 'Editó la pregunta: ' . $this->codpregunta;
                break;
            case 'deleted':
                return 'Eliminó la pregunta: ' . $this->codpregunta;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function getOpcionesAttribute($value)
    {
        $opciones = $this->opciones()->orderBy('opciones.codopcion')->get();
        $x = 'A';
        $alp = range('A', 'Z');
        $opciones->map(function ($obj, $key) use ($alp) {
            $obj->numero = $alp[$key];
            return $obj;
        });

        return $opciones;
    }

    //Relacion uno a muchos con opciones
    public function opciones()
    {
        return $this->hasMany('Modules\Parametros\Entities\Opcion', 'codpregunta', 'codpregunta');
    }

    //Relacion muchos a uno con actividad
    public function actividad()
    {
        return $this->belongsTo('Modules\Parametros\Entities\Actividad', 'codactividad');
    }

    //spopes
    public function ScopePreguntasactaleatorio($query, $datos)
    {
        $query->join('actividades', 'preguntas.codactividad', '=', 'actividades.codactividad')
        ->join('conocimientos', 'actividades.codconocimiento', '=', 'conocimientos.codconocimiento')
        ->where('conocimientos.codconocimiento', $datos['codconocimiento'])
        ->inRandomOrder()
        ->limit($datos['cant_preguntas']);

        return $query;
    }

    public function ScopeTodaspreguntas($query, $data){
        
        $query->select('preguntas.*', 'normas.codigo as norma', 'conocimientos.nombre as conocimiento', 'actividades.nombre as actividad')
        ->join('actividades', 'preguntas.codactividad', '=', 'actividades.codactividad')
        ->join('conocimientos', 'actividades.codconocimiento', '=', 'conocimientos.codconocimiento')
        ->join('normas', 'conocimientos.codnorma', '=', 'normas.codnorma');

        if(isset($data['codnorma']) && !empty($data['codnorma'])){
            $query->where('normas.codnorma', $data['codnorma']);
        }

        if(isset($data['codconocimiento']) && !empty($data['codconocimiento'])){
            $query->where('conocimientos.codconocimiento', $data['codconocimiento']);
        }

        if(isset($data['codactividad']) && !empty($data['codactividad'])){
            $query->where('actividades.codactividad', $data['codactividad']);
        }
        
        return $query;
    }


}
