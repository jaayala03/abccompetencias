<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Opcion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'opciones';
    protected $primaryKey = 'codopcion';
    protected $fillable   = [
        'opcion',
        'correcta',
        'codpregunta',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codopcion',
        'opcion',
        'correcta',
        'codpregunta',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la opcion: ' . $this->codopcion;
                break;
            case 'updated':
                return 'Editó la opcion: ' . $this->codopcion;
                break;
            case 'deleted':
                return 'Eliminó la opcion: ' . $this->codopcion;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    //relacion de muchos a uno con pregunta
    public function pregunta()
    {
        return $this->belongsTo('Modules\Parametros\Entities\Pregunta', 'codpregunta');
    }

}
