<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Indicador extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'indicadores';
    protected $primaryKey = 'codindicador';
    protected $fillable   = [
        'nombre',
        'codnorma',
        'tipo',
    ];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codindicador',
        'nombre',
        'codnorma',
        'tipo',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el indicador: ' . $this->codindicador;
                break;
            case 'updated':
                return 'Editó el indicador: ' . $this->codindicador;
                break;
            case 'deleted':
                return 'Eliminó el indicador: ' . $this->codindicador;
                break;
        }
        return '';
    }

}
