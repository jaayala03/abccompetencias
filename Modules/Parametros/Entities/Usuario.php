<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Usuario extends Model
{

    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table = 'usuarios';

    protected $primaryKey = 'codusuario';

    protected $fillable = [
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    //**********************************************************************
    protected $appends    = ['nombrecompleto'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codusuario',
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];


    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                 return 'Creó el Usuario: '.$this->email;
                break;
            case 'updated':
                return 'Editó el Usuario: '.$this->email;
                break;
            case 'deleted':
                return 'Eliminó el Usuario: '.$this->email;
                break;
        }
        return '';
    }

    //**********************************************************************
    public function getNombrecompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primerapellido;
    }
}
