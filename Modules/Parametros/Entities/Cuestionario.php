<?php

namespace Modules\Parametros\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Cuestionario extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'cuestionarios';
    protected $primaryKey = 'codcuestionario';
    protected $fillable   = [
        'nombre',
        'segundos_x_pregunta',
        'tipo',
        'instrucciones'
    ];
    //**********************************************************************

      protected $appends = ['preguntas'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codcuestionario',
        'nombre',
        'segundos_x_pregunta',
        'tipo',
        'instrucciones'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el cuestionario: ' . $this->codcuestionario;
                break;
            case 'updated':
                return 'Editó el cuestionario: ' . $this->codcuestionario;
                break;
            case 'deleted':
                return 'Eliminó el cuestionario: ' . $this->codcuestionario;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function getPreguntasAttribute($value)
    {
        return $this->preguntas()->get();
    }

    //Relacion muchos a muchos con preguntas
    public function preguntas()
    {
        return $this->belongsToMany('Modules\Parametros\Entities\Pregunta', 'preguntas_cuestionarios', 'codcuestionario', 'codpregunta')->withPivot('codpregunta_cuestionario')->withTimestamps();
    }


}
