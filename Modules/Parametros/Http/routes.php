<?php

Route::group(['middleware' => 'web', 'prefix' => 'parametros', 'namespace' => 'Modules\Parametros\Http\Controllers'], function()
{
    Route::get('importar', [
        'uses' => 'ParametrosController@importar',
        'as'   => 'parametros.importar',
    ]); 
    Route::post('subir_importacion', [
        'uses' => 'ParametrosController@subir_importacion',
        'as'   => 'parametros.subir_importacion',
    ]); 
    //**********************************************************************
    //Grupo de rutas para conocimientos
    //**********************************************************************
    Route::group(['prefix' => 'conocimientos'], function () {
        Route::post('buscarConocimientosNorma', [
            'uses' => 'ConocimientosController@buscarConocimientosNorma',
            'as'   => 'parametros.conocimientos.buscarConocimientosNorma',
        ]); 
    }); 
    //**********************************************************************
    //Grupo de rutas para actividades
    //**********************************************************************
    Route::group(['prefix' => 'actividades'], function () {
        Route::post('buscarActividadesConocimiento', [
            'uses' => 'ActividadesController@buscarActividadesConocimiento',
            'as'   => 'parametros.actividades.buscarActividadesConocimiento',
        ]); 
    }); 
    //**********************************************************************
    //Grupo de rutas para preguntas
    //**********************************************************************
    Route::group(['prefix' => 'preguntas'], function () {       
        //Ruta listar preguntas
        Route::get('/', [
            'uses' => 'PreguntasController@listar',
            'as'   => 'parametros.preguntas.listar',
        ]);          

        //Ruta cargar crear
        Route::get('cargarcrear', [
            'uses' => 'PreguntasController@cargarcrear',
            'as'   => 'parametros.preguntas.cargarcrear',
        ]);           

        //Ruta crear
        Route::post('crear', [
            'uses' => 'PreguntasController@crear',
            'as'   => 'parametros.preguntas.crear',
        ]);          

        //Ruta cargar editar
        Route::get('cargareditar/{codpregunta}', [
            'uses' => 'PreguntasController@cargareditar',
            'as'   => 'parametros.preguntas.cargareditar',
        ]);      

    	//Ruta editar
        Route::post('editar', [
            'uses' => 'PreguntasController@editar',
            'as'   => 'parametros.preguntas.editar',
        ]);     

        //Ruta listar preguntas
        Route::post('datatable', [
            'uses' => 'PreguntasController@datatable',
            'as'   => 'parametros.preguntas.datatable',
        ]);        

        //Ruta listar preguntas
        Route::post('eliminar', [
            'uses' => 'PreguntasController@eliminar',
            'as'   => 'parametros.preguntas.eliminar',
        ]);

        //Ruta obtener preguntas aleatorias por conocimiento
        Route::post('obtenerpreguntas_aleatorio', [
            'uses' => 'PreguntasController@obtenerpreguntas_aleatorio',
            'as'   => 'parametros.preguntas.obtenerpreguntas_aleatorio',
        ]);

    }); 

    //**********************************************************************
    //Grupo de rutas para cuestionarios
    //**********************************************************************
    Route::group(['prefix' => 'cuestionarios'], function () {       
        //Ruta listar cuestionarios
        Route::get('/', [
            'uses' => 'CuestionariosController@listar',
            'as'   => 'parametros.cuestionarios.listar',
        ]);          

        //Ruta cargar crear
        Route::get('cargarcrear', [
            'uses' => 'CuestionariosController@cargarcrear',
            'as'   => 'parametros.cuestionarios.cargarcrear',
        ]);           

        //Ruta crear
        Route::post('crear', [
            'uses' => 'CuestionariosController@crear',
            'as'   => 'parametros.cuestionarios.crear',
        ]);          

        //Ruta cargar editar
        Route::get('cargareditar/{codpregunta}', [
            'uses' => 'CuestionariosController@cargareditar',
            'as'   => 'parametros.cuestionarios.cargareditar',
        ]);      

        //Ruta editar
        Route::post('editar', [
            'uses' => 'CuestionariosController@editar',
            'as'   => 'parametros.cuestionarios.editar',
        ]);     

        //Ruta listar cuestionarios
        Route::post('datatable', [
            'uses' => 'CuestionariosController@datatable',
            'as'   => 'parametros.cuestionarios.datatable',
        ]);        

        //Ruta listar cuestionarios
        Route::post('eliminar', [
            'uses' => 'CuestionariosController@eliminar',
            'as'   => 'parametros.cuestionarios.eliminar',
        ]);

        //Ruta cargar editar
        Route::get('imprimir/{codcuestionariocifrado}', [
            'uses' => 'CuestionariosController@imprimir',
            'as'   => 'parametros.cuestionarios.imprimir',
        ]);   
    }); 

    //**********************************************************************
    //Grupo de rutas para normas
    //**********************************************************************
    Route::group(['prefix' => 'normas'], function () {

        Route::post('/buscarNorma', [
            'uses' => 'NormasController@buscarNorma',
            'as'   => 'parametros.normas.buscarNorma',
        ]);
        //Ruta listar normas
        Route::get('/', [
            'uses' => 'NormasController@listar',
            'as'   => 'parametros.normas.listar',
        ]);          

        //Ruta cargar crear
        Route::get('cargarcrear', [
            'uses' => 'NormasController@cargarcrear',
            'as'   => 'parametros.normas.cargarcrear',
        ]);           

        //Ruta crear
        Route::post('crear', [
            'uses' => 'NormasController@crear',
            'as'   => 'parametros.normas.crear',
        ]);          

        //Ruta cargar editar
        Route::get('cargareditar/{codpregunta}', [
            'uses' => 'NormasController@cargareditar',
            'as'   => 'parametros.normas.cargareditar',
        ]);      

    	//Ruta editar
        Route::post('editar', [
            'uses' => 'NormasController@editar',
            'as'   => 'parametros.normas.editar',
        ]);     

        //Ruta listar normas
        Route::post('datatable', [
            'uses' => 'NormasController@datatable',
            'as'   => 'parametros.normas.datatable',
        ]);        

        //Ruta listar normas
        Route::post('eliminar', [
            'uses' => 'NormasController@eliminar',
            'as'   => 'parametros.normas.eliminar',
        ]);        

        //Ruta listar obtener conocimientos
        Route::post('obtener_conocimientos', [
            'uses' => 'NormasController@obtener_conocimientos',
            'as'   => 'parametros.normas.obtener_conocimientos',
        ]);
        //**********************************************************************
        //Grupo de rutas para indicadores
        //**********************************************************************
        Route::group(['prefix' => 'indicadores'], function () {
            //Ruta cargar listar
            Route::get('listar/{codnorma}', [
                'uses' => 'IndicadoresController@listar',
                'as'   => 'parametros.normas.indicadores.listar',
            ]);
            //Ruta guardar
            Route::post('guardar', [
                'uses' => 'IndicadoresController@guardar',
                'as'   => 'parametros.normas.indicadores.guardar',
            ]);
        });
    }); 
});
