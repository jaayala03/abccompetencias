<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Input;
use Modules\Parametros\Entities\Norma;
use Modules\Parametros\Entities\Pregunta;
use Modules\Parametros\Entities\Opcion;
use Modules\Parametros\Entities\Conocimiento;
use Modules\Parametros\Entities\Indicador;

class ConocimientosController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscarConocimientosNorma(Request $request){
        return Conocimiento::where('codnorma', $request->codnorma)->get();
    }
}
