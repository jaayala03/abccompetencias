<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Parametros\Entities\Actividad;
use Modules\Parametros\Entities\Pregunta;
use Modules\Parametros\Entities\Conocimiento;
use Modules\Parametros\Entities\Opcion;
use Modules\Parametros\Entities\Norma;


class PreguntasController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Preguntas',
                'componente' => $this->componente('PARAM_CUEST_PREG'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Preguntas'],
        ]);

        return view('parametros::preguntas.listar');
    }

    public function datatable(Request $request)
    {
        $preguntas = Pregunta::todaspreguntas($request->all());

        return Datatables::of($preguntas)->addColumn('opciones', function (Pregunta $pregunta) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($this->verificarpermiso("PARAM_CUEST_PREG_ELI") ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminar-pregunta tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . \Crypt::encrypt($pregunta->codpregunta) . '"><i class="fa fa-times"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("PARAM_CUEST_PREG_EDI") ? '<a href="#'.route("parametros.preguntas.cargareditar", ['codpregunta' => \Crypt::encrypt($pregunta->codpregunta)], false).'"  class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>' : '');
            return $html;
        })->rawColumns(['opciones'])->make(true);
    }

    public function cargarcrear()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Crear Pregunta',
                'componente' => $this->componente('PARAM_CUEST_PREG'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Preguntas', 'url' => '#' . route('parametros.preguntas.listar', [], false)],
            ['name' => 'Crear'],
        ]);

        return view('parametros::preguntas.crear');
    }

    public function crear(Request $request)
    {

        $pregunta = new Pregunta($request->all());

        $status = $pregunta->save();

        if ($status) {
            foreach ($request->opciones as $value) {
                $opcion = new Opcion([
                    'opcion' => $value['opcion'],
                    'correcta' => $value['correcta'],
                    'codpregunta' => $pregunta->codpregunta,
                ]);

                $opcion->save();
            }
        }

        $urlretorno = route('parametros.preguntas.listar', [], false);
        return response()->json([
            'url'     => $urlretorno,
            'message' => 'Pregunta creada correctamente',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function cargareditar($codpregunta)
    {
        $pregunta = Pregunta::findOrFail(\Crypt::decrypt($codpregunta));
        $actividad = Actividad::findOrFail($pregunta->codactividad);
        $conocimiento = Conocimiento::findOrFail($actividad->codconocimiento);
        $norma = Norma::findOrFail($conocimiento->codnorma);

          JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar Pregunta',
                'componente' => $this->componente('PARAM_CUEST_PREG'),
            ],
            'datos' => [
                'pregunta' => $pregunta,
                'actividad' => $actividad,
                'conocimiento' => $conocimiento,
                'norma' => $norma,
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Preguntas', 'url' => '#' . route('parametros.preguntas.listar', [], false)],
            ['name' => 'Editar'],
        ]);

        return view('parametros::preguntas.editar');
    }

    public function editar(Request $request)
    {
        $pregunta = Pregunta::findOrFail($request->codpregunta);
        $pregunta->fill($request->all());

        $status = $pregunta->save();

        if ($status) {

            foreach ($request->opciones_eliminar as $item) {
                $eliminar = Opcion::findOrFail($item['codopcion']);
                $eliminar->delete();
            }

            foreach ($request->opciones as $value) {
                if (!empty($value['codopcion'])) {
                    $editar = Opcion::findOrFail($value['codopcion']);
                    $editar->opcion = $value['opcion'];
                    $editar->correcta = $value['correcta'];
                    $editar->save();
                }else{
                    $crear = new Opcion([
                        'opcion' => $value['opcion'],
                        'correcta' => $value['correcta'],
                        'codpregunta' => $pregunta->codpregunta,
                    ]);

                    $crear->save();
                }
                
            }
        }

        $urlretorno = route('parametros.preguntas.listar', [], false);
        return response()->json([
            'url'     => $urlretorno,
            'message' => 'Pregunta editada correctamente',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function eliminar(Request $request)
    {
        $pregunta = Pregunta::findOrFail(\Crypt::decrypt($request->codpreguntacifrado));

        $pregunta->opciones()->delete();

        $status = $pregunta->delete();
        
        if ($status) {
            return response()->json([
                'message' => 'Pregunta eliminada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        }

        return response()->json([
            'message' => 'No se pudo eliminar la pregunta',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }

    public function obtenerpreguntas_aleatorio(Request $request)
    {
        $preguntas = Pregunta::preguntasactaleatorio($request->all())->get();

        return $preguntas;
    }
}
