<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Parametros\Entities\Cuestionario;
use Modules\Parametros\Entities\Pregunta;
use Modules\Parametros\Entities\Conocimiento;
use Modules\Parametros\Entities\Norma;
use Modules\Parametros\Entities\Pregunta_cuestionario;
use ABCcomptencias\Libraries\Fpdf_formato;

class CuestionariosController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Cuestionarios',
                'componente' => $this->componente('PARAM_CUEST_CUES'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Cuestionarios'],
        ]);

        return view('parametros::cuestionarios.listar');
    }

    public function datatable(Request $request)
    {
        $cuestionarios = Cuestionario::where('tipo', \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.P'));

        return Datatables::of($cuestionarios)
        ->addColumn('preguntas', function (Cuestionario $cuestionario) {
            return '<span class="badge badge-info">'.$cuestionario->preguntas()->count().'</span>';
        })->addColumn('opciones', function (Cuestionario $cuestionario) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($cuestionario->preguntas()->count() ? '<a data-placement="top" title="Imprimir Cuestionario" data-toggle="tooltip" target="_blank" href="' . route('parametros.cuestionarios.imprimir', [\Crypt::encrypt($cuestionario->codcuestionario)]) . '" class="btn btn-warning btn-xs rounded tooltip-opcion"><i class="fa fa-print"></i></a>' : '');
            // $html = $html . ($this->verificarpermiso("PARAM_CUEST_CUES_EDI") ? '<a href="#'.route("parametros.cuestionarios.cargareditar", ['codcuestionario' => \Crypt::encrypt($cuestionario->codcuestionario)], false).'"  class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>' : '');
            // $html = $html . ($this->verificarpermiso("PARAM_CUEST_CUES_ELI") ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminar-cuestionario tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . \Crypt::encrypt($cuestionario->codcuestionario) . '"><i class="fa fa-times"></i></a>' : '');
            return $html;
        })->rawColumns(['opciones', 'preguntas'])->make(true);
    }

    public function imprimir(Request $request)
    {
        $codcuestionario = \Crypt::decrypt($request->codcuestionariocifrado);
        $cuestionario = Cuestionario::findOrFail($codcuestionario);
        $preguntas = $cuestionario->preguntas;
        $preguntas = $preguntas->map(function ($obj, $key) {
            $obj->value = '';
            $obj->numero = $key + 1;
            
            return $obj;
        });

        $norma = Norma::normacuestionario($codcuestionario)->first();

        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode('EVALUACIÓN TEORICA')));

        $pdf->setCodigo(utf8_decode('CODIGO: T-01-F06'));
        $pdf->setVersion(utf8_decode('Versión 01'));
        $pdf->setFecha(utf8_decode('2018/04/12'));

        $pdf->SetTitle(utf8_decode('Imprimir cuestionario'));
        $pdf->AliasNbPages();

        $pdf->AddPage();

        //seccion datos del producto
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(30, 8, utf8_decode('Cuestionario:'), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(80, 8, utf8_decode($cuestionario->nombre), 0, 1, 'L');


        //BLOQUE 3------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode($norma->nombre), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(80, 6, utf8_decode($norma->codigo), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->MultiCell(180, 6, utf8_decode(!empty($cuestionario->instrucciones) ? $cuestionario->instrucciones : 'Señor Candidato: La prueba se conforma por enunciados (textos que describen problemas, situaciones, casos, etc.) distinguidos con números, cada una de éstos cuenta con cuatro opciones de respuesta identificados con las letras a, b, c y d, donde sólo uno responde correctamente a la pregunta. Rellene el ovalo que usted considera responde a la pregunta en el formato "Hoja de Respuestas".'), 1, 'J', false);

        $pdf->Ln(2);
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->CellFitScale(180, 4, utf8_decode('Este Cuestionario debe ser devuelto al evaluador sin dañarlo ni modificarlo en ninguna de sus partes.'), 0, 1, 'C', false);

        $pdf->Ln(4);

        $pdf->SetFont('Arial', '', 12);
        foreach ($preguntas as $pregunta) {
            $pdf->Cell(7, 6, utf8_decode($pregunta->numero . '.'), 0, 0, 'J');
            $pdf->MultiCell(173, 6, utf8_decode($pregunta->pregunta), 0, 'J', false);
            $pdf->Ln(2);
            $opciones = $pregunta->opciones;
            $x = 'A';
            foreach ($opciones as $opcion) {
                $pdf->Cell(7, 6, '', 0, 0, 'J');
                $pdf->Cell(7, 6, utf8_decode($x . '.'), 0, 0, 'J');
                $pdf->CellFitScale(166, 6, utf8_decode($opcion->opcion), 0, 1, 'J');
                $x++;
            }
            $pdf->Ln(2);
        }

        //----------------------------------------------------------------------------------------------
        //agregar hoja de respuestas --------------------------------------------------------------------
        $pdf->setTitulo(strtoupper(utf8_decode('HOJA DE RESPUESTAS')));
        $pdf->setCodigo(utf8_decode('CODIGO: T-01-F07'));
        $pdf->AddPage();

        //BLOQUE 1-----------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('DATOS DEL CANDIDATO'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(30, 6, utf8_decode('NOMBRES:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('APELLIDOS:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('IDENTIFICACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('LUGAR DE EXPEDICIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 2------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('DATOS DE APLICACIÓN'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA DE APLICACIÓN:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA ENTREGA DE RESULTADOS:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('LUGAR DE EVALUACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CIUDAD:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('NOMBRE EVALUADOR:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 3------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode($norma->nombre), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(80, 6, utf8_decode($norma->codigo), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->MultiCell(180, 6, utf8_decode('Señor  Candidato: Con  base  en  el  cuestionario  leído  y  comprendido, usted debe seleccionar frente al numeral la respuesta correcta y rellenar el óvalo correspondiente.'), 1, 'C', false);

        $pdf->Ln(6);

        $columnas        = round(count($preguntas) / 2, 0, PHP_ROUND_HALF_UP);
        $xp               = $pdf->GetX();
        $yp               = $pdf->GetY();

        foreach ($preguntas as $index => $pregunta) {
            $pdf->SetFont('Arial', 'B', 12);
            if ($index == $columnas) {
                $pdf->SetXY($xp + 98, $yp);
            }
            if ($index > $columnas) {
                $pdf->SetX($xp + 98);
            }
            $pdf->CellFitScale(8, 6, '', 0, 0, 'J');
            $pdf->CellFitScale(7, 6, utf8_decode($pregunta->numero), 1, 0, 'J');
            $opciones = $pregunta->opciones;
            $x = 'A';
            $Xop = 0;
            $Yop = 0;
            foreach ($opciones as $opcion) {
                $pdf->SetFont('Arial', '', 12);
                $pdf->Cell(15, 6, utf8_decode($x), 1, 0, 'L');
                $Xop = $pdf->GetX();
                $Yop = $pdf->GetY();
                $pdf->Ellipse($Xop - 6, $Yop + 3, 3, 2, 'D');
                $x++;
            }
            $pdf->Ln(6);
        }

        $pdf->Ln(6);

        $Xc = 0;
        $Yc = 0;
        $Xnc = 0;
        $Ync = 0;

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(80, 6, utf8_decode('JUICIO DE LA COMPETENCIA:'), 0, 0, 'R', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CUMPLE:'), 0, 0, 'L', false);
        $Xc = $pdf->GetX();
        $Yc = $pdf->GetY();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(19, 206, 102);
        $pdf->CellFitScale(10, 6, '', 1, 0, 'L', true);
        $pdf->Ellipse($Xc + 5, $Yc + 3, 3, 2, 'D');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->CellFitScale(25, 6, utf8_decode('AÚN NO CUMPLE:'), 0, 0, 'L', false);
        $Xnc = $pdf->GetX();
        $Ync = $pdf->GetY();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(254, 73, 73);
        $pdf->CellFitScale(10, 6, '', 1, 1, 'L', true);
        $pdf->Ellipse($Xnc + 5, $Ync + 3, 3, 2, 'D');

        $pdf->Ln(2);

        //BLOQUE 6------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('IDENTIFICACIÓN DE LA(S) COMPETENCIA(S) FALTANTE(S) - ANÁLISIS DEL EVALUADOR-'), 1, 1, 'C', true);
        $pdf->CellFitScale(0, 30, utf8_decode(''), 1, 1, 'L', false);

        $pdf->Ln(20);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->CellFitScale(25, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL CANDIDATO'), 'T', 0, 'C', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL EVALUADOR'), 'T', 0, 'C', false);

        // ob_end_clean();
        $pdf->Output("Cuestionario.pdf", 'I');
    }

    public function cargarcrear()
    {
        $normas = Norma::all();

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Crear Cuestionario',
                'componente' => $this->componente('PARAM_CUEST_CUES'),
            ],
            'normas' => $normas,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Cuestionarios'],
        ]);

        return view('parametros::cuestionarios.crear');
    }

    public function crear(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $cuestionario = new Cuestionario([
                    'nombre' => $request->nombre,
                    'tipo' => \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.P', 1),
                    'segundos_x_pregunta' => $request->segundos_x_pregunta,
                ]);
                $cuestionario->save();
        
                foreach ($request->preguntas_cuestionario as $value) {
                    $pregunta = Pregunta::find($value['codpregunta']);
                    $correctas = $pregunta->opciones()->where('correcta', true)->count();
                    if ($correctas == 0) {
                        throw new \Exception("La pregunta $pregunta->codpregunta no tiene ninguna opcion correcta: ". \Str::limit($pregunta->pregunta, 20, '...'));
                    }

                    if ($correctas > 1) {
                        throw new \Exception("La pregunta $pregunta->codpregunta tiene más de una opcion correcta: ". \Str::limit($pregunta->pregunta, 20, '...'));
                    }

                    $pregunta_cuestionario = new Pregunta_cuestionario([
                        'codpregunta' => $value['codpregunta'],
                        'codcuestionario' => $cuestionario->codcuestionario,
                    ]);
                    $pregunta_cuestionario->save();
                }
            }, 5);

            $urlretorno = route('parametros.cuestionarios.listar', [], false);

            return response()->json([
                'url'     => $urlretorno,
                'message' => 'Cuestionario creado correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        } catch (\Exception $e) {
            // dd($e);
            $message = __('messages.error');
            if ($e instanceof \Throwable) {
                $message = $e->getMessage();
                return response()->error(400, $message);
            }

            return response()->error(500, $message);
        }
    }

    public function cargareditar($codcuestionario)
    {
        $normas = Norma::all();
        $cuestionario = Cuestionario::findOrFail(\Crypt::decrypt($codcuestionario));
        $codnorma = Conocimiento::findOrFail($cuestionario->preguntas()->first()->codconocimiento)->codnorma;

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar Cuestionario',
                'componente' => $this->componente('PARAM_CUEST_CUES'),
            ],
            'cuestionario' => $cuestionario,
            'normas' => $normas,
            'codnorma' => $codnorma,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Cuestionarios'],
        ]);

        return view('parametros::cuestionarios.editar');
    }

    public function editar(Request $request)
    {
        $cuestionario = Cuestionario::findOrFail($request->codcuestionario);
        $cuestionario->fill($request->all());

        $status = $cuestionario->save();

        if ($status) {
            foreach ($request->preguntas_cuestionario_eliminar as $item) {
                $eliminar = Pregunta_cuestionario::findOrFail($item['pivot']['codpregunta_cuestionario']);
                $eliminar->delete();
            }

            foreach ($request->preguntas_cuestionario as $value) {
                if (!empty($value['pivot']['codpregunta_cuestionario'])) {
                    $editar = Pregunta_cuestionario::findOrFail($value['pivot']['codpregunta_cuestionario']);
                    $editar->codpregunta = $value['codpregunta'];
                    $editar->save();
                } else {
                    $crear = new Pregunta_cuestionario([
                        'codpregunta' => $value['codpregunta'],
                        'codcuestionario' => $request->codcuestionario
                    ]);

                    $crear->save();
                }
            }
        }

        $urlretorno = route('parametros.cuestionarios.listar', [], false);
        return response()->json([
            'url'     => $urlretorno,
            'message' => 'Cuestionario editado correctamente',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function eliminar(Request $request)
    {
        $cuestionario = Cuestionario::findOrFail(\Crypt::decrypt($request->codcuestionariocifrado));

        $cuestionario->preguntas()->delete();

        $status = $cuestionario->delete();
        
        if ($status) {
            return response()->json([
                'message' => 'Cuestionario eliminada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        }

        return response()->json([
            'message' => 'No se pudo eliminar la cuestionario',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }
}
