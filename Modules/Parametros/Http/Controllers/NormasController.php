<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Parametros\Entities\Norma;
use Modules\Parametros\Entities\Conocimiento;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;


class NormasController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Normas',
                'componente' => $this->componente('PARAM_NOR'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Normas'],
        ]);

        return view('parametros::normas.listar');
    }

    public function datatable(Request $request)
    {
        $normas = Norma::all();

        return Datatables::of($normas)->addColumn('opciones', function (Norma $norma) {

            $solicitudes = $norma->solicitudes()->count();
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($this->verificarpermiso("PARAM_NOR_ELI") && $solicitudes <= 0 ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminar-norma tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . \Crypt::encrypt($norma->codnorma) . '"><i class="fa fa-times"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("PARAM_NOR_EDI") ? '<a href="#' . route("parametros.normas.cargareditar", ['codnorma' => \Crypt::encrypt($norma->codnorma)], false) . '"  class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>' : '');
            $html = $html . (isset($norma->archivo) ? '<a href="' . $norma->rutaarchivo . '" target="_blank" class="btn btn-warning rounded btn-xs tooltip-opcion" data-placement="top" data-original-title="Ver Adjunto"><i class="fa fa-download"></i></a>' : '');
            $html = $html . (true ? '<a href="#' . route("parametros.normas.indicadores.listar", ['codnorma' => \Crypt::encrypt($norma->codnorma)], false) . '"  class="btn btn-info btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Indicadores"><i class="fa fa-bookmark"></i></a>' : '');
            return $html;
        })->rawColumns(['opciones'])->make(true);
    }

    public function cargarcrear()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Crear Norma',
                'componente' => $this->componente('PARAM_NOR'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Normas', 'url' => '#' . route('parametros.normas.listar', [], false)],
            ['name' => 'Crear'],
        ]);

        return view('parametros::normas.crear');
    }

    public function crear(Request $request)
    {
        $norma = new Norma($request->all());

        if ($request->file) {
            $adjunto  = Input::file('file');
            $nombre = uniqid(rand(), true) . str_replace(" ", "", microtime()) . '.' . $adjunto->getClientOriginalExtension();
            $subruta         = \Config::get('paths.DOCUMENTOS_NORMAS') . $nombre;
            $guardar_archivo = Storage::disk('web_files')->put($subruta, \File::get($adjunto));

            if ($guardar_archivo) {
                $norma->archivo = $nombre;
                // $norma->nombre_original = $adjunto->getClientOriginalName();
            }
        }
        $status = $norma->save();

        if ($status) {
            foreach ($request->conocimientos as $value) {
                $conocimiento = new Conocimiento([
                    'nombre' => $value,
                    'codnorma' => $norma->codnorma,
                ]);

                $conocimiento->save();
            }
        }

        $urlretorno = route('parametros.normas.listar', [], false);
        return response()->json([
            'url'     => $urlretorno,
            'message' => 'Norma creada correctamente',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function cargareditar($codnorma)
    {
        $norma = Norma::findOrFail(\Crypt::decrypt($codnorma));

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar Norma',
                'componente' => $this->componente('PARAM_NOR'),
            ],
            'norma' => $norma,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Normas'],
        ]);

        return view('parametros::normas.editar');
    }

    public function editar(Request $request)
    {
        $norma = Norma::findOrFail($request->codnorma);
        $norma->fill($request->all());

        if ($request->file) {
            if (!empty($norma->archivo)) {
                $archivo_anterior = \Config::get('paths.DOCUMENTOS_NORMAS') . $norma->archivo;
                Storage::disk('web_files')->delete($archivo_anterior);
            }
            $adjunto  = Input::file('file');
            $nombre = uniqid(rand(), true) . str_replace(" ", "", microtime()) . '.' . $adjunto->getClientOriginalExtension();
            $subruta         = \Config::get('paths.DOCUMENTOS_NORMAS') . $nombre;
            $guardar_archivo = Storage::disk('web_files')->put($subruta, \File::get($adjunto));

            if ($guardar_archivo) {
                $norma->archivo = $nombre;
                // $norma->nombre_original = $adjunto->getClientOriginalName();
            }
        }

        $status = $norma->save();
        if ($status) {
            if ($request->conocimientos_codigo) {
                foreach ($request->conocimientos_codigo as $indice1 => $codigo) {
                    if (!empty($codigo)) {
                        $edit_conocimiento = Conocimiento::findOrFail($codigo);
                        $edit_conocimiento->nombre = $request->conocimientos[$indice1];
                        $edit_conocimiento->save();
                    } else {
                        $new_conocimiento = new Conocimiento();
                        $new_conocimiento->nombre = $request->conocimientos[$indice1];
                        $new_conocimiento->codnorma = $request->codnorma;
                        $new_conocimiento->save();
                    }
                }
            }

            if ($request->conocimientos_eliminar) {
                foreach ($request->conocimientos_eliminar as $key => $value) {
                    $delete_conocimiento = Conocimiento::findOrFail($value);
                    $delete_conocimiento->delete();
                }
            }


            $urlretorno = route('parametros.normas.listar', [], false);
            return response()->json([
                'url'     => $urlretorno,
                'message' => 'Norma editada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        }

        return response()->json([
            'message' => 'No se ha podido editar la norma',
            'title'   => 'Error',
            'type'    => 'error',
        ]);

    }

    public function eliminar(Request $request)
    {
        $norma = Norma::findOrFail(\Crypt::decrypt($request->codnormacifrado));

        $norma->conocimientos()->delete();

        if (!empty($norma->archivo)) {
            $subruta         = \Config::get('paths.DOCUMENTOS_NORMAS') . $norma->archivo;
            $eliminar_archivo = Storage::disk('web_files')->delete($subruta);
        }

        $status = $norma->delete();

        if ($status) {
            return response()->json([
                'message' => 'Norma eliminada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        }

        return response()->json([
            'message' => 'No se pudo eliminar la norma',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }

    public function obtener_conocimientos(Request $request)
    {
        $norma = Norma::findOrFail($request->codnorma);

        return $norma->conocimientos()->get();
    }

    public function buscarNorma(Request $request){
        return Norma::search($request['query'])->limit(20)->where('normas.estado', 1)->get();
    }
}
