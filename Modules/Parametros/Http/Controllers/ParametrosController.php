<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use DB;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Input;
use Modules\Parametros\Entities\Norma;
use Modules\Parametros\Entities\Conocimiento;
use Modules\Parametros\Entities\Actividad;
use Modules\Parametros\Entities\Pregunta;
use Modules\Parametros\Entities\Opcion;
use Modules\Parametros\Entities\Indicador;

class ParametrosController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Importar',
                'componente' => $this->componente('PARAM_IMPOR'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Importar'],
        ]);

        return view('parametros::importar');
    }

    public function subir_importacion(Request $request)
    {
        $file   = Input::file('file');

        $arr_normas = [];
        $arr_conocimientos = [];
        $arr_actividades = [];

        $i           = 1;
        $arrResponse = [
            'items'   => [],
            'success' => true,
        ];

        \DB::beginTransaction();
        try {
            \Excel::load($file, function ($reader) use (&$arr_normas, &$arr_conocimientos, &$arr_actividades, &$arrResponse, &$i) {
                $excel = $reader->get();

                $reader->each(function ($sheet) use (&$arr_normas, &$arr_conocimientos, &$arr_actividades, &$arrResponse, &$i) {
                    //obtener nombre de la hoja
                    $nombre_hoja = $sheet->getTitle();

                    if ($nombre_hoja == 'norma') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_norma) use (&$arr_normas, &$arrResponse, &$i) {
                            if(!empty(trim($fila_norma->codigo))){
                                $exists_norma = Norma::where('codigo', trim($fila_norma->codigo))->exists();
                                if (!$exists_norma) {
                                    $norma = new Norma([
                                        'codigo' => trim($fila_norma->codigo),
                                        'nombre' => trim($fila_norma->nombre),
                                        'valor' => $fila_norma->valor,
                                        'descripcion' => trim($fila_norma->descripcion)
                                    ]);
                                    $norma->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $norma->codnorma,
                                        'msg'     => 'Norma: ' . $norma->codigo . ', creada',
                                    ];
                                    $i++;
                                    array_push($arr_normas, [
                                        'id_norma' => $fila_norma->id,
                                        'codnorma' => $norma->codnorma
                                    ]);
                                }
                            }                           
                        });
                    }
                    if ($nombre_hoja == 'conocimientos') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_conocimiento) use (&$arr_normas, &$arr_conocimientos, &$arr_actividades, &$arrResponse, &$i) {
                            if(!empty(trim($fila_conocimiento->nombre))){
                                $indice_norm = array_search($fila_conocimiento->norma, array_column($arr_normas, 'id_norma'));
                                $norm = $arr_normas[$indice_norm];
    
                                $exists_conocimiento = Conocimiento::where('nombre', trim($fila_conocimiento->nombre))
                                    ->where('codnorma', $norm['codnorma'])
                                    ->exists();
    
                                if (!$exists_conocimiento) {
                                    $conocimiento = new Conocimiento([
                                        'nombre' => trim($fila_conocimiento->nombre),
                                        'codnorma' => $norm['codnorma'],
                                    ]);
                                    $conocimiento->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $conocimiento->codconocimiento,
                                        'msg'     => 'Conocimiento: ' . $conocimiento->nombre.', creado',
                                    ];
                                    $i++;
                                    array_push($arr_conocimientos, [
                                        'id_conocimiento' => $fila_conocimiento->id,
                                        'codconocimiento' => $conocimiento->codconocimiento
                                    ]);
                                }
                            }
                        });
                    }
                    if ($nombre_hoja == 'actividades') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_actividad) use (&$arr_normas, &$arr_conocimientos, &$arr_actividades, &$arrResponse, &$i) {
                            if(!empty(trim($fila_actividad->nombre))){
                                $indice_conocim = array_search($fila_actividad->conocimiento, array_column($arr_conocimientos, 'id_conocimiento'));
                                $conocim = $arr_conocimientos[$indice_conocim];
    
                                $exists_actividad = Actividad::where('nombre', trim($fila_actividad->nombre))
                                    ->where('codconocimiento', $conocim['codconocimiento'])
                                    ->exists();
    
                                if (!$exists_actividad) {
                                    $actividad = new Actividad([
                                        'nombre' => trim($fila_actividad->nombre),
                                        'codconocimiento' => $conocim['codconocimiento'],
                                    ]);
                                    $actividad->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $actividad->codactividad,
                                        'msg'     => 'Actividad: ' . $actividad->nombre.', creada',
                                    ];
                                    $i++;
                                    array_push($arr_actividades, [
                                        'id_actividad' => $fila_actividad->id,
                                        'codactividad' => $actividad->codactividad
                                    ]);
                                }
                            }
                        });
                    }

                    if ($nombre_hoja == 'preguntas') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_pregunta) use (&$arr_actividades, &$arrResponse, &$i) {
                            if(!empty(trim($fila_pregunta->pregunta))){
                                $indice_act = array_search($fila_pregunta->actividad, array_column($arr_actividades, 'id_actividad'));
                                $act = $arr_actividades[$indice_act];
    
                                $exists_pregunta = Pregunta::where('pregunta', trim($fila_pregunta->pregunta))
                                    ->where('codactividad', $act['codactividad'])
                                    ->exists();
    
                                if (!$exists_pregunta) {
                                    $pregunta = new Pregunta([
                                        'pregunta' => trim($fila_pregunta->pregunta),
                                        'codactividad' => $act['codactividad'],
                                    ]);
                                    $pregunta->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $pregunta->codpregunta,
                                        'msg'     => 'Pregunta: ' . $pregunta->pregunta . ', creada',
                                    ];
                                    $i++;
                                    $opcionA = new Opcion([
                                        'opcion' => trim($fila_pregunta->opciona),
                                        'correcta' => (strtoupper($fila_pregunta->correcta) == 'A') ? true : false,
                                        'codpregunta' => $pregunta->codpregunta
                                    ]);
                                    $opcionA->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $opcionA->codopcion,
                                        'msg'     => 'Opcion: ' . $opcionA->opcion . ', creada',
                                    ];
    
                                    $opcionB = new Opcion([
                                        'opcion' => trim($fila_pregunta->opcionb),
                                        'correcta' => (strtoupper($fila_pregunta->correcta) == 'B') ? true : false,
                                        'codpregunta' => $pregunta->codpregunta
                                    ]);
                                    $opcionB->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $opcionB->codopcion,
                                        'msg'     => 'Opcion: ' . $opcionB->opcion . ', creada',
                                    ];
    
                                    $opcionC = new Opcion([
                                        'opcion' => trim($fila_pregunta->opcionc),
                                        'correcta' => (strtoupper($fila_pregunta->correcta) == 'C') ? true : false,
                                        'codpregunta' => $pregunta->codpregunta
                                    ]);
                                    $opcionC->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $opcionC->codopcion,
                                        'msg'     => 'Opcion: ' . $opcionC->opcion . ', creada',
                                    ];
    
                                    $opcionD = new Opcion([
                                        'opcion' => trim($fila_pregunta->opciond),
                                        'correcta' => (strtoupper($fila_pregunta->correcta) == 'D') ? true : false,
                                        'codpregunta' => $pregunta->codpregunta
                                    ]);
                                    $opcionD->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $opcionD->codopcion,
                                        'msg'     => 'Opcion: ' . $opcionD->opcion . ', creada',
                                    ];
                                }
                            }
                        });
                    }

                    if ($nombre_hoja == 'indicadores_desempeno') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_desempeno) use (&$arr_normas, &$arrResponse, &$i) {
                            if(!empty(trim($fila_desempeno->indicador))){
                                $indice_norm = array_search($fila_desempeno->norma, array_column($arr_normas, 'id_norma'));
                                $norm = $arr_normas[$indice_norm];
    
                                $exists_indicador = Indicador::where('nombre', trim($fila_desempeno->indicador))
                                    ->where('codnorma', $norm['codnorma'])
                                    ->where('tipo', \Config::get('dominios.TIPO_EVALUACION_INDICADOR.VALORES.DES'))
                                    ->exists();
    
                                if (!$exists_indicador) {
                                    $indicador = new Indicador([
                                        'nombre' => trim($fila_desempeno->indicador),
                                        'codnorma' => $norm['codnorma'],
                                        'tipo' => \Config::get('dominios.TIPO_EVALUACION_INDICADOR.VALORES.DES')
                                    ]);
                                    $indicador->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $indicador->codindicador,
                                        'msg'     => 'Indicador de desempeño: ' . $indicador->nombre . ', creado',
                                    ];
                                }
                            }                            
                        });
                    }

                    if ($nombre_hoja == 'indicadores_producto') {
                        //recorrer las filas de la hoja
                        $sheet->each(function ($fila_producto) use (&$arr_normas, &$arrResponse, &$i) {
                            if(!empty(trim($fila_producto->indicador))){
                                $indice_norm = array_search($fila_producto->norma, array_column($arr_normas, 'id_norma'));
                                $norm = $arr_normas[$indice_norm];
    
                                $exists_indicador = Indicador::where('nombre', trim($fila_producto->indicador))
                                    ->where('codnorma', $norm['codnorma'])
                                    ->where('tipo', \Config::get('dominios.TIPO_EVALUACION_INDICADOR.VALORES.PROD'))
                                    ->exists();
    
                                if (!$exists_indicador) {
                                    $indicador = new Indicador([
                                        'nombre' => trim($fila_producto->indicador),
                                        'codnorma' => $norm['codnorma'],
                                        'tipo' => \Config::get('dominios.TIPO_EVALUACION_INDICADOR.VALORES.PROD')
                                    ]);
                                    $indicador->save();
                                    $arrResponse['items'][] = [
                                        'success' => true,
                                        'id'      => $indicador->codindicador,
                                        'msg'     => 'Indicador de producto: ' . $indicador->nombre . ', creado',
                                    ];
                                }
                            }
                        });
                    }
                });
            });
            $status = true;
            \DB::commit();
            $msg = "Subido Correctamente";
        } catch (\Exception $e) {
            //dd($e);
            (int) $errorCode = $e->errorInfo[0];
            if ($e instanceof \PDOException) {
                if (\App::environment() !== 'production') {
                    $msg = "Código del error: " . $errorCode . ", " . $e->getMessage();
                } else {
                    $msg = "Código del error: " . $errorCode;
                }
            } else {
                $msg = "Error Inesperado";
            }

            $arrResponse['items'][] = [
                'success' => false,
                'id'      => 0,
                'msg'     => $msg,
            ];

            $status = false;
            \DB::rollback();
            \Log::debug("Error al importar: " . $e->getMessage() . "Data: ");
        }


        $error = collect($arrResponse['items'])->first(function ($value, $key) {
            return $value['success'] == false;
        });
        if (!empty($error)) {
            $arrResponse['success'] = false;
        }
        return response()->json($arrResponse);
    }
}
