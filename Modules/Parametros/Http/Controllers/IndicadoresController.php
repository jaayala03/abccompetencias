<?php

namespace Modules\Parametros\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Crypt;
use Datatables;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Parametros\Entities\Indicador;
use Modules\Parametros\Entities\Norma;


class IndicadoresController extends Controller
{
    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar($codnormacifrado)
    {
        $norma = Norma::findOrFail(\Crypt::decrypt($codnormacifrado));
        $tipos_indicador = \Config::get('dominios.TIPO_EVALUACION_INDICADOR');

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Indicadores de la Norma',
                'componente' => $this->componente('PARAM_NOR'),
            ],
            'norma' => $norma,
            'tipos_indicador' => $tipos_indicador,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Normas', 'url' => '#'.route('parametros.normas.listar', [], false)],
            ['name' => 'Indicadores'],
        ]);

        return view('parametros::indicadores.listar');
    }
    
    public function guardar(Request $request){

        foreach ($request->indicadores_eliminar as $key1 => $indicador_eli) {
            if (isset($indicador_eli['codindicador'])) {
                $eliminar_indicador = Indicador::findOrFail($indicador_eli['codindicador']);
                $eliminar_indicador->delete();
            }
        }

        foreach ($request->ind_desempeno as $key2 => $indicador_des) {
            if(!isset($indicador_des['codindicador'])){
                $crear_indicador = new Indicador($indicador_des);
                $crear_indicador->save();
            }else{
                $editar_indicador = Indicador::findOrFail($indicador_des['codindicador']);
                $editar_indicador->fill($indicador_des);
                $editar_indicador->save();
            }
        }

        foreach ($request->ind_producto as $key3 => $indicador_prod) {
            if(!isset($indicador_prod['codindicador'])){
                $crear_indicador = new Indicador($indicador_prod);
                $crear_indicador->save();
            }else{
                $editar_indicador = Indicador::findOrFail($indicador_prod['codindicador']);
                $editar_indicador->fill($indicador_prod);
                $editar_indicador->save();
            }
        }

        $urlretorno = route('parametros.normas.listar', [], false);
        return response()->json([
            'url'     => $urlretorno,
            'message' => 'Indicadores modificados correctamente',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }
}
