// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        money: {
            decimal: ",",
            thousands: ".",
            prefix: "$ ",
            suffix: "",
            precision: 0,
            masked: false
        },
        codnorma: window.norma.codnorma,
        codigo: window.norma.codigo,
        nombre: window.norma.nombre,
        descripcion: window.norma.descripcion,
        adjunto: '',
        archivo: window.norma.archivo,
        rutaarchivo: window.norma.rutaarchivo,
        valor: window.norma.valor,
        conocimientos: window.norma.conocimientos,
        creando: false,
        conocimientos_eliminar: [],
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        agregarconocimiento() {
            this.conocimientos.push({
                nombre: ""
            });
        },
        eliminarconocimiento(index) {
            this.conocimientos_eliminar.push(this.conocimientos[index]);
            this.conocimientos.splice(index, 1);
        },
        editar_norma() {
            var self = this;
            this.creando = true;
            this.$validator.validateAll("form_editar").then(result => {
                if (result) {
                    var form = new FormData();
                    form.append("codnorma", self.codnorma);
                    form.append("file", self.adjunto);
                    form.append("codigo", self.codigo);
                    form.append("nombre", self.nombre);
                    form.append("descripcion", self.descripcion);
                    form.append("valor", self.valor);
                    $.each(self.conocimientos, function(index, val) {
                        form.append("conocimientos[]", val.nombre);
                        form.append("conocimientos_codigo[]", val.codconocimiento);
                    });
                    $.each(self.conocimientos_eliminar, function(index, val) {
                        form.append("conocimientos_eliminar[]", val.codconocimiento);
                    });

                    axios
                        .post(baseurl + "/parametros/normas/editar", form)
                        .then(function(response) {
                            var data = response.data;
                            if (data.type == "error") {
                                self.creando = false;
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            } else {
                                self.creando = false;
                                routie(data.url);
                                self.$notify({
                                    title: data.title,
                                    message: data.message,
                                    type: data.type
                                });
                            }
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                } else {
                    self.creando = false;
                    // self.$message({
                    //   showClose: true,
                    //   message: "Campos incompletos.",
                    //   type: "error"
                    // });
                }
            });
        }
    }
});
$(function() {
    $(document).on('change', '#adjunto', function() {
        app.adjunto = this.files[0];
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready(function() {
        $('#adjunto').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });
    });
});