// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        preguntas: [],
        conocimientos: [],
        normas: [],
        codnorma: "",
        codcuestionario: "",
        nombre: "",
        instrucciones: "",
        segundos_x_pregunta: 0,
        preguntas_cuestionario: [],
        preguntas_aleatorias: [],
        search: "",
        search2: "",
        totalaleatorias: 0,
        creando: false,
        loading: false,
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {
        codnorma: function(val) {
            this.conocimientos = [];
            this.preguntas = [];
            this.preguntas_cuestionario = [];
            this.preguntas_aleatorias = [];
            this.obtener_conocimientos(val);
            console.log(this.normas);
        }
    },
    methods: {
        buscarNormas(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;
                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/parametros/normas/buscarNorma", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.normas = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.normas = [];
            }
        },
        obtener_conocimientos(codnorma) {
            if (codnorma) {
                this.conocimientos = _.find(this.normas, {
                    codnorma: codnorma
                }).conocimientos;

                $.each(this.conocimientos, function(i, conocimiento) {
                    conocimiento.cantpreguntas = 0;
                    $.each(conocimiento.actividades, function(j, actividad) {
                        conocimiento.cantpreguntas = conocimiento.cantpreguntas + actividad.cantpreguntas;
                    });
                });

                console.log(this.conocimientos);

                this.nombre =
                    "Cuestionario " +
                    _.find(this.normas, { codnorma: codnorma }).codigo +
                    " - " +
                    moment().format("MM/DD/YYYY");
            } else {
                this.conocimientos = [];
                this.preguntas = [];
                this.preguntas_cuestionario = [];
                this.preguntas_aleatorias = [];
                this.totalaleatorias = 0;
                this.nombre = "";
            }
        },
        cambio(conocimiento) {
            var self = this;
            if (conocimiento.porcentaje === "") {
                conocimiento.porcentaje = 0;
            }
            cant_preg_act = Math.round(
                (conocimiento.cantpreguntas * parseFloat(conocimiento.porcentaje)) / 100
            ); //cantidad de preguntas por conocimiento
            conocimiento.preguntasaleatorias = cant_preg_act;
            self.obtener_preguntas_conocimiento(conocimiento.codconocimiento, cant_preg_act);
        },
        obtener_preguntas_conocimiento(codconocimiento, cant_preguntas) {
            var self = this;
            console.log(codconocimiento, cant_preguntas);
            var data = {
                codconocimiento: codconocimiento,
                cant_preguntas: cant_preguntas
            };
            axios
                .post(
                    baseurl + "/parametros/preguntas/obtenerpreguntas_aleatorio",
                    data
                )
                .then(function(response) {
                    var data = response.data;
                    self.preguntas_cuestionario[codconocimiento] = data;

                    self.preguntas_aleatorias = self.preguntas_cuestionario.flat(1);

                    var cant = 0;
                    $.each(self.conocimientos, function(index, value) {
                        cant = cant + value.preguntasaleatorias;
                    });
                    self.totalaleatorias = cant;
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        agregar_pregunta(index, row) {
            this.preguntas.splice(index, 1);
            this.preguntas_aleatorias.push(row);
        },
        quitar_pregunta(index, row) {
            this.preguntas_aleatorias.splice(index, 1);
            this.preguntas.push(row);
        },
        crear_cuestionario() {
            var self = this;
            this.creando = true;
            this.$validator.validateAll("form_crear").then(result => {
                if (result) {
                    var data = {
                        preguntas_cuestionario: self.preguntas_aleatorias,
                        nombre: self.nombre,
                        segundos_x_pregunta: self.segundos_x_pregunta,
                    };
                    axios
                        .post(baseurl + "/parametros/cuestionarios/crear", data)
                        .then(({ data }) => {
                            self.creando = false;
                            if (data.type !== "error") {
                                routie(data.url);
                            }
                            self.$notify({
                                title: data.title,
                                message: data.message,
                                type: data.type
                            });
                        })
                        .catch(({ response: { data } }) => {
                            self.creando = false;
                            self.$notify({
                                title: data.title,
                                message: data.message,
                                type: data.type
                            });
                        });
                } else {
                    self.creando = false;
                    // self.$message({
                    //   showClose: true,
                    //   message: "Campos incompletos.",
                    //   type: "error"
                    // });
                }
            });
        }
    }
});