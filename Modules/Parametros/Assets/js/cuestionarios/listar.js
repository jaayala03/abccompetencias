$(document).ready(function() {
    tabla_cuestionarios = $('#tbl_cuestionarios').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/parametros/cuestionarios/datatable',
            "type": "POST",
            "data": function(d) {}
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'nombre',
            name: 'nombre',
        }, {
            className: "text-center",
            data: 'preguntas',
            searchable: false,
            orderable: false
        },{
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
});

//******************************************************************************
//EVENTO AJAX PARA ELIMINAR NOTICIA
//******************************************************************************
$("#tbl_cuestionarios").on("click", ".btn-eliminar-cuestionario", function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var data = {
        "codcuestionariocifrado": id
    };
    dialogo_confirmacion_ajax({
        title: "Eliminar Pregunta",
        message: "¿Está seguro que desea eliminar?",
        labelconfirm: "Si / Eliminar",
        callbackfunction: function(data) {
            eliminar_cuestionario(data);
        },
        data: data
    });
});

function eliminar_cuestionario(data) {
    var url = baseurl + '/parametros/cuestionarios/eliminar';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function(data) {
            notifier.show(data.title, data.message, 'success', 5000);
            tabla_cuestionarios.ajax.reload();
        }
    });
}