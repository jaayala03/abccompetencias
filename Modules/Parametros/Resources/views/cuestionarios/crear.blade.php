@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/cuestionarios/crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Crear </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
{{--                     <div class="pull-left">
                        <h3 class="panel-title">Pregunta</h3>
                    </div> --}}
                    <div class="pull-right">                       
                          <a title="Guardar">
                              <button :disabled="creando" @click.prevent="crear_cuestionario()" class='btn btn-success' type="button"><i class='fa fa-save'></i> Guardar</button>
                          </a>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <form data-vv-scope="form_crear">
                        <div class="form-body">
                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.codnorma') }">
                                {{ Form::label('codnorma', 'Norma') }}
                                <el-select style="display: block;" v-model="codnorma" clearable filterable remote reserve-keyword
                                  placeholder="Ingrese codigo" :remote-method="buscarNormas" :loading="loading">
                                  <el-option v-for="item in normas" :key="item.codnorma" :label="item.codigo" :value="item.codnorma">
                                  </el-option>
                                </el-select>
                                <span v-show="errors.has('form_crear.codnorma')" class="help-block">@{{ errors.first('form_crear.codnorma') }}</span>
                            </div>  
                            
                            <template v-if="conocimientos.length > 0">
                              <div class="table-responsive" style="margin-top: -1px;">
                                <table class="table table-bordered table-primary">
                                  <thead>
                                    <tr>
                                      {{-- <th class="text-center border-right" style="width: 1%;">No.</th> --}}
                                      <th>Conocimiento</th>
                                      <th>Actividades</th>
                                      <th class="text-center">Cantidad de Preguntas</th>
                                      <th class="text-center">%</th>
                                      <th class="text-center">Cantidad Aleatorias</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr v-for="conocimiento in conocimientos">
                                      <td>
                                        @{{conocimiento.nombre}}
                                      </td>
                                      <td>
                                        <ul>
                                          <template v-for="actividad in conocimiento.actividades">
                                            <li>@{{ actividad.nombre }}</li>
                                          </template>
                                        </ul>
                                      </td>
                                      <td class="text-center">    
                                        @{{conocimiento.cantpreguntas}}
                                      </td>
                                      <td class="text-center">
                                        <input @change="cambio(conocimiento)" min="0" type="number" v-model="conocimiento.porcentaje">
                                      </td>
                                      <td class="text-center">
                                        @{{conocimiento.preguntasaleatorias}}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </template>

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.nombre')}">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input v-validate="'required'" v-model="nombre" id="nombre" name="nombre" class="form-control" type="text">
                                <span v-show="errors.has('form_crear.nombre')" class="help-block">@{{ errors.first('form_crear.nombre') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.instrucciones')}">
                                <label for="instrucciones" class="control-label">Instrucciones</label>
                                <textarea rows="4" v-model="instrucciones" id="instrucciones" name="instrucciones" class="form-control" ></textarea>
                                <span v-show="errors.has('form_crear.instrucciones')" class="help-block">@{{ errors.first('form_crear.instrucciones') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.segundos_x_pregunta')}">
                                <label for="segundos_x_pregunta" class="control-label">Segundos por pregunta</label>
                                <input v-validate="'required'" v-model="segundos_x_pregunta" id="segundos_x_pregunta" name="segundos_x_pregunta" class="form-control" type="number" min="0">
                                <span v-show="errors.has('form_crear.segundos_x_pregunta')" class="help-block">@{{ errors.first('form_crear.segundos_x_pregunta') }}</span>
                            </div><!-- /.form-group -->

                            
                            <div class="form-group form-group-divider">
                              <div class="form-inner">
                                <h4 class="no-margin">
                                <span class="label label-success">@{{totalaleatorias}} Preguntas Asignadas</span>
                                </h4>
                              </div>
                            </div>
                            
                            <div class="col-md-6">
                                <el-table
                                    :data="preguntas.filter(data => !search || data.pregunta.toLowerCase().includes(search.toLowerCase()))"
                                    style="width: 100%">

                                    <el-table-column
                                      label="Codigo"
                                      prop="codpregunta">
                                    </el-table-column>

                                    <el-table-column
                                      label="Pregunta"
                                      prop="pregunta">
                                    </el-table-column>

                                    <el-table-column
                                      align="right">
                                      <template slot="header" slot-scope="scope">
                                        <el-input
                                          v-model="search"
                                          size="mini"
                                          placeholder="Buscar"/>
                                      </template>

                                      <template slot-scope="scope">
                                        <el-button
                                          icon="el-icon-check" 
                                          circle
                                          size="mini"
                                          type="success"
                                          @click="agregar_pregunta(scope.$index, scope.row)"></el-button>
                                      </template>
                                    </el-table-column>
                                  </el-table>
                            </div>

                            <div class="col-md-6">
                                <el-table
                                    :data="preguntas_aleatorias.filter(data => !search2 || data.pregunta.toLowerCase().includes(search2.toLowerCase()))"
                                    style="width: 100%">

                                    <el-table-column
                                      label="Codigo"
                                      prop="codpregunta">
                                    </el-table-column>

                                    <el-table-column
                                      label="Pregunta"
                                      prop="pregunta">
                                    </el-table-column>

                                    <el-table-column
                                      align="right">
                                      <template slot="header" slot-scope="scope">
                                        <el-input
                                          v-model="search2"
                                          size="mini"
                                          placeholder="Buscar"/>
                                      </template>

                                      <template slot-scope="scope">
                                        <el-button
                                          icon="el-icon-delete" 
                                          circle
                                          size="mini"
                                          type="danger"
                                          @click="quitar_pregunta(scope.$index, scope.row)"></el-button>
                                      </template>
                                    </el-table-column>
                                  </el-table>
                            </div>

                        </div><!-- /.form-body -->
                    </form>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
