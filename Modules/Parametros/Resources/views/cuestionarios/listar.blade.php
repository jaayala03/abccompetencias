@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/cuestionarios/listar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Cuestionarios </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Cuestionarios</h3>
                    </div>
                    <div class="pull-right">                       
                        @if(array_key_exists("PARAM_CUEST_CUES_CRE", session("permisos")))
                            <a title="Agregar Cuestionario"  href="#{{ route('parametros.cuestionarios.cargarcrear', [], false) }}">
                                <button class='btn btn-primary' type="button"><i class='fa fa-plus'></i> Agregar Cuestionario</button>
                            </a>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                    <!-- Start datatable -->
                        <table id="tbl_cuestionarios" class="table table-danger table-bordered table-striped table-hover" style="width: 100%;" >
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Preguntas</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>                            
                        </table>
                    </div><!-- /.responsive -->                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
