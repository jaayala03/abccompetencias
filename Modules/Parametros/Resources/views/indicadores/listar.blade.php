@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/indicadores/listar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Indicadores </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
{{--                     <div class="pull-left">
                        <h3 class="panel-title">Pregunta</h3>
                    </div> --}}
                    <div class="pull-right">                       
                        <a title="Guardar">
                            <button @click.prevent="guardar()" class='btn btn-success' type="button"><i class='fa fa-save'></i> Guardar</button>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h3 class="panel-title">Desempeño</h3>
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.panel-heading -->
                                <div class="panel-body no-padding">
                                    <div class="table-responsive" style="margin-top: -1px;">
                                        <table class="table table-bordered table-success">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center">Indicadores <span class="label label-inverse">@{{ ind_desempeno.length }}<span></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <form data-vv-scope="form_desempeno">
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="form-body">
                                                                <div class="form-group" style="display: grid;">
                                                                    <div class="input-group">
                                                                        <input placeholder="Ingrese indicador..." class="form-control" type="text" name="desempeno" v-model="desempeno">
                                                                        <span class="input-group-btn">
                                                                            <button @click.prevent="agregar_desempeno()" type="button" class="btn btn-success btn-md btn-rounded"><i class="fa fa-plus"></i>Agregar</button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </td>                                                        
                                                    </tr>
                                                    <template v-if="ind_desempeno.length > 0">
                                                        <tr v-for="(desempeno, index) in ind_desempeno">
                                                            <td>
                                                                <input style="width: 100%;" type="text" v-model="desempeno.nombre">
                                                            </td>
                                                            <td class="text-center">
                                                                <button @click.prevent="eliminarindicador_des(index)" type="button" class="btn btn-danger btn-xs btn-rounded"><i class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    </template>
                                                </form>
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.panel-body -->
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h3 class="panel-title">Producto</h3>
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.panel-heading -->
                                <div class="panel-body no-padding">
                                    <div class="table-responsive" style="margin-top: -1px;">
                                        <table class="table table-bordered table-danger">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center">Indicadores <span class="label label-inverse">@{{ ind_producto.length }}<span></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <form data-vv-scope="form_producto">
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="form-body">
                                                                <div class="form-group" style="display: grid;">
                                                                    <div class="input-group">
                                                                        <input placeholder="Ingrese indicador..." class="form-control" type="text" name="producto" v-model="producto">
                                                                        <span class="input-group-btn">
                                                                            <button @click.prevent="agregar_producto()" type="button" class="btn btn-success btn-md btn-rounded"><i class="fa fa-plus"></i>Agregar</button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <template v-if="ind_producto.length > 0">
                                                        <tr v-for="(producto, index) in ind_producto">
                                                            <td>
                                                                @{{ producto.nombre }}
                                                            </td>
                                                            <td class="text-center">
                                                                <button @click.prevent="eliminarindicador_prod(index)" type="button" class="btn btn-danger btn-xs btn-rounded"><i
                                                                        class="fa fa-trash"></i></button>
                                                            </td>
                                                        </tr>
                                                    </template>
                                                </form>
                                            </tbody>
                                        </table>
                                    </div><!-- /.table-responsive -->
                                </div><!-- /.panel-body -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
