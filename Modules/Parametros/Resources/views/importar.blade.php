@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/importar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link rel="stylesheet" type="text/css" href="{{ asset('modules/parametros/css/importar.css') }}">
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Importar </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-metalic">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title"><i class="fa fa-upload"></i> Importar Parametros</h3>
                    </div>
                    <div class="pull-right">
                        <a type="button" href="{{ asset('storage/plantilla_importar.xlsx') }}" title="Plantilla" class="btn btn-success"><i class="fa fa-download"></i> Descargar Plantilla Excel</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-sh">
                    <div class="container-upload">
                      
                      <div v-show="$refs.upload && $refs.upload.dropActive" class="drop-active">
                            <h3>Soltar para agregar el archivo</h3>
                      </div>
                      <div class="upload" v-show="!isOption">
                        <animated-fade-in> 
                            <div class="upload-drop-zone" id="drop-zone" v-if="files.length == 0">
                               <i class="fa fa-hand-o-right fa-2x animated infinite slideInLeft" aria-hidden="true"></i> Arrastre el archivo para agregar aquí
                            </div>
                        </animated-fade-in>

                        <div class="table-responsive" v-show="files.length > 0">
                          <table class="table table-hover table-bordered" >
                            <thead>
                              <tr>
                                <th width="10%" class="text-center"><b>#</b></th>
                                <th width="30%" class="text-center"><b>Nombre</b></th>
                                <th width="10%" class="text-center"><b>Tamaño</b></th>
                                <th width="10%" class="text-center"><b>Estado</b></th>
                                <th width="10%" class="text-center"><b>Progreso</b></th>
                                <th width="30%" class="text-center"></th>
                              </tr>
                            </thead>
                            <tbody name="fade" is="transition-group">
                              
                              <tr :key="'row_file2'+index" v-for="(file, index) in files" :key="file.id">
                                <td class="text-center">@{{ index + 1}}</td>
                                <td class="text-center">
                                  <div class="filename">
                                    @{{file.name}}
                                  </div>
                                </td>
                                <td class="text-center"><span class="badge badge-inverse rounded">@{{file.size | formatSize }}</span></td>
                                <td v-if="file.error"> <span class="label label-danger">Error: @{{file.error}}</span> </td>
                                <td class="text-center" v-else-if="file.success"><span class="label label-success rounded">Terminado</span></td>
                                <td class="text-center" v-else-if="file.active"><span class="label label-warning rounded">Subiendo...</span></td>
                                <td class="text-center" v-else><span class="label label-warning rounded">Pendiente</span></td>
                                <td class="text-center">
                                    <div class="progress" v-if="file.active || file.progress !== '0.00'">
                                    <div :class="{'rounded': true, 'progress-bar': true, 'progress-bar-danger': file.error, 'progress-bar-success': file.progress == 100.00 }" role="progressbar" :style="{width: file.progress + '%'}">@{{file.progress}}%</div>
                                  </div>
                                  <div v-else>
                                      <span class="label label-warning">0%</span>
                                  </div>
                                </td>
                                <td class="text-center">
                                      <a v-if="file.active" title="Cancelar" :class="{'rounded':true, 'btn': true,'btn-sm': true,'btn-danger': true, disabled: !file.active}"  @click.prevent="file.active ? $refs.upload.update(file, {error: 'cancel'}) : false"><i class="fa fa-times" aria-hidden="true"></i></a>

                                      <a v-else title="Subir" :class="{'rounded':true,'btn': true,'btn-success': true, 'btn-sm': true, disabled: file.success || file.error === 'compressing'}"  v-else @click.prevent="file.success || file.error === 'compressing' ? false : $refs.upload.update(file, {active: true})"><i class="fa fa-cloud-upload" aria-hidden="true"></i></a>

                                      <a class="btn btn-warning btn-sm rounded" title="Abortar" v-if="file.active" @click.prevent="$refs.upload.update(file, {active: false})"><i class="fa fa-times" aria-hidden="true"></i></a>

                                      <a class="btn btn-primary btn-sm rounded" title="Reintentar" v-else-if="file.error && file.error !== 'compressing' && $refs.upload.features.html5" @click.prevent="$refs.upload.update(file, {active: true, error: '', progress: '0.00'})"><i class="fa fa-refresh" aria-hidden="true"></i></a>

                                      <a title="Eliminar" :class="{'rounded':true,'btn': true,'btn-danger': true, 'btn-sm': true}"  @click.prevent="eliminararchivo(file)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

                                      <a v-if="file.success" @click.prevent="vererrores(file)"  title="Ver Detalles" :class="{'rounded':true,'btn': true,'btn-warning': true, 'btn-sm': true}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="example-foorer text-center">
                         
                          <div class="btn-group">
                            <file-upload
                              class="btn btn-primary rounded dropdown-toggle"
                              :post-action="postAction"
                              :extensions="extensions"
                              :accept="accept"
                              :multiple="multiple"
                              :directory="directory"
                              :size="size || 0"
                              :thread="thread < 1 ? 1 : (thread > 5 ? 5 : thread)"
                              :headers="headersupload"
                              :drop="drop"
                              :drop-directory="dropDirectory"
                              :add-index="addIndex"
                              v-model="files"
                              @input-filter="inputFilter"
                              @input-file="inputFile"
                              ref="upload">
                              <i class="fa fa-plus"></i>
                              Examinar
                            </file-upload>

                            {{-- <div class="dropdown-menu">
                              <li><a class="dropdown-item" @click.prevent="onAddFolader">Carpeta</a></li>
                            </div> --}}

                          </div>
                          <button type="button" class="btn btn-success rounded" v-if="!$refs.upload || !$refs.upload.active" @click.prevent="$refs.upload.active = true">
                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                            Subir todos
                          </button>
                          <button type="button" class="btn btn-danger rounded"  v-else @click.prevent="$refs.upload.active = false">
                            <i class="fa fa-stop" aria-hidden="true"></i>
                            Detener
                          </button>
                        </div>
                      </div>
                  

                    </div>
                </div>
            </div>
        </div>
    </div>

    <transition-group name="fade" tab="div">
        <template v-if="file_selected.logs.length > 0">
            <div key="logs_div" class="row">
                <div class="col-md-12">
                    <div class="panel panel-accent">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title"><i class="fa fa-history"></i> Logs</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body panel-sh">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="mini-stat-type-3 shadow border-danger panel-sh">
                                        <div class="ribbon-wrapper">
                                            <span v-if="file_selected.count_failed > 0" class="ribbon ribbon-danger ribbon-shadow">Errores</span>
                                            <span v-else class="ribbon ribbon-success ribbon-shadow">Correcto</span>
                                        </div>
                                        <span class="text-uppercase text-block text-center">@{{ file_selected.name | truncate(10) }}</span>
                                        <h3 class="text-strong text-center"> @{{ file_selected.logs.length }} Registros</h3>
                                        <div class="meta-stat">
                                            <span class="pull-left">Correctos: @{{ file_selected.count_success }}</span>
                                            <span class="pull-right">Incorrectos: @{{ file_selected.count_failed }}</span>
                                            <span class="clearfix"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="table-responsive mb-20">
                                        <table class="table">
                                            <thead>
                                              <tr>
                                                  <th width="10%" class="text-center">Nro Registro</th>
                                                  <th width="40%" class="text-center">ID</th>
                                                  <th width="50%" class="text-center">Detalle</th>
                                              </tr>
                                            </thead>
                                            <tbody name="fade" is="transition-group">
                                              <tr :key="'row_dt_log'+index" :class="{'text-center':true, 'danger': !item.success, 'success': item.success }" v-for="(item, index) in file_selected.logs">
                                                  <td class="text-center">@{{ index + 1 }}</td>
                                                  <td class="text-center">@{{ item.id }}</td>
                                                  <td class="text-center">@{{ item.msg }}</td>
                                              </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </template>
    </transition-group>

</div>
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
