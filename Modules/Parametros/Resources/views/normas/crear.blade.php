@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/normas/crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style type="text/css">
.el-checkbox-button:last-child .el-checkbox-button__inner {
    border-radius: 0 0 0 0;
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Crear Norma </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
{{--                     <div class="pull-left">
                        <h3 class="panel-title">Pregunta</h3>
                    </div> --}}
                    <div class="pull-right">                       
                            <a title="Guardar">
                                <button :disabled="creando" @click.prevent="crear_norma()" class='btn btn-success' type="button"><i class='fa fa-save'></i> Guardar</button>
                            </a>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <form data-vv-scope="form_crear">
                        <div class="form-body">
                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.codigo')}">
                                <label for="codigo" class="control-label">Codigo</label>
                                <input v-validate="'required'" v-model="codigo" id="codigo" name="codigo" class="form-control" type="text">
                                <span v-show="errors.has('form_crear.codigo')" class="help-block">@{{ errors.first('form_crear.codigo') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.nombre')}">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input v-validate="'required'" v-model="nombre" id="nombre" name="nombre" class="form-control" type="text">
                                <span v-show="errors.has('form_crear.nombre')" class="help-block">@{{ errors.first('form_crear.nombre') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.valor')}">
                                <label for="valor" class="control-label">Valor</label>
                                <money v-validate="'required'" v-model="valor" id="valor" name="valor" v-bind="money" class="form-control"></money>
                                <span v-show="errors.has('form_crear.valor')" class="help-block">@{{ errors.first('form_crear.valor') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.descripcion')}">
                                <label for="descripcion" class="control-label">Descripción</label>
                                <input v-model="descripcion" id="descripcion" name="descripcion" class="form-control" type="text">
                                <span v-show="errors.has('form_crear.descripcion')" class="help-block">@{{ errors.first('form_crear.descripcion') }}</span>
                            </div><!-- /.form-group -->

                            <div :class="{'form-group': true }">

                                  <label class="control-label">Archivo:</label>
                                    <div class="input-group">

                                        <label class="input-group-btn">
                                            <span class="btn btn-success">
                                                 Examinar <i class="fa fa-file-pdf-o"></i> <input accept="application/pdf" type="file" id="adjunto" name="adjunto" style="display: none;">
                                            </span>
                                        </label>
                                        <input id="lbl_file" style="font-weight: bold" type="text" class="form-control" readonly>
                                    </div>

                              </div>

                            <div class="form-group form-group-divider">
                                <div class="form-inner">
                                    <h4 class="no-margin">
                                        <button @click.prevent="agregarconocimiento()" class='btn-xs btn btn-success' type="button"><i class="fa fa-plus"></i> Agregar Conocimiento</button>
                                    </h4>
                                </div>
                            </div>

                        <template v-for="(conocimiento, index) in conocimientos">
                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.conocimiento') }">
                                <label for="nombre">Conocimiento @{{ index+1 }} (*)</label>
                                <div class="input-group">
                                    {{Form::text('nombre', null, ['v-validate'=> "'required'", 'v-model'=>'conocimiento.nombre','class' => 'form-control', 'placeholder'=>'Ingrese nombre'])}}
                                    <span class="input-group-btn">
                                        <el-button v-if="index > 0" @click.prevent="eliminarconocimiento(index)" type="danger" icon="el-icon-delete" circle size="mini"></el-button>

                                        <el-button v-else type="danger" disabled icon="el-icon-delete" circle size="mini"></el-button>
                                    </span>
                                </div>
                                <span v-show="errors.has('form_crear.conocimiento')" class="help-block">@{{ errors.first('form_crear.conocimiento') }}</span>
                            </div>
                        </template>
                        </div><!-- /.form-body -->
                    </form>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
