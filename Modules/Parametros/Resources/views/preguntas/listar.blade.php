@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/preguntas/listar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Preguntas </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Preguntas</h3>
                    </div>
                    <div class="pull-right">                       
                        @if(array_key_exists("PARAM_CUEST_PREG_CRE", session("permisos")))
                            <a title="Agregar Pregunta"  href="#{{ route('parametros.preguntas.cargarcrear', [], false) }}">
                                <button class='btn btn-primary' type="button"><i class='fa fa-plus'></i> Agregar Pregunta</button>
                            </a>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-heading text-center">
                    <form class="form-inline">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="codnorma">Norma</label>
                                <el-select @change="actualizardatatable" v-model="codnorma" clearable filterable remote reserve-keyword
                                    placeholder="Ingrese codigo" :remote-method="buscarNormas" :loading="loading">
                                    <el-option v-for="item in normas" :key="item.codnorma" :label="item.codigo" :value="item.codnorma">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="codconocimiento">Conocimiento</label>
                                <el-select @change="actualizardatatable" v-model="codconocimiento" clearable filterable placeholder="Seleccione norma">
                                    <el-option v-for="item in conocimientos" :key="item.codconocimiento" :label="item.nombre" :value="item.codconocimiento">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="codconocimiento">Actividad</label>
                                <el-select @change="actualizardatatable" v-model="codactividad" clearable filterable placeholder="Seleccione conocimiento">
                                    <el-option v-for="item in actividades" :key="item.codactividad" :label="item.nombre" :value="item.codactividad">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                    <!-- Start datatable -->
                        <table id="tbl_preguntas" class="table table-danger table-bordered table-striped table-hover" style="width: 100%;" >
                            <thead>
                                <tr>
                                    <th>Pregunta</th>
                                    <th>Actividad</th>
                                    <th>Conocimiento</th>
                                    <th>Norma</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>                            
                        </table>
                    </div><!-- /.responsive -->                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
