@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/parametros/js/preguntas/crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style type="text/css">
.el-checkbox-button:last-child .el-checkbox-button__inner {
    border-radius: 0 0 0 0;
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-plus"></i> Crear Pregunta</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
{{--                     <div class="pull-left">
                        <h3 class="panel-title">Pregunta</h3>
                    </div> --}}
                    <div class="pull-right">                       
                            <a title="Guardar">
                                <button :disabled="creando" @click.prevent="crear_pregunta()" class='btn btn-success' type="button"><i class='fa fa-save'></i> Guardar</button>
                            </a>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <form data-vv-scope="form_crear">
                        <div class="form-body">

                            <div class="form-group">
                                <label for="codnorma">Norma</label>
                                <el-select style="display: block;" v-model="codnorma" clearable filterable remote reserve-keyword
                                    placeholder="Ingrese codigo" :remote-method="buscarNormas" :loading="loading">
                                    <el-option v-for="item in normas" :key="item.codnorma" :label="item.codigo" :value="item.codnorma">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="codconocimiento">Conocimiento</label>
                                <el-select style="display: block;" v-model="codconocimiento" clearable filterable
                                    placeholder="Seleccione norma">
                                    <el-option v-for="item in conocimientos" :key="item.codconocimiento" :label="item.nombre"
                                        :value="item.codconocimiento">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->                    

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.codactividad') }">
                                {{ Form::label('codactividad', 'Actividad') }}
                                <el-select style="display: block;" v-model="codactividad" clearable filterable placeholder="Seleccione conocimiento">
                                    <el-option v-for="item in actividades" :key="item.codactividad" :label="item.nombre"
                                        :value="item.codactividad">
                                    </el-option>
                                </el-select>
                                <span v-show="errors.has('form_crear.codactividad')" class="help-block">@{{ errors.first('form_crear.codactividad') }}</span>
                            </div>

                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.pregunta')}">
                                <label for="pregunta" class="control-label">Pregunta</label>
                                <input v-validate="'required'" v-model="pregunta" id="pregunta" name="pregunta" class="form-control" type="text">
                                <span v-show="errors.has('form_crear.pregunta')" class="help-block">@{{ errors.first('form_crear.pregunta') }}</span>
                            </div><!-- /.form-group -->
                            <div class="form-group form-group-divider">
                                <div class="form-inner">
                                    <h4 class="no-margin">
                                        <button @click.prevent="agregaropcion()" class='btn-xs btn btn-success' type="button"><i class="fa fa-plus"></i> Agregar Opción de Respuesta</button>
                                    </h4>
                                </div>
                            </div>

                        <template v-for="(opcion, index) in opciones">
                            <div :class="{'form-group':true, 'has-error': errors.has('form_crear.correcta') || errors.has('form_crear.opcion') }">
                                <label for="opcion">Opción @{{ index+1 }} (*)</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <el-checkbox-group v-model="opcion.correcta" size="medium">
                                          <el-checkbox-button>Correcta</el-checkbox-button>
                                        </el-checkbox-group>
                                    </span>
                                    {{Form::text('opcion', null, ['v-validate'=> "'required'", 'v-model'=>'opcion.opcion','class' => 'form-control', 'placeholder'=>'Ingrese opcion'])}}
                                    <span class="input-group-btn">
                                        <el-button v-if="index > 0" @click.prevent="eliminaropcion(index)" type="danger" icon="el-icon-delete" circle size="mini"></el-button>

                                        <el-button v-else type="danger" disabled icon="el-icon-delete" circle size="mini"></el-button>
                                    </span>
                                </div>
                                <span v-show="errors.has('form_crear.correcta')" class="help-block">@{{ errors.first('form_crear.correcta') }}</span>
                                <span v-show="errors.has('form_crear.opcion')" class="help-block">@{{ errors.first('form_crear.opcion') }}</span>
                            </div>
                        </template>
                        </div><!-- /.form-body -->
                    </form>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
