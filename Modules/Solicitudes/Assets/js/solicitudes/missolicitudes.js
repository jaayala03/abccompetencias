$(document).ready(function () {

    

    $('#tbl_missolicitudes').on('click', '.btn-pay', function () {



    });

    tabla_noticias = $('#tbl_missolicitudes').DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/solicitudes/todosdatosmissolicitudes',
            "type": "POST",
            "data": function (d) {}
        },
        "fnDrawCallback": function (oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $('.image-lightbox').featherlight({
                type: 'image'
            });
        },
        columns: [{
            data: 'codigo',
            name: 'codigo',
        }, {
            data: 'nombre',
            name: 'nombre'
        }, {
            data: 'fecha',
            name: 'fecha',
        }, {
            className: "text-center",
            data: 'estado',
            searchable: false,
            orderable: false
        }, {
            className: "text-center",
            data: 'opciones',
            searchable: false,
            orderable: false
        }]
    });

    $("#tbl_missolicitudes_vigentes").on("click", ".veragendamientos", function (e) {
        e.preventDefault();
        var codsolicitudcifrado = $(this).data("id");
        app.modalveragendamientos(codsolicitudcifrado);
    });
});

// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        solicitud: [],
        solicitudes: [],
        loading: false
    },
    components: {},
    computed: {},
    mounted: function () {
        this.fetchSolicitudes();
        handler = ePayco.checkout.configure({
            key: config_epayco.key,
            test: config_epayco.test
        });
    },
    watch: {

    },
    methods: {
        pagar(codsolicitud) {
            var self = this;
            // var codsolicitud = $(this).data('codsolicitud');
            swal({
                title: 'Preparando el pago',
                html: 'Por favor espere...',
                showCloseButton: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                focusConfirm: false,
                showCancelButton: false,
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    swal.showLoading()
                },
                onOpen: function () {
                    axios.post(baseurl + '/solicitudes/enlacepago', {
                            codsolicitud: codsolicitud
                        })
                        .then(function (response) {
                            var data = response.data;
                            if (data.paid) {
                                swal('Atención', 'El certificado ya registra pago.', 'success');
                                self.fetchSolicitudes();
                            } else if (data.state == 3) {
                                swal('Atención', 'Transacción pendiente por validación.', 'warning');
                                self.fetchSolicitudes();
                            } else {
                                var pagar = function () {
                                    var dataepayco = {
                                        //Parametros compra (obligatorio)
                                        name: data.description,
                                        description: data.description,
                                        invoice: data.referenceCode,
                                        currency: "cop",
                                        amount: data.amount,
                                        tax_base: "0",
                                        tax: "0",
                                        country: "CO",
                                        lang: "es",

                                        //Onpage="false" - Standard="true"
                                        external: "true",

                                        //Atributos opcionales
                                        extra1: data.codsolicitud,
                                        // extra2: "extra2",
                                        // extra3: "extra3",
                                        confirmation: data.confirmationUrl,
                                        response: data.responseUrl,

                                        //Atributos cliente
                                        name_billing: data.buyerName,
                                        // address_billing: "",
                                        type_doc_billing: "cc",
                                        // mobilephone_billing: "3050000000",
                                        number_doc_billing: data.buyerDoc,
                                        email_billing: data.buyerEmail
                                    }
                                    handler.open(dataepayco)

                                }
                                swal.close()

                                swal({
                                    title: '<strong>Atención</strong>',
                                    type: 'info',
                                    html: " <b>Va a ser redirigido a la plataforma de pagos, al finalizar la transacción retornará al sistema.</b>",
                                    showCloseButton: false,
                                    allowEscapeKey: false,
                                    allowEnterKey: false,
                                    focusConfirm: false,
                                    showCancelButton: true,
                                    allowOutsideClick: false,
                                    confirmButtonText: '<i class="fa fa-credit-card"></i> Ir a pagar',
                                    confirmButtonAriaLabel: 'Ir a pagar',
                                    cancelButtonText: '<i class="fa fa-times"></i> Cancelar',
                                    cancelButtonAriaLabel: 'Cancelar',
                                    cancelButtonColor: "#ff0000",
                                }).then((result) => {
                                    if (result.value) {
                                        pagar()
                                    } else {
                                        tabla_noticias.ajax.reload(null, false);
                                    }
                                })
                            }

                        })
                        .catch(error => {
                            console.log(error.response);
                            swal('Error', 'No se ha podido generar el enlace de pago.', 'error');
                        });
                },
                onClose: () => {

                }
            })
        },
        fetchSolicitudes() {
            this.loading = true;
            axios.get('/solicitudes/todosdatosmissolicitudes')
                .then(response => {
                    // handle success
                    this.solicitudes = response.data;
                    this.loading = false;
                })
                .catch(error => {
                    // handle error
                    console.log(error);
                    this.loading = false;
                })
        },
        /**
         * metodo abrir un modal
         */
        showmodal(name) {
            this.$refs[name].open();
        },
        /**
         * metodo cerrar un modal
         */
        hidemodal(name) {
            this.$refs[name].close();
        },
        format_date(fecha) {
            return moment(fecha).format('YYYY/M/DD');
        },
        modalveragendamientos(codsolicitudcifrado) {
            this.solicitud = [];
            var datos = {
                codsolicitudcifrado: codsolicitudcifrado,
            };
            var self = this;
            axios
                .post(baseurl + "/solicitudes/getSolicitud", datos)
                .then(function (response) {
                    var data = response.data;
                    self.solicitud = data;
                    self.showmodal("modal_veragendamientos");

                })
                .catch(function (error) {
                    self.solicitud = [];
                    console.log(error);
                });
        }

    }
});