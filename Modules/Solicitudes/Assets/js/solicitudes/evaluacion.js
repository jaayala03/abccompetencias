// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        evaluacionsolicitud: window.evaluacionsolicitud,
        codevaluacionsolicitudcifrado: window.codevaluacionsolicitudcifrado,
        fecha_resultados: "",
        indicadores: window.indicadores,
        evaluadores: window.evaluadores,
        solicitud: window.solicitud,
        cumple: 0,
        no_cumple: 0,
    },
    components: {},
    computed: {},
    mounted: function() {
        this.calcularresultado();
        $("#btn_evaluar").attr('disabled', 'disabled');
    },
    watch: {
        cumple: function(val, old) {
            if (val <= 0) {
                $("#btn_evaluar").attr('disabled', 'disabled');
            } else {
                $("#btn_evaluar").removeAttr('disabled');
            }
        }
    },
    methods: {
        registrarvaloracion(indicador) {
            var self = this;
            axios
                .post(baseurl + "/solicitudes/registrarvaloracion", indicador)
                .then(function(response) {
                    var data = response.data;
                    if (data.type == "success") {
                        self.indicadores = data.indicadores;
                        self.calcularresultado();
                    } else {
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        registrarevaluacionsolicitud() {
            var self = this;
            axios
                .post(
                    baseurl + "/solicitudes/registrarevaluacionsolicitud",
                    self.evaluacionsolicitud
                )
                .then(function(response) {
                    var data = response.data;
                    if (data.type == "success") {
                        self.evaluacionsolicitud = data.evaluacionsolicitud;
                    } else {
                        self.$notify({
                            title: data.title,
                            message: data.message,
                            type: data.type
                        });
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        formatfecha(fecha) {
            return moment(fecha).format("DD-MM-YYYY");
        },
        calcularresultado(){
            const total_indicadores = this.indicadores.length;
            if(total_indicadores > 0){
                const cumplen = this.indicadores.filter(item => item.cumple);
                const no_cumplen = this.indicadores.filter(item => !item.cumple);
    
                this.cumple = (cumplen.length * 100)/total_indicadores;
                this.no_cumple = (no_cumplen.length * 100)/total_indicadores;
    
                $('#cumple').html(this.cumple.toFixed(2)+'%');
                $('#no_cumple').html(this.no_cumple.toFixed(2)+'%');
            }
        },
        generarpdf() {
            var url =
                baseurl +
                laroute.url("/solicitudes/generarpdfevaluacion", [
                    this.codevaluacionsolicitudcifrado
                ]);
            var win = window.open(url, "_blank");
            win.focus();
        },
        evaluar(){
            var self = this;
            this.$validator.validateAll("form_evaluar").then(result => {
                if (result) {
                    $("#btn_evaluar").attr('disabled', 'disabled');
                    swal({
                        showCloseButton: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        focusConfirm: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        title: 'Guardando la Evaluacón',
                        html: '<span class="label label-success rounded">Por favor espere...</span>',
                        onBeforeOpen: () => {
                            swal.showLoading()
                        },
                        onOpen: function () {
                            axios
                            .post(baseurl + "/solicitudes/evaluar", {
                                evaluacionsolicitud : self.evaluacionsolicitud,
                                cumple: self.cumple.toFixed(2),
                                no_cumple: self.no_cumple.toFixed(2),
                            })
                            .then(function(response) {
                                $("#btn_evaluar").removeAttr('disabled');
                                var data = response.data;
                                
                                swal.close();
                                swal('Éxito', 'Evaluación guardada correctamente.', 'success');
                            })
                            .catch(function(error) {
                                console.log(error)
                                swal('Error', 'No se ha podido guardar la evaluación.', 'error')
                            });
                            },
                            onClose: () => {
                
                            }
                    });
                } else {
                    self.$message({
                        showClose: true,
                        message: "Campos incompletos.",
                        type: "error"
                    });
                }
            });
        }
    }
});