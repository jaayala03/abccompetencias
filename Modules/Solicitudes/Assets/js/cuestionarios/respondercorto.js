// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        cuestionario: {},
        solicitud: {},
        preguntas: {
            currentpage: 1,
            perpage: 50,
            items: [],
            datapaginate: []
        },
        finalizado: false
    },
    components: {},
    computed: {
        respondidas() {
            return this.preguntas.items.filter(function (a) {
                return a.value;
            });
        },
        no_respondidas() {
            return this.preguntas.items.filter(function (a) {
                return !a.value;
            });
        }
    },
    mounted: function () {
        this.preguntas.items = window.preguntas.map((ob, index) => {
            ob.numero = index + 1;
            return ob;
        });

        if (this.preguntas.items.length > 0) {
            this.preguntas.datapaginate = this.preguntas.items.slice(0, this.preguntas.items.length / (this.preguntas.items.length / this.preguntas.perpage))
        }
        this.cuestionario = window.cuestionario;
        this.solicitud = window.solicitud;


        $(document).on('click', '.go-to', function(e){
            setTimeout(() => { window.location.hash = $(this).data('href') }, 500);
        });
    },
    watch: {},
    methods: {
        cantPregJoin(item) {
            var s = [];
            _.forEach(item.opciones, function (value, index) {
                s.push(value.codopcion);
            });
            return s
        },
        finalizar() {
            var self = this;
            // this.$validator.validateAll('formC').then((result) => {
            //     if (result) {
            self.finalizado = true;
            if (this.respondidas.length > 0) {
                var mensaje = 'Usted ha respondido ' + this.respondidas.length + ' pregunta(s) de un todal de ' + this.preguntas.items.length + ' pregunta(s).';

                if (this.no_respondidas.length > 0) {
                    mensaje += '<br> <strong>Preguntas faltantes:</strong>';
                    mensaje += '<ul style="list-style: none">';
                    _.forEach(this.no_respondidas, function (value, index) {
                        mensaje += '<li><a class="go-to" href="#" data-href="#block_pregunta_'+value.codpregunta+'">' + value.numero  +'. '+ value.pregunta.substring(0,22) +'...</a></li>';
                    });
                    mensaje += '<ul>';
                }

                self.$confirm(mensaje, '¿Está seguro que desea enviar el cuestionario?', {
                    confirmButtonText: 'Si / Enviar',
                    cancelButtonText: 'Cancelar',
                    type: 'warning',
                    center: true,
                    dangerouslyUseHTMLString: true
                }).then(() => {
                    window.onbeforeunload = function(e) {
                        
                    };
                    route = 'solicitudes.cuestionarios.finalizar';
                    var url = baseurl + laroute.route(route, {
                        codcuestionariocifrado: window.codcuestionario_solicitudcifrado,
                    });
                    window.location.href = url;
                }).catch(() => { });
            }
            //     }
            // });
        },
        onCurrentPageP(page) {
            this.preguntas.datapaginate = this.preguntas.items.slice((page * this.preguntas.perpage) - this.preguntas.perpage, page * this.preguntas.perpage);
        },
        onChangeResp(pregunta) {
            //console.log(pregunta.value)
            var opciones = _.cloneDeep(pregunta.opciones);;
            // var opcion = opciones.find((item) => item.codopcion = pregunta.value);
            var opcion = _.find(opciones, {codopcion: pregunta.value});
            var data = (opcion) ? opcion : {codpregunta: pregunta.codpregunta};
            data.codcuestionario_solicitudcifrado = window.codcuestionario_solicitudcifrado;
            data.codpregunta_cuestionario = pregunta.codpregunta_cuestionario;
            // if(!opcion) {
            //     pregunta.value = null;
            // }
            axios.post(baseurl + '/solicitudes/cuestionarios/preguntas/responder', data)
            .then((response) => {
                //console.log(response);
                var data = response.data;   
            })
            .catch((error) => {
                swal('Error', 'Ha ocurrido un error al guardar', 'error');
            });
        },
        onClearResp(pregunta) {
           
        }
    }
});