// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        cuestionario: {},
        solicitud: {},
        preguntas: {
            currentpage: 1,
            perpage: 10,
            items: [],
            datapaginate: []
        },
        finalizado: false
    },
    components: {},
    computed: {
        respondidas() {
            return this.preguntas.items.filter(function (a) {
                return a.value;
            });
        },
        no_respondidas() {
            return this.preguntas.items.filter(function (a) {
                return !a.value;
            });
        }
    },
    mounted: function () {
        this.preguntas.items = window.preguntas.map((ob, index) => {
            ob.numero = index + 1;
            return ob;
        });

        if (this.preguntas.items.length > 0) {
            this.preguntas.datapaginate = this.preguntas.items.slice(0, this.preguntas.items.length / (this.preguntas.items.length / this.preguntas.perpage))
        }
        this.cuestionario = window.cuestionario;
        this.solicitud = window.solicitud;
        this.rep_tiempo();

        window.onbeforeunload = function(e) {
            e = e || window.event;
            if (e) {
                e.returnValue = 'ADVERTENCIA: va a salir del cuestionario';
            }
            return 'ADVERTENCIA: va a salir del cuestionario';
        };

        $(document).on('click', '.go-to', function(e){
            setTimeout(() => { window.location.hash = $(this).data('href') }, 500);
        });
    },
    watch: {},
    methods: {
        cantPregJoin(item) {
            var s = [];
            _.forEach(item.opciones, function (value, index) {
                s.push(value.codopcion);
            });
            return s
        },
        finalizar() {
            var self = this;
            // this.$validator.validateAll('formC').then((result) => {
            //     if (result) {
            self.finalizado = true;
            if (this.respondidas.length > 0) {
                var mensaje = 'Usted ha respondido ' + this.respondidas.length + ' pregunta(s) de un todal de ' + this.preguntas.items.length + ' pregunta(s).';

                if (this.no_respondidas.length > 0) {
                    mensaje += '<br> <strong>Preguntas faltantes:</strong>';
                    mensaje += '<ul style="list-style: none">';
                    _.forEach(this.no_respondidas, function (value, index) {
                        mensaje += '<li><a class="go-to" href="#" data-href="#block_pregunta_'+value.codpregunta+'">' + value.numero  +'. '+ value.pregunta.substring(0,22) +'...</a></li>';
                    });
                    mensaje += '<ul>';
                }

                self.$confirm(mensaje, '¿Está seguro que desea enviar el cuestionario?', {
                    confirmButtonText: 'Si / Enviar',
                    cancelButtonText: 'Cancelar',
                    type: 'warning',
                    center: true,
                    dangerouslyUseHTMLString: true
                }).then(() => {
                    window.onbeforeunload = function(e) {
                        
                    };
                    route = 'solicitudes.cuestionarios.finalizar';
                    var url = baseurl + laroute.route(route, {
                        codcuestionariocifrado: window.codcuestionario_solicitudcifrado,
                    });
                    window.location.href = url;
                }).catch(() => { });
            }
            //     }
            // });
        },
        onCurrentPageP(page) {
            this.preguntas.datapaginate = this.preguntas.items.slice((page * this.preguntas.perpage) - this.preguntas.perpage, page * this.preguntas.perpage);
        },
        onChangeResp(opcion, pregunta) {
            var data = opcion;
            data.codcuestionario_solicitudcifrado = window.codcuestionario_solicitudcifrado;
            data.codpregunta_cuestionario = pregunta.codpregunta_cuestionario;
            axios.post(baseurl + '/solicitudes/cuestionarios/preguntas/responder', data)
                .then(function (response) {
                    //console.log(response);
                })
                .catch(function (error) {
                    swal('Error', 'Ha ocurrido un error al guardar', 'error');
                });
        },
        contar_tiempo(data) {
            var minutos = Math.floor(data.segundos_restantes / 60);
            var segundos = (data.segundos_restantes - minutos * 60) + 2;

            var duracion = moment.duration({
                'hours': 0,
                'minutes': minutos,
                'seconds': segundos
            });

            var intervalo = 1;
            var conteo = setInterval(function () {
                duracion = moment.duration(duracion.asSeconds() - intervalo, 'seconds');
                var horas = duracion.hours();
                var min = duracion.minutes();
                var seg = duracion.seconds();

                seg -= 1;
                if (min < 0) {
                    return clearInterval(conteo);
                }
                if (min < 10 && min.length != 2) {
                    min = '0' + min;
                }
                if (seg < 0 && min != 0) {
                    min -= 1;
                    seg = 59;
                } else if (seg < 10 && length.seg != 2) {
                    seg = '0' + seg;
                }

                if (horas < 10 && length.horas != 2) {
                    horas = '0' + horas;
                }

                $('#countdown').text('Tiempo restante: ' + horas + ':' + min + ':' + seg);
                if (horas == 0 && min == 0 && seg == 0) {
                    window.onbeforeunload = function(e) {
                        
                    };
                    clearInterval(conteo);
                    route = 'solicitudes.cuestionarios.tiempo_finalizado';
                    var url = baseurl + laroute.route(route, {
                        codcuestionariocifrado: window.codcuestionario_solicitudcifrado,
                    });
                    window.location.href = url;
                }
            },
                1000);
        },
        rep_tiempo() {
            var self = this;
            axios.post(baseurl + '/solicitudes/cuestionarios/rep_tiempo', { d: moment().valueOf(), codsolicitud: this.solicitud.codsolicitud })
                .then(function (response) {
                    self.contar_tiempo(response.data);
                })
                .catch(function (error) {
                    swal({
                        type: 'error',
                        title: 'Error',
                        text: 'Ha ocurrido un error inesperado',
                        showCancelButton: false,
                        showConfirmButton: true,
                        confirmButtonText: 'Entendido',
                        showCloseButton: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        focusConfirm: false,
                        allowOutsideClick: false,
                    }).then((result) => {
                        if (result.value) {
                            var url = baseurl + laroute.route('home', {
                                hash: '/solicitudes/missolicitudes',
                            });
                            window.location.href = url;
                        }
                    })
                });
        }
    }
});