<?php

namespace Modules\Solicitudes\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;

//Paquetes
use Modules\Solicitudes\Entities\Titulacion;
use Modules\Solicitudes\Entities\Norma;
use Modules\Solicitudes\Entities\Solicitud;
use Modules\Solicitudes\Entities\Tipo_titulacion;
use Modules\Solicitudes\Entities\Usuario;
use Modules\Solicitudes\Entities\Cuestionario;
use Modules\Solicitudes\Entities\PagoSolicitud;
use ABCcomptencias\Mail\RegistroSolicitud;
use Mail;

//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;

//**********************************************************************

class PagosSolicitudesController extends Controller
{

    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detalle(Request $request)
    {
        //dd($request);
        $codpagosolicitud = \Crypt::decrypt($request->codpagosolicitud);
        $PagoSolicitud = PagoSolicitud::findOrFail($codpagosolicitud);

        $codsolicitud = $PagoSolicitud->codsolicitud;
        $Solicitud = Solicitud::find($codsolicitud);
        $Norma = $Solicitud->norma;

        $referenceCode = $request->referenceCode;
        $amount = $Norma->valor;
        $currency = 'COP';

        $data = json_decode($PagoSolicitud->respuesta, true);
        $data['transactionState'] = (int) $PagoSolicitud->state;


        View::share('parametros', [
            'meta_title' => 'Solicitudes',
            'componente' => $this->componente('SOLIC_TODAS'),
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Respuesta pago'],
        ]);

        $data['transactionId'] = $PagoSolicitud->transaction_id;
        $data['value'] = $PagoSolicitud->value;
        $data['reference_pol'] = $PagoSolicitud->reference;
        $data['currency'] = $PagoSolicitud->currency;
        $data['description'] = $PagoSolicitud->description;
        $data['franchise'] = $PagoSolicitud->franchise;
        

        // dd($data);
        //session()->forget('firmacreada');

        return view('solicitudes::respuesta_pago')->with($data);
    }
  
}
