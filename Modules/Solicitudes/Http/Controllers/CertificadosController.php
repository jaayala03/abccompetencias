<?php

namespace Modules\Solicitudes\Http\Controllers;

use Storage;
use JavaScript;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Illuminate\Support\Facades\View;
use Modules\Solicitudes\Entities\Opcion;
use ABCcomptencias\Libraries\FPDF_CellFit;
use ABCcomptencias\Libraries\Fpdf_formato;
use Modules\Solicitudes\Entities\Respuesta;
use Modules\Solicitudes\Entities\Solicitud;
use Modules\Solicitudes\Entities\Certificado;
//Traits
use Modules\Solicitudes\Entities\Cuestionario;

class CertificadosController extends Controller
{

    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ver($codcertificadocifrado)
    {
        
        $certificado = Certificado::findOrFail(\Crypt::decrypt($codcertificadocifrado));
        $solicitud = $certificado->solicitud;
        $usuario = $solicitud->usuario;
        $norma = $solicitud->norma;

        $pdf          = new FPDF_CellFit();

        $pdf->SetTitle(utf8_decode('Ver Certificado'));
        
        $pdf->AddPage('L');
        $pdf->SetMargins(15, 25 , 15);

        
        $pdf->Ln(30);
        //seccion datos del producto
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetDrawColor(237, 237, 237);

        $x= $pdf->getX();
        $y= $pdf->getY();
        $pdf->Cell(132, 80, utf8_decode(''), 1, 0, 'L');
        $pdf->Cell(132, 80, utf8_decode(''), 1, 1, 'L');

        //Imagen
        $ruta_nombre = '';
        if (!empty($usuario->imagencodificada)) {
            $nombre_imagen_codificado = $usuario->imagencodificada;
            $subruta                  = \Config::get('paths.IMAGENES_PERFIL') . $nombre_imagen_codificado;
            $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
        } else {
            $nombre_imagen_codificado = \Config::get('dominios.DEFAULT_IMG_USER');
            $subruta                  = \Config::get('paths.IMAGENES_PERFIL') . $nombre_imagen_codificado;
            $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
        }

        $pdf->Image($ruta_nombre, $x + 5, $y + 5, 40, 50);

        $pdf->SetFont('Arial', 'B', 43);
        $pdf->SetTextColor(255, 192, 203);

        
        $pdf->Image(asset('images/logo2.png'), $x + 65, $y + 2, 45, 20);
        $pdf->Image(asset('images/logo2marca.png'), $x + 150, $y + 6, 100, 60);
                
        if(\Carbon::parse($certificado->fecha_vencimiento)->format('d-m-Y') <= now()->format('d-m-Y')){
            $pdf->RotatedText(50, 95, 'V E N C I D O', 20);
            $pdf->RotatedText(170, 95, 'V E N C I D O', 20);
        }
        
        $pdf->setY($y+22);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(62, 66, 95);
        $pdf->CellFitScale(50, 7, utf8_decode(''), 0, 0, 'L');
        $pdf->CellFitScale(20, 7, utf8_decode('Código:'), 0, 0, 'L');
        $pdf->CellFitScale(60, 7, utf8_decode($certificado->codigo), 0, 1, 'L');

        $pdf->CellFitScale(50, 7, utf8_decode(''), 0, 0, 'L');
        $pdf->CellFitScale(20, 7, utf8_decode('Nombre:'), 0, 0, 'L');
        $pdf->CellFitScale(60, 7, utf8_decode($usuario->nombrecompleto), 0, 1, 'L');

        $pdf->CellFitScale(50, 7, utf8_decode(''), 0, 0, 'L');
        $pdf->CellFitScale(20, 7, utf8_decode('C.C:'), 0, 0, 'L');
        $pdf->CellFitScale(60, 7, utf8_decode(number_format($usuario->documento, 0, '.', '.')), 0, 1, 'L');

        $pdf->CellFitScale(50, 7, utf8_decode(''), 0, 0, 'L');
        $pdf->CellFitScale(20, 7, utf8_decode('F. Expedición:'), 0, 0, 'L');
        $pdf->CellFitScale(60, 7, utf8_decode(\Carbon::parse($certificado->fecha)->format('d-m-Y')), 0, 1, 'L');

        $pdf->CellFitScale(50, 7, utf8_decode(''), 0, 0, 'L');
        $pdf->CellFitScale(20, 7, utf8_decode('F. Vencimiento:'), 0, 0, 'L');
        $pdf->CellFitScale(60, 7, utf8_decode(\Carbon::parse($certificado->fecha_vencimiento)->format('d-m-Y')), 0, 1, 'L');


        $pdf->setY($y+62);
        $pdf->SetFillColor(0, 177, 225);
        // $pdf->Cell(132, 20, utf8_decode(''), 1, 0, 'L', true);
        $pdf->Image(asset('images/targ.png'), $x, $y + 48, 132, 32);
        $pdf->Image(asset('images/qr.png'), $x + 110, $y + 60, 20, 20);

        $pdf->setX($x);
        $pdf->setY($y+66);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setX($x);
        $pdf->MultiCell(110, 3, utf8_decode($norma->nombre), 0, 'C', false);
        $pdf->setX($x);
        $pdf->Cell(110, 6, utf8_decode($norma->codigo), 0, 0, 'C', false);


        $pdf->SetFont('Arial', 'B', 12);
        
        $pdf->setY($y+42);
        $pdf->setX($x+175);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(0, 0, 0);
        $pdf->Cell(50, 6, utf8_decode('Firma Autorizada'), 'T', 0, 'C', false);
        $pdf->Image(asset('images/firma_cert.png'), $x + 173, $y + 33, 50, 20);

        $pdf->Code128($x + 153,  $y + 64, md5($certificado->codigo), 95, 8);

        $pdf->setY($y+72);
        $pdf->setX($x+155);
        $pdf->SetFont('Arial', 'B', 6);
        $pdf->Cell(90, 4, utf8_decode(md5($norma->codigo)), 0, 0, 'C', false);

        
        // ob_end_clean();
        $pdf->Output("Certificado_".$certificado->codigo.".pdf", 'I');
    }
}
