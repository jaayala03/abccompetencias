<?php

namespace Modules\Solicitudes\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use DB;
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;

//Paquetes
use Modules\Solicitudes\Entities\Titulacion;
use Modules\Solicitudes\Entities\Norma;
use Modules\Solicitudes\Entities\Solicitud;
use Modules\Solicitudes\Entities\Tipo_titulacion;
use Modules\Solicitudes\Entities\Usuario;
use Modules\Solicitudes\Entities\Cuestionario;
use Modules\Solicitudes\Entities\PagoSolicitud;
use Modules\Solicitudes\Entities\Valoracion;
use Modules\Solicitudes\Entities\Indicador;
use Modules\Solicitudes\Entities\EtapaSolicitud;
use Modules\Solicitudes\Entities\Certificado;
use ABCcomptencias\Mail\RegistroSolicitud;
use ABCcomptencias\Libraries\Fpdf_formato;
use Mail;

//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;
use Modules\Solicitudes\Entities\Agendada;
use Modules\Solicitudes\Entities\CuestionarioSolicitud;
use ABCcomptencias\Mail\SolicitudesAgendadas;
use Modules\Solicitudes\Entities\Evaluacion_solicitud;
use Modules\Solicitudes\Entities\Indicador_evaluacion;
use Modules\Solicitudes\Entities\Valoracion_indicador;

//**********************************************************************

class SolicitudesController extends Controller
{

    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth')->except('confirmacion_pago','respuesta_pago');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function registrar()
    {
        $titulaciones = Titulacion::where('estado', 1)->get();
        $normas = Norma::where('estado', 1)->get();
        $tipos_titulacion = Tipo_titulacion::all();
        $tipos_titulacion = Tipo_titulacion::all();

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Registrar Solicitud',
                'componente' => $this->componente('SOLIC_REG'),
            ],
            'titulaciones' => $titulaciones,
            'normas' => $normas,
            'tipos_titulacion' => $tipos_titulacion,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Registrar Solicitud'],
        ]);


        return view('solicitudes::registrar');
    }

    public function registrar_titulaciones(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                foreach ($request->solicitud as $key => $solicitud) {
                    $titulacion = Titulacion::find($solicitud['codtitulacion']);
                    $solicitud_pendientes = Solicitud::join('etapas_solicitudes', 'solicitudes.codsolicitud', '=', 'etapas_solicitudes.codsolicitud')
                    ->where('solicitudes.codtitulacion', $solicitud['codtitulacion'])
                    ->whereIn('etapas_solicitudes.etapa', [1,2,3,4,5,6])
                    ->where('etapas_solicitudes.ultima', true)
                    ->where('solicitudes.codusuario', \Auth::user()->codusuario)
                    ->count();

                    if($solicitud_pendientes > 0) {
                        throw new \Exception("Usted ya tiene una solicitud vigente para la titulacion: ".$titulacion->codigo);
                    }

                    $solicitud = new Solicitud([
                        'codusuario' => \Auth::user()->codusuario,
                        'codtitulacion' => $solicitud['codtitulacion'],
                        'fecha' => now(),
                    ]);
                    $solicitud->save();
        
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.inscrito'),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }
        
                $ruta_descargas = route('outside.descargas', [], true);
        
                Mail::to(\Auth::user()->email)
                    ->send(new RegistroSolicitud([
                        'usuario' => \Auth::user(),
                        'ruta_descargas' => $ruta_descargas,
                        'solicitud' => $request->solicitud,
                    ]));
            }, 5);
                
            return response()->success();
        } catch (\Exception $e) {
            //dd($e);
            $message = __('messages.error');
            if ($e instanceof \Throwable) {
                $message = $e->getMessage();
                return response()->error(400, $message);
            }

            return response()->error(500, $message);
        }
    }

    public function registrar_normas(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {

                foreach ($request->solicitud as $key => $solicitud) {
                    $norma = Norma::find($solicitud['codnorma']);
                    $solicitud_pendientes = Solicitud::join('etapas_solicitudes', 'solicitudes.codsolicitud', '=', 'etapas_solicitudes.codsolicitud')
                    ->where('solicitudes.codnorma', $solicitud['codnorma'])
                    ->whereIn('etapas_solicitudes.etapa', [1,2,3,4,5,6])
                    ->where('etapas_solicitudes.ultima', true)
                    ->where('solicitudes.codusuario', \Auth::user()->codusuario)
                    ->count();

                    if($solicitud_pendientes > 0) {
                        throw new \Exception("Usted ya tiene una solicitud vigente para la norma: ".$norma->codigo);
                    }
                    
                    $solicitud = new Solicitud([
                        'codusuario' => \Auth::user()->codusuario,
                        'codnorma' => $solicitud['codnorma'],
                        'fecha' => now(),
                    ]);
                    $solicitud->save();
        
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.inscrito'),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }
        
                $ruta_descargas = route('outside.descargas', [], true);
        
                Mail::to(\Auth::user()->email)
                    ->send(new RegistroSolicitud([
                        'usuario' => \Auth::user(),
                        'ruta_descargas' => $ruta_descargas,
                        'solicitud' => $request->solicitud,
                    ]));
            }, 5);
                
            return response()->success();
        } catch (\Exception $e) {
            //dd($e);
            $message = __('messages.error');
            if ($e instanceof \Throwable) {
                $message = $e->getMessage();
                return response()->error(400, $message);
            }

            return response()->error(500, $message);
        }
    }

    public function missolicitudes()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Mis Solicitudes',
                'componente' => $this->componente('SOLIC_MIS'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Mis Solicitudes'],
        ]);

        return view('solicitudes::missolicitudes');
    }

    public function todosdatosmissolicitudes(Request $request)
    {
        $data =  Solicitud::solicitudesusuario(\Auth::user()->codusuario)->orderBy('fecha', 'DESC')->get();
        $data->map(function($fila) {
            $solicitud = Solicitud::findOrFail($fila->codsolicitud);
            $cuestinario_solicitudes_pendientes = $fila->cuestinario_solicitudes()->where('terminado', false)->get();
            $cuestinario_solicitudes_terminados = $fila->cuestinario_solicitudes()->where('terminado', true)->get();

            $pagos_sin_respuesta = $fila->pagos_solicitud()->whereNull('respuesta')->get();
            $PagoSolicitud = PagoSolicitud::where('codsolicitud', $fila->codsolicitud)->first();

            if ($PagoSolicitud) {
                $fila->detallepago = \route('solicitudes.pagos.detalle', [\Crypt::encrypt($PagoSolicitud->codpagosolicitud)]);
            }
            
            $opciones = '';
            if ((int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.inscrito', 1)) {
                // $opciones .= '<button data-placement="top" data-toggle="tooltip" title="Ir a pagar" data-codsolicitud="' . \Crypt::encrypt($fila->codsolicitud) . '" class="btn btn-success  rounded btn-pay tooltip-opcion"><i class="fa fa-credit-card"></i></button> ';
            }
            if ((int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.pago_registrado', 2)) {
                
                if ($PagoSolicitud) {
                    $opciones .= '<a target="_blank" data-placement="top" title="Ver detalles del pago" data-toggle="tooltip" href="' . route('solicitudes.pagos.detalle', [\Crypt::encrypt($PagoSolicitud->codpagosolicitud)]) . '" class="btn btn-success rounded tooltip-opcion"><i class="fa fa-eye"></i> Ver pago</a> ';
                }
            }

            if ($solicitud->agendadas()->count() > 0) {
                $opciones .= '<a data-placement="top" href="javascript:void(0)" data-id="' . \Crypt::encrypt($solicitud->codsolicitud) . '" title="Ver Agendamientos" data-toggle="tooltip" class="veragendamientos btn btn-success  rounded tooltip-opcion"><i class="fa fa-calendar"></i> Ver agendamiento</a> ';
            }

            if (((int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.habilitado', 4) or (int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.iniciado', 5)) && count($cuestinario_solicitudes_pendientes) > 0) {
                $cuestinario_solicitud = $fila->cuestinario_solicitudes()->where('terminado', false)->first();
                $opciones .= '<a target="_blank" data-placement="top" title="Reponder cuestionario" data-toggle="tooltip" href="' . route('solicitudes.cuestionarios.responder', [\Crypt::encrypt($cuestinario_solicitud->codcuestionario_solicitud)]) . '" class="btn btn-primary mt-5 rounded tooltip-opcion"><i class="fa fa-check-square-o"></i> Responder cuestionario</a> ';
            }

            if (count($cuestinario_solicitudes_terminados) > 0) {
                $cuestinario_solicitud = $fila->cuestinario_solicitudes()->where('terminado', true)->first();
                $opciones .= '<a target="_blank" data-placement="top" title="Ver resultados cuestionario" data-toggle="tooltip" href="' . route('solicitudes.cuestionarios.resultados', [\Crypt::encrypt($fila->codsolicitud)]) . '" class="btn btn-warning mt-5 rounded tooltip-opcion"><i class="fa fa-print"></i> Ver resultados</a> ';
            }

            $certificado = $solicitud->certificados->first();
            if($certificado){
                $opciones .= '<a data-placement="top" target="_blank" title="Ver Certificación" data-toggle="tooltip" href="' . route('solicitudes.certificados.ver', [\Crypt::encrypt($certificado->codcertificado)]) . '" class="btn btn-inverse mt-5 rounded tooltip-opcion"><i class="fa fa-graduation-cap"></i> Ver certificación</a> ';
            }
            $fila->h_opciones = $opciones;

            $fila->h_estado = '<span class="badge badge-' . \Config::get('dominios.ETAPA_SOLICITUD.ALL.' . $fila->etapa)['class'] . '">' . \Config::get('dominios.ETAPA_SOLICITUD.ALL.' . $fila->etapa)['value'] . '</span>';
            
            return $fila;
        });

        return $data;

        // $datos = Solicitud::solicitudesusuario(\Auth::user()->codusuario)->get();
        // return Datatables::of($datos)->addColumn('estado', function ($fila) {
        
        // })->addColumn('opciones', function ($fila) {
        //     
        // })->rawColumns(['estado', 'opciones'])->make(true);
    }

    public function todas()
    {
        $etapas_solicitud = \Config::get('dominios.ETAPA_SOLICITUD.ALL');
        $cuestionarios = Cuestionario::where('tipo', \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.P', 1))->get();
        $evaluadores = Usuario::where('codperfil', '4')->get();
        $normas = Norma::all();

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Solicitudes',
                'componente' => $this->componente('SOLIC_TODAS'),
            ],
            'etapas_solicitud' => $etapas_solicitud,
            'cuestionarios' => $cuestionarios,
            'evaluadores' => $evaluadores,
            'normas' => $normas,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Solicitudes'],
        ]);


        return view('solicitudes::todas')
            ->with('etapas_solicitud', $etapas_solicitud);
    }

    public function datatable_todas(Request $request)
    {
        $datos = Solicitud::todas($request->all())->get();
        return Datatables::of($datos)->addColumn('usuario', function ($fila) {
            $usuario = Usuario::findOrFail($fila->codusuario);
            return $usuario->nombrecompleto;
        })->addColumn('opciones', function ($fila) {
            $html = '';
            $cuestinario_solicitudes_terminados = $fila->cuestinario_solicitudes()->where('terminado', true)->get();

            $solicitud = Solicitud::findOrFail($fila->codsolicitud);
            $PagoSolicitud = PagoSolicitud::where('codsolicitud', $fila->codsolicitud)->where('state', 1)->first();
            if ($PagoSolicitud) {
                $html .= '<a target="_blank" data-placement="top" title="Ver detalles del pago" data-toggle="tooltip" href="' . route('solicitudes.pagos.detalle', [\Crypt::encrypt($PagoSolicitud->codpagosolicitud)]) . '" class="btn btn-success btn-xs rounded tooltip-opcion"><i class="fa fa-eye"></i></a> ';
            }

            if ($solicitud->agendadas()->count() > 0) {
                $html .= '<a data-placement="top" href="javascript:void(0)" data-id="' . \Crypt::encrypt($solicitud->codsolicitud) . '" title="Ver Agendamientos" data-toggle="tooltip" class="veragendamientos btn btn-success btn-xs rounded tooltip-opcion"><i class="fa fa-calendar"></i></a> ';
            }
            
            if ((int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.procesado', 6) || (int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.no_aprobado', 8)) {
                foreach ($solicitud->evaluaciones as $evaluacionsolicitud) {
                    $icono = \Config::get('dominios.TIPO_EVALUACION_INDICADOR.ALL.'. $evaluacionsolicitud->codevaluacion)['icon'];
                    $class = \Config::get('dominios.TIPO_EVALUACION_INDICADOR.ALL.'. $evaluacionsolicitud->codevaluacion)['class'];
                    $html .= '<a target="_blank" data-placement="top" title="Evaluar ' . $evaluacionsolicitud->nombre . '" data-toggle="tooltip" href="#' . route('solicitudes.evaluacion', [\Crypt::encrypt($evaluacionsolicitud->pivot->codevaluacionsolicitud)], false) . '" class="btn btn-'.$class.' btn-xs rounded tooltip-opcion"><i class="'.$icono.'"></i></a>';
                }
            }
            
            if (count($cuestinario_solicitudes_terminados) > 0) {
                $cuestinario_solicitud = $fila->cuestinario_solicitudes()->where('terminado', true)->first();
                $html .= '<a  target="_blank" data-placement="top" title="Ver resultados cuestionario" data-toggle="tooltip" href="' . route('solicitudes.cuestionarios.resultados', [\Crypt::encrypt($fila->codsolicitud)]) . '" class="btn btn-warning btn-xs rounded tooltip-opcion"><i class="fa fa-print"></i></a> ';
            }

            if ((int) $fila->etapa == (int) \Config::get('dominios.ETAPA_SOLICITUD.VALORES.habilitado', 4)) {
                $cuestinario_solicitud = $fila->cuestinario_solicitudes()->where('terminado', false)->first();
                $html .= '<a data-placement="top" target="_blank" title="Imprimir cuestionario" data-toggle="tooltip" href="' . route('solicitudes.cuestionarios.imprimircuestionario', [\Crypt::encrypt($fila->codsolicitud)]) . '" class="btn btn-danger btn-xs rounded tooltip-opcion"><i class="fa fa-file-pdf-o"></i></a> ';

                if ($cuestinario_solicitud) {
                    $html .= '<a data-placement="top" target="_blank" title="Subir resultados" data-toggle="tooltip" href="' . route('solicitudes.cuestionarios.respondercorto', [\Crypt::encrypt($cuestinario_solicitud->codcuestionario_solicitud)]) . '" class="btn btn-info btn-xs rounded tooltip-opcion"><i class="fa fa-upload"></i></a> ';
                }
            }

            $certificado = $solicitud->certificados->first();
            if($certificado){
                $html .= '<a data-placement="top" target="_blank" title="Ver Certificación" data-toggle="tooltip" href="' . route('solicitudes.certificados.ver', [\Crypt::encrypt($certificado->codcertificado)]) . '" class="btn btn-inverse btn-xs rounded tooltip-opcion"><i class="fa fa-graduation-cap"></i></a> ';
            }

            return $html;
        })->rawColumns(['opciones', 'usuario', 'check'])->make(true);
    }

    public function pagoregistrado(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                $codsolicitudes = $request->solicitudes;
                foreach ($codsolicitudes as $codsolicitud) {
                    $solicitud = Solicitud::findOrFail($codsolicitud);
                    
                    $solicitud->etapassolicitud()->update(['ultima' => false]);
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.pago_registrado'),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }
            }, 5);

            return response()->success();
        } catch (\Exception $e) {
            //dd($e);
            return response()->error();
        }
    }

    public function getSolicitud(Request $request)
    {
        $solicitud = Solicitud::findOrFail(\Crypt::decrypt($request->codsolicitudcifrado));
        return $solicitud;
    }

    public function evaluacion($codevaluacionsolicitudcifrado)
    {
        $evaluacionsolicitud = Evaluacion_solicitud::findOrFail(\Crypt::decrypt($codevaluacionsolicitudcifrado));
        $solicitud = Solicitud::findOrFail($evaluacionsolicitud->codsolicitud);
        
        $ind = Indicador::where('codnorma', $solicitud->codnorma)->where('tipo', $evaluacionsolicitud->codevaluacion)->get();



        foreach ($ind as $in) {
            $Indicador_evaluacion = Indicador_evaluacion::where('codindicador', $in->codindicador)->where('codevaluacionsolicitud', $evaluacionsolicitud->codevaluacionsolicitud)->first();
            
            if (empty($Indicador_evaluacion)) {
                $indicador_evaluacion = Indicador_evaluacion::create([
                    'codevaluacionsolicitud' => $evaluacionsolicitud->codevaluacionsolicitud,
                    'codindicador' => $in->codindicador,
                ]);

                $valoracion_indicador = new Valoracion_indicador();
                $valoracion_indicador->codindicadorevaluacion = $indicador_evaluacion->codindicadorevaluacion;
                $valoracion_indicador->save();
            }
        }
        
        $indicadores = Indicador::listas($evaluacionsolicitud->codevaluacion)
            ->get();

        // dd($indicadores);
        $evaluadores = Usuario::where('codperfil', '4')->get();

        if (empty($evaluacionsolicitud->fecha_aplicacion)) {
            $evaluacionsolicitud->fecha_aplicacion = now();
            $evaluacionsolicitud->save();
        }


        JavaScript::put([
            'parametros' => [
                'meta_title' => $evaluacionsolicitud->evaluacion->descripcion,
                'componente' => $this->componente('SOLIC_TODAS'),
            ],
            'indicadores' => $indicadores,
            'evaluacionsolicitud' => $evaluacionsolicitud,
            'solicitud' => $solicitud,
            'evaluadores' => $evaluadores,
            'codevaluacionsolicitudcifrado' => \Crypt::encrypt($evaluacionsolicitud->codevaluacionsolicitud),
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Lista de Checkeo'],
        ]);

        return view('solicitudes::evaluacion')->with('evaluacionsolicitud', $evaluacionsolicitud);
    }

    public function generarpdfevaluacion($codevaluacionsolicitudcifrado)
    {
        $evaluacionsolicitud = Evaluacion_solicitud::findOrFail(\Crypt::decrypt($codevaluacionsolicitudcifrado));
        $tipoevaluacion = $evaluacionsolicitud->evaluacion;
        $solicitud = Solicitud::findOrFail($evaluacionsolicitud->codsolicitud);
        $indicadores = Indicador::listas($evaluacionsolicitud->codevaluacion)
        ->get();
        // dd($indicadores);
        
        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode($tipoevaluacion->descripcion . ' de evidencia de ' . $tipoevaluacion->nombre)));
        if ($tipoevaluacion->nombre == \Config::get('dominios.TIPO_EVALUACION_INDICADOR.TXT.1')) {
            $pdf->setCodigo(utf8_decode('CODIGO: T-01-F08'));
            $pdf->setVersion(utf8_decode('Versión 01'));
            $pdf->setFecha(utf8_decode('2018/04/12'));
        } elseif ($tipoevaluacion->nombre == \Config::get('dominios.TIPO_EVALUACION_INDICADOR.TXT.2')) {
            $pdf->setCodigo(utf8_decode('CODIGO: T-01-F09'));
            $pdf->setVersion(utf8_decode('Versión 01'));
            $pdf->setFecha(utf8_decode('2018/04/12'));
        }
        
        $pdf->SetTitle(utf8_decode($tipoevaluacion->descripcion).' - '.$evaluacionsolicitud->codevaluacion);

        $pdf->AddPage();
        $pdf->AliasNbPages();

        //BLOQUE 1-----------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('DATOS DEL CANDIDATO'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(30, 6, utf8_decode('NOMBRES:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->nombres), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('APELLIDOS:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->primerapellido.' '.$solicitud->usuario->segundoapellido), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('IDENTIFICACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->documento), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('LUGAR DE EXPEDICIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 2------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('DATOS DE APLICACIÓN'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA DE APLICACIÓN:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(\Carbon::parse($evaluacionsolicitud->fecha_aplicacion)->format('d')), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(\Carbon::parse($evaluacionsolicitud->fecha_aplicacion)->format('m')), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(\Carbon::parse($evaluacionsolicitud->fecha_aplicacion)->format('Y')), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA ENTREGA DE RESULTADOS:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(isset($evaluacionsolicitud->fecha_resultados) ? \Carbon::parse($evaluacionsolicitud->fecha_resultados)->format('d') : ''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(isset($evaluacionsolicitud->fecha_resultados) ? \Carbon::parse($evaluacionsolicitud->fecha_resultados)->format('m') : ''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(isset($evaluacionsolicitud->fecha_resultados) ? \Carbon::parse($evaluacionsolicitud->fecha_resultados)->format('Y') : ''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('LUGAR DE EVALUACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(isset($evaluacionsolicitud->lugar) ? $evaluacionsolicitud->lugar : ''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CIUDAD:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode(isset($evaluacionsolicitud->ciudad) ? $evaluacionsolicitud->ciudad : ''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('NOMBRE EVALUADOR:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(isset($evaluacionsolicitud->codevaluador) ? $evaluacionsolicitud->evaluador->nombrecompleto : ''), 'TBR', 1, 'L', false);
        
        $pdf->Ln(2);

        //BLOQUE 3------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode($solicitud->norma->nombre), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(80, 6, utf8_decode($solicitud->norma->codigo), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 4------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);

        $pdf->Cell(10, 18, utf8_decode('No.'), 1, 0, 'C', false);
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->CellfitScale(90, 6, utf8_decode('MOMENTOS DE OBSERVACIÓN EXIGIDOS EN LA NORMA'), 1, 0, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(20, 6, utf8_decode('1°'), 1, 0, 'C', false);
        $pdf->Cell(20, 6, utf8_decode('2°'), 1, 0, 'C', false);
        $pdf->Cell(40, 18, utf8_decode('OBSERVACIONES'), 1, 1, 'L', false);

        $pdf->SetY($y+6);
        $pdf->Cell(10, 12, '', 0, 0, 'C', false);
        $pdf->Cell(90, 12, utf8_decode('INDICADORES DE EVALUACIÓN'), 1, 0, 'C', false);
        $pdf->SetX($x+90);
        $pdf->CellFitScale(20, 6, utf8_decode('CUMPLE'), 1, 0, 'C', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CUMPLE'), 1, 1, 'C', false);
        $x2 = $pdf->GetX();
        $pdf->SetX($x2+100);
        $pdf->CellFitScale(10, 6, utf8_decode('SI'), 1, 0, 'C', false);
        $pdf->CellFitScale(10, 6, utf8_decode('NO'), 1, 0, 'C', false);
        $pdf->CellFitScale(10, 6, utf8_decode('SI'), 1, 0, 'C', false);
        $pdf->CellFitScale(10, 6, utf8_decode('NO'), 1, 1, 'C', false);

        if ($indicadores->count() == 0) {
            $pdf->SetFillColor(252, 240, 212);
            $pdf->SetTextColor(200, 138, 10);
            $pdf->CellFitScale(0, 6, utf8_decode('Advertencia! La norma no posee indicadores asociados.'), 1, 1, 'C', true);
        }
        foreach ($indicadores as $key => $indicador) {
            $calificado = false;
            if(!empty($indicador->cumple)){
                $calificado = true;
            }
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->CellFitScale(10, 6, utf8_decode($key+1), 1, 0, 'C', false);
            $pdf->CellFitScale(90, 6, utf8_decode($indicador->nombre), 1, 0, 'L', false);
            $pdf->SetFont('Arial', 'B', 15);
            $pdf->SetTextColor(19, 206, 102);
            $pdf->Cell(10, 6, utf8_decode($indicador->cumple && $calificado ? 'x' : ''), 1, 0, 'C', false);
            $pdf->SetFont('Arial', 'B', 15);
            $pdf->SetTextColor(254, 73, 73);
            $pdf->Cell(10, 6, utf8_decode(!$indicador->cumple && $calificado ? 'x' : ''), 1, 0, 'C', false);
            $pdf->SetFont('Arial', '', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Cell(10, 6, utf8_decode(''), 1, 0, 'C', false);
            $pdf->Cell(10, 6, utf8_decode(''), 1, 0, 'C', false);
            $pdf->CellFitScale(40, 6, utf8_decode(isset($indicador->observacion) ? $indicador->observacion : ''), 1, 1, 'L', false);
        }

        $pdf->Ln(2);

        //BLOQUE 5------------------------------------------------------------------------------------------
        $cantidadindicadores = 0;
        $cantidadindicadores_true = 0;
        $cantidadindicadores_false = 0;
        $porc_indicadores_true = 0;
        $porc_indicadores_false = 0;

        if ($indicadores->count() > 0) {
            $cantidadindicadores = $indicadores->count();
            $cantidadindicadores_true = $indicadores->where('cumple', true)->count();
            $cantidadindicadores_false = $indicadores->where('cumple', false)->count();
            $porc_indicadores_true = ($cantidadindicadores_true * 100) / $cantidadindicadores;
            $porc_indicadores_false = ($cantidadindicadores_false * 100) / $cantidadindicadores;
        }

        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(100, 6, utf8_decode('VALORACIÓN FINAL DE COMPETENCIA:'), 1, 0, 'L', true);
        $pdf->CellFitScale(20, 6, utf8_decode('CUMPLE:'), 'TBL', 0, 'L', false);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(19, 206, 102);
        $pdf->CellFitScale(20, 6, round($porc_indicadores_true, 2).'%', 'TBR', 0, 'L', false);
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->CellFitScale(20, 6, utf8_decode('AÚN NO CUMPLE:'), 'TBL', 0, 'L', false);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(254, 73, 73);
        $pdf->CellFitScale(20, 6, round($porc_indicadores_false, 2).'%', 'TBR', 1, 'L', false);
        
        $pdf->Ln(2);

        //BLOQUE 6------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', '', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('ANÁLISIS DEL EVALUADOR - COMPETENCIAS(S) FALTANTES(S)'), 1, 1, 'L', true);
        $pdf->CellFitScale(0, 30, utf8_decode(''), 1, 1, 'L', false);

        $pdf->Ln(20);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->CellFitScale(25, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL CANDIDATO'), 'T', 0, 'C', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL EVALUADOR'), 'T', 0, 'C', false);


        // ob_end_clean();
        $pdf->Output($tipoevaluacion->descripcion .' - '.$evaluacionsolicitud->codevaluacion. '.pdf', 'I');
    }

    public function registrarvaloracion(Request $request)
    {
        if (isset($request->codvaloracionindicador)) {
            $valoracion = Valoracion_indicador::findOrFail($request->codvaloracionindicador);
            $valoracion->fill($request->all());
            $status = $valoracion->save();
        } else {
            $valoracion = new Valoracion_indicador($request->all());
            $status = $valoracion->save();
        }

        if ($status) {
            $indicadores = Indicador::listas($request->codevaluacion)
                ->get();

            return response()->json([
                'message' => 'Indicador actualizado correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
                'indicadores' => $indicadores,
            ]);
        }
        return response()->json([
            'message' => 'No se ha podido actualizar el indicador',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }

    public function registrarevaluacionsolicitud(Request $request)
    {
        $evaluacionsolicitud = Evaluacion_solicitud::findOrFail($request->codevaluacionsolicitud);
        $evaluacionsolicitud->fill($request->all());
        $status = $evaluacionsolicitud->save();

        if ($status) {
            return response()->json([
                'message' => 'Información actualizada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
                'evaluacionsolicitud' => $evaluacionsolicitud,
            ]);
        }
        return response()->json([
            'message' => 'No se ha podido actualizar la información',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }

    public function evaluar(Request $request)
    {
        $valoracion = Valoracion::where('codsolicitud', $request->evaluacionsolicitud['codsolicitud'])->first();
        $solicitud = Solicitud::findOrFail($request->evaluacionsolicitud['codsolicitud']);

        if ($request->evaluacionsolicitud['evaluacion']['nombre'] == \Config::get('dominios.TIPO_EVALUACION_INDICADOR.TXT.1', 'Desempeño')) {
            $valoracion->desempeno = $request->cumple;
        }
        if ($request->evaluacionsolicitud['evaluacion']['nombre'] == \Config::get('dominios.TIPO_EVALUACION_INDICADOR.TXT.2', 'Producto')) {
            $valoracion->producto = $request->cumple;
        }

        $status = $valoracion->save();

        if ($status) {

            if((float)$valoracion->desempeno < 100 || (float) $valoracion->producto < 100){
                $solicitud->etapassolicitud()->update(['ultima' => false]);

                $existeetapa= EtapaSolicitud::where('etapa', \Config::get('dominios.ETAPA_SOLICITUD.VALORES.no_aprobado', 8))
                ->where('codsolicitud', $solicitud->codsolicitud)
                ->exists();
                if(!$existeetapa){
                    $etapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.no_aprobado', 8),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                    $etapaSolicitud->save();
                }
            }elseif((float)$valoracion->desempeno >= 100 && (float) $valoracion->producto >= 100){
                $solicitud->etapassolicitud()->update(['ultima' => false]);
                $etapaSolicitud = EtapaSolicitud::create([
                    'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.certificado', 7),
                    'codsolicitud' => $solicitud->codsolicitud,
                    'ultima' => true
                ]);
                $etapaSolicitud->save();
                
                $certificado = Certificado::create([
                    'fecha' =>  \Carbon::now(),
                    'fecha_vencimiento' => \Carbon::now()->addYears(1),
                    'codsolicitud' => $solicitud->codsolicitud
                ]);
                $certificado->save();
                $certificado->codigo = mb_strtoupper(uniqid($certificado->codcertificado));
                $certificado->save();

            }
            

            return response()->json([
                'message' => 'Evaluación guardada correctamente',
                'title'   => 'Exito',
                'type'    => 'success',
            ]);
        }

        return response()->json([
            'message' => 'No se ha podido guardar la evaluación',
            'title'   => 'Error',
            'type'    => 'danger',
        ]);
    }

    public function agendar(Request $request)
    {
        $evaluador = Usuario::findOrFail($request->codevaluador);

        foreach ($request->codssolicitudes as $codsolicitud) {
            $solicitud = Solicitud::findOrFail($codsolicitud);

            $agendamiento = new Agendada([
                'fecha_agendada' => $request->fecha_agendar,
                'hora' => $request->hora_agendar,
                'codsolicitud' => $codsolicitud,
                'codevaluador' => $request->codevaluador
            ]);

            $status = $agendamiento->save();

            if ($status) {
                $solicitud->etapassolicitud()->update(['ultima' => false]);
                $EtapaSolicitud = EtapaSolicitud::create([
                    'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.agendado'),
                    'codsolicitud' => $solicitud->codsolicitud,
                    'ultima' => true
                ]);

                Mail::to($solicitud->usuario->email)
                    ->send(new SolicitudesAgendadas([
                        'solicitud' => $solicitud,
                        'agendamiento' => $agendamiento,
                    ]));
            }
        }

        $solicitudes_agendadas = Solicitud::whereIn('codsolicitud', $request->codssolicitudes)->get();

        Mail::to($evaluador->email)
            ->send(new SolicitudesAgendadas([
                'evaluador' => $evaluador,
                'solicitudes_agendadas' => $solicitudes_agendadas,
                'agendamiento' => $agendamiento,
            ]));

        return response()->json([
            'message' => 'Se han agendado correctamente las solicitudes seleccionadas',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function habilitar(Request $request)
    {
        $cuestionario = Cuestionario::findOrFail($request->codcuestionario);
        foreach ($request->codssolicitudes as $codsolicitud) {
            $solicitud = Solicitud::findOrFail($codsolicitud);
            $cuestionariosolicitud = new CuestionarioSolicitud([
                'codsolicitud' => $codsolicitud,
                'codcuestionario' => $request->codcuestionario,
                'terminado' => false,
            ]);

            $status = $cuestionariosolicitud->save();

            if ($status) {
                $solicitud->etapassolicitud()->update(['ultima' => false]);
                $EtapaSolicitud = EtapaSolicitud::create([
                    'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.habilitado'),
                    'codsolicitud' => $solicitud->codsolicitud,
                    'ultima' => true
                ]);


                // Mail::to(\Auth::user()->email)
                //     ->send(new SolicitudesAgendadas([
                //         'solicitud' => $solicitud,
                //         'agendamiento' => $agendamiento,
                //     ]));
            }
        }
        return response()->json([
            'message' => 'Se han habilitado correctamente las solicitudes seleccionadas',
            'title'   => 'Exito',
            'type'    => 'success',
        ]);
    }

    public function respuesta_pago(Request $request)
    {

        $data = $request->all();

        if(!isset($data['x_ref_payco'])){
            return redirect()->route('home');
        }

        $p_cust_id_cliente = config('pay.p_cust_id_cliente');
        $p_key             = config('pay.p_key');
        $x_ref_payco      = $data['x_ref_payco'];
        $x_transaction_id = $data['x_transaction_id'];
        $x_amount         = $data['x_amount'];
        $x_currency_code  = $data['x_currency_code'];
        $x_signature      = $data['x_signature'];
        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
        $x_response     = $data['x_response'];
        $x_motivo       = $data['x_response_reason_text'];
        $x_id_invoice   = $data['x_id_invoice'];
        $x_autorizacion = $data['x_approval_code'];
        
        DB::beginTransaction();
        try {
            $codsolicitud = \Crypt::decrypt($data['x_extra1']);
            $referenceCode = md5($codsolicitud);
            $PagoSolicitud = PagoSolicitud::where('reference', $referenceCode)->first();
            
            $state = ((int) $PagoSolicitud->state !== 1) ? 3 : 1;
            
            if (isset($PagoSolicitud)) {
                

                $PagoSolicitud->fill([
                    'payu_order_id' => $data['x_ref_payco'],
                    'transaction_id' => $data['x_transaction_id'],
                    'franchise' => $data['x_franchise'],
                    'currency' => $data['x_currency_code'],
                    'description' => $data['x_description'],
                    'state' => $state,
                    'value' => $data['x_amount'],
                    'user_id' => \Auth::id(),
                    'respuesta' => json_encode($data)
                ]);
            } else {
                $PagoSolicitud = new PagoSolicitud([
                    'reference' => $referenceCode,
                    'payu_order_id' => $data['x_ref_payco'],
                    'transaction_id' => $data['x_transaction_id'],
                    'franchise' => $data['x_franchise'],
                    'currency' => $data['x_currency_code'],
                    'description' => $data['x_description'],
                    'state' => $state,
                    'value' => $data['x_amount'],
                    'user_id' => \Auth::id(),
                    'codsolicitud' => $codsolicitud,
                    'respuesta' => json_encode($data)
                ]);
            }

            $PagoSolicitud->save();


            DB::commit();
        } catch (\Exception $e) {
            //dd($e);
            DB::rollback();
            abort(505);
        }

        return redirect()->route('solicitudes.pagos.detalle', [\Crypt::encrypt($PagoSolicitud->codpagosolicitud)]);
    }

    public function confirmacion_pago(Request $request)
    {
        $data = $request->all();
        $p_cust_id_cliente = config('pay.p_cust_id_cliente');
        $p_key             = config('pay.p_key');
        $x_ref_payco      = $data['x_ref_payco'];
        $x_transaction_id = $data['x_transaction_id'];
        $x_amount         = $data['x_amount'];
        $x_currency_code  = $data['x_currency_code'];
        $x_signature      = $data['x_signature'];
        $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
        $x_response     = $data['x_response'];
        $x_motivo       = $data['x_response_reason_text'];
        $x_id_invoice   = $data['x_id_invoice'];
        $x_autorizacion = $data['x_approval_code'];
        
        DB::beginTransaction();
        try {
            $codsolicitud = \Crypt::decrypt($data['x_extra1']);
            $referenceCode = md5($codsolicitud);
            $PagoSolicitud = PagoSolicitud::where('reference', $referenceCode)->first();
            if (isset($PagoSolicitud)) {
                $PagoSolicitud->fill([
                    'payu_order_id' => $data['x_ref_payco'],
                    'transaction_id' => $data['x_transaction_id'],
                    'franchise' => $data['x_franchise'],
                    'currency' => $data['x_currency_code'],
                    'description' => $data['x_description'],
                    'state' => $data['x_cod_response'],
                    'value' => $data['x_amount'],
                    'user_id' => \Auth::id(),
                    'respuesta' => json_encode($data)
                ]);
            } else {
                $PagoSolicitud = new PagoSolicitud([
                    'reference' => $referenceCode,
                    'payu_order_id' => $data['x_ref_payco'],
                    'transaction_id' => $data['x_transaction_id'],
                    'franchise' => $data['x_franchise'],
                    'currency' => $data['x_currency_code'],
                    'description' => $data['x_description'],
                    'state' => $data['x_cod_response'],
                    'value' => $data['x_amount'],
                    'user_id' => \Auth::id(),
                    'codsolicitud' => $codsolicitud,
                    'respuesta' => json_encode($data)
                ]);
            }

            $PagoSolicitud->save();

            //Validamos la firma
            if ($x_signature == $signature) {
                /*Si la firma esta bien podemos verificar los estado de la transacción*/
                $x_cod_response = $data['x_cod_response'];
                switch ((int) $x_cod_response) {
                    case 1:
                        # code transacción aceptada
                        $Solicitud = Solicitud::findOrFail($codsolicitud);

                        $Solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.pago_registrado', 2),
                            'codsolicitud' => $Solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                        break;
                    case 2:
                        # code transacción rechazada
                        $Solicitud = Solicitud::findOrFail($codsolicitud);

                        $Solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.anulada', 9),
                            'codsolicitud' => $Solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                        break;
                    case 3:
                        # code transacción pendiente
                        // echo "transacción pendiente";
                        break;
                    case 4:
                        # code transacción fallida
                        // echo "transacción fallida";
                        break;
                }
            } else {
                abort(500, 'Firma no válida');
            }

            DB::commit();
            return 'OK';
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            abort(505);
        }
    }

    public function enlacepago(Request $request)
    {
        $codsolicitud = \Crypt::decrypt($request->codsolicitud);
        $Solicitud = Solicitud::find($codsolicitud);
        $Norma = $Solicitud->norma;
       
        $referenceCode = md5($codsolicitud);
        $amount = $Norma->valor;
        $currency = 'COP';

        $PagoSolicitud = $Solicitud->pagos_solicitud()->first();
        $paid = false;
        $dataResponse = [];
        $message = "";
        $status = 200;
        if (empty($PagoSolicitud)) {
            $PagoSolicitud = PagoSolicitud::create([
                'codsolicitud' => $codsolicitud,
                'reference' => $referenceCode,
                'value' => $amount
            ]);
        } else {
            try {
                $epayco = new \Epayco\Epayco(array(
                    "apiKey" => config('pay.public_key'),
                    "privateKey" => config('pay.private_key'),
                    "lenguage" => "ES",
                    "test" => config('pay.test')
                ));
                
                $efectivos = ['BA', 'EF', 'GA', 'PR', 'RS'];
                $credit_cards = ['VS', 'MC', 'AM', 'DC', 'CR', 'SP'];
                $pse = ['PSE'];
                
                if(!empty($PagoSolicitud->payu_order_id)) {

                    if(in_array($PagoSolicitud->franchise, $efectivos)) {
                        $pay = $epayco->cash->transaction($PagoSolicitud->payu_order_id);
                    } else if(in_array($PagoSolicitud->franchise, $credit_cards)) {
                        $pay = $epayco->charge->transaction($PagoSolicitud->payu_order_id);
                    } else {
                        $pay = $epayco->bank->get($PagoSolicitud->payu_order_id);
                    }

                    if($pay->success) {
                        $data = $pay->data;
                        $PagoSolicitud->transaction_id = $data->x_transaction_id;
                        $PagoSolicitud->payu_order_id = $data->x_ref_payco;
                        $PagoSolicitud->state = $data->x_cod_response;
                        $PagoSolicitud->value = $data->x_amount;
                        $PagoSolicitud->franchise = $data->x_franchise;
                        $PagoSolicitud->currency = $data->x_currency_code;
                        $PagoSolicitud->description = $data->x_description;
                        $PagoSolicitud->user_id = \Auth::id();
                        $PagoSolicitud->save();

                        if($data->x_cod_response == 1) {
                            $Solicitud->etapassolicitud()->update(['ultima' => false]);
                            $EtapaSolicitud = EtapaSolicitud::create([
                                'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.pago_registrado', 2),
                                'codsolicitud' => $Solicitud->codsolicitud,
                                'ultima' => true
                            ]);
                            $paid = true;
                        }
                    } else {
                        throw new \Exception("Error al conectar con la plataforma de pagos, intente nuevamente."); 
                    }
                }
                
            } catch (\Exception $e) {
                // dd($e);
                if ($e instanceof \Throwable) {
                    $message = $e->getMessage();
                } else {
                    $message = "No se ha podido generar el enlace de pago.";
                }
                $status = 500;
            }
        }

        $dataResponse = [
            'description' => $Norma->nombre,
            'referenceCode' => $referenceCode,
            'amount' => $amount,
            'tax' => '0',
            'taxReturnBase' => '0',
            'currency' => $currency,
            'test' => '0',
            'buyerEmail' => \Auth::user()->email,
            'buyerName' => \Auth::user()->nombrecompleto,
            'buyerDoc' => \Auth::user()->documento,
            'responseUrl' => url('/') . '/solicitudes/pagos/respuesta',
            'confirmationUrl' => url('/') . '/solicitudes/pagos/confirmacion',
            'codsolicitud' => \Crypt::encrypt($Solicitud->codsolicitud),
            'paid' => $paid,
            'message' => $message,
            'state' => $PagoSolicitud->state
        ];

        return response()->json($dataResponse, $status);
    }
}
