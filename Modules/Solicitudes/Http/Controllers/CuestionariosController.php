<?php

namespace Modules\Solicitudes\Http\Controllers;

use Carbon;
use JavaScript;
use Illuminate\Http\Request;
use Amenadiel\JpGraph\Graph\Graph;
use Illuminate\Routing\Controller;
use ABCcomptencias\Traits\Permisos;
use Amenadiel\JpGraph\Plot\BarPlot;
use Illuminate\Support\Facades\View;
use Modules\Solicitudes\Entities\Usuario;
use ABCcomptencias\Libraries\Fpdf_formato;
use Modules\Solicitudes\Entities\Agendada;
use Modules\Solicitudes\Entities\Solicitud;
use Amenadiel\JpGraph\Themes\UniversalTheme;
use Modules\Solicitudes\Entities\Evaluacion;
use Modules\Solicitudes\Entities\Valoracion;
use ABCcomptencias\Mail\SolicitudesAgendadas;
use Modules\Solicitudes\Entities\Conocimiento;
use Modules\Solicitudes\Entities\Cuestionario;
use Modules\Solicitudes\Entities\EtapaSolicitud;
use Modules\Solicitudes\Entities\Evaluacion_solicitud;
//Traits
use Modules\Solicitudes\Entities\PreguntaCuestionario;
use Modules\Solicitudes\Entities\CuestionarioSolicitud;

class CuestionariosController extends Controller
{

    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function imprimircuestionario($codsolicitudcifrado)
    {
        $codsolicitud = \Crypt::decrypt($codsolicitudcifrado);
        $solicitud = Solicitud::find($codsolicitud);

        $CuestionarioSolicitud = CuestionarioSolicitud::where('codsolicitud', $solicitud->codsolicitud)->where('terminado', false)->first();
        $cuestionario = $CuestionarioSolicitud->cuestionario;
        $preguntas = Cuestionario::preguntasCuestionarioSolicitud($CuestionarioSolicitud->codcuestionario, $CuestionarioSolicitud->codcuestionario_solicitud);

        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode('EVALUACIÓN TEORICA')));

        $pdf->setCodigo(utf8_decode('CODIGO: T-01-F06'));
        $pdf->setVersion(utf8_decode('Versión 01'));
        $pdf->setFecha(utf8_decode('2018/04/12'));

        $pdf->SetTitle(utf8_decode('Imprimir cuestionario'));
        $pdf->AliasNbPages();

        $pdf->AddPage();

        //seccion datos del producto
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(30, 8, utf8_decode('Cuestionario:'), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(80, 8, utf8_decode($cuestionario->nombre), 0, 1, 'L');


        //BLOQUE 3------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode($solicitud->norma->nombre), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(80, 6, utf8_decode($solicitud->norma->codigo), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->MultiCell(180, 6, utf8_decode(!empty($cuestionario->instrucciones) ? $cuestionario->instrucciones : 'Señor Candidato: La prueba se conforma por enunciados (textos que describen problemas, situaciones, casos, etc.) distinguidos con números, cada una de éstos cuenta con cuatro opciones de respuesta identificados con las letras a, b, c y d, donde sólo uno responde correctamente a la pregunta. Rellene el ovalo que usted considera responde a la pregunta en el formato "Hoja de Respuestas".'), 1, 'J', false);

        $pdf->Ln(2);
        $pdf->SetFont('Arial', 'BU', 10);
        $pdf->CellFitScale(180, 4, utf8_decode('Este Cuestionario debe ser devuelto al evaluador sin dañarlo ni modificarlo en ninguna de sus partes.'), 0, 1, 'C', false);

        $pdf->Ln(4);

        // preguntas ------------------------------------------------------------------
        // $pdf->Cell(180, 8, utf8_decode('PREGUNTAS'), 0, 1, 'C');

        // $pdf->Ln(3);

        $pdf->SetFont('Arial', '', 12);
        foreach ($preguntas as $pregunta) {
            $pdf->Cell(7, 6, utf8_decode($pregunta->numero . '.'), 0, 0, 'J');
            $pdf->MultiCell(173, 6, utf8_decode($pregunta->pregunta), 0, 'J', false);
            $pdf->Ln(2);
            $opciones = $pregunta->opciones;
            $x = 'A';
            foreach ($opciones as $opcion) {
                $pdf->Cell(7, 6, '', 0, 0, 'J');
                $pdf->Cell(7, 6, utf8_decode($x . '.'), 0, 0, 'J');
                $pdf->CellFitScale(166, 6, utf8_decode($opcion->opcion), 0, 1, 'J');
                $x++;
            }
            $pdf->Ln(2);
        }

        //----------------------------------------------------------------------------------------------
        //agregar hoja de respuestas --------------------------------------------------------------------
        $pdf->setTitulo(strtoupper(utf8_decode('HOJA DE RESPUESTAS')));
        $pdf->setCodigo(utf8_decode('CODIGO: T-01-F07'));
        $pdf->AddPage();

        //BLOQUE 1-----------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('DATOS DEL CANDIDATO'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(30, 6, utf8_decode('NOMBRES:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->nombres), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('APELLIDOS:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->primerapellido . ' ' . $solicitud->usuario->segundoapellido), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('IDENTIFICACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->documento), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(30, 6, utf8_decode('LUGAR DE EXPEDICIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 2------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('DATOS DE APLICACIÓN'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA DE APLICACIÓN:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(90, 6, utf8_decode('FECHA ENTREGA DE RESULTADOS:'), 1, 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
        $pdf->CellFitScale(15, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('LUGAR DE EVALUACIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CIUDAD:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode(''), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(45, 6, utf8_decode('NOMBRE EVALUADOR:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(135, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->Ln(2);

        //BLOQUE 3------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
        $pdf->SetFont('Arial', '', 10);
        $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(160, 6, utf8_decode($solicitud->norma->nombre), 'TBR', 1, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(80, 6, utf8_decode($solicitud->norma->codigo), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

        $pdf->MultiCell(180, 6, utf8_decode('Señor  Candidato: Con  base  en  el  cuestionario  leído  y  comprendido, usted debe seleccionar frente al numeral la respuesta correcta y rellenar el óvalo correspondiente.'), 1, 'C', false);

        $pdf->Ln(6);

        $columnas        = round(count($preguntas) / 2, 0, PHP_ROUND_HALF_UP);
        $xp               = $pdf->GetX();
        $yp               = $pdf->GetY();

        foreach ($preguntas as $index => $pregunta) {
            $pdf->SetFont('Arial', 'B', 12);
            if ($index == $columnas) {
                $pdf->SetXY($xp + 98, $yp);
            }
            if ($index > $columnas) {
                $pdf->SetX($xp + 98);
            }
            $pdf->CellFitScale(8, 6, '', 0, 0, 'J');
            $pdf->CellFitScale(7, 6, utf8_decode($pregunta->numero), 1, 0, 'J');
            $opciones = $pregunta->opciones;
            $x = 'A';
            $Xop = 0;
            $Yop = 0;
            foreach ($opciones as $opcion) {
                $pdf->SetFont('Arial', '', 12);
                $pdf->Cell(15, 6, utf8_decode($x), 1, 0, 'L');
                $Xop = $pdf->GetX();
                $Yop = $pdf->GetY();
                $pdf->Ellipse($Xop - 6, $Yop + 3, 3, 2, 'D');
                $x++;
            }
            $pdf->Ln(6);
        }

        $pdf->Ln(6);

        $Xc = 0;
        $Yc = 0;
        $Xnc = 0;
        $Ync = 0;

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(80, 6, utf8_decode('JUICIO DE LA COMPETENCIA:'), 0, 0, 'R', false);
        $pdf->CellFitScale(20, 6, utf8_decode('CUMPLE:'), 0, 0, 'L', false);
        $Xc = $pdf->GetX();
        $Yc = $pdf->GetY();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(19, 206, 102);
        $pdf->CellFitScale(10, 6, '', 1, 0, 'L', true);
        $pdf->Ellipse($Xc + 5, $Yc + 3, 3, 2, 'D');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->CellFitScale(25, 6, utf8_decode('AÚN NO CUMPLE:'), 0, 0, 'L', false);
        $Xnc = $pdf->GetX();
        $Ync = $pdf->GetY();
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(254, 73, 73);
        $pdf->CellFitScale(10, 6, '', 1, 1, 'L', true);
        $pdf->Ellipse($Xnc + 5, $Ync + 3, 3, 2, 'D');

        $pdf->Ln(2);

        //BLOQUE 6------------------------------------------------------------------------------------------
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);
        $pdf->CellFitScale(0, 6, utf8_decode('IDENTIFICACIÓN DE LA(S) COMPETENCIA(S) FALTANTE(S) - ANÁLISIS DEL EVALUADOR-'), 1, 1, 'C', TRUE);
        $pdf->CellFitScale(0, 30, utf8_decode(''), 1, 1, 'L', false);

        $pdf->Ln(20);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->CellFitScale(25, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL CANDIDATO'), 'T', 0, 'C', false);
        $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL EVALUADOR'), 'T', 0, 'C', false);

        // ob_end_clean();
        $pdf->Output("Cuestionario.pdf", 'I');
    }

    public function verpdf($codsolicitudcifrado)
    {
        $codsolicitud = \Crypt::decrypt($codsolicitudcifrado);
        $solicitud = Solicitud::find($codsolicitud);

        $CuestionariosSolicitud = CuestionarioSolicitud::where('codsolicitud', $solicitud->codsolicitud)->get();

        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode('RESULTADOS CUESTIONARIO')));

        $pdf->setCodigo(utf8_decode('CODIGO: T-000000'));
        $pdf->setVersion(utf8_decode('Versión 01'));
        $pdf->setFecha(utf8_decode('2018/04/12'));

        $pdf->SetTitle(utf8_decode('Imprimir cuestionario'));
        $pdf->AliasNbPages();

        //todo: $pdf->setCodigo(utf8_decode(''));
        //todo: $pdf->setVersion(utf8_decode(''));
        //todo: $pdf->setFecha(utf8_decode(''));

        $pdf->SetTitle(utf8_decode('Resultados cuestionario'));
        $pdf->AliasNbPages();


        foreach ($CuestionariosSolicitud as $CuestionarioSolicitud) {

            $cuestionario = $CuestionarioSolicitud->cuestionario;
            $solicitud = $CuestionarioSolicitud->solicitud;
            $norma = $solicitud->norma;
            $conocimientos = $norma->conocimientos;

            $agendamiento = $solicitud->agendadas()->orderby('agendadas.created_at', 'DESC')->first();

            $horainiciodt = \Carbon::parse($CuestionarioSolicitud->hora_inicio);

            $pdf->AddPage();

            //seccion datos del producto
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(30, 8, utf8_decode('Cuestionario:'), 0, 0, 'L');
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(80, 8, utf8_decode($cuestionario->nombre), 0, 1, 'L');

            //BLOQUE 1-----------------------------------------------------------------------------------------
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFillColor(238, 238, 238);
            $pdf->CellFitScale(0, 6, utf8_decode('DATOS DEL CANDIDATO'), 1, 1, 'L', true);
            $pdf->SetFont('Arial', '', 10);
            $pdf->CellFitScale(30, 6, utf8_decode('NOMBRES:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->nombres), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(30, 6, utf8_decode('APELLIDOS:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->primerapellido . ' ' . $solicitud->usuario->segundoapellido), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(30, 6, utf8_decode('IDENTIFICACIÓN:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(60, 6, utf8_decode($solicitud->usuario->documento), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(30, 6, utf8_decode('LUGAR DE EXPEDICIÓN:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(60, 6, utf8_decode(''), 'TBR', 1, 'L', false);

            $pdf->Ln(2);

            //BLOQUE 2------------------------------------------------------------------------------------------
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFillColor(238, 238, 238);
            $pdf->Cell(0, 6, utf8_decode('DATOS DE APLICACIÓN'), 1, 1, 'L', true);
            $pdf->SetFont('Arial', '', 10);
            $pdf->CellFitScale(90, 6, utf8_decode('FECHA DE APLICACIÓN:'), 1, 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode($horainiciodt->format('d')), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode($horainiciodt->format('m')), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode($horainiciodt->format('Y')), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(90, 6, utf8_decode('FECHA ENTREGA DE RESULTADOS:'), 1, 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('DÍA:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode(Date('d')), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('MES:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode(Date('m')), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode('AÑO:'), 'TB', 0, 'L', false);
            $pdf->CellFitScale(15, 6, utf8_decode(Date('Y')), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(45, 6, utf8_decode('LUGAR DE EVALUACIÓN:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(135, 6, utf8_decode(''), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode('CIUDAD:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(160, 6, utf8_decode(''), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(45, 6, utf8_decode('NOMBRE EVALUADOR:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(135, 6, utf8_decode($agendamiento->evaluador->nombrecompleto), 'TBR', 1, 'L', false);

            $pdf->Ln(2);

            //BLOQUE 3------------------------------------------------------------------------------------------
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFillColor(238, 238, 238);
            $pdf->Cell(0, 6, utf8_decode('NORMA DE COMPETENCIA LABORAL A EVALUAR'), 1, 1, 'L', true);
            $pdf->SetFont('Arial', '', 10);
            $pdf->CellFitScale(20, 6, utf8_decode('NOMBRE:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(160, 6, utf8_decode($solicitud->norma->nombre), 'TBR', 1, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode('CODIGO:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(80, 6, utf8_decode($solicitud->norma->codigo), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode('VERSIÓN:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 0, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode('VIGENCIA:'), 'TBL', 0, 'L', false);
            $pdf->CellFitScale(20, 6, utf8_decode(''), 'TBR', 1, 'L', false);

            $start_y = $pdf->GetY();
            foreach ($conocimientos as $key_conocimiento => $conocimiento) {
                $preguntas = $conocimiento->preguntasconocimiento($conocimiento->codconocimiento)->get();
                $cont = $key_conocimiento + 1;
                if (count($preguntas)) {
                    if ($key_conocimiento > 0) {
                        $start_y += 60;
                        $pdf->SetY($start_y);
                    }

                    $resultados = Conocimiento::Resultados($conocimiento->codconocimiento, $cuestionario->codcuestionario);
                    $correctas = count($resultados['correctas']);
                    $incorrectas = count($resultados['incorrectas']);
                    $no_respondidas = count($resultados['no_respondidas']);
                    $total_preguntas = $correctas + $incorrectas + $no_respondidas;

                    $porcentaje_acierto = ($correctas * 100) /  $total_preguntas;

                    $data = [
                        'Correctas' => $correctas,
                        'Incorrectas' => $incorrectas,
                        'No respondidas' => $no_respondidas
                    ];

                    $ttal_preg_act = array_sum($data);

                    $pdf->Ln(8);

                    $valX = $pdf->GetX();
                    $valY = $pdf->GetY();

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->Cell(90, 5, 'Conocimiento ' . $cont, 0, 0, 'C');
                    $pdf->SetFont('Arial', 'BIU', 12);
                    $pdf->Cell(90, 5, utf8_decode('Porcentaje de acierto: ' . round($porcentaje_acierto) . '%'), 0, 1, 'C');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->MultiCell(90, 6, utf8_decode($conocimiento->nombre), 0, 'J', false);

                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 10);

                    $pdf->setFillColor(156, 201, 108);
                    $pdf->SetX($valX + 30);
                    $pdf->Cell(30, 5, 'Correctas', 1, 0, 'L', 1);
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(6, 5, $data['Correctas'], 1, 1, 'C', 1);

                    $pdf->SetX($valX + 30);
                    $pdf->setFillColor(247, 108, 82);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(30, 5, 'Incorrectas', 1, 0, 'L', 1);
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(6, 5, $data['Incorrectas'], 1, 1, 'C', 1);

                    $pdf->SetX($valX + 30);
                    $pdf->setFillColor(235, 170, 75);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(30, 5, 'No respondidas', 1, 0, 'R', 1);
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(6, 5, $data['No respondidas'], 1, 1, 'C', 1);

                    $pdf->SetX($valX + 30);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(30, 5, 'Total', 1, 0, 'R', false);
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(6, 5, $ttal_preg_act, 1, 1, 'C', false);

                    $pdf->Ln(8);
                    $pdf->SetX($valX);

                    $pdf->SetXY(90, $valY);
                    // $col1=array(156, 201, 109);
                    // $col2=array(247, 108, 83);
                    // $col3=array(235, 170, 77);
                    // $pdf->PieChart(100, 35, $data, '%l (%p)', array($col1 ,$col2, $col3));

                    $datay = array($correctas, $incorrectas, $no_respondidas);

                    // Create the graph. These two calls are always required
                    $graph = new Graph(220, 160, 'auto');
                    $graph->SetScale("textlin");

                    $theme_class = new UniversalTheme;
                    $graph->SetTheme($theme_class);

                    $graph->Set90AndMargin(100, 40, 40, 40);
                    $graph->img->SetAngle(90);

                    // set major and minor tick positions manually
                    $graph->SetBox(false);

                    //$graph->ygrid->SetColor('gray');
                    $graph->ygrid->Show(true);
                    $graph->ygrid->SetFill(false);
                    $graph->xaxis->SetTickLabels(array('Correctas', 'Incorrectas', 'No respondidas'));
                    $graph->yaxis->HideLine(false);
                    $graph->yaxis->HideTicks(false, false);

                    // Create the bar plots
                    $b1plot = new BarPlot($datay);
                    $b1plot->SetFillColor(array('#9CC96C', '#F76C52', '#EBAA4C'));
                    // ...and add it to the graPH
                    $graph->Add($b1plot);

                    $b1plot->SetWeight(0);
                    //$b1plot->SetFillGradient("#808000","#90EE90",GRAD_HOR);
                    $b1plot->SetWidth(15);

                    // Display the graph
                    $img = $graph->Stroke(_IMG_HANDLER);
                    ob_start();
                    imagepng($img);
                    $imageData = ob_get_contents();
                    $pic = 'data://text/plain;base64,' . base64_encode($imageData);
                    ob_end_clean();

                    $pdf->Image($pic, $pdf->GetX() + 30, $pdf->GetY() + 6, 0, 0, 'png');
                }
            }

            // $resultados = $cuestionario->resultados;
            // $correctas = count($resultados['correctas']);
            // $incorrectas = count($resultados['incorrectas']);
            // $no_respondidas = count($resultados['no_respondidas']);
            // $total_preguntas = $correctas + $incorrectas + $no_respondidas;

            // $porcentaje_correctas = ($correctas * 100) / $total_preguntas;
            // $porcentaje_incorrectas = ($incorrectas * 100) / $total_preguntas;
            // $porcentaje_no_respondidas = ($no_respondidas * 100) / $total_preguntas;


            $pdf->Ln(3);

            $pdf->SetXY($valX, $valY + 70);

            // $pdf->Ln(30);

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->CellFitScale(25, 6, utf8_decode(''), 0, 0, 'L', false);
            $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL CANDIDATO'), 'T', 0, 'C', false);
            $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
            $pdf->CellFitScale(55, 6, utf8_decode('FIRMA DEL EVALUADOR'), 'T', 0, 'C', false);
        }


        // ob_end_clean();
        $pdf->Output("Consolidado.pdf", 'I');
    }

    public function resultados($codsolicitudcifrado)
    {
        $codsolicitud = \Crypt::decrypt($codsolicitudcifrado);
        $solicitud = Solicitud::find($codsolicitud);

        $CuestionariosSolicitud = CuestionarioSolicitud::where('codsolicitud', $solicitud->codsolicitud)->get();
        $CuestionariosSolicitud->map(function ($obj) {
            $preguntas = Cuestionario::preguntasCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);
            $porcentaje = Cuestionario::porcentajeCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);
            $resultados = Cuestionario::resultadosCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);

            $obj['preguntas'] = $preguntas;
            $obj['porcentaje'] = $porcentaje;
            $obj['resultados'] = $resultados;
            return $obj;
        });


        $evaluadores = Usuario::where('codperfil', '4')->get();

        $params = [
            'meta_title' => 'Resultados cuestionario',
            'componente' => $this->componente('SOLIC_MIS'),
        ];


        JavaScript::put([
            'parametros' => $params,
            // 'preguntas' => $preguntas,
            //'cuestionario' => $cuestionario,
            //'codcuestionario_solicitudcifrado' => $codcuestionario_solicitudcifrado,
            'solicitud' => $solicitud,
            'CuestionariosSolicitud' => $CuestionariosSolicitud,
            'evaluadores' => $evaluadores,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Cuestionarios'],
            ['name' => 'resultados'],
        ]);


        View::share('parametros', $params);

        return view('solicitudes::cuestionarios.resultados')->with(compact('solicitud'));
    }

    public function generarnuevo(Request $request)
    {
        try {
            return \DB::transaction(function () use ($request) {
                $message = "";
                $tipo = $request->agendar['tipo'];
                $codcuestionario_solicitudcifrado = $request->codcuestionario_solicitudcifrado;
                $codcuestionario_solicitud = \Crypt::decrypt($codcuestionario_solicitudcifrado);

                $CuestionarioSolicitud = CuestionarioSolicitud::find($codcuestionario_solicitud);

                $cuestionario = $CuestionarioSolicitud->cuestionario;
                $solicitud = $CuestionarioSolicitud->solicitud;

                $preguntas = Cuestionario::preguntasCuestionarioSolicitud($CuestionarioSolicitud->codcuestionario, $CuestionarioSolicitud->codcuestionario_solicitud);

                $arr_incorrectas = [];
                $arr_ids_preguntas = [];
                foreach ($preguntas as $key => $obj) {
                    $correcta = $obj->opciones()->where('correcta', true)->first();
                    $opciones = $obj->opciones;

                    if ((empty($obj->value)) or (!empty($obj->value) && $obj->value !== $correcta->codopcion)) {
                        array_push($arr_incorrectas, $obj);
                    }
                    array_push($arr_ids_preguntas, $obj->codpregunta);
                }

                $agendamiento = new Agendada($request->agendar);
                $agendamiento->codsolicitud = $solicitud->codsolicitud;
                $agendamiento->save();

                \Mail::to($solicitud->usuario->email)
                    ->send(new SolicitudesAgendadas([
                        'solicitud' => $solicitud,
                        'agendamiento' => $agendamiento,
                    ]));

                $solicitudes_agendadas = Solicitud::where('codsolicitud', $solicitud->codsolicitud)->get();

                \Mail::to($agendamiento->evaluador->email)
                    ->send(new SolicitudesAgendadas([
                        'evaluador' => $agendamiento->evaluador,
                        'solicitudes_agendadas' => $solicitudes_agendadas,
                        'agendamiento' => $agendamiento,
                    ]));


                if ($tipo == 1) {
                    $solicitud->etapassolicitud()->update(['ultima' => false]);
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.agendado'),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }

                if ($tipo == 2) {
                    $solicitud->etapassolicitud()->update(['ultima' => false]);
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.habilitado'),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }

                $Cuestionario_nuevo = Cuestionario::create([
                    'nombre' => $cuestionario->nombre . ' - ' . Date('Y-m-d H:i:s'),
                    'segundos_x_pregunta' => $cuestionario->segundos_x_pregunta,
                    'tipo' => \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.S', 2)
                ]);
                // $Cuestionario_nuevo->preguntas()->attach($arr_incorrectas);
                $arr_ids_nuevas = [];
                foreach ($arr_incorrectas as $pregunta) {
                    $conocimiento = $pregunta->getconocimiento($pregunta->codpregunta)->first();
                    $arr_ids_not = array_unique(array_merge($arr_ids_nuevas, $arr_ids_preguntas));

                    $pregunta_nueva = Conocimiento::preguntasconocimiento($conocimiento->codconocimiento)->whereNotIn('preguntas.codpregunta', $arr_ids_not)->first();
                    if ($pregunta_nueva) {
                        PreguntaCuestionario::create([
                            'codcuestionario' => $Cuestionario_nuevo->codcuestionario,
                            'codpregunta' => $pregunta_nueva->codpregunta,
                        ]);
                        array_push($arr_ids_nuevas, $pregunta_nueva->codpregunta);
                    } else {
                        throw new \Exception("No se ha encontrado pregunta para el conocimiento: " . $conocimiento->nombre);
                    }
                }

                $CuestionarioSolicitud_nuevo = CuestionarioSolicitud::create([
                    'codsolicitud' => $CuestionarioSolicitud->codsolicitud,
                    'codcuestionario' => $Cuestionario_nuevo->codcuestionario,
                    'terminado' => false
                ]);

                $CuestionariosSolicitud = CuestionarioSolicitud::where('codsolicitud', $solicitud->codsolicitud)->get();
                $CuestionariosSolicitud->map(function ($obj) {
                    $preguntas = Cuestionario::preguntasCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);
                    $porcentaje = Cuestionario::porcentajeCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);
                    $resultados = Cuestionario::resultadosCuestionarioSolicitud($obj->cuestionario->codcuestionario, $obj->codcuestionario_solicitud);

                    $obj['preguntas'] = $preguntas;
                    $obj['porcentaje'] = $porcentaje;
                    $obj['resultados'] = $resultados;
                    return $obj;
                });

                return response()->json([
                    'cuestionarios_solicitud' => $CuestionariosSolicitud,
                    'message' => $message
                ]);
            }, 5);
        } catch (\Exception $e) {
            //dd($e);
            $message = "";
            if ($e instanceof \Throwable) {
                $message = $e->getMessage();
            }
            return response()->json([
                'cuestionarios_solicitud' => [],
                'message' => $message
            ], 500);
        }
    }

    public function rep_tiempo(Request $request)
    {
        try {
            return \DB::transaction(function () use ($request) {
                $hora_actual = Carbon::now();
                $codsolicitud = $request->codsolicitud;
                $Solicitud = Solicitud::find($codsolicitud);

                $Solicitud->etapassolicitud()->update(['ultima' => false]);
                $EtapaSolicitud = EtapaSolicitud::create([
                    'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.iniciado', 5),
                    'codsolicitud' => $Solicitud->codsolicitud,
                    'ultima' => true
                ]);

                $codcuestionario = session('resp_cuestionario')['codcuestionario'];
                $Cuestionario = Cuestionario::find($codcuestionario);

                $CuestionarioSolicitud = CuestionarioSolicitud::where('codcuestionario', $codcuestionario)
                    ->where('codsolicitud', $codsolicitud)->first();

                if (empty($CuestionarioSolicitud->hora_inicio)) {
                    $CuestionarioSolicitud->hora_inicio = $hora_actual->format('Y-m-d H:i:s');
                    $preguntas = Cuestionario::preguntasCuestionarioSolicitud($CuestionarioSolicitud->codcuestionario, $CuestionarioSolicitud->codcuestionario_solicitud);
                    $segundos = (int) $Cuestionario->segundos_x_pregunta * count($preguntas);
                    $hora_fin_estimado = $hora_actual->copy()->addSeconds($segundos);
                    $CuestionarioSolicitud->hora_fin_estimado = $hora_fin_estimado->format('Y-m-d H:i:s');
                    $CuestionarioSolicitud->save();
                } else {
                    $hora_fin_estimado = \Carbon::parse($CuestionarioSolicitud->hora_fin_estimado);
                }

                $segundos_restantes = $hora_actual->diffInSeconds($hora_fin_estimado);

                if ($hora_actual->format('Y-m-d H:i:s') > $hora_fin_estimado->format('Y-m-d H:i:s')) {
                    $segundos_restantes = 0;
                }

                session(['resp_cuestionario' => [
                    'hora_inicio' => $CuestionarioSolicitud->hora_inicio,
                    'codcuestionario' => $codcuestionario
                ]]);

                return response()->json([
                    'rep_tiempo' => session('resp_cuestionario'),
                    'cuestionariosolicitud' => $CuestionarioSolicitud,
                    'segundos_restantes' => $segundos_restantes
                ]);
            }, 5);
        } catch (\Exception $e) {
            //dd($e);
            return response()->json([], 500);
        }
    }

    public function finalizar($codcuestionario_solicitudcifrado)
    {
        try {
            $solicitud = \DB::transaction(function () use ($codcuestionario_solicitudcifrado) {
                $codcuestionario_solicitud = \Crypt::decrypt($codcuestionario_solicitudcifrado);
                $CuestionarioSolicitud = CuestionarioSolicitud::find($codcuestionario_solicitud);
                $cuestionario = $CuestionarioSolicitud->cuestionario;
                $solicitud = $CuestionarioSolicitud->solicitud;

                $porcentaje = Cuestionario::PorcentajeCuestionarioSolicitud($cuestionario->codcuestionario, $codcuestionario_solicitud);

                $Valoracion = Valoracion::where('codsolicitud', $solicitud->codsolicitud)->first();
                if (empty($Valoracion)) {
                    $Valoracion = new Valoracion([
                        'codsolicitud'  => $solicitud->codsolicitud
                    ]);
                }

                if ((int) $cuestionario->tipo == (int) \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.P', 1)) {
                    $Valoracion->cuestionario1 = $porcentaje;

                    if ($porcentaje >= 85) {
                        $Evaluaciones = Evaluacion::all();
                        foreach ($Evaluaciones as $ev) {
                            Evaluacion_solicitud::create([
                                'codsolicitud' => $solicitud->codsolicitud,
                                'codevaluacion' => $ev->codevaluacion,
                                'aprobo' => false
                            ]);
                        }
                    }

                    $solicitud->etapassolicitud()->update(['ultima' => false]);
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.procesado', 6),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }

                if ((int) $cuestionario->tipo == (int) \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.S', 2)) {
                    $Valoracion->cuestionario2 = $porcentaje;

                    if ($porcentaje >= 100) {
                        $Evaluaciones = Evaluacion::all();
                        foreach ($Evaluaciones as $ev) {
                            Evaluacion_solicitud::create([
                                'codsolicitud' => $solicitud->codsolicitud,
                                'codevaluacion' => $ev->codevaluacion,
                                'aprobo' => false
                            ]);
                        }
                        $solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.procesado', 6),
                            'codsolicitud' => $solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                    } else {
                        $solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.no_aprobado', 8),
                            'codsolicitud' => $solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                    }
                }
                $Valoracion->save();

                $CuestionarioSolicitud->terminado = true;
                $CuestionarioSolicitud->hora_fin = Date('Y-m-d H:i:s');
                $CuestionarioSolicitud->save();

                return $solicitud;
            }, 5);

            return redirect()->route('solicitudes.cuestionarios.resultados', [\Crypt::encrypt($solicitud->codsolicitud)]);
        } catch (\Exception $e) {
            dd($e);
            abort(500);
        }
    }

    public function tiempo_finalizado($codcuestionario_solicitudcifrado)
    {
        try {
            $solicitud = \DB::transaction(function () use ($codcuestionario_solicitudcifrado) {
                $codcuestionario_solicitud = \Crypt::decrypt($codcuestionario_solicitudcifrado);
                $CuestionarioSolicitud = CuestionarioSolicitud::find($codcuestionario_solicitud);
                $codsolicitud = $CuestionarioSolicitud->codsolicitud;
                $solicitud = Solicitud::find($codsolicitud);
                $cuestionario = $CuestionarioSolicitud->cuestionario;
                $CuestionarioSolicitud->terminado = true;
                $CuestionarioSolicitud->hora_fin = Date('Y-m-d H:i:s');

                $porcentaje = Cuestionario::PorcentajeCuestionarioSolicitud($cuestionario->codcuestionario, $codcuestionario_solicitud);
                $Valoracion = Valoracion::where('codsolicitud', $solicitud->codsolicitud)->first();
                if (empty($Valoracion)) {
                    $Valoracion = new Valoracion([
                        'codsolicitud'  => $solicitud->codsolicitud
                    ]);
                }

                if ((int) $cuestionario->tipo == (int) \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.P', 1)) {
                    $Valoracion->cuestionario1 = $porcentaje;

                    if ($porcentaje >= 85) {
                        $Evaluaciones = Evaluacion::all();
                        foreach ($Evaluaciones as $ev) {
                            Evaluacion_solicitud::create([
                                'codsolicitud' => $solicitud->codsolicitud,
                                'codevaluacion' => $ev->codevaluacion
                            ]);
                        }
                    }

                    $solicitud->etapassolicitud()->update(['ultima' => false]);
                    $EtapaSolicitud = EtapaSolicitud::create([
                        'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.procesado', 6),
                        'codsolicitud' => $solicitud->codsolicitud,
                        'ultima' => true
                    ]);
                }

                if ((int) $cuestionario->tipo == (int) \Config::get('dominios.TIPO_CUESTIONARIO.VALORES.S', 2)) {
                    $Valoracion->cuestionario2 = $porcentaje;

                    if ($porcentaje >= 100) {
                        $Evaluaciones = Evaluacion::all();
                        foreach ($Evaluaciones as $ev) {
                            Evaluacion_solicitud::create([
                                'codsolicitud' => $solicitud->codsolicitud,
                                'codevaluacion' => $ev->codevaluacion
                            ]);
                        }
                        $solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.procesado', 6),
                            'codsolicitud' => $solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                    } else {
                        $solicitud->etapassolicitud()->update(['ultima' => false]);
                        $EtapaSolicitud = EtapaSolicitud::create([
                            'etapa' => \Config::get('dominios.ETAPA_SOLICITUD.VALORES.no_aprobado', 8),
                            'codsolicitud' => $solicitud->codsolicitud,
                            'ultima' => true
                        ]);
                    }
                }
                $Valoracion->save();
                $EtapaSolicitud->save();
                $CuestionarioSolicitud->save();

                return $solicitud;
            }, 5);

            return redirect()->route('solicitudes.cuestionarios.resultados', [\Crypt::encrypt($solicitud->codsolicitud)]);
        } catch (\Exception $e) {
            //dd($e);
            abort(500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function responder($codcuestionario_solicitud)
    {
        $codcuestionario_solicitud = \Crypt::decrypt($codcuestionario_solicitud);
        $CuestionarioSolicitud = CuestionarioSolicitud::find($codcuestionario_solicitud);
        $solicitud = $CuestionarioSolicitud->solicitud;
        $cuestionario = $solicitud->cuestionarios()->where('terminado', false)->first();

        $preguntas = Cuestionario::preguntasCuestionarioSolicitud($cuestionario->codcuestionario, $CuestionarioSolicitud->codcuestionario_solicitud);

        session(['resp_cuestionario' => [
            'hora_inicio' => null,
            'codcuestionario' => $cuestionario->codcuestionario
        ]]);

        $params = [
            'meta_title' => 'Responder cuestionario',
            'componente' => $this->componente('SOLIC_MIS'),
        ];

        JavaScript::put([
            'parametros' => $params,
            'preguntas' => $preguntas,
            'cuestionario' => $cuestionario,
            'codcuestionario_solicitudcifrado' => \Crypt::encrypt($cuestionario->pivot->codcuestionario_solicitud),
            'solicitud' => $solicitud,
            'CuestionarioSolicitud' => $CuestionarioSolicitud
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Mis Solicitudes'],
            ['name' => 'Responder cuestionario'],
        ]);

        View::share('parametros', $params);

        return view('solicitudes::cuestionarios.responder');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function respondercorto($codcuestionario_solicitud)
    {
        $codcuestionario_solicitud = \Crypt::decrypt($codcuestionario_solicitud);
        $CuestionarioSolicitud = CuestionarioSolicitud::find($codcuestionario_solicitud);
        $solicitud = $CuestionarioSolicitud->solicitud;
        $cuestionario = $solicitud->cuestionarios()->where('terminado', false)->first();

        $preguntas = Cuestionario::preguntasCuestionarioSolicitud($cuestionario->codcuestionario, $CuestionarioSolicitud->codcuestionario_solicitud);

        session(['resp_cuestionario' => [
            'hora_inicio' => null,
            'codcuestionario' => $cuestionario->codcuestionario
        ]]);

        $params = [
            'meta_title' => 'Responder cuestionario',
            'componente' => $this->componente('SOLIC_MIS'),
        ];

        JavaScript::put([
            'parametros' => $params,
            'preguntas' => $preguntas,
            'cuestionario' => $cuestionario,
            'codcuestionario_solicitudcifrado' => \Crypt::encrypt($cuestionario->pivot->codcuestionario_solicitud),
            'solicitud' => $solicitud,
            'CuestionarioSolicitud' => $CuestionarioSolicitud
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Mis Solicitudes'],
            ['name' => 'Responder cuestionario'],
        ]);

        View::share('parametros', $params);

        return view('solicitudes::cuestionarios.respondercorto');
    }
}
