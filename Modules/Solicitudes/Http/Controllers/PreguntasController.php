<?php

namespace Modules\Solicitudes\Http\Controllers;

use Modules\Solicitudes\Entities\Cuestionario;
use Modules\Solicitudes\Entities\Solicitud;
use Modules\Solicitudes\Entities\Opcion;
use Modules\Solicitudes\Entities\Respuesta;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use JavaScript;
//Traits
use ABCcomptencias\Traits\Permisos;

class PreguntasController extends Controller
{

    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function responder(Request $request)
    {
        try {
            \DB::transaction(function () use($request) {
                $codopcion = $request->codopcion;
                $codcuestionario_solicitud = \Crypt::decrypt($request->codcuestionario_solicitudcifrado);
                $Opcion = Opcion::find($codopcion);
                $opciones_pregunta = Opcion::where('codpregunta', $request->codpregunta)->get();
                if (count($opciones_pregunta)) {
                    foreach ($opciones_pregunta as $value) {
                        if ($value->respuestas()->count() > 0) {
                            $value->respuestas()->where('respuestas.codcuestionario_solicitud', $codcuestionario_solicitud)->delete();
                        }
                    }
                }
                if(!empty($codopcion)) {
                    $Respuesta = new Respuesta($request->all());
                    $Respuesta->codcuestionario_solicitud = $codcuestionario_solicitud;
                    $Respuesta->save();
                }
            }, 5);
            return response()->json([], 200);
        } catch (\Exception $e) {
            //dd($e);
            return response()->json([], 500);
        }

    }
}
