<?php

Route::group(['middleware' => 'web', 'prefix' => 'solicitudes', 'namespace' => 'Modules\Solicitudes\Http\Controllers'], function()
{
    Route::get('registrar', [
        'uses' => 'SolicitudesController@registrar',
        'as'   => 'solicitudes.registrar',
    ]);    

    Route::post('registrar_titulaciones', [
        'uses' => 'SolicitudesController@registrar_titulaciones',
        'as'   => 'solicitudes.registrar_titulaciones',
    ]);    

    Route::post('registrar_normas', [
        'uses' => 'SolicitudesController@registrar_normas',
        'as'   => 'solicitudes.registrar_normas',
    ]);

    Route::get('missolicitudes', [
        'uses' => 'SolicitudesController@missolicitudes',
        'as'   => 'solicitudes.missolicitudes',
    ]);     

    Route::get('todosdatosmissolicitudes', [
        'uses' => 'SolicitudesController@todosdatosmissolicitudes',
        'as'   => 'solicitudes.todosdatosmissolicitudes',
    ]); 

    Route::get('todas', [
        'uses' => 'SolicitudesController@todas',
        'as'   => 'solicitudes.todas',
    ]);     

    Route::post('datatable_todas', [
        'uses' => 'SolicitudesController@datatable_todas',
        'as'   => 'solicitudes.datatable_todas',
    ]);     

    Route::post('cambiaretapa', [
        'uses' => 'SolicitudesController@cambiaretapa',
        'as'   => 'solicitudes.cambiaretapa',
    ]);  

    Route::post('agendar', [
        'uses' => 'SolicitudesController@agendar',
        'as'   => 'solicitudes.agendar',
    ]);  

    Route::post('getSolicitud', [
        'uses' => 'SolicitudesController@getSolicitud',
        'as'   => 'solicitudes.getSolicitud',
    ]);  

    Route::get('evaluacion/{codevaluacionsolicitudcifrado}', [
        'uses' => 'SolicitudesController@evaluacion',
        'as'   => 'solicitudes.evaluacion',
    ]);  

    Route::get('generarpdfevaluacion/{codevaluacionsolicitudcifrado}', [
        'uses' => 'SolicitudesController@generarpdfevaluacion',
        'as'   => 'solicitudes.generarpdfevaluacion',
    ]);  
    
    Route::post('registrarvaloracion', [
        'uses' => 'SolicitudesController@registrarvaloracion',
        'as'   => 'solicitudes.registrarvaloracion',
    ]);  

    Route::post('registrarevaluacionsolicitud', [
        'uses' => 'SolicitudesController@registrarevaluacionsolicitud',
        'as'   => 'solicitudes.registrarevaluacionsolicitud',
    ]);  

    Route::post('evaluar', [
        'uses' => 'SolicitudesController@evaluar',
        'as'   => 'solicitudes.evaluar',
    ]);  

    Route::post('habilitar', [
        'uses' => 'SolicitudesController@habilitar',
        'as'   => 'solicitudes.habilitar',
    ]);

    Route::post('pagoregistrado', [
        'uses' => 'SolicitudesController@pagoregistrado',
        'as'   => 'solicitudes.pagoregistrado',
    ]);

    Route::post('enlacepago', [
        'uses' => 'SolicitudesController@enlacepago',
        'as'   => 'solicitudes.enlacepago',
    ]);  

    Route::group(['prefix' => 'pagos'], function() {
        Route::match(['get', 'post'], 'respuesta', [
            'uses' => 'SolicitudesController@respuesta_pago',
            'as'   => 'solicitudes.pagos.respuesta_pago',
        ]); 
        
        Route::match(['get', 'post'], 'confirmacion', [
            'uses' => 'SolicitudesController@confirmacion_pago',
            'as'   => 'solicitudes.pagos.confirmacion_pago',
        ]); 

        Route::get('detalle/{codpagosolicitud}', [
            'uses' => 'PagosSolicitudesController@detalle',
            'as'   => 'solicitudes.pagos.detalle',
        ]); 
    });

    Route::group(['prefix' => 'certificados'], function() {
        Route::get('ver/{codcertificadocifrado}', [
            'uses' => 'CertificadosController@ver',
            'as'   => 'solicitudes.certificados.ver',
        ]); 
    });

    Route::group(['prefix' => 'cuestionarios'], function() {
        Route::get('responder/{codcuestionario_solicitud}', [
            'uses' => 'CuestionariosController@responder',
            'as'   => 'solicitudes.cuestionarios.responder',
        ]);

        Route::get('respondercorto/{codcuestionario_solicitud}', [
            'uses' => 'CuestionariosController@respondercorto',
            'as'   => 'solicitudes.cuestionarios.respondercorto',
        ]);

        Route::get('tiempo_finalizado/{codcuestionariocifrado}', [
            'uses' => 'CuestionariosController@tiempo_finalizado',
            'as'   => 'solicitudes.cuestionarios.tiempo_finalizado',
        ]);

        Route::get('finalizar/{codcuestionariocifrado}', [
            'uses' => 'CuestionariosController@finalizar',
            'as'   => 'solicitudes.cuestionarios.finalizar',
        ]);

        Route::post('rep_tiempo', [
            'uses' => 'CuestionariosController@rep_tiempo',
            'as'   => 'solicitudes.cuestionarios.rep_tiempo',
        ]);

        Route::group(['prefix' => 'preguntas'], function() {
            Route::post('responder', [
                'uses' => 'PreguntasController@responder',
                'as'   => 'solicitudes.cuestionarios.preguntas.responder',
            ]); 
        });

        Route::get('resultados/{codsolicitudcifrado}', [
            'uses' => 'CuestionariosController@resultados',
            'as'   => 'solicitudes.cuestionarios.resultados',
        ]);

        Route::get('imprimircuestionario/{codsolicitudcifrado}', [
            'uses' => 'CuestionariosController@imprimircuestionario',
            'as'   => 'solicitudes.cuestionarios.imprimircuestionario',
        ]);

        Route::get('verpdf/{codsolicitudcifrado}', [
            'uses' => 'CuestionariosController@verpdf',
            'as'   => 'solicitudes.cuestionarios.verpdf',
        ]);

        Route::post('generarnuevo', [
            'uses' => 'CuestionariosController@generarnuevo',
            'as'   => 'solicitudes.cuestionarios.generarnuevo',
        ]);

    });

    
    
});
