<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Parametro extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'parametros';
    protected $primaryKey = 'codparametro';
    protected $fillable   = [
        'conocimientos',
        'desempeno',
        'producto',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codparametro',
        'conocimientos',
        'desempeno',
        'producto',
    ];


    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el parametro: ' . $this->codparametro;
                break;
            case 'updated':
                return 'Editó el parametro: ' . $this->codparametro;
                break;
            case 'deleted':
                return 'Eliminó el parametro: ' . $this->codparametro;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    

}
