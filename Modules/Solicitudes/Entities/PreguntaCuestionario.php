<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PreguntaCuestionario extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'preguntas_cuestionarios';
    protected $primaryKey = 'codpregunta_cuestionario';
    protected $fillable   = [
        'codcuestionario',
        'codpregunta',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codpregunta_cuestionario',
        'codcuestionario',
        'codpregunta',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la pregunta cuestionario: ' . $this->codpregunta_cuestionario;
                break;
            case 'updated':
                return 'Editó la pregunta cuestionario: ' . $this->codpregunta_cuestionario;
                break;
            case 'deleted':
                return 'Eliminó la pregunta cuestionario: ' . $this->codpregunta_cuestionario;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function respuestas()
    {
        return $this->hasMany('Modules\Solicitudes\Entities\Respuesta', 'codpregunta_cuestionario', 'codpregunta_cuestionario');
    }
}
