<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Valoracion_indicador extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'valoracion_indicadores';
    protected $primaryKey = 'codvaloracionindicador';
    protected $fillable   = [
        'codindicadorevaluacion',
        'cumple',
        'observacion',
    ];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codvaloracionindicador',
        'codindicadorevaluacion',
        'cumple',
        'observacion',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el Valoracion_indicador: ' . $this->codvaloracionindicador;
                break;
            case 'updated':
                return 'Editó el Valoracion_indicador: ' . $this->codvaloracionindicador;
                break;
            case 'deleted':
                return 'Eliminó el Valoracion_indicador: ' . $this->codvaloracionindicador;
                break;
        }
        return '';
    }

}
