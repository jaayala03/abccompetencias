<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Evaluacion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'evaluaciones';
    protected $primaryKey = 'codevaluacion';
    protected $fillable   = [
        'nombre',
        'descripcion'
    ];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitud';

    protected static $logAttributes = [
        'codevaluacion',
        'nombre',
        'descripcion'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la Evaluacion: ' . $this->nombre;
                break;
            case 'updated':
                return 'Editó la Evaluacion: ' . $this->nombre;
                break;
            case 'deleted':
                return 'Eliminó la Evaluacion: ' . $this->nombre;
                break;
        }
        return '';
    }

}
