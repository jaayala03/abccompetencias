<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Evaluacion_solicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'evaluaciones_solicitudes';
    protected $primaryKey = 'codevaluacionsolicitud';
    protected $fillable   = [
        'codsolicitud',
        'codevaluacion',
        'aprobo',
        'codevaluador',
        'fecha_aplicacion',
        'fecha_resultados',
        'lugar',
        'ciudad',
    ];

    protected $appends = ['evaluacion', 'evaluador'];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codevaluacionsolicitud',
        'codsolicitud',
        'codevaluacion',
        'aprobo',
        'codevaluador',
        'fecha_aplicacion',
        'fecha_resultados',
        'lugar',
        'ciudad',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el evaluaciones_solicitudes: ' . $this->codevaluacionsolicitud;
                break;
            case 'updated':
                return 'Editó el evaluaciones_solicitudes: ' . $this->codevaluacionsolicitud;
                break;
            case 'deleted':
                return 'Eliminó el evaluaciones_solicitudes: ' . $this->codevaluacionsolicitud;
                break;
        }
        return '';
    }

    public function getEvaluacionAttribute($value)
    {
        $key = 'evaluacion';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Evaluacion(['nombre' => '']);
        $this->setRelation($key, $value);
        return $value;
    }

    public function getEvaluadorAttribute($value)
    {
        $key = 'evaluador';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Usuario(['nombres' => '']);
        $this->setRelation($key, $value);
        return $value;
    }

    function indicadores_evaluacion()
    {
        return $this->hasMany('Modules\Solicitudes\Entities\Indicador_evaluacion', 'codevaluacionsolicitud', 'codevaluacionsolicitud');
    }

    public function evaluacion()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Evaluacion', 'codevaluacion');
    }

    public function evaluador()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Usuario', 'codevaluador', 'codusuario');
    }

}
