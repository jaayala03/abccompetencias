<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Certificado extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'certificados';
    protected $primaryKey = 'codcertificado';
    protected $fillable   = [
        'fecha',
        'fecha_vencimiento',
        'codsolicitud',
        'codigo'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codcertificado',
        'fecha',
        'fecha_vencimiento',
        'codsolicitud',
        'codigo'
    ];

    protected $with = ['solicitud'];


    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el certificado: ' . $this->codcertificado;
                break;
            case 'updated':
                return 'Editó el certificado: ' . $this->codcertificado;
                break;
            case 'deleted':
                return 'Eliminó el certificado: ' . $this->codcertificado;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    
    //relacion con solicitud
    public function solicitud()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Solicitud', 'codsolicitud');
    }
}
