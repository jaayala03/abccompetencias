<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Norma extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'normas';
    protected $primaryKey = 'codnorma';
    protected $fillable   = [
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion'
    ];
    //**********************************************************************

    // protected $appends = ['tipotitulacion'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codnorma',
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la norma: ' . $this->codigo;
                break;
            case 'updated':
                return 'Editó la norma: ' . $this->codigo;
                break;
            case 'deleted':
                return 'Eliminó la norma: ' . $this->codigo;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    // //**********************************************************************
    // public function getRutaImagenAttribute()
    // {
    //     return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }    

    // public function getRutaImagenPublicaAttribute()
    // {
    //     return route('outside.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }
    
    // public function getTipotitulacionAttribute($value)
    // {
    //     $key = 'tipotitulacion';
    //     if (array_key_exists($key, $this->relations)) {
    //         $value = $this->relations[$key];
    //     } elseif (method_exists($this, $key)) {
    //         $value = $this->getRelationshipFromMethod($key);
    //     }
    //     $value = $value ?: new Tipo_titulacion(['nombre' => '']);
    //     $this->setRelation($key, $value);
    //     return $value;
    // }


    //relaciones********************************************************************
    //relacion de muchos a uno con tipos de titulacion
    
    public function tipotitulacion()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Tipo_titulacion', 'codtipo_titulacion');
    }

    function conocimientos() {
        return $this->hasMany('Modules\Solicitudes\Entities\Conocimiento', 'codnorma', 'codnorma');
    }
}
