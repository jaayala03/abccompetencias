<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Valoracion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'valoraciones';
    protected $primaryKey = 'codvaloracion';
    protected $fillable   = [
        "cuestionario1",
        "cuestionario2",
        "producto",
        "desempeno",
        "aprobacion",
        "codsolicitud",
    ];
    //**********************************************************************

      // protected $appends = ['rutaimagen', 'rutaimagenpublica'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        "codvaloracion",
        "cuestionario1",
        "cuestionario2",
        "producto",
        "desempeno",
        "aprobacion",
        "codsolicitud",
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la valoración: ' . $this->codvaloracion;
                break;
            case 'updated':
                return 'Editó la valoración: ' . $this->codvaloracion;
                break;
            case 'deleted':
                return 'Eliminó la valoración: ' . $this->codvaloracion;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }


}
