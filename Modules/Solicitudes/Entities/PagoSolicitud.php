<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;

class PagoSolicitud extends Model
{

    protected $table      = 'pagos_solicitudes';
    protected $primaryKey = 'codpagosolicitud';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference',
        'payu_order_id',
        'transaction_id',
        'state',
        'value',
        'user_id',
        'codsolicitud',
        'respuesta',
        'franchise',
        'currency',
        'description'
    ];

    //relacion con solicitudes
    public function solicitud()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Solicitud', 'codsolicitud');
    }
}
