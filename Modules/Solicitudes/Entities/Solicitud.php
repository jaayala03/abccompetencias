<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Solicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'solicitudes';
    protected $primaryKey = 'codsolicitud';
    protected $fillable   = [
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];
    //**********************************************************************

    protected $appends = ['usuario', 'norma', 'agendadas', 'codsolicitudcifrado'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codsolicitud',
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la solicitud: ' . $this->codsolicitud;
                break;
            case 'updated':
                return 'Editó la solicitud: ' . $this->codsolicitud;
                break;
            case 'deleted':
                return 'Eliminó la solicitud: ' . $this->codsolicitud;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    //**********************************************************************

    public function getAgendadasAttribute($value)
    {
        return $this->agendadas()->get();
    }

    public function getUsuarioAttribute($value)
    {
        $key = 'usuario';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Usuario(['nombres' => '']);
        $this->setRelation($key, $value);
        return $value;
    }
    

    public function getNormaAttribute($value)
    {
        $key = 'norma';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Norma(['nombre' => '']);
        $this->setRelation($key, $value);
        return $value;
    }

    public function getCodsolicitudcifradoAttribute($value)
    {
        return \Crypt::encrypt($this->codsolicitud);
    }

    function cuestinario_solicitudes() {
        return $this->hasMany('Modules\Solicitudes\Entities\CuestionarioSolicitud', 'codsolicitud', 'codsolicitud');
    }

    function agendadas() {
        return $this->hasMany('Modules\Solicitudes\Entities\Agendada', 'codsolicitud', 'codsolicitud');
    }

    function etapassolicitud() {
        return $this->hasMany('Modules\Solicitudes\Entities\EtapaSolicitud', 'codsolicitud', 'codsolicitud');
    }

    function pagos_solicitud() {
        return $this->hasMany('Modules\Solicitudes\Entities\PagoSolicitud', 'codsolicitud', 'codsolicitud');
    }

    //relacion con norma
    public function norma()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Norma', 'codnorma');
    }
    
    //relacion con certificados
    public function certificados()
    {
        return $this->hasMany('Modules\Solicitudes\Entities\Certificado', 'codsolicitud', 'codsolicitud');
    }

    //relacion con usuario
    public function usuario()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Usuario', 'codusuario');
    }

    //relacion con cuestionarios
    public function cuestionarios()
    {
        return $this->belongsToMany('Modules\Solicitudes\Entities\Cuestionario', 'cuestionarios_solicitud', 'codsolicitud', 'codcuestionario')
        ->withPivot('codcuestionario_solicitud', 'terminado')
        ->withTimestamps();
    }

    //relacion con evaluaciones
    public function evaluaciones()
    {
        return $this->belongsToMany('Modules\Solicitudes\Entities\Evaluacion', 'evaluaciones_solicitudes', 'codsolicitud', 'codevaluacion')
        ->withPivot('codevaluacionsolicitud', 'aprobo')
        ->withTimestamps();
    }

    public function scopeSolicitudesusuario($query, $codusuario)
    {
        $query->leftjoin('titulaciones', 'solicitudes.codtitulacion', '=', 'titulaciones.codtitulacion')
        ->leftjoin('etapas_solicitudes', 'solicitudes.codsolicitud', '=', 'etapas_solicitudes.codsolicitud')
        ->leftjoin('normas', 'solicitudes.codnorma', '=', 'normas.codnorma')
        ->select('normas.codigo', 'normas.nombre', 'normas.valor', 'etapas_solicitudes.etapa', \DB::raw("to_char(etapas_solicitudes.created_at, 'DD/MM/YYYY HH12:MI PM') as fecha"), 'solicitudes.codsolicitud')
        ->where('ultima', true);
        
        if(isset($codusuario)){
            $query->where('solicitudes.codusuario', $codusuario);
        }

        return $query;
    }    

    public function scopeTodas($query, $request)
    {
        $query->select('usuarios.documento', 'normas.codigo', 'normas.nombre', 'etapas_solicitudes.etapa', \DB::raw("to_char(etapas_solicitudes.created_at, 'DD/MM/YYYY HH12:MI PM') as fecha"), 'solicitudes.codsolicitud', 'usuarios.codusuario')
        ->join('etapas_solicitudes', 'etapas_solicitudes.codsolicitud', '=', 'solicitudes.codsolicitud')
        ->leftjoin('titulaciones', 'solicitudes.codtitulacion', '=', 'titulaciones.codtitulacion')
        ->leftjoin('normas', 'solicitudes.codnorma', '=', 'normas.codnorma')
        ->join('usuarios', 'solicitudes.codusuario', '=', 'usuarios.codusuario');

        if(!empty($request['etapa'])) {
            $query->where('etapas_solicitudes.etapa', $request['etapa'])
            ->where('ultima', true);
        }

        if(!empty($request['norma_filtros'])){
            $query->where('solicitudes.codnorma', $request['norma_filtros']);
        }

        if(!empty($request['fecha_inicio']) && !empty($request['fecha_fin'])){
            $query->whereBetween('solicitudes.fecha', [$request['fecha_inicio'], $request['fecha_fin']]);
        }

        return $query;
    }

}
