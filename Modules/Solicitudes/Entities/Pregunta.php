<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pregunta extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'preguntas';
    protected $primaryKey = 'codpregunta';
    protected $fillable   = [
        'pregunta',
        'codconocimiento',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codpregunta',
        'pregunta',
        'codconocimiento',
    ];

    protected $appends = ['opciones'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la pregunta: ' . $this->pregunta;
                break;
            case 'updated':
                return 'Editó la pregunta: ' . $this->pregunta;
                break;
            case 'deleted':
                return 'Eliminó la pregunta: ' . $this->pregunta;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function getOpcionesAttribute($value)
    {
        $opciones = $this->opciones()->orderBy('opciones.codopcion')->get();
        $x = 'A';
        $alp = range('A', 'Z');
        $opciones->map(function ($obj, $key) use ($alp) {
            $obj->numero = $alp[$key];
            return $obj;
        });

        return $opciones;
    }

    function opciones() {
        return $this->hasMany('Modules\Solicitudes\Entities\Opcion', 'codpregunta', 'codpregunta');
    }

    // public function conocimiento()
    // {
    //     return $this->belongsTo('Modules\Solicitudes\Entities\Conocimiento', 'codconocimiento');
    // }

    public function scopeGetConocimiento($query, $codpregunta){
        $query->select('conocimientos.*')
            ->join('actividades', 'preguntas.codactividad', '=', 'actividades.codactividad')
            ->join('conocimientos', 'actividades.codconocimiento', '=', 'conocimientos.codconocimiento')
            ->where('preguntas.codpregunta', '=', $codpregunta);
        return $query;
    }
    

}
