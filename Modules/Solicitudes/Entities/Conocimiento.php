<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Conocimiento extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'conocimientos';
    protected $primaryKey = 'codconocimiento';
    protected $fillable   = [
        'nombre',
        'codnorma',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'parametros';

    protected static $logAttributes = [
        'codconocimiento',
        'nombre',
        'codnorma',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la conocimiento: ' . $this->codconocimiento;
                break;
            case 'updated':
                return 'Editó la conocimiento: ' . $this->codconocimiento;
                break;
            case 'deleted':
                return 'Eliminó la conocimiento: ' . $this->codconocimiento;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'parametros';
    }

    public function scopePreguntasCuestionario($query, $codconocimiento,  $codcuestionario)
    {
        $preguntas = Pregunta::join('actividades', 'preguntas.codactividad', '=', 'actividades.codactividad')
            ->join('conocimientos', 'actividades.codconocimiento', '=', 'conocimientos.codconocimiento')
            ->join('preguntas_cuestionarios', 'preguntas_cuestionarios.codpregunta', '=', 'preguntas.codpregunta')
            ->select('preguntas.*', 'preguntas_cuestionarios.codpregunta_cuestionario')
            ->where('conocimientos.codconocimiento', $codconocimiento)
            ->where('preguntas_cuestionarios.codcuestionario', $codcuestionario)
            ->distinct()
            ->get();
            
        $preguntas = $preguntas->map(function ($obj, $key) {
            $obj->value = '';
            $obj->numero = $key + 1;
            $codpregunta_cuestionario = $obj->codpregunta_cuestionario;
            $PreguntaCuestionario = PreguntaCuestionario::find($codpregunta_cuestionario);

            $respuestas = $PreguntaCuestionario->respuestas;
            if (count($respuestas) > 0) {
                $respuesta = $PreguntaCuestionario->respuestas()->first();
                $obj->value = $respuesta->codopcion;
            }
            return $obj;
        });
        return $preguntas;
    }

    public function scopeResultados($query, $codconocimiento, $codcuestionario)
    {
        $preguntas = Conocimiento::PreguntasCuestionario($codconocimiento, $codcuestionario);
        $correctas = [];
        $incorrectas = [];
        $no_respondidas = [];

        foreach ($preguntas as $key => $obj) {
            $correcta = $obj->opciones()->where('correcta', true)->first();
            $opciones = $obj->opciones;

            if(!empty($correcta) && $obj->value == $correcta->codopcion){
                array_push($correctas, $obj);
            } else if(!empty($correcta) && !empty($obj->value) && $obj->value !== $correcta->codopcion){
                array_push($incorrectas, $obj);
            } else {
                array_push($no_respondidas, $obj);
            }
        }
        return [
            'correctas' => $correctas,
            'incorrectas' => $incorrectas,
            'no_respondidas' => $no_respondidas,
        ];
    }

    public function scopePreguntasConocimiento($query, $codconocimiento){
        $query->select('preguntas.*')
            ->join('actividades', 'conocimientos.codconocimiento', '=', 'actividades.codconocimiento')
            ->join('preguntas', 'actividades.codactividad', '=', 'preguntas.codactividad')
            ->where('conocimientos.codconocimiento', '=', $codconocimiento);
        return $query;
    }

    function preguntas() {
        return $this->hasMany('Modules\Solicitudes\Entities\Pregunta', 'codconocimiento', 'codconocimiento');
    }

}
