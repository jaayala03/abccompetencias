<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Indicador_evaluacion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'indicadores_evaluacion';
    protected $primaryKey = 'codindicadorevaluacion';
    protected $fillable   = [
        'codevaluacionsolicitud',
        'codindicador',
    ];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codindicadorevaluacion',
        'codevaluacionsolicitud',
        'codindicador',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el indicadores_evaluacion: ' . $this->codindicadorevaluacion;
                break;
            case 'updated':
                return 'Editó el indicadores_evaluacion: ' . $this->codindicadorevaluacion;
                break;
            case 'deleted':
                return 'Eliminó el indicadores_evaluacion: ' . $this->codindicadorevaluacion;
                break;
        }
        return '';
    }

}
