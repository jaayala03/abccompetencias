<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class EtapaSolicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'etapas_solicitudes';
    protected $primaryKey = 'codetapasolicitud';
    protected $fillable   = [
        "etapa",
        "ultima",
        "codsolicitud"
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        "etapa",
        "ultima",
        "codsolicitud",
        "created_at",
        "updated_at",
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
            case 'updated':
                return 'Editó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
            case 'deleted':
                return 'Eliminó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
        }
        return '';
    }

    //relacion con solicitud
    public function solicitud()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Solicitud', 'codsolicitud');
    }
}
