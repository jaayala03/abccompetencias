<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Titulacion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'titulaciones';
    protected $primaryKey = 'codtitulacion';
    protected $fillable   = [
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
    ];
    //**********************************************************************

      // protected $appends = ['rutaimagen', 'rutaimagenpublica'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codtitulacion',
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la titulación: ' . $this->codigo;
                break;
            case 'updated':
                return 'Editó la titulación: ' . $this->codigo;
                break;
            case 'deleted':
                return 'Eliminó la titulación: ' . $this->codigo;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    // //**********************************************************************
    // public function getRutaImagenAttribute()
    // {
    //     return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }    

    // public function getRutaImagenPublicaAttribute()
    // {
    //     return route('outside.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    // }

}
