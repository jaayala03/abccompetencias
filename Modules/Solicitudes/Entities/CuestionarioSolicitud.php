<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CuestionarioSolicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'cuestionarios_solicitud';
    protected $primaryKey = 'codcuestionario_solicitud';
    protected $fillable   = [
        'codsolicitud',
        'codcuestionario',
        'terminado',
        'hora_inicio',
        'hora_fin',
        'hora_fin_estimado'
    ];

    protected $with = ['cuestionario', 'solicitud'];

    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codcuestionario_solicitud',
        'codsolicitud',
        'codcuestionario',
        'terminado'
    ];

    protected $appends = ['codcuestionario_solicitudcifrado'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el cuestionario: ' . $this->codcuestionario_solicitud;
                break;
            case 'updated':
                return 'Editó el cuestionario: ' . $this->codcuestionario_solicitud;
                break;
            case 'deleted':
                return 'Eliminó el cuestionario: ' . $this->codcuestionario_solicitud;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function getCodcuestionarioSolicitudcifradoAttribute($value)
    {
        return \Crypt::encrypt($this->codcuestionario_solicitud);
    }

    //relacion con cuestionario
    public function cuestionario()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Cuestionario', 'codcuestionario');
    }

    //relacion con solicitudes
    public function solicitud()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Solicitud', 'codsolicitud');
    }

}
