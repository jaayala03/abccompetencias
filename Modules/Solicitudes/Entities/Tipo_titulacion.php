<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Tipo_titulacion extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'tipos_titulacion';
    protected $primaryKey = 'codtipo_titulacion';
    protected $fillable   = [
        'nombre',
    ];
    //**********************************************************************

    protected $appends = ['normas'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codtipo_titulacion',
        'nombre',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el tipo de titulacion: ' . $this->nombre;
                break;
            case 'updated':
                return 'Editó el tipo de titulacion: ' . $this->nombre;
                break;
            case 'deleted':
                return 'Eliminó el tipo de titulacion: ' . $this->nombre;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function getNormasAttribute($value)
    {
        return $this->normas()->get();
    }

    //relaciones********************************************************************
    
    //Relacion uno a muchos con normas
    public function normas()
    {
        return $this->hasMany('Modules\Solicitudes\Entities\Norma', 'codtipo_titulacion', 'codtipo_titulacion');
    }
}
