<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Agendada extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'agendadas';
    protected $primaryKey = 'codagendada';
    protected $fillable   = [
        'fecha_agendada',
        'hora',
        'codsolicitud',
        'codevaluador'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codagendada',
        'fecha_agendada',
        'hora',
        'codsolicitud',
        'codevaluador'
    ];

    protected $appends = ['evaluador'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el Agendamiento de la solicitud: ' . $this->codsolicitud;
                break;
            case 'updated':
                return 'Editó el Agendamiento de la solicitud: ' . $this->codsolicitud;
                break;
            case 'deleted':
                return 'Eliminó el Agendamiento de la solicitud: ' . $this->codsolicitud;
                break;
        }
        return '';
    }

    public function getEvaluadorAttribute($value)
    {
        $key = 'evaluador';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Usuario(['nombres' => '']);
        $this->setRelation($key, $value);
        return $value;
    }

    //relacion con evaluador
    public function evaluador()
    {
        return $this->belongsTo('Modules\Solicitudes\Entities\Usuario', 'codevaluador');
    }
}
