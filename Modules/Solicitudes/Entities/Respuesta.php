<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Respuesta extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'respuestas';
    protected $primaryKey = 'codrespuesta';
    protected $fillable   = [
        'codopcion',
        'codpregunta_cuestionario',
        'codcuestionario_solicitud'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codrespuesta',
        'codopcion',
        'codpregunta_cuestionario',
        'codcuestionario_solicitud'
    ];


    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la respuesta: ' . $this->codrespuesta;
                break;
            case 'updated':
                return 'Editó la respuesta: ' . $this->codrespuesta;
                break;
            case 'deleted':
                return 'Eliminó la respuesta: ' . $this->codrespuesta;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }
    

}
