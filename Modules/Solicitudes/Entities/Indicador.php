<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Indicador extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'indicadores';
    protected $primaryKey = 'codindicador';
    protected $fillable   = [
        'nombre',
        'codnorma',
        'tipo',
    ];
    
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitud';

    protected static $logAttributes = [
        'codindicador',
        'nombre',
        'codnorma',
        'tipo',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el indicador: ' . $this->codindicador;
                break;
            case 'updated':
                return 'Editó el indicador: ' . $this->codindicador;
                break;
            case 'deleted':
                return 'Eliminó el indicador: ' . $this->codindicador;
                break;
        }
        return '';
    }

    public function ScopeListas($query, $codevaluacion){
        $query->select('indicadores.codnorma','indicadores.codindicador', 'indicadores.nombre', 'valoracion_indicadores.cumple', 'valoracion_indicadores.observacion', 'valoracion_indicadores.codvaloracionindicador', 'indicadores_evaluacion.codindicadorevaluacion', 'indicadores_evaluacion.codevaluacionsolicitud', 'evaluaciones_solicitudes.codevaluacion')
        ->leftjoin('indicadores_evaluacion', 'indicadores.codindicador', '=', 'indicadores_evaluacion.codindicador')
        ->leftjoin('valoracion_indicadores', 'indicadores_evaluacion.codindicadorevaluacion', '=', 'valoracion_indicadores.codindicadorevaluacion')
        ->leftjoin('evaluaciones_solicitudes', 'indicadores_evaluacion.codevaluacionsolicitud', '=', 'evaluaciones_solicitudes.codevaluacionsolicitud')
        ->where('evaluaciones_solicitudes.codevaluacion', $codevaluacion);
            
        return $query;
    }
}
