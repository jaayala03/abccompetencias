<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Opcion extends Model
{
    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'opciones';
    protected $primaryKey = 'codopcion';
    protected $fillable   = [
        'opcion',
        'correcta',
        'codpregunta'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codopcion',
        'opcion',
        'correcta',
        'codpregunta'
    ];

    protected $appends = ['respuestas'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la opcion: ' . $this->opcion;
                break;
            case 'updated':
                return 'Editó la opcion: ' . $this->opcion;
                break;
            case 'deleted':
                return 'Eliminó la opcion: ' . $this->opcion;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function getRespuestasAttribute($value)
    {
        return $this->respuestas()->get();
    }

    public function respuestas()
    {
        return $this->hasMany('Modules\Solicitudes\Entities\Respuesta', 'codopcion', 'codopcion');
    }
}
