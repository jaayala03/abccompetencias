<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Usuario extends Model
{

    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table = 'usuarios';

    protected $primaryKey = 'codusuario';

    protected $fillable = [
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    //**********************************************************************
    protected $appends    = ['nombrecompleto', 'urlfoto'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codusuario',
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];


    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                 return 'Creó el Usuario: '.$this->email;
                break;
            case 'updated':
                return 'Editó el Usuario: '.$this->email;
                break;
            case 'deleted':
                return 'Eliminó el Usuario: '.$this->email;
                break;
        }
        return '';
    }

    //**********************************************************************
    public function getNombrecompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primerapellido. ' '. $this->segundoapellido;
    }

    public function getUrlFotoAttribute()
    {
        return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codusuario), 'option' => \Crypt::encrypt(1)]);
    }
}
