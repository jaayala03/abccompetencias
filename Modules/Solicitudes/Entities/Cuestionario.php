<?php

namespace Modules\Solicitudes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Cuestionario extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'cuestionarios';
    protected $primaryKey = 'codcuestionario';
    protected $fillable   = [
        'nombre',
        'codconocimiento',
        'segundos_x_pregunta',
        'tipo'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        'codcuestionario',
        'nombre',
        'codconocimiento',
        'segundos_x_pregunta',
        'tipo'
    ];

    protected $appends = ['codcuestionariocifrado'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el cuestionario: ' . $this->codcuestionario;
                break;
            case 'updated':
                return 'Editó el cuestionario: ' . $this->codcuestionario;
                break;
            case 'deleted':
                return 'Eliminó el cuestionario: ' . $this->codcuestionario;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'solicitudes';
    }

    public function getCodcuestionariocifradoAttribute($value)
    {
        return \Crypt::encrypt($this->codcuestionario);
    }

    public function scopePreguntasCuestionarioSolicitud($query, $codcuestionario, $codcuestionario_solicitud)
    {
        $preguntas = Pregunta::join('preguntas_cuestionarios', 'preguntas_cuestionarios.codpregunta', '=', 'preguntas.codpregunta')
            ->select('preguntas.*', 'preguntas_cuestionarios.codpregunta_cuestionario')
            ->where('preguntas_cuestionarios.codcuestionario', $codcuestionario)
            ->distinct()
            ->get();
 
        $preguntas = $preguntas->map(function ($obj, $key) use($codcuestionario_solicitud) {
            $obj->value = '';
            $obj->numero = $key + 1;
            $codpregunta_cuestionario = $obj->codpregunta_cuestionario;
            $PreguntaCuestionario = PreguntaCuestionario::find($codpregunta_cuestionario);

            $respuestas = $PreguntaCuestionario->respuestas()->where('respuestas.codcuestionario_solicitud', $codcuestionario_solicitud)->get();
            if (count($respuestas) > 0) {
                $respuesta = $PreguntaCuestionario->respuestas()->where('respuestas.codcuestionario_solicitud', $codcuestionario_solicitud)->first();
                $obj->value = $respuesta->codopcion;
            }
            return $obj;
        });
        return $preguntas;
    }

    public function scopeResultadosCuestionarioSolicitud($query, $codcuestionario, $codcuestionario_solicitud)
    {
        $preguntas = Cuestionario::preguntasCuestionarioSolicitud($codcuestionario, $codcuestionario_solicitud);
        $correctas = [];
        $incorrectas = [];
        $no_respondidas = [];

        foreach ($preguntas as $key => $obj) {
            $correcta = $obj->opciones()->where('correcta', true)->first();
            $opciones = $obj->opciones;

            if(!empty($correcta) && $obj->value == $correcta->codopcion){
                array_push($correctas, $obj);
            } else if(!empty($obj->value) && !empty($correcta) && $obj->value !== $correcta->codopcion){
                array_push($incorrectas, $obj);
            } else {
                array_push($no_respondidas, $obj);
            }
        }
        return [
            'correctas' => $correctas,
            'incorrectas' => $incorrectas,
            'no_respondidas' => $no_respondidas,
        ];
    }

    public function scopePorcentajeCuestionarioSolicitud($query, $codcuestionario, $codcuestionario_solicitud)
    {
        $preguntas = Cuestionario::preguntasCuestionarioSolicitud($codcuestionario, $codcuestionario_solicitud);
        if (count($preguntas)) {
            $resultados = Cuestionario::resultadosCuestionarioSolicitud($codcuestionario, $codcuestionario_solicitud);
            $correctas = count($resultados['correctas']);
            $porcentaje = (($correctas * 100) / count($preguntas));
            return round($porcentaje);
        }
        return 0;
    }

    //relacion con cuestionarios
    public function preguntas()
    {
        return $this->belongsToMany('Modules\Solicitudes\Entities\Pregunta', 'preguntas_cuestionarios', 'codcuestionario', 'codpregunta')
        ->withPivot('codpregunta_cuestionario')
        ->withTimestamps();
    }

}
