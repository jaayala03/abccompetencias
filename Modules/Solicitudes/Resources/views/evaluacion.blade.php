@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/solicitudes/js/solicitudes/evaluacion.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
.bold {
    font-weight: bold;
}
.w100 {
    display: block;
    position: relative;
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
<h2><i class="fa fa-envelope"></i> Evaluación {{$evaluacionsolicitud->evaluacion->nombre}}</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                    <h3 class="panel-title">@{{evaluacionsolicitud.evaluacion.descripcion}}</h3>
                    </div>
                    <div class="pull-right">
                        <button @click.prevent="generarpdf()" class='btn btn-success' type="button"><i class='fa fa-file-pdf-o'></i> Generar PDF</button>
                        <button id="btn_evaluar" @click.prevent="evaluar()" class='btn btn-primary' type="button"><i class='fa fa-save'></i> Evaluar</button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" style="margin-top: -1px;">
                            <table class="table table-default" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="width: 1%;">DATOS DEL CANDIDATO</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="bold">NOMBRES:</td>
                                        <td class="border-right">@{{solicitud.usuario.nombres}}</td>
                                        <td class="bold">APELLIDOS:</td>
                                        <td>@{{solicitud.usuario.primerapellido}} @{{solicitud.usuario.segundoapellido}}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">IDENTIFICACIÓN:</td>
                                        <td class="border-right">@{{solicitud.usuario.documento}}</td>
                                        <td class="bold">LUGAR DE EXPEDICIÓN:</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>   
                        <div class="table-responsive" style="margin-top: -1px;">
                            <form data-vv-scope="form_evaluar">
                                <table class="table table-default" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th colspan="4" style="width: 1%;">DATOS DE APLICACIÓN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="bold">FECHA APLICACIÓN:</td>
                                            <td>
                                                <input :value="formatfecha(evaluacionsolicitud.fecha_aplicacion)" class="form-control" readonly type="text" name="fecha_aplicacion" id="fecha_aplicacion">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">FECHA DE ENTEGA DE RESULTADOS:</td>
                                            <td>
                                                <div :class="{'form-group':true, 'has-error': errors.has('form_evaluar.fecha_resultados') }">
                                                    <el-date-picker v-validate="'required'" name="fecha_resultados" @change="registrarevaluacionsolicitud()" format="dd-MM-yyyy" style="width: 100%;" v-model="evaluacionsolicitud.fecha_resultados" type="date" placeholder="Seleccione fecha">
                                                    </el-date-picker>
                                                    <span v-show="errors.has('form_evaluar.fecha_resultados')" class="help-block">@{{ errors.first('form_evaluar.fecha_resultados') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">LUGAR DE EVALUACIÓN:</td>
                                            <td>
                                                <div :class="{'form-group':true, 'has-error': errors.has('form_evaluar.lugar') }">
                                                    <input v-validate="'required'" @change="registrarevaluacionsolicitud()" v-model="evaluacionsolicitud.lugar" placeholder="Ingrese lugar" class="form-control" type="text" name="lugar" id="lugar">
                                                    <span v-show="errors.has('form_evaluar.lugar')" class="help-block">@{{ errors.first('form_evaluar.lugar') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">CIUDAD:</td>
                                            <td>
                                                <div :class="{'form-group':true, 'has-error': errors.has('form_evaluar.ciudad') }">
                                                    <input v-validate="'required'" @change="registrarevaluacionsolicitud()" v-model="evaluacionsolicitud.ciudad" placeholder="Ingrese ciudad" class="form-control" type="text" name="ciudad" id="ciudad">
                                                    <span v-show="errors.has('form_evaluar.ciudad')" class="help-block">@{{ errors.first('form_evaluar.ciudad') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">NOMBRE EVALUADOR:</td>
                                            <td>
                                                <div :class="{'form-group':true, 'has-error': errors.has('form_evaluar.codevaluador') }">
                                                    <el-select v-validate="'required'" name="codevaluador" class="w100" @change="registrarevaluacionsolicitud()" clearable v-model="evaluacionsolicitud.codevaluador" name="codevaluador" filterable
                                                        placeholder="Seleccione evaluador">
                                                        <el-option v-for="evaluador in evaluadores" :key="evaluador.codusuario" :label="evaluador.nombrecompleto"
                                                            :value="evaluador.codusuario">
                                                        </el-option>
                                                    </el-select>
                                                    <span v-show="errors.has('form_evaluar.codevaluador')" class="help-block">@{{ errors.first('form_evaluar.codevaluador') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>  
                        <div class="table-responsive" style="margin-top: -1px;">
                            <table class="table table-default" style="width:100%; overflow:hidden;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="width: 1%;">NORMA DE COMPETENCIA LABORAL A EVALUAR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="bold">NOMBRE:</td>
                                        <td class="border-right">@{{solicitud.norma.nombre}}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">CÓDIGO:</td>
                                        <td class="border-right">@{{solicitud.norma.codigo}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                        <div class="table-responsive" style="margin-top: -1px;">
                            <table class="table table-default table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="width: 1%;">MOMENTOS DE OBSERVACIÓN EXIGIDOS EN LA NORMA</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="bold">NRO:</td>
                                        <td class="bold">INDICADORES DE EVALUACIÓN</td>
                                        <td class="bold">CUMPLE</td>
                                        <td class="bold">OBSERVACIONES</td>
                                    </tr>
                                    <template v-if="indicadores.length == 0">
                                        <tr class="text-center bold">
                                            <td colspan="4">
                                                <div class="alert alert-warning">
                                                    <strong>Advertencia!</strong> La norma no posee indicadores asociados.
                                                </div>
                                            </td>
                                        </tr>
                                    </template>
                                    <tr v-for="(indicador, index) in indicadores">
                                        <td class="text-center" width="5%">@{{index+1}}</td>
                                        <td>@{{indicador.nombre}}</td>
                                        <td class="text-center">
                                            <el-switch 
                                            @change="registrarvaloracion(indicador)"
                                            style="display: block" 
                                            v-model="indicador.cumple" 
                                            active-color="#13ce66" 
                                            inactive-color="#ff4949"
                                            active-text="Si" 
                                            inactive-text="No">
                                            </el-switch>
                                        </td>
                                        <td><input @change="registrarvaloracion(indicador)" class="form-control" type="text" v-model="indicador.observacion" name="observacion" id="observacion"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                        <div class="table-responsive" style="margin-top: -1px;">
                            <table class="table table-default table-bordered" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="width: 1%;">VALORACIÓN FINAL DE COMPETENCIA:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="bold text-center">CUMPLE</td>
                                        <td class="bold text-center">
                                            <span style="font-size: 20px;" class="badge badge-success" id="cumple">0%</span>
                                        </td>
                                        <td class="bold text-center">AÚN NO CUMPLE</td>
                                        <td class="bold text-center">
                                            <span style="font-size: 20px;" class="badge badge-danger" id="no_cumple">0%</span>
                                        </td>
                                    </tr>                                    
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>              
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
