@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/solicitudes/js/solicitudes/registrar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Registrar Solicitud </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Registrar</h3>
                    </div>
                    <div class="pull-right">                       
{{--                         @if(array_key_exists("PARAM_FACH_NOT_CRE", session("permisos")))
                            <a title="Agregar Noticia"  href="#{{ route('outside.noticias.cargarcrear', [], false) }}">
                                <button class='btn btn-primary' type="button"><i class='fa fa-plus'></i> Agregar Noticia</button>
                            </a>
                        @endif --}}
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">

                  			<form action="#">
                                <div class="form-body">
                                    <div class="form-group text-center">
                                        <h3 class="control-label">Tipo de Solicitud</h3>
                                        <div>
                                        	{{-- <el-radio v-model="tipo_solicitud" label="1" border>Por Titulación</el-radio> --}}
                                        	
    										<el-radio v-model="tipo_solicitud" label="2" border>Por Norma Competencia Laboral</el-radio>
                                        </div>
                                    </div><!-- /.form-group -->

                                    {{-- <div v-if="tipo_solicitud == 1" class="form-group">
                                        <el-table
										    ref="multipleTable"
										    border
										    :data="titulaciones"
										    @selection-change="handleSelectionChange">

											<el-table-column 
												label="Titulaciones" 
												header-align="center">
											    
											    <el-table-column
										    	  property="codigo"
											      label="Codigo">
											    </el-table-column>

											    <el-table-column
											      property="nombre"
											      label="Nombre">
											    </el-table-column>

											    <el-table-column
											      type="selection">
											    </el-table-column>
											</el-table-column>
									  	</el-table>

									  	<div class="panel-footer text-center">
									  		<button :disabled="seleccionadas.length <= 0" @click="confirm_registrar_titulaciones()" type="button" class="btn btn-primary rounded">Registrar</button>

									  		<button data-toggle="modal" data-target="#ver_requisitos" v-if="seleccionadas.length > 0" type="button" class="btn btn-warning rounded">Ver Requisitos</button>
									  	</div>
                                    </div><!-- /.form-group --> --}}

                                    <div v-if="tipo_solicitud == 2" class="form-group">
                                        <el-table
                                            ref="multipleTable"
                                            border
                                            :data="normas"
                                            @selection-change="handleSelectionChange">

                                            <el-table-column 
                                                label="Normas" 
                                                header-align="center">
                                                
                                                <el-table-column
                                                  property="codigo"
                                                  label="Codigo">
                                                </el-table-column>

                                                <el-table-column
                                                  property="nombre"
                                                  label="Nombre">
                                                </el-table-column>

                                                <el-table-column
                                                  type="selection">
                                                </el-table-column>
                                            </el-table-column>
                                        </el-table>

                                        <div class="panel-footer text-center">
                                            <button id="btn_registrar" :disabled="seleccionadas.length <= 0" @click="confirm_registrar_normas()" type="button" class="btn btn-primary rounded"><i class="fa fa-check"></i> Registrar</button>

                                            <button data-toggle="modal" data-target="#ver_requisitos" v-if="seleccionadas.length > 0" type="button" class="btn btn-warning rounded"><i class="fa fa-file-o"></i> Ver Requisitos</button>
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                            </form>

                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')
<!-- crear -->
<div class="modal modal-warning fade" id="ver_requisitos" role="dialog" aria-labelledby="ver_requisitos">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titulo-modal">
                    Requisitos
                </h4>
            </div>
            <div class="modal-body">
				<ul>
				    <li>
				        Dos (2) fotos de 3X4 cm
				    </li>
				    <li>
				        Hoja de Vida en papel blanco
				    </li>
				    <li>
				        Fotocopia del documento de identidad
				    </li>
				    <li>
				        Certificado de Bachiller y/o Grado Escolaridad
				    </li>
				    <li>
				        Certificados de formaciones y/o Capacitaciones en el campo a certificar Res. 90902/13, NTC 3631 2a. Act., NTC 2505 4a. Act.)
				    </li>
				    <li>
				        Certificados o constancia de la experiencia en el sector a certificar
				    </li>
				    <li>
				        Constancia de trabajo que especifique el tiempo y las funciones realizadas o que realiza actualmente relacionadas con la competencia a certificar (Si es empleado).
				    </li>
				</ul>
            </div>
        </div>
    </div>
</div>
@endsection
