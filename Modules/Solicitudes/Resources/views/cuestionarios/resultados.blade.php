@extends('krauff::layouts.nomenu')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset(mix('modules/solicitudes/js/cuestionarios/resultados.js')) }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link rel="stylesheet" href="{{ asset(mix('modules/solicitudes/css/cuestionarios/responder.css')) }}">

<style>
.chart-container {
    width: 150px;
    height: 150px
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Cuestionarios </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                    <h3 class="panel-title">Resultados cuestionario</h3>
                    </div>
                    <div class="pull-right">
                        
                        @if(array_key_exists("CUEST_GEN_SEC", session("permisos")))
                            <el-button v-if="cuestionarios_secundarios.length == 0 && cuestionarios_aprobados.length == 0 && cuestionarios_solicitud_pendientes.length == 0" @click.prevent="showmodal('modal_agendar')" v-tooltip.hover.focus="'Generar un nuevo cuestionario'" type="primary" round><i class="fa fa-question-circle"></i> Nuevo cuestionario</el-button>
                        @endif   
                        
                        <el-button @click.prevent="verpdf()" v-tooltip.hover.focus="'Ver en PDF'" type="danger" round><i class="fa fa-file-pdf-o"></i> PDF</el-button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">

                    <template v-for="cuestionario_solicitud in cuestionarios_solicitud">
                        <div class="row ">
                            <h3 class="text-center"><b>Cuestionario: @{{ cuestionario_solicitud.cuestionario.nombre | truncate(50) }}</b></h3>
                            <hr>
                            <div class="col-md-offset-2 col-md-8 panel1 mb-10 bloque-resultado-widget">
                
                                    <h4 class="titulo-bloque-resultado-widget text-center">Resumen</h4>
                                    <div class="bloque-resultado">
                                        <div class="bloque-resultado-contenido text-center">
    
                                            <template v-if="aprobado(cuestionario_solicitud)">
                                                <i class="fa fa-check-circle fa-4x" aria-hidden="true" style="color: #28A745"></i>
                                                <p style="font-weight: bold" >Aprobado</p>
                                            </template>
    
                                            <template v-else>
                                                <i class="fa fa-times-circle fa-4x" aria-hidden="true" style="color: #DC3545"></i>
                                                <p style="font-weight: bold">No Aprobado</p>
                                            </template>
    
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <p style="
                                        color: #9CC96B;
                                        font-weight: bold;
                                    ">Preguntas correctas: <span class="badge badge-success rounded">@{{ correctas(cuestionario_solicitud).length }}</span></p> 
                                    <p style="
                                        color: #F66C51;
                                        font-weight: bold;
                                    ">Preguntas incorrectas: <span class="badge badge-danger rounded">@{{ incorrectas(cuestionario_solicitud).length }}</span></p>
                                    <p style="
                                        color: #EAAA4B;
                                        font-weight: bold;
                                    ">Preguntas no respondidas: <span class="badge badge-warning rounded">@{{ no_respondidas(cuestionario_solicitud).length }}</span></p>
                                    </div>
    
                                    <div class="row mt-30">
                                        <div class="col-md-7 mt-10">
                                            <p class="text-center" style="font-weight: bold">Datos del usuario y cuestionario</p>
                                            <span class="spand"><b>Nombres:</b> {{$solicitud->usuario->nombrecompleto}}</span>
                                            <span class="spand"><b>Email:</b> {{$solicitud->usuario->email}}</span>
                                            <span class="spand"><b>Documento:</b> {{$solicitud->usuario->documento}}</span>
                                            <span class="spand"><b>Cuestionario:</b> @{{ cuestionario_solicitud.cuestionario.nombre}}</span>
                                            <span class="spand"><b>Fecha y hora de inicio:</b> @{{ cuestionario_solicitud.hora_inicio }}</span>
                                            <span class="spand"><b>Fecha y hora de fin:</b> @{{ cuestionario_solicitud.hora_fin }}</span>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="chart-container" >
                                                <canvas :id="'chart-'+cuestionario_solicitud.codcuestionario_solicitud"></canvas>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        
                        @if(array_key_exists("CUEST_DET_CUES", session("permisos")))
                            <div class="row" v-for="(item, index) in cuestionario_solicitud.preguntas">
                                <div class="col-md-12 panel1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title text-blue">
                                            <i class="fa fa-question-circle fa-2x"></i> @{{ item.numero}}. @{{ item.pregunta}}
                                            </h3>
                                        </div>
                                        <div class="panel-body two-col">
                                            <div class="row">
                                                <div class="col-md-6" v-for="(opcion, index_opcion) in item.opciones">
                                                    <div :class="{'frb': true, 'frb-success': item.value == opcion.codopcion && opcion.correcta, 'frb-danger': item.value == opcion.codopcion && !opcion.correcta, 'margin-bottom-none': true}">
                                                        
                                                        <input :class="{correcta: opcion.correcta}" :checked="checked_op(item, opcion)" disabled type="radio" :id="'radio-button-'+item.codpregunta+'-'+opcion.codopcion" :name="'radio-button'+item.codpregunta" :value="opcion.codopcion" >
                                                        
                                                        <label :for="'radio-button-'+item.codpregunta+'-'+opcion.codopcion">
                                                            <span class="frb-title">@{{ opcion.opcion }}</span>
                                                            <span class="frb-description"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        @endif
                
                        {{-- <div class="row">
                            <div class="col-md-12" >
                                <div class="text-center">
                                    <el-pagination
                                        layout="prev, pager, next"
                                        :page-size="preguntas.perpage"
                                        :current-page.sync="preguntas.currentpage"
                                        @current-change="onCurrentPageP"
                                        :total="preguntas.items.length">
                                    </el-pagination>
                                </div>
                            </div>
                        </div> --}}
                    </template>
                    
                </div>
            </div>
        </div>
    </div>

    <sweet-modal ref="modal_agendar" overlay-theme="dark">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-success panel-shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title"><i class="fa fa-calendar"></i>Generar nuevo cuestionario</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body panel-sh">
                        <form data-vv-scope="form_agendar">
                            <div class="row mt-10">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.tipo') }">
                                        <el-radio name="tipo" v-model="agendar.tipo" :label="1">Agendar</el-radio>
                                        <el-radio name="tipo" v-model="agendar.tipo" :label="2">Agendar y habilitar</el-radio>
                                        <span v-show="errors.has('form_agendar.tipo')" class="help-block">@{{ errors.first('form_agendar.tipo') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.fecha_agendada') }">
                                        {{ Form::label('fecha_agendada', 'Fecha') }}
                                        <el-date-picker
                                        name="fecha_agendada"
                                        class="w100" 
                                        value-format="yyyy-MM-dd" 
                                        v-validate="'required'" 
                                        v-model="agendar.fecha_agendada"
                                        type="date" 
                                        placeholder="Elija una fecha">
                                        </el-date-picker>
                                        <span v-show="errors.has('form_agendar.fecha_agendada')" class="help-block">@{{ errors.first('form_agendar.fecha_agendada') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.hora') }">
                                        {{ Form::label('hora', 'Hora') }}
                                        <el-time-select class="w100" 
                                        name="hora"
                                        v-validate="'required'" 
                                        v-model="agendar.hora" 
                                        :picker-options="{
                                            start: '08:30',
                                            step: '00:15',
                                            end: '18:30'
                                          }" placeholder="Elija una hora">
                                        </el-time-select>
                                        <span v-show="errors.has('form_agendar.hora')" class="help-block">@{{ errors.first('form_agendar.hora') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.codevaluador') }">
                                        {{ Form::label('codevaluador', 'Evaluador') }}
                                        <el-select class="w100" 
                                        v-validate="'required'" 
                                        name="codevaluador"
                                        clearable 
                                        v-model="agendar.codevaluador"
                                        filterable 
                                        placeholder="Seleccione evaluador">
                                            <el-option 
                                            v-for="evaluador in evaluadores" 
                                            :key="evaluador.codusuario" 
                                            :label="evaluador.nombrecompleto" 
                                            :value="evaluador.codusuario">
                                            </el-option>
                                        </el-select>
                                        <span v-show="errors.has('form_agendar.codevaluador')" class="help-block">@{{ errors.first('form_agendar.codevaluador') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button @click.prevent="confirmar_generar_nuevo()" id="btn_agendar" class='btn btn-primary' type="button"><i class='fa fa-check'></i> Agendar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </sweet-modal>


</div><!-- /.body-content -->
<!--/ End body content -->
<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
