@extends('krauff::layouts.nomenu')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset(mix('modules/solicitudes/js/cuestionarios/responder.js')) }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link rel="stylesheet" href="{{ asset(mix('modules/solicitudes/css/cuestionarios/responder.css')) }}">
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Cuestionarios </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Responder cuestionario</h3>
                    </div>
                    <div class="pull-right">                       
                        <span class="text text-danger" style="font-size: 20px" id="countdown">Tiempo restante: </span>
                        <button @click.prevent="finalizar" class="btn btn-warning rounded"><i class="fa fa-check-circle" aria-hidden="true"></i> Finalizar</button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">

                    {{-- <div class="row" v-if="errors.all().length > 0" style="margin-top: 10px"> --}}
                    <div class="row" v-if="respondidas.length == 0 && finalizado" style="margin-top: 10px">
                        <div class="col-md-offset-4 col-md-4" >
                            <div class="alert alert-danger ">
                                <span class="alert-icon"><i class="fa fa-exclamation-circle"></i></span>
                                <div class="notification-info">
                                    <strong>ERROR: ninguna pregunta respondida.</strong>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <form data-vv-scope="formC">
                        <div :id="'block_pregunta_'+item.codpregunta" class="row" v-for="(item, index) in preguntas.datapaginate">
                            <div class="col-md-12 panel1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title text-blue">
                                            <i class="fa fa-question-circle fa-2x"></i> @{{ item.numero}}. @{{ item.pregunta}}    
                                        </h3>
                                    </div>
                                    <div class="panel-body two-col">
                                        <div class="row">
                                            <div class="col-md-6" v-for="(opcion, index_opcion) in item.opciones">
                                                <div class="frb frb-success margin-bottom-none">
                                                    
                                                    <input v-validate="{ required: true, included: cantPregJoin(item) }" :data-vv-as="'pregunta'+ item.numero" @change="onChangeResp(opcion, item)" type="radio" :id="'radio-button-'+item.codpregunta+'-'+opcion.codopcion" :name="'radio-button'+item.codpregunta" :value="opcion.codopcion" v-model="item.value">
                                                    
                                                    <label :for="'radio-button-'+item.codpregunta+'-'+opcion.codopcion">
                                                        <span class="frb-title">@{{ opcion.numero }}. @{{ opcion.opcion }}</span>
                                                        <span class="frb-description"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-12" v-show="errors.has('radio-button'+item.codpregunta)">
                                <div class="alert alert-danger">
                                    @{{ errors.first('radio-button'+item.codpregunta) }}
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="text-center">
                                <el-pagination
                                    layout="prev, pager, next"
                                    :page-size="preguntas.perpage"
                                    :current-page.sync="preguntas.currentpage"
                                    @current-change="onCurrentPageP"
                                    :total="preguntas.items.length">
                                </el-pagination>
                            </div>
                        </div>
                    </div>
  
                      
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->


<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
