@extends('krauff::layouts.nomenu')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/solicitudes/js/cuestionarios/respondercorto.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link rel="stylesheet" href="{{ asset(mix('modules/solicitudes/css/cuestionarios/responder.css')) }}">
<style>
.itm-preg-r {
-webkit-box-shadow: 0px 0px 26px -5px rgba(0,135,14,1);
-moz-box-shadow: 0px 0px 26px -5px rgba(0,135,14,1);
box-shadow: 0px 0px 26px -5px rgba(0,135,14,1);
}
.itm-preg-nr {
-webkit-box-shadow: 0px 0px 26px -5px rgba(232,226,44,1);
-moz-box-shadow: 0px 0px 26px -5px rgba(232,226,44,1);
box-shadow: 0px 0px 26px -5px rgba(232,226,44,1);
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Cuestionarios </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Responder cuestionario</h3>
                    </div>
                    <div class="pull-right">                       
                        <button @click.prevent="finalizar" class="btn btn-success rounded"><i class="fa fa-check-circle" aria-hidden="true"></i> Finalizar</button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">

                    {{-- <div class="row" v-if="errors.all().length > 0" style="margin-top: 10px"> --}}
                    <div class="row" v-if="respondidas.length == 0 && finalizado" style="margin-top: 10px">
                        <div class="col-md-offset-4 col-md-4" >
                            <div class="alert alert-danger ">
                                <span class="alert-icon"><i class="fa fa-exclamation-circle"></i></span>
                                <div class="notification-info">
                                    <strong>ERROR: ninguna pregunta respondida.</strong>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <form data-vv-scope="formC">
                        <div class="mt-15 row">
                            
                            <div :id="'block_pregunta_'+item.codpregunta" v-for="(item, index) in preguntas.datapaginate">
                                <div class="col-md-3">
                                 
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <div style="padding: 5px">
                                                <h3 class="panel-title text-blue text-center" style="display: inline-block">
                                                    <template v-if="item.value">
                                                        <span class="animated fadeIn label label-success rounded">@{{ item.numero}}</span>
                                                    </template>
                                                    <template v-else>
                                                        <span class="label label-warning rounded">@{{ item.numero}}</span>
                                                    </template>
                                                </h3>
                                                <el-select
                                                    @clear="onClearResp(item)"
                                                    @change="onChangeResp(item)"
                                                    :class="{'itm-preg-r': item.value, 'itm-preg-nr': !item.value}"
                                                    placeholder="Seleccione" 
                                                    v-model="item.value" 
                                                    clearable 
                                                    placeholder="Select">
                                                    <el-option
                                                    v-for="(opcion, index_opcion) in item.opciones"
                                                    :key="'opcion_'+opcion.codopcion+'_pregunta_'+ item.codpregunta"
                                                    :label="opcion.numero"
                                                    :value="opcion.codopcion">
                                                    </el-option>
                                                </el-select>
                                                
                                                <template v-if="!item.value">
                                                    <i class="fa fa-exclamation-circle text-warning" style="display: inline-block; font-size: 20px" aria-hidden="true"></i>
                                                </template>

                                                <template v-if="item.value">
                                                    <i class="animated fadeIn fa fa-check-circle text-success" style="display: inline-block; font-size: 20px" aria-hidden="true"></i>
                                                </template>
                                                
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6" v-for="(opcion, index_opcion) in item.opciones">
                                            <div class="frb frb-success margin-bottom-none">
                                                
                                                
                                                <input v-validate="{ required: true, included: cantPregJoin(item) }" :data-vv-as="'pregunta'+ item.numero" @change="onChangeResp(opcion, item)" type="radio" :id="'radio-button-'+item.codpregunta+'-'+opcion.codopcion" :name="'radio-button'+item.codpregunta" :value="opcion.codopcion" v-model="item.value">
                                                
                                                <label :for="'radio-button-'+item.codpregunta+'-'+opcion.codopcion">
                                                    <span class="frb-title">@{{ opcion.opcion }}</span>
                                                    <span class="frb-title">@{{ opcion.numero }}</span>
                                                    <span class="frb-description"></span>
                                                </label>

                                            </div>
                                        </div> --}}
                                    </div>
                               
                                </div>
                            </div>
                        </div>


                    </form>
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="text-center">
                                <el-pagination
                                    layout="prev, pager, next"
                                    :page-size="preguntas.perpage"
                                    :current-page.sync="preguntas.currentpage"
                                    @current-change="onCurrentPageP"
                                    :total="preguntas.items.length">
                                </el-pagination>
                            </div>
                        </div>
                    </div>
  
                      
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->


<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
