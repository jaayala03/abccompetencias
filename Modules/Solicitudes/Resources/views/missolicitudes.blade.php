@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->

<script src="{{ asset(mix('modules/solicitudes/js/solicitudes/missolicitudes.js')) }}"></script>

@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
table { overflow: hidden;  }
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Mis Solicitudes </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            {{-- <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Solicitudes</h3>
                    </div>
                    <div class="pull-right">                       

                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                    <!-- Start datatable -->
                        <table id="tbl_missolicitudes" class="table table-danger table-bordered table-striped table-hover" style="width: 100%;" >
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>                            
                        </table>
                    </div><!-- /.responsive -->                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel --> --}}

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Solicitudes vigentes</b></h3>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tbl_missolicitudes_vigentes" class="table table-no-border table-middle table-lilac" v-loading="loading">
                            
                            <tbody>
                                <tr v-for="(item, index) in solicitudes">
                                    {{-- <td>
                                        <img src="https://via.placeholder.com/50/ffffff" alt="...">
                                    </td> --}}
                                    <td class="text-justify">
                                        <b class="text-block">@{{ item.nombre }}</b>
                                    </td>
                                    <td class="text-center">
                                        <b class="text-block">@{{ item.codigo }}</b>
                                        {{-- <span class="text-block text-muted">reserved</span>
                                        <div class="progress progress-xxs no-margin">
                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                                        </div> --}}
                                    </td>
                                    <td>
                                        <b class="text-block">@{{ item.fecha }}</b>
                                        <span class="text-block text-muted" v-html="item.h_estado"></span>
                                    </td>
                                    <template v-if="item.etapa == 1">
                                        <td class="text-center">
                                            <span class="text-block text-muted">Valor a pagar</span>
                                            <b class="text-block">@{{ item.valor | currency }}</b>
                                        </td>
                                        <td class="text-center">
                                            <a v-if="item.detallepago" target="_blank" data-placement="top" title="Ver detalles del pago" data-toggle="tooltip" :href="item.detallepago" class="btn btn-success btn-xs rounded tooltip-opcion"><i class="fa fa-eye"></i> Detalle</a>
                                            <button v-else @click.prevent="pagar(item.codsolicitudcifrado)" style="padding: 0; background: none; border: none; cursor: pointer;" class="epayco-button-render"><img width="100px" src="https://369969691f476073508a-60bf0867add971908d4f26a64519c2aa.ssl.cf5.rackcdn.com/btns/epayco/boton_de_cobro_epayco2.png"></button>
                                        </td>
                                    </template>
                                    <template v-else>
                                        <td v-html="item.h_opciones" class="text-center">
                                        </td>
                                    </template>

                                    <td class="text-center">
                                        <span v-tooltip.left.hover.focus="item.etapa == 1 ? 'Pendiente' : 'Pagado'" 
                                        :class="{'label': true, 'label-warning': item.etapa == 1, 'label-success': item.etapa != 1, 'label-circle': true}">&nbsp;
                                        </span>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.panel-body -->
            </div>

            <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title text-center">
                      <a data-toggle="collapse" href="#collapse1">Solicitudes anteriores</a>
                    </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="alert alert-warning text-center" role="alert">
                             <a href="#" class="alert-link">Sin resultados</a>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->

    <sweet-modal ref="modal_veragendamientos" overlay-theme="dark">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-success panel-shadow">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h3 class="panel-title"><i class="fa fa-calendar"></i>Agendadas</h3>
                            </div>
                            <div class="pull-right">
                            </div>
                            <div class="clearfix"></div>
                        </div><!-- /.panel-heading -->
                        <div class="panel-body panel-sh">
                            <div class="block">
                                <el-timeline>
                                    <template v-for="(item, index) in solicitud.agendadas">
                                        <el-timeline-item :timestamp="format_date(item.fecha_agendada)" placement="top">
                                            <el-card>
                                                <h4>Evaluador: @{{item.evaluador.nombrecompleto}}</h4>
                                                <p>Fecha de presentación: @{{item.fecha_agendada}}</p>
                                                <p>Hora de presentación: @{{item.hora}}</p>
                                            </el-card>
                                        </el-timeline-item>
                                    </template>
                                </el-timeline>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </sweet-modal>
</div><!-- /.body-content -->
<!--/ End body content -->

<form method="post" action="" class="hidden" id="frm_pay">

</form>

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
