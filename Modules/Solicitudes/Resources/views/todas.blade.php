@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset(mix('modules/solicitudes/js/solicitudes/todas.js')) }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style type="text/css">
    table.dataTable tbody>tr.selected, table.dataTable tbody>tr>.selected {
    background-color: #6A9DB0;
}

table.dataTable tbody tr.selected a, table.dataTable tbody th.selected a, table.dataTable tbody td.selected a {
    color: white;
}

.w100 {
display: block !important;
position: relative;
}


.el-date-editor.el-input, .el-date-editor.el-input__inner {
width: 100% !important;
}
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-envelope"></i> Solicitudes </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Solicitudes</h3>
                    </div>
                    <div class="pull-right">
                        <button v-if="seleccionados.length > 0 && activetab == 1" @click.prevent="pago_registrado(seleccionados)" class='btn btn-success' type="button"><i class='fa fa-calendar'></i> Siguiente etapa @{{seleccionados.length}} Solicitud(es)</button>
                        <button v-if="seleccionados.length > 0 && activetab == 2" @click.prevent="showmodal('modal_agendar')" class='btn btn-success' type="button"><i class='fa fa-calendar'></i> Agendar @{{seleccionados.length}} Solicitud(es)</button>
                        <button v-if="seleccionados.length > 0 && activetab == 3" @click.prevent="showmodal('modal_habilitar')" class='btn btn-success' type="button"><i class='fa fa-check'></i> Habilitar @{{seleccionados.length}} Solicitud(es)</button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-heading text-center">
                    <form class="form-inline">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="form-label" for="fecha_inicio">Desde</label>
                                <input v-model="fecha_inicio" name="fecha_inicio" type="date" class="form-control" id="fecha_inicio">
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="form-label" for="fecha_fin">Hasta</label>
                                <input v-model="fecha_fin" name="fecha_fin" type="date" class="form-control" id="fecha_fin">
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label class="form-label" for="norma_filtros">Norma</label>
                                <el-select clearable v-model="norma_filtros" filterable placeholder="Seleccione norma">
                                    <el-option v-for="norma in normas" :key="norma.codnorma" :label="norma.codigo"
                                        :value="norma.codnorma">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                            <button @click.prevent="reset_datatables()" type="button" class="btn btn-success"><i class="fa fa-search"></i> Filtrar</button>
                        </div><!-- /.form-body -->
                    </form>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                     <div class="row">
                        <div class="col-md-12">
                            <!-- Start color horizontal tabs -->
                            <div class="panel panel-tab panel-tab-double shadow">
                                <!-- Start tabs heading -->
                                <div class="panel-heading no-padding">
                                    <ul class="nav nav-tabs">
                                        @foreach ($etapas_solicitud as $etapa)
                                            <li class="nav-border nav-border-top-{{$etapa['class']}} {{($etapa['key'] == 1 ? 'active' : '')}}">
                                                <a href="#tab_{{$etapa['key']}}" data-toggle="tab" class="text-center" aria-expanded="true">
                                                    <div>
                                                        {{$etapa['value']}}    
                                                    </div>
                                                    <div>
                                                        <span id="cont_{{$etapa['key']}}" class="badge badge-{{$etapa['class']}}">0</span>
                                                    </div>

                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div><!-- /.panel-heading -->
                                <!--/ End tabs heading -->

                                <!-- Start tabs content -->
                                <div class="panel-body">
                                    <div class="tab-content">
                                        @foreach ($etapas_solicitud as $etapa)
                                            <div class="tab-pane fade {{($etapa['key']== 1 ? 'in active' : '')}}" id="tab_{{$etapa['key']}}">
                                                <div class="table-responsive">
                                                <!-- Start datatable -->
                                                    <table id="tbl_todas_{{$etapa['key']}}" class="tablas_solicitud table table-{{$etapa['class']}} table-bordered table-striped table-hover" style="width: 100%;" >
                                                        <thead>
                                                            <tr>
                                                                <th data-hide="phone">
                                                                    <div class="ckbox ckbox-inverse"><input type="checkbox" data-etapa="{{$etapa['key']}}" class="seleccionartodo" name="select_all" value="" id="seleccionartodo_{{$etapa['key']}}"> <label for="seleccionartodo_{{$etapa['key']}}"></label></div>
                                                                </th>
                                                                <th>Documento</th>
                                                                <th>Usuario</th>
                                                                <th>Codigo</th>
                                                                <th>Nombre</th>
                                                                <th>Fecha</th>
                                                                <th>Opciones</th>
                                                            </tr>
                                                        </thead>
                                                        <!--tbody section is required-->
                                                        <tbody></tbody>                            
                                                    </table>
                                                </div><!-- /.responsive -->    
                                            </div>
                                        @endforeach
                                    </div>
                                </div><!-- /.panel-body -->
                                <!--/ End tabs content -->
                            </div><!-- /.panel -->
                            <!--/ End color horizontal tabs -->
                        </div>
                    </div>              
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->

    <sweet-modal ref="modal_agendar" overlay-theme="dark">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-success panel-shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title"><i class="fa fa-calendar"></i>Agendar @{{seleccionados.length}} Solicitud(es)</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body panel-sh">
                        <form data-vv-scope="form_agendar">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.fecha_agendar') }">
                                        {{ Form::label('fecha_agendar', 'Fecha') }}
                                        <el-date-picker class="w100" value-format="yyyy-MM-dd" v-validate="'required'" name="fecha_agendar" v-model="fecha_agendar"  type="date" placeholder="Elija una fecha">
                                        </el-date-picker>
                                        <span v-show="errors.has('form_agendar.fecha_agendar')" class="help-block">@{{ errors.first('form_agendar.fecha_agendar') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.hora_agendar') }">
                                        {{ Form::label('hora_agendar', 'Hora') }}
                                        <el-time-select class="w100" v-validate="'required'" v-model="hora_agendar" name="hora_agendar" :picker-options="{
                                            start: '08:30',
                                            step: '00:15',
                                            end: '18:30'
                                          }" placeholder="Elija una hora">
                                        </el-time-select>
                                        <span v-show="errors.has('form_agendar.hora_agendar')" class="help-block">@{{ errors.first('form_agendar.hora_agendar') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_agendar.codevaluador') }">
                                        {{ Form::label('codevaluador', 'Evaluador') }}
                                        <el-select class="w100" v-validate="'required'" clearable v-model="codevaluador" name="codevaluador" filterable placeholder="Seleccione evaluador">
                                            <el-option v-for="evaluador in evaluadores" :key="evaluador.codusuario" :label="evaluador.nombrecompleto" :value="evaluador.codusuario">
                                            </el-option>
                                        </el-select>
                                        <span v-show="errors.has('form_agendar.codevaluador')" class="help-block">@{{ errors.first('form_agendar.codevaluador') }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button @click.prevent="agendar()" id="btn_agendar" class='btn btn-primary' type="button"><i class='fa fa-check'></i> Agendar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </sweet-modal>

    <sweet-modal ref="modal_habilitar" overlay-theme="dark">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-success panel-shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title"><i class="fa fa-check"></i>Habilitar @{{seleccionados.length}} Solicitud(es)</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body panel-sh">
                        <form data-vv-scope="form_habilitar">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div :class="{'form-group':true, 'has-error': errors.has('form_habilitar.codcuestionario') }">
                                        {{ Form::label('codcuestionario', 'Cuestionario') }}
                                        <el-select name="codcuestionario"  class="w100" v-validate="'required'" clearable v-model="codcuestionario" name ="codcuestionario" filterable placeholder="Seleccione cuestionario">
                                            <el-option v-for="cuestionario in cuestionarios" :key="cuestionario.codcuestionario" :label="cuestionario.nombre"
                                                :value="cuestionario.codcuestionario">
                                            </el-option>
                                        </el-select>
                                        <span v-show="errors.has('form_habilitar.codcuestionario')" class="help-block">@{{ errors.first('form_habilitar.codcuestionario') }}</span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <button @click.prevent="habilitar()" id="btn_habilitar" class='btn btn-primary' type="button"><i class='fa fa-check'></i> Habilitar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </sweet-modal>

    <sweet-modal ref="modal_veragendamientos" overlay-theme="dark">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-success panel-shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title"><i class="fa fa-calendar"></i>Agendadas</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body panel-sh">
                        <div class="block">
                            <el-timeline>
                                <template v-for="(item, index) in solicitud.agendadas">
                                    <el-timeline-item :timestamp="format_date(item.fecha_agendada)" placement="top">
                                        <el-card>
                                            <h4>Evaluador: @{{item.evaluador.nombrecompleto}}</h4>
                                            <p>Fecha de presentación: @{{item.fecha_agendada}}</p>
                                            <p>Hora de presentación: @{{item.hora}}</p>
                                        </el-card>
                                    </el-timeline-item>
                                </template>
                            </el-timeline>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </sweet-modal>

</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
