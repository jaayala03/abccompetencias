@extends('krauff::layouts.master')
@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
@endsection @section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
.info-pago { -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: 'Noto Sans', sans-serif; letter-spacing: 0px; font-size: 14px; color: #2e3139; font-weight: 400; line-height: 26px; }
.panel-pago { background-color: #fff; border-radius: 8px; border: 2px solid #e9ebef; padding:  2px 50px; margin-bottom: 5px; }
.panel-pago-title { margin-bottom: 30px; text-transform: uppercase; font-size: 16px; font-weight: bold; color: #094bde; letter-spacing: 2px; }
.item-detalle { border-bottom: 2px solid #e9ebef; padding-bottom: 6px; margin-bottom: 6px; }
.item-detalle:last-child { border-bottom: 0px; margin-bottom: 0px; padding-bottom: 0px; }
.item-detalle-texto p { font-weight: bold; }
.item-detalle-texto label { font-size: 20px; margin-bottom: 15px; font-weight: 400; }
.item-detalle-texto-azul { right: 0px; color: #094bde; font-size: 15px; letter-spacing: -1px; line-height: 1.5; bottom: 43px; }
.bloque-resultado { border-bottom: 2px solid #d7d9de; }
.bloque-resultado:last-child { border-bottom: 0px; }
.bloque-resultado-contenido { padding: 15px 0px; }
.bloque-resultado-widget { margin-bottom: 30px; background-color: #e9ebef; padding: 8px 50px; border-radius: 6px; }
.bloque-resultado-widget:last-child { border-bottom: 0px; }
.titulo-bloque-resultado-widget { color: #094bde; font-size: 16px; font-weight: bold; text-transform: uppercase; margin-bottom: 25px; letter-spacing: 1px; display: table; line-height: 1; }

@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}

</style>
@endsection @section('content')
<section id="page-content">
    <!-- Start page header -->
    <div class="header-content no-print">
        <h2>
            <i class="fa fa-home"></i>Respuesta de pago <span>Solicitudes</span>
        </h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">Usted esta en:</span> @include('krauff::layouts.partials.breadcrumbs')
        </div>
    </div>
    <!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn" style="min-height: 600px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel rounded shadow">
<!--                     <div class="panel-heading">
                        <h3 class="panel-title"></h3>
                    </div> -->
                    <!-- /.panel-heading -->
                    <div class="panel-body info-pago">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                                <div class="panel-pago">
                                    <h3 class="panel-pago-title text-center">Resumen Transacción</h3>
                              
                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                                ID de la transaccion
                                            </p>
                                            
                                            @isset($transactionId)
                                                <span class="item-detalle-texto-azul">{{ $transactionId }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>
                                    
                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Referencia de la transaccion
                                            </p>
                                            
                                            @isset($reference_pol)
                                                <span class="item-detalle-texto-azul">{{ $reference_pol }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>

                                    @if(isset($pseBank))
                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            cus
                                            </p>
                                            
                                            @isset($cus)
                                                <span class="item-detalle-texto-azul">{{ $cus }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>
                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Banco
                                            </p>
                                            
                                            @isset($pseBank)
                                                <span class="item-detalle-texto-azul">{{ $pseBank }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>
                                       
                                    @endif

                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Valor total
                                            </p>
                                            @isset($value)
                                            <span class="item-detalle-texto-azul">${{ number_format($value) }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>

                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Moneda
                                            </p>
                                            
                                            @isset($currency)
                                                <span class="item-detalle-texto-azul">{{ $currency }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>

                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Descripción
                                            </p>
                                            
                                            @isset($description)
                                                <span class="item-detalle-texto-azul">{{ $description }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>

                                    <div class="item-detalle">
                                        <div class="item-detalle-texto">
                                            <p>
                                            Entidad
                                            </p>
                                            
                                            @isset($franchise)
                                                <span class="item-detalle-texto-azul">{{ \Config::get('dominios.FRANQUICIAS_EPAYCO.TXT.'.$franchise) }}</span>
                                            @else
                                                <span class="item-detalle-texto-azul">N/R</span> 
                                            @endisset
                                        </div>
                                    </div>

                                </div>

                                <div class="text-center no-print">
                                    <a href="#" onclick="window.print()" class="btn btn-primary btn-lg mb30 rounded"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Imprimir</a>
                                    <a href="{{ url('/home').'#/solicitudes/missolicitudes' }}" class="btn btn-success btn-lg mb30 rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar a mis solicitudes</a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                <div class="bloque-resultado-widget">
                                    <h4 class="titulo-bloque-resultado-widget text-center">Resultado de la transacción</h4>
                                    <div class="bloque-resultado">
                                        <div class="bloque-resultado-contenido text-center">
                                            @if($transactionState == 1)
                                                <i class="fa fa-check-circle fa-4x" aria-hidden="true" style="color: #28A745"></i>
                                            @endif
                                            @if($transactionState == 2)
                                                <i class="fa fa fa-times-circle fa-4x" aria-hidden="true" style="color: #DC3545"></i>
                                            @endif
                                            @if($transactionState == 4)
                                                <i class="fa fa-check-circle fa-4x" aria-hidden="true" style="color: #DC3545"></i>
                                            @endif
                                            @if($transactionState == 3)
                                                <i class="fa fa-exclamation-circle fa-4x" aria-hidden="true" style="color: #FFC107"></i>
                                            @endif
                                            
                                            <p style="font-weight: bold">{{ $x_transaction_state }}</p>
                                            <p>{{ $x_response_reason_text }}</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.body-content -->
    <!--/ End body content -->
</section>
<!-- /#page-content -->
<!--/ END PAGE CONTENT -->
@endsection