<?php

namespace Modules\Outside\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Imagen_fachada extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'imagenes_fachada';
    protected $primaryKey = 'codimagenfachada';
    protected $fillable   = [
        'nombre',
        'descripcion',
    ];
    //**********************************************************************

      protected $appends = ['rutaimagen', 'rutaimagenpublica'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'outside';

    protected static $logAttributes = [
        'codimagenfachada',
        'nombre',
        'descripcion',
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la imagen de fachada: ' . $this->nombre;
                break;
            case 'updated':
                return 'Editó la imagen de fachada: ' . $this->nombre;
                break;
            case 'deleted':
                return 'Eliminó la imagen de fachada: ' . $this->nombre;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'outside';
    }
    //**********************************************************************
    public function getRutaImagenAttribute()
    {
        return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codimagenfachada), 'option' => \Crypt::encrypt(2), 'size' => '']);
    }    

    public function getRutaImagenPublicaAttribute()
    {
        return route('outside.getimage', ['id' => \Crypt::encrypt($this->codimagenfachada), 'option' => \Crypt::encrypt(2), 'size' => '']);
    }

}
