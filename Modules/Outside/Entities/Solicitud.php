<?php

namespace Modules\Outside\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Solicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'solicitudes';
    protected $primaryKey = 'codsolicitud';
    protected $fillable   = [
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'outside';

    protected static $logAttributes = [
        'codsolicitud',
        'codusuario',
        'codtitulacion',
        'codnorma',
        'fecha',
    ];


    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la solicitud: ' . $this->codsolicitud;
                break;
            case 'updated':
                return 'Editó la solicitud: ' . $this->codsolicitud;
                break;
            case 'deleted':
                return 'Eliminó la solicitud: ' . $this->codsolicitud;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'outside';
    }
    //**********************************************************************

    
    //relacion con certificados
    public function certificados()
    {
        return $this->hasMany('Modules\Outside\Entities\Certificado', 'codsolicitud', 'codsolicitud');
    }


    public function scopeSolicitudesusuario($query, $documento)
    {
        $query
        ->leftjoin('normas', 'solicitudes.codnorma', '=', 'normas.codnorma')
        ->leftjoin('usuarios', 'solicitudes.codusuario', '=', 'usuarios.codusuario')
        ->leftjoin('etapas_solicitudes', 'solicitudes.codsolicitud', '=', 'etapas_solicitudes.codsolicitud')
        ->where('usuarios.documento', $documento)
        ->where('etapas_solicitudes.ultima', true);
        return $query;
    }    


}
