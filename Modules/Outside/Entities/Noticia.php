<?php

namespace Modules\Outside\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Cviebrock\EloquentSluggable\Sluggable;

class Noticia extends Model
{

    use LogsActivity, Sluggable;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'noticias';
    protected $primaryKey = 'codnoticia';
    protected $fillable   = [
        'titulo',
        'contenido',
        'descripcion',
        'imagen',
        'estado',
    ];
    //**********************************************************************

      protected $appends = ['rutaimagen', 'rutaimagenpublica'];

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'outside';

    protected static $logAttributes = [
        'codnoticia',
        'titulo',
        'contenido',
        'descripcion',
        'imagen',
        'estado',
        'slug'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la noticia: ' . $this->titulo;
                break;
            case 'updated':
                return 'Editó la noticia: ' . $this->titulo;
                break;
            case 'deleted':
                return 'Eliminó la noticia: ' . $this->titulo;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'outside';
    }
    //**********************************************************************
    public function getRutaImagenAttribute()
    {
        return route('krauff.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    }    

    public function getRutaImagenPublicaAttribute()
    {
        return route('outside.getimage', ['id' => \Crypt::encrypt($this->codnoticia), 'option' => \Crypt::encrypt(3), 'size' => '']);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
     public function sluggable()
     {
         return [
             'slug' => [
                 'source' => 'titulo'
             ]
         ];
     }

}
