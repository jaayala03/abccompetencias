<?php

Route::group(['middleware' => 'web', 'prefix' => 'outside', 'namespace' => 'Modules\Outside\Http\Controllers'], function()
{
    Route::get('inicio', [
        'uses' => 'OutsideController@inicio',
        'as'   => 'outside.inicio',
    ]);


    Route::get('quienes_somos', [
        'uses' => 'OutsideController@quienes_somos',
        'as'   => 'outside.quienes_somos',
    ]);

    Route::get('servicios', [
        'uses' => 'OutsideController@servicios',
        'as'   => 'outside.servicios',
    ]);

    Route::get('acerca_de', [
        'uses' => 'OutsideController@acerca_de',
        'as'   => 'outside.acerca_de',
    ]);

    Route::get('pqrs', [
        'uses' => 'OutsideController@pqrs',
        'as'   => 'outside.pqrs',
    ]);

    Route::post('registrar_pqrs', [
        'uses' => 'OutsideController@registrar_pqrs',
        'as'   => 'outside.registrar_pqrs',
    ]);

    Route::get('consultas', [
        'uses' => 'OutsideController@consultas',
        'as'   => 'outside.consultas',
    ]);

    Route::get('descargas', [
        'uses' => 'OutsideController@descargas',
        'as'   => 'outside.descargas',
    ]);

    Route::get('ingresar', [
        'uses' => 'OutsideController@ingresar',
        'as'   => 'outside.ingresar',
    ]);

    //Ruta para mostrar una imagen
    Route::get('getimage/{id}', [
        'uses' => 'OutsideController@getimage',
        'as'   => 'outside.getimage',
    ]);
    //Ruta para mostrar una imagen
    Route::get('getfile/{id}', [
        'uses' => 'OutsideController@getfile',
        'as'   => 'outside.getfile',
    ]);

    //**********************************************************************
    //Grupo de rutas para usuarios
    //**********************************************************************
    Route::group(['prefix' => 'usuarios'], function () {

        //Ruta para registro usuarios
        Route::post('registrarme', [
            'uses' => 'UsuariosController@registrarme',
            'as'   => 'krauff.registrarme',
        ]);

        //Ruta para validar documento
        Route::post('validar_documento', [
            'uses' => 'UsuariosController@validar_documento',
            'as'   => 'outside.usuarios.validar_documento',
        ]);

        //Ruta para validar email
        Route::post('validar_email', [
            'uses' => 'UsuariosController@validar_email',
            'as'   => 'outside.usuarios.validar_email',
        ]);

        //Ruta para validar email
        Route::get('activar_usuario/{codusuario_cifrado}', [
            'uses' => 'UsuariosController@activar_usuario',
            'as'   => 'outside.usuarios.activar_usuario',
        ]);

    });

    //**********************************************************************
    //Grupo de rutas para imagenes de fachada
    //**********************************************************************
    Route::group(['middleware' => ['funcionalidades:PARAM_FACH_IMG'], 'prefix' => 'imagenes_fachada'], function () {

        Route::get('/', [
            'uses' => 'Imagenes_fachadaController@listar',
            'as'   => 'outside.imagenes_fachada.listar',
        ]);

        Route::get('cargarcrear', [
            'uses' => 'Imagenes_fachadaController@cargarcrear',
            'as'   => 'outside.imagenes_fachada.cargarcrear',
        ]);

        Route::post('crear', [
            'uses' => 'Imagenes_fachadaController@crear',
            'as'   => 'outside.imagenes_fachada.crear',
        ]);

    });

    //**********************************************************************
    //Grupo de rutas para noticias
    //**********************************************************************
    Route::group(['prefix' => 'noticias'], function () {

        Route::get('/{slug}', [
            'uses' => 'OutsideController@vernoticia',
            'as'   => 'outside.vernoticia',
        ]);

        Route::group(['middleware' => ['funcionalidades:PARAM_FACH_NOT'], 'prefix' => 'admin'], function () {

            Route::get('listar', [
                'uses' => 'NoticiasController@listar',
                'as'   => 'outside.noticias.listar',
            ]);
    
            Route::post('datatable', [
                'uses' => 'NoticiasController@datatable',
                'as'   => 'outside.noticias.datatable',
            ]);
    
            Route::get('cargarcrear', [
                'uses' => 'NoticiasController@cargarcrear',
                'as'   => 'outside.noticias.cargarcrear',
            ]);
    
            Route::post('crear', [
                'uses' => 'NoticiasController@crear',
                'as'   => 'outside.noticias.crear',
            ]);
    
            Route::get('cargareditar/{codnoticia}', [
                'uses' => 'NoticiasController@cargareditar',
                'as'   => 'outside.noticias.cargareditar',
            ]);
    
            Route::post('editar', [
                'uses' => 'NoticiasController@editar',
                'as'   => 'outside.noticias.editar',
            ]);
    
            Route::post('eliminar', [
                'uses' => 'NoticiasController@eliminar',
                'as'   => 'outside.noticias.eliminar',
            ]);

        });

    });


     //**********************************************************************
    //Grupo de rutas para solicitudes
    //**********************************************************************
    Route::group(['prefix' => 'solicitudes'], function () {
        //Ruta para consultar
        Route::post('consultar', [
            'uses' => 'SolicitudesController@consultar',
            'as'   => 'outside.solicitudes.consultar',
        ]);
    });


    //**********************************************************************
    //Grupo de rutas para imagenes de fachada
    //**********************************************************************
    Route::group(['prefix' => 'normas'], function () {
        Route::post('todosdatosNormas', [
            'uses' => 'NormasController@todosdatosNormas',
            'as'   => 'outside.normas.todosdatosNormas',
        ]);
    });

});
