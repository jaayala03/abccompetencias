<?php

namespace Modules\Outside\Http\Controllers;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use ABCcomptencias\Mail\RegistrarPQRS;
use JavaScript;
use Mail;

use Modules\Outside\Entities\Imagen_fachada;
use Modules\Outside\Entities\Noticia;
use Modules\Outside\Entities\Norma;

class OutsideController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function inicio()
    {
        $imagenes_fachada = Imagen_fachada::all();
        $noticias = Noticia::orderby('updated_at', 'DESC')->get();
        
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Inicio',
            ],
            'imagenes_fachada' => $imagenes_fachada,
            'noticias' => $noticias,
        ]);


        return view('outside::inicio')
        ->with('imagenes_fachada', $imagenes_fachada)
        ->with('noticias', $noticias);
    }    

    public function vernoticia($slug){

        $noticia = Noticia::where('slug', $slug)->firstOrFail();
        return view('outside::vernoticia')->with('noticia', $noticia);
    }

    public function quienes_somos()
    {
        return view('outside::quienes_somos');
    }    

    public function servicios()
    {
        return view('outside::servicios');
    }    

    public function acerca_de()
    {
        return view('outside::acerca_de');
    }    

    public function pqrs()
    {
        return view('outside::pqrs');
    }    

    public function consultas()
    {
        return view('outside::consultas');
    }    

    public function descargas()
    {
        return view('outside::descargas');
    }    

    public function ingresar()
    {
        return view('outside::ingresar');
    }

    public function registrar_pqrs(Request $request){
        $v = \Validator::make(request()->all(), [
            'g-recaptcha-response' => 'recaptcha',
        ]);
        $errors = $v->errors();
       if (count($errors)) {
            return response()->json([
                'type' => 'error',
                'message' => 'Error al validar CAPTCHA',
                'title' => 'Error',
                'type' => 'danger']);
       }else{
            Mail::to(env('MAIL_USERNAME', 'abccompetencias.contacto@gmail.com'))
            ->send(new RegistrarPQRS($request->all()));
        
            return response()->status(true);
       }
    } 

    //**********************************************************************
    //Funcion para obtener imagenes
    //**********************************************************************
    public function getimage(Request $request)
    {
        $option = Crypt::decrypt($request->get('option'));
        $size   = $request->get('size');
        switch ($option) {
            //Para obtener la imagen de la fachada
            case 2:
                $codimagenfachada  = Crypt::decrypt($request->id);
                $imagen_fachada     = Imagen_fachada::find($codimagenfachada);
                $img         = (!empty($imagen_fachada->nombre)) ? $imagen_fachada->nombre : \Config::get('dominios.DEFAULT_IMG_FACHADA');
                $subruta     = \Config::get('paths.IMAGENES_FACHADA') . $img;
                $ruta_nombre = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;            

            //Para obtener la imagen de la noticia
            case 3:
                $codnoticia  = Crypt::decrypt($request->id);
                $noticia     = Noticia::find($codnoticia);
                $img         = (!empty($noticia->imagen)) ? $noticia->imagen : \Config::get('dominios.DEFAULT_IMG_NOTICIA');
                $subruta     = \Config::get('paths.IMAGENES_NOTICIA') . $img;
                $ruta_nombre = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;
        }

        if (!empty($size)) {
            ob_end_clean();
            return Image::make($ruta_nombre)->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            })->response();
        }

        $response = Image::make($ruta_nombre)->response();
        ob_end_clean();
        return $response;
    }

    public function getfile(Request $request)
    {
        $option = \Crypt::decrypt($request->get('option'));
        $type   = \Crypt::decrypt($request->get('type'));
        
        switch ($option) {
            //Para obtener los archivos adjuntos del seguimiento de un comparendo
            case 1:
                $id             = \Crypt::decrypt($request->id);
                $archivoadjunto = Norma::findOrFail($id);
                $nombrearchivo  = $archivoadjunto->archivo;
                $subruta        = \Config::get('paths.DOCUMENTOS_NORMAS') . $archivoadjunto->archivo;
                $ruta_nombre    = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;
        }

        //Ver
        if ($type == 1) {
            return response()->file($ruta_nombre, [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $nombrearchivo . '"',
                'Cache-Control'       => 'private, max-age=0, must-revalidate',
            ]);
        }else{
            //Descargar
            return response()->download($ruta_nombre);
        }
    }

}
