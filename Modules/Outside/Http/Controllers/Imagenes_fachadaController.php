<?php

namespace Modules\Outside\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;

//Paquetes
use Modules\Outside\Entities\Imagen_fachada;

//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;

//**********************************************************************

class Imagenes_fachadaController extends Controller
{

    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Imagenes Fachada',
                'componente' => $this->componente('PARAM_FACH_IMG'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Imagenes Fachada'],
        ]);

        $imagenes_fachada = Imagen_fachada::all();

        return view('outside::imagenes_fachada.listar')->with('imagenes_fachada', $imagenes_fachada);
    }
    
    public function cargarcrear()
    {
        return view('outside::imagenes_fachada.crear');
    }

    public function crear(Request $request)
    {
        $imagen_fachada = new Imagen_fachada($request->all());
        $foto    = $request->imagenAdjunta;
        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".png";
                $subruta                  = Config::get('paths.IMAGENES_FACHADA') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('png'));

                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();
                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $imagen_fachada->nombre = $nombre_imagen_codificado;
            }
        }

        $status = $imagen_fachada->save();

        return response()->status($status);
    }

}
