<?php

namespace Modules\Outside\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;

//Paquetes
use Modules\Outside\Entities\Norma;

//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;

//**********************************************************************

class NormasController extends Controller
{

    public function todosdatosNormas(Request $request)
    {
        $normas = Norma::all();

        return Datatables::of($normas)->addColumn('opciones', function (Norma $norma) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '';
            if(isset($norma->archivo)){
                $html .= '<a href="'.$norma->rutaarchivopublica.'" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>';
            }
            
            return $html;
        })->rawColumns(['opciones'])->make(true);
    }

}
