<?php

namespace Modules\Outside\Http\Controllers;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use ABCcomptencias\Mail\RegistroUsuarios;
use Mail;
use JavaScript;
use DB;

use Modules\Outside\Entities\Usuario;

class UsuariosController extends Controller
{
    
    public function registrarme(Request $request)
    {
        DB::beginTransaction();
        try {
            $usuario = new Usuario($request->all());

            $usuario->tipodoc = $request->tipo_usuario;
            $usuario->password = bcrypt($request->password);
            $usuario->email = mb_strtolower($request->email);
            $usuario->estado = \Config::get('dominios.ESTADOUSU.VALORES.In');
            switch ($request->tipo_usuario) {
                case \Config::get('dominios.TIPO_USUARIO.VALORES.PN'):
                        $usuario->codperfil = 2; //persona
                    break;

                case \Config::get('dominios.TIPO_USUARIO.VALORES.EMP'):
                        $usuario->codperfil = 3; //persona
                    break;
                
                default:
                    # code...
                    break;
            }

            $status = $usuario->save();

            if($status){

                $ruta_activar = \route('outside.usuarios.activar_usuario', ['codusuario_cifrado' => \Crypt::encrypt($usuario->codusuario)], true);

                Mail::to($usuario->email)
                ->send(new RegistroUsuarios([
                    'usuario' => $usuario, 
                    'ruta_activar' => $ruta_activar,
                ]));
                DB::commit();
            }

            return response()->status($status);

            
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            abort(505);
        }
    }

    public function validar_documento(Request $request)
    {
        $existe = Usuario::where('documento', $request->documento)->exists();

        return response()->status($existe);
    }    

    public function validar_email(Request $request)
    {
        $existe = Usuario::where('email', mb_strtolower($request->email))->exists();
        
        return response()->status($existe);
    }

    public function activar_usuario($codusuario_cifrado)
    {
        $usuario = Usuario::findOrFail(\Crypt::decrypt($codusuario_cifrado));
        $usuario->estado = \Config::get('dominios.ESTADOUSU.VALORES.Act');
        
        $status = $usuario->save();

        if ($status) {
            JavaScript::put([
                'estado_activacion' => $status
            ]);

           return view('outside::ingresar');
        }

    }
}
