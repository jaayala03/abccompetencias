<?php

namespace Modules\Outside\Http\Controllers;

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use ABCcomptencias\Mail\RegistroUsuarios;
use Mail;
use JavaScript;
use Modules\Outside\Entities\Usuario;
use Modules\Outside\Entities\Solicitud;

class SolicitudesController extends Controller
{
    
    public function consultar(Request $request)
    {
        $documento = $request->documento;

        $v = \Validator::make(request()->all(), [
            'g-recaptcha-response' => 'recaptcha',
        ]);
        $errors = $v->errors();
       if (count($errors)) {
        return [
            'type' => 'error',
            'message' => 'Error al validar CAPTCHA',
            'title' => 'Error',
            'type' => 'danger'];
       }
       else{
            $datos = Solicitud::solicitudesusuario($documento)
            ->select('solicitudes.codsolicitud', 'normas.*', 'usuarios.documento', 'etapas_solicitudes.etapa', 'etapas_solicitudes.created_at as fecha')
            ->orderBy('fecha', 'DESC')
            ->get();
            $datos->map(function($fila) {

                $certificado = $fila->certificados->first();
                if($certificado){
                    $fila->certificado = '<a data-placement="top" target="_blank" title="Ver Certificación" data-toggle="tooltip" href="' . route('solicitudes.certificados.ver', [\Crypt::encrypt($certificado->codcertificado)]) . '" class="btn btn-inverse btn-xs rounded tooltip-opcion"><i class="fa fa-graduation-cap"></i></a> ';
                }else{
                    $fila->certificado = '';
                }

                $fila->estado = '<span class="badge badge-' . \Config::get('dominios.ETAPA_SOLICITUD.ALL.' . $fila->etapa)['class'] . '">' . \Config::get('dominios.ETAPA_SOLICITUD.ALL.' . $fila->etapa)['value'] . '</span>';
                return $fila;
            });
            return [
                'solicitudes'  => $datos
            ];
       }        
    }
}
