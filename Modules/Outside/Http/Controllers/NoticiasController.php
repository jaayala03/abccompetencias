<?php

namespace Modules\Outside\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;

//Paquetes
use Modules\Outside\Entities\Noticia;

//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;

//**********************************************************************

class NoticiasController extends Controller
{

    //permisos trait
    use Permisos;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Noticias',
                'componente' => $this->componente('PARAM_FACH_NOT'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Noticias'],
        ]);


        return view('outside::noticias.listar');
    }

    public function datatable(Request $request)
    {
        $noticias = Noticia::all();

        return Datatables::of($noticias)->editColumn('imagen', function (Noticia $noticia) {
            return '<a class="image-lightbox" href="' . route('outside.getimage', ['codnoticia' => Crypt::encrypt($noticia->codnoticia), 'option' => Crypt::encrypt(3)]) . '">'
            . '<img width="80" height="50" src="' . route('outside.getimage', ['codnoticia' => Crypt::encrypt($noticia->codnoticia), 'option' => Crypt::encrypt(3)]) . '" class="text-center scale-img img-square" alt="img">'
                . '</a>';
        })->addColumn('opciones', function (Noticia $noticia) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($this->verificarpermiso("PARAM_FACH_NOT_ELI") ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminar-noticia tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . \Crypt::encrypt($noticia->codnoticia) . '"><i class="fa fa-times"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("PARAM_FACH_NOT_EDI") ? '<a href="#'.route("outside.noticias.cargareditar", ['codnoticia' => \Crypt::encrypt($noticia->codnoticia)], false).'"  class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>' : '');
            return $html;
        })->rawColumns(['opciones', 'imagen'])->make(true);
    }
    
    public function cargarcrear()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Agregar Noticia',
                'componente' => $this->componente('PARAM_FACH_NOT'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Noticias', 'url' => '#'.route('outside.noticias.listar', [], false)],
            ['name' => 'Agregar'],
        ]);

        return view('outside::noticias.crear');
    }

    public function crear(Request $request)
    {

        $noticia = new Noticia($request->all());
        $foto    = $request->imagenAdjunta;

        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".png";
                $subruta                  = Config::get('paths.IMAGENES_NOTICIA') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('png'));

                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();
                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $noticia->imagen = $nombre_imagen_codificado;
            }
        }

        $status = $noticia->save();

        return response()->status($status);
    }

    public function cargareditar($codnoticia)
    {

        $noticia = Noticia::findOrFail(\Crypt::decrypt($codnoticia));

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar Noticia',
                'componente' => $this->componente('PARAM_FACH_NOT'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Noticias', 'url' => '#'.route('outside.noticias.listar', [], false)],
            ['name' => 'Editar'],
        ]);

        return view('outside::noticias.editar')
        ->with('noticia', $noticia);
    }

    public function editar(Request $request)
    {
        $noticia = Noticia::findOrFail(\Crypt::decrypt($request->codnoticiacifrado));
        $noticia->fill($request->all());

        $foto    = $request->imagenAdjunta;

        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Eliminar Imagen Anterior
                //******************************************************************
                $imagen_anterior = Config::get('paths.IMAGENES_NOTICIA') . $noticia->imagen;
                if (!empty($noticia->imagen)) {
                    Storage::disk('web_files')->delete($imagen_anterior);
                }
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".png";
                $subruta                  = Config::get('paths.IMAGENES_NOTICIA') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('png'));

                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();
                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $noticia->imagen = $nombre_imagen_codificado;
            }
        }

        $status = $noticia->save();

        return response()->status($status);
    }

    public function eliminar(Request $request)
    {
        $noticia = Noticia::findOrFail(\Crypt::decrypt($request->codnoticiacifrado));
        $status = $noticia->delete();

        return response()->status($status);
    }

}
