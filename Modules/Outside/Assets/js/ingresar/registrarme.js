$(document).ready(function() {
    if (window.estado_activacion) {
        swal({
            type: 'success',
            title: 'Éxito',
            text: 'La activación de su usuario fue exitosa, por favor inicie sesion para continuar!',
        });
    }
    ///////////////////////////////////////////////////////////////
    $("#frm_registrarme").validate({
        submitHandler: function(form) {
            var url = baseurl + '/outside/usuarios/registrarme';
            $('#registrarme').attr('disabled', 'true');
            $('#registrarme').html('Registrando...');
            call_ajax({
                url: url,
                data: $("#frm_registrarme").serialize(),
                callbackfunctionsuccess: function(response) {
                    if (response.type == 'success') {
                        $('#registrarme').removeAttr('disabled');
                        $('#registrarme').html('Registrarme');
                        $("#frm_registrarme").trigger("reset");
                        swal({
                            type: 'success',
                            title: 'Registro Exitoso',
                            text: 'Por favor, revise su correo electronico para la activación de su usuario!',
                        });
                        // routie('/outside/ingresar/');
                    } else {
                        notifier.show(response.title, response.message, response.type, 5000);
                    }
                }
            });
        },
        rules: {
            tipo_usuario: {
                required: true
            },
            documento: {
                required: true
            },
            nombres: {
                required: true
            },
            primerapellido: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 20
            },
            password2: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Este campo es requerido.",
                minlength: "La contraseña debe tener minimo 8 carcteres.",
                maxlength: "La contraseña debe tener maximo 20 carcteres."
            },
            password2: {
                required: "Este campo es requerido.",
                equalTo: "Las contraseñas no coinciden."
            }
        },
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error has-feedback');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        }
    });
});
$(document).on('click', '#registrarme', function() {
    $("#frm_registrarme").submit();
});
// validar documento
$(document).on('change', '#nro_documento', function() {
    var url = baseurl + '/outside/usuarios/validar_documento';
    if ($(this).val() !== '') {
        call_ajax({
            url: url,
            data: {
                'documento': $(this).val()
            },
            callbackfunctionsuccess: function(response) {
                if (response.type == 'success') {
                    swal({
                        type: 'warning',
                        title: 'Advertencia',
                        text: 'Ya existe otro usuario con ese numero de documento!',
                    });
                    $('#nro_documento').val('');
                    $('#nro_documento').focus();
                }
            }
        });
    }
});
// validar email
$(document).on('change', '#emailr', function() {
    var url = baseurl + '/outside/usuarios/validar_email';
    if ($(this).val() !== '') {
        call_ajax({
            url: url,
            data: {
                'email': $(this).val()
            },
            callbackfunctionsuccess: function(response) {
                if (response.type == 'success') {
                    swal({
                        type: 'warning',
                        title: 'Advertencia',
                        text: 'Ya existe otro usuario con ese correo electronico!',
                    });
                    $('#emailr').val('');
                    $('#emailr').focus();
                }
            }
        });
    }
});