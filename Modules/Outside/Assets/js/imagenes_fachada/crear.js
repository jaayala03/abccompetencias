$(document).ready(function() {
    var $image = $(".image-crop > img");
    $($image).cropper({
        aspectRatio: 16 / 9,
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });
    var $inputImage = $("#FileInput");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                files = this.files,
                file;
            if (!files.length) {
                return;
            }
            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }
    ///////////////////////////////////////////////////////////////
    $("#frm_agregar").validate({
        submitHandler: function(form) {
            procesar_imagen({
                image: $(".image-crop > img"),
                form: $("#frm_agregar"),
                hiddenfield: $("#imagenAdjunta")
            });
            var url = baseurl + '/outside/imagenes_fachada/crear';
            call_ajax({
                url: url,
                data: $("#frm_agregar").serialize(),
                callbackfunctionsuccess: function(response) {
                    if (response.type == 'success') {
                        window.parent.show_message(response);
                        window.parent.$('#AgregarImagen').modal('hide');
                        window.parent.$('#AgregarImagen').find('form').trigger('reset');

                        location.reload();
                    }
                    show_message(response);
                }
            });
        },
        rules: {
            descripcion: {
                required: true
            },
        },
        highlight: function(element) {
            $(element).parents('.form-group').addClass('has-error has-feedback');
        },
        unhighlight: function(element) {
            $(element).parents('.form-group').removeClass('has-error');
        }
    });
    //    cerrar modal de crear grupo
    $('#btn-cerrar-modal').on("click", function(e) {
        window.parent.$('#AgregarImagen').modal('hide');
    });
});