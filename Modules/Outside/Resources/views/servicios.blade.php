@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
{{--
<script src="{{ asset('modules/krauff/js/perfiles/perfiles.js') }}">
</script>
--}}
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="container" style="background: white">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 panelcontenidos">
            <div class="headpaneles">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-briefcase">
                        </i>
                        Servicios
                    </h3>
                </a>
            </div>
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">
                    <a class="btn-link" href="#">
                        <h4 class="titulonoticia">
                            Nuestra empresa
                        </h4>
                    </a>
                    <p class="redaccionoticia-index">
                        descripcion...
                    </p>
                    <br>
                        <a class="btn btn-success btnvermas btn-xs" href="#">
                            Ver más
                        </a>
                        <em class="text-muted small pull-right">
                            {{\Carbon::now()}}
                        </em>
                    </br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modales')

@endsection
