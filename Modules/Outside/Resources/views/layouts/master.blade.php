<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <meta content="Aplicacion ABCcomptencias" name="description"/>
        <meta content="ABCcomptencias" name="keywords"/>
        <meta content="jaayala03" name="author"/>
        <link href="{{ asset('images/favicon.ico', Request::secure()) }}" rel="shortcut icon" type="image/x-icon"/>
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <title>
            ABC Competencias
        </title>

        <!-- START @FONT STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <link href="{{ asset(mix('/css/app.css'), Request::secure()) }}" rel="stylesheet"/>
        <link href="{{ asset(mix('/css/all.css'), Request::secure()) }}" rel="stylesheet"/>

        <link href="{{ asset('modules/outside/css/inicio.css') }}" rel="stylesheet" />
        <link href="{{ asset('modules/outside/css/section.css') }}" rel="stylesheet" />
        
        @yield('css')
        <script>
            var baseurl = @json(url("/"));
            var session = {warnings: @json(session('warnings'))};
        </script>

        {!! RecaptchaV3::initJs() !!}

    </head>
    <body class="cuerpo-pagina">
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&appId=2315038455282857&autoLogAppEvents=1"></script>

        <div class="container">
            <div class="row">
                <header class="sb-page-header">
                    <img alt="abccompetencias" src="{{ asset('images/banner.png') }}"/>
                </header>
                <!-- Navigation -->
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container-fluid wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{ route('outside.inicio') }}">
                                    <i class="fa fa-home"></i> Inicio
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.quienes_somos') }}">
                                    <i class="fa fa-users"></i> ¿Quiénes somos?
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.servicios') }}">
                                    <i class="fa fa-briefcase"></i> Servicios
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.acerca_de') }}">
                                    <i class="fa fa-certificate"></i> Acerca de
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.pqrs') }}">
                                    <i class="fa fa-question-circle"></i> Quejas y Apelaciones
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.consultas') }}">
                                    <i class="fa fa-search"></i> Consulta el estado de la Certificación
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('outside.descargas') }}">
                                    <i class="fa fa-download"></i> Descargas
                                </a>
                            </li>
                            <li style="border: none;">
                                <a href="{{ route('outside.ingresar') }}">
                                    <i class="fa fa-sign-in"></i> Ingresar
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div>
                <ul class="nav navbar-right">
                    <li>
                        <span style="cursor: none; font-weight: bold; color: #516FB4; font-size: 12px;">
                            {!! utf8_encode(ucfirst(Carbon::now()->formatLocalized('%A %d de %B de %Y'))) !!}
                        </span>
                    </li>
                </ul>
            </div>
            <!-- Heading Row -->
        </div>
        
    	@yield('content')

        @yield('modales')

        <footer id="footer">
        <div class="footer-menu container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <img src="{{ asset('images/logo2.png') }}" alt="..." class="footer-logo" style="width: 200px; height: 100px">
                        {{-- <p style="color: #000000">
                            Lorem ipsum dolor amet consecte adipisicing elit sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco.
                        </p> --}}
                        
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <h4 class="footer-title">Contáctenos</h4>
                        <div class="media">
                            
                            <div class="media-body" style="color: #000000; font-size: 14px;">
                                Cra 0 No 0-00, Bucaramanga, Santander <br>
                                 (7) 6000000
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <h4 class="footer-title">Nuestras redes</h4>
                        <section>
                            <ul class="social-network ">
                                <li>
                                    <a class="icon-linksb round facebook fillsb"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a class="icon-linksb round twitter fillsb"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="icon-linksb round youtube fillsb"><i class="fa fa-youtube"></i></a>
                                </li>
                                <li>
                                    <a class="icon-linksb round envelope fillsb"><i class="fa fa-envelope"></i></a>
                                </li>
                            </ul>  
                    </section> 
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-link container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="wow bounceIn footer-copyright" >Copyright © <span id="footer-copyright">{!! utf8_encode(ucfirst(Carbon::now()->formatLocalized('%Y'))) !!}</span>. <a href="{{ route('outside.inicio') }}" target="_blank">ABC comptencias</a></div>
                    </div>
                
                </div>
            </div>
        </div>
    </footer>

        <script src="{{ asset(mix('/js/app.js'), Request::secure()) }}">
        </script>
        <script src="{{ asset(mix('/js/all.js'), Request::secure()) }}">
        </script>

        <script type="text/javascript">
            paceOptions = {
              ajax: true,
              document: true,
              eventLag: false
            };
            AOS.init();
        </script>
        @yield('js')         

        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d47040108da8d67"></script>

        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        
        @include('krauff::layouts.partials.msg')
    </body>
</html>