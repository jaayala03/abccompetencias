@extends('krauff::layouts.modal_master')
<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/outside/js/imagenes_fachada/crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->
<!-- Start body content -->

<div class="row">
    <div class="col-sm-12">
        <!-- Start comment form -->
        <div class="panel rounded">                                        
            <div class="form-body">
                <div class="form-group">
                    {{Form::open(['method' => 'POST', 'id'=>'frm_agregar'])}}
                    <div class="form-group">
                        {{Form::label('descripcion', 'Descripcion')}}
                        {{Form::text('descripcion', null, ['id'=>'descripcion','class' => 'form-control rounded', 'placeholder'=>'Ingrese descripción'])}}
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="imagenAdjunta" id="imagenAdjunta">
                            <label title="Adjuntar Imagen" for="FileInput" class="btn btn-success col-xs-4">
                                <input type="file" accept="image/*" name="file" id="FileInput" class="hide">
                                Examinar Imagen
                            </label>
                            <div class="col-xs-8">
                                <div class="image-crop">
                                    <img width="100" src=""/>
                                </div>
                            </div>
                        </div>
                    </div>                     

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn-cerrar-modal">Cerrar</button>
                        <button type="sumbit" class="btn btn-theme" id="btn-guardar">Agregar</button>
                    </div>
                    {{ Form::close() }}
                </div>
                <!--/ End tabs content -->
            </div><!-- /.panel -->
        </div><!-- /.row -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection