@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/outside/js/imagenes_fachada/listar.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Imagenes Fachada </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">

            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Imagenes</h3>
                    </div>
                    <div class="pull-right">                       
                        @if(array_key_exists("PARAM_FACH_IMG_CRE", session("permisos")))
                            <a  title='Agregar Imagen'  data-toggle="modal" data-target="#AgregarImagen">
                                <button class='btn btn-primary' type="button"><i class='fa fa-plus'></i> Agregar Imagen</button>
                            </a>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    @if(count($imagenes_fachada) > 0)
                        @foreach ($imagenes_fachada as $key => $imagen_fachada)
                            <div class="col-md-6 col-sm-6">
                            <!-- Start toolbar location bottom -->
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{$imagen_fachada->descripcion}}</h3>
                                    </div><!-- /.panel-heading -->
                                    <div class="panel-body text-center">
                                        <img width="450px" height="150px" src="{{$imagen_fachada->rutaimagen}}">
                                    </div><!-- /.panel-body -->
                                    <div class="panel-footer">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-sm btn-primary">Eliminar</button>
                                        </div>
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-sm btn-danger">Editar</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div><!-- /.panel-footer -->
                                </div><!-- /.panel -->
                            <!--/ End toolbar location bottom -->
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-warning ">
                            <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
                            <div class="notification-info">
                                <ul class="clearfix notification-meta">
                                    <li class="pull-left notification-sender">Sin Imagenes de fachada</li>
                                </ul>
                                <p>
                                    Por favor hacer clic en el boton "Agregar Imagen"
                            </div>
                        </div>
                    @endif
                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->


        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->

</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')
<!-- crear -->
<div aria-hidden="true" class="modal modal-iframe fade" id="AgregarImagen" role="dialog" aria-labelledby="AgregarImagen" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="titulo-modal">
                    Agregar Imagen
                </h4>
            </div>
            <div class="modal-body">
                <iframe data-iframe-src="{{url('outside/imagenes_fachada/cargarcrear')}}" frameborder="0" height="500" scrolling="yes" width="100%">
                </iframe>
            </div>
        </div>
    </div>
</div>
@endsection
