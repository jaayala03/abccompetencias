@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset(mix('modules/outside/js/solicitudes/consultar.js')) }}">
</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
.g-recaptcha{
    margin: 0 auto;
    max-width: fit-content;
    padding-bottom: 5px;
}
</style>
@endsection

@section('content')
<div class="container" style="background: white" id="app" v-cloak>
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                       <i class="fa fa-search" aria-hidden="true"></i>
                       Consulta el estado de la Certificación
                    </h3>
                </a>
            </div>
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">

                    <div class="row mb-20">
                        <div class="col-md-offset-3 col-md-6">
                            <form action="#" @submit.prevent="submit" data-vv-scope="formC" class="search-basic-form">
                                
                                {{-- <div class="form-group">
                                    <label class="control-label">Documento</label>
                                    <div class="input-group">

                                        <div class="input-cont">
                                            <input placeholder="Ingrese número de documento..." class="form-control" type="text">
                                        </div>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div> --}}
                                <div :class="{'form-group':true, 'has-error': errors.has('formC.documento') || errors.has('g-recaptcha-response') }">
                                    <label class="control-label">Documento:</label>
                                    <div class="input-control">
                                        <input name="documento" v-validate="'required|numeric'" data-vv-as="documento" v-model="form_consulta.documento" placeholder="Ingrese número de documento..." class="form-control" type="text">
                                        <span v-show="errors.has('formC.documento')" class="help-block">@{{ errors.first('formC.documento') }}</span>

                                        <span v-show="errors.has('g-recaptcha-response')" class="help-block">@{{ errors.first('g-recaptcha-response') }}</span>
                                    </div>
                                </div>

                                {!! htmlFormSnippet() !!}

                                <div class="text-center">
                                    <button :disabled="busy" type="submit" class="btn btn-success rounded ">@{{ busy ? 'Consultando' : 'Consultar' }} <i class="fa fa-search"></i></button>
                                </div>

                                {{-- <div class="form-group">
                                    <label class="control-label">Captcha:</label>
                                    <input class="form-control" type="text">
                                </div> --}}
                            </form>
                        </div>
                    </div>
                    <div class="row mt-20" v-if="consultado">

                        <div v-if="solicitudes.length == 0" class="col-md-offset-3 col-md-6">
                            <div class="alert alert-warning ">
                                <span class="alert-icon"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>
                                <div class="notification-info">
                                    <ul class="clearfix notification-meta">
                                        <li class="pull-left notification-sender">Sin resultados</li>
                                    </ul>
                                    <p>
                                        No se han encontrado resultados para el documento: @{{ form_consulta.documento }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div v-else class="col-md-12 p-20">

                            <div class="table-responsive rounded mb-20">
                                <table id="tour-16" class="table table-bordered table-striped table-theme">
                                    <thead>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Fecha</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(item, index) in solicitudes">
                                            <td class="text-center border-right">@{{item.codigo}}</td>
                                            <td>@{{ item.nombre }}</td>
                                            <td>@{{ item.fecha }}</td>
                                            <td class="text-center" v-html="item.estado"></td>
                                            <td v-html="item.certificado" class="text-center">
                                              
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                  
                                </table>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modales')

@endsection
