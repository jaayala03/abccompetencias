@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/outside/js/noticias/crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style type="text/css">
    .cropper-container {
        width: 350px;
        height: 300px;
    }
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Noticias </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Agregar Noticia</h3>
                    </div>
                    <div class="pull-right">                       
                        @if(array_key_exists("PARAM_FACH_NOT_CRE", session("permisos")))
                            
                                <button class='btn btn-primary' id="btn_agregar_noticia" type="button"><i class='fa fa-plus'></i> Agregar</button>
                           
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="form-body">
                    <form method="POST" id="frm_agregar_noticia">
                        <div class="form-group">
                            <label for="titulo" class="control-label" ">Titulo:</label>
                            <input class="form-control" type="text" name="titulo" id="titulo">
                        </div>

                        <div class="form-group">
                            <label for="descripcion" class="control-label" ">Descripción:</label>
                            <textarea class="form-control" name="descripcion" id="descripcion"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="contenido" class="control-label" ">Contenido:</label>
                            <textarea id="contenido" name="contenido" class="form-control" rows="10" placeholder="Enter text ..."></textarea>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="imagenAdjunta" id="imagenAdjunta">
                            <label title="Adjuntar Imagen" for="FileInput" class="btn btn-success">
                            <input type="file" accept="image/*" name="file" id="FileInput" class="hide">
                                Cambiar Imagen
                            </label>
                            <div class="image-crop">
                                <img width="50" src=""/>
                            </div>
                        </div>
                    </form>        
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
