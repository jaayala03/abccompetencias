@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->

<script src="{{ asset('modules/outside/js/descargas.js') }}">
</script>

@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="container" style="background: white" data-aos="zoom-in">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-download">
                        </i>
                        Descargas
                    </h3>
                </a>
            </div>
            <div class="row">
                    <div class="col-md-12">
                        <h4>Descargue las normas y formatos, para que conozca más sobre los procesos de certificación.</h4>
                        <!-- Start accordion menu -->
                        <div class="panel-group rounded shadow" id="accordion2">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color: #F3F3F3;">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-1" aria-expanded="true"><span class="fa fa-plus"></span>  Normas de Competencia Laboral</a>
                                    </h4>
                                </div><!-- /.panel-heading -->
                                <div id="collapse2-1" class="panel-collapse collapse in" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="table_descargas" class="tablas_solicitud table table-primary table-bordered table-striped table-hover" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Norma</th>
                                                        <th><i class="fa fa-cog"></i></th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody></tbody>   
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.panel -->
                        </div><!-- /.panel-group -->
                        <!-- End accordion menu -->
                        <!-- Start accordion menu -->
                        <div class="panel-group rounded shadow" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading" style="background-color: #F3F3F3;">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion3" href="#collapse2-2" aria-expanded="true"><span class="fa fa-plus"></span>  Otros Documentos</a>
                                    </h4>
                                </div><!-- /.panel-heading -->
                                <div id="collapse2-2" class="panel-collapse collapse in" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table  class="table table-primary table-bordered table-striped table-hover" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th class="text-center"><i class="fa fa-cog"></i></th>
                                                    </tr>
                                                </thead>
                                                <!--tbody section is required-->
                                                <tbody>
                                                    <tr>
                                                        <td>T-01-F11 CODIGO DE CONDUCTA Y ETICA PROFESIONAL</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/T-01-F11.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>T-01-F12 DERECHOS Y DEBERES</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/T-01-F12.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>M-02-PRO-02 PROCEDIMIENTO PQRSF</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/M-02-PRO-02.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280202086 V2 EVALUAR FUNCIONAMIENTO DE EQUIPOS A GAS</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280202086_V2.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280202088 V1 REPARAR EQUIPOS A GAS SEGÚN PROCEDIMIENTOS TÉCNICOS</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280202088_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280202090 V2 EVALUAR INSTALACIONES DE GAS SEGÚN PROCEDIMIENTOS TÉCNICOS</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280202090_V2.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280601093 V1 ACONDICIONAR VEHÍCULOS DE ACUERDO CON PROCEDIMIENTOS DE MANTENIMIENTO</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280601093_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280601096 V1 REPARAR SISTEMA DE COMBUSTIBLE DE ACUERDO CON PROCEDIMIENTOS</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280601096_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>280601117 V1 INSTALAR EQUIPO DE GAS VEHICULAR DE ACUERDO CON PROCEDIMIENTOS</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/280601117_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>290302022 V1 SUMINISTRAR GAS NATURAL COMPRIMIDO VEHICULAR</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/290302022_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>290302024 V1 INSTALAR SISTEMA DE GAS NATURAL COMPRIMIDO</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/290302024_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>290302025 V1 MANTENER SISTEMA DE GAS NATURAL COMPRIMIDO</td>
                                                        <td class="text-center">
                                                            <a href="{{ asset('storage/290302025_V1.pdf') }}" target="_blank" class="btn btn-success rounded"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                </tbody>   
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.panel -->
                        </div><!-- /.panel-group -->
                        <!-- End accordion menu -->
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@section('modales')

@endsection
