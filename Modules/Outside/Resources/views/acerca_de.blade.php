@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
{{--
<script src="{{ asset('modules/krauff/js/perfiles/perfiles.js') }}">
</script>
--}}
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="container" style="background: white">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 panelcontenidos">
            <div class="headpaneles">
                <a class="btn-link text-center">
                    <h3>
                        <i aria-hidden="true" class="fa fa-briefcase">
                        </i>
                        Acerca de
                    </h3>
                </a>
            </div>
        </div>
    </div>

    <div class="row secciones intro" data-aos="fade-up">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">
                    <div class="col-md-12 panelnoticias text-center">
                        <img src="{{ asset('images/pages/mv2.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">
                    <a class="btn-link">
                        <h3 class="titulonoticia text-center">Misión</h3>
                    </a>
                    <p class="redaccionoticia-index">
                        Somos una organización privada de profesionales de diferentes campos, dedicados a la Evaluación y Certificación de Competencias Laborales en el sector del gas, que se rige por la normatividad técnica y legal aplicable a los diferentes roles y oficios en dicho sector y que orienta sus acciones bajo los principios de responsabilidad, calidad, imparcialidad e integridad.
                    </p>
                </div>
            </div>
        </div>
    </div>    

    <div class="row secciones intro" data-aos="fade-up">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">
                    <a class="btn-link">
                        <h3 class="titulonoticia text-center">Visión</h3>
                    </a>
                    <p class="redaccionoticia-index">
                        En el año 2025 ABC COMPETENCIAS S.A.S.  se consolida a nivel nacional como el Organismo de Evaluación y Certificación de Competencias Laborales líder del mercado de la certificación de personas, reconocido por la mejora continua de sus procesos, confiabilidad, profesionalismo, excelencia y buen clima organizacional, adheridos y respetuosos de nuestros compromisos con la imparcialidad, independencia e integridad.
                    </p>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias">
                    <div class="col-md-12 panelnoticias text-center">
                        <img src="{{ asset('images/pages/mv1.jpg') }}" width="400" height="400">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row secciones"></div>
</div>
@endsection

@section('modales')

@endsection
