@extends('outside::layouts.master')

@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/outside/js/inicio.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style type="text/css">

/*::-webkit-scrollbar{
    width: 10px;
    background-color: #F5F5F5;
}
::-webkit-scrollbar-track{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}
::-webkit-scrollbar-thumb{
    background-color: #516FB4; 
    background-image: -webkit-linear-gradient(90deg,rgba(255, 255, 255, .2) 25%,transparent 25%,transparent 50%,rgba(255, 255, 255, .2) 50%,rgba(255, 255, 255, .2) 75%,transparent 75%,transparent)
}*/
</style>
@endsection

@section('content')
    <div class="container" style="background: white">
            <div class="row">
                <div class="col-xs-12 panelsinpading">
                    <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">

                        @if(count($imagenes_fachada) > 0)
                            @foreach ($imagenes_fachada as $imagen)
                                <div data-src="{{ $imagen->rutaimagenpublica }}">
                                    <div class="camera_caption fadeFromBottom">
                                        <div class="desc_mainslider">
                                            {{$imagen->descripcion}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div data-src="{{ asset('images/logo2.png') }}">
                                <div class="camera_caption fadeFromBottom">
                                    <div class="desc_mainslider">
                                        Sin imagenes de fachada
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="background: white">
            <div class="row secciones">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos" data-aos="zoom-in-right">
                    <div class="headpaneles text-center">
                        <a class="btn-link" href="{{ route('outside.quienes_somos') }}">
                            <h3>
                                <i aria-hidden="true" class="fa fa-users">
                                </i>
                                ¿Quienes somos?
                            </h3>
                        </a>
                    </div>
                    <div class="contenidopaneles intro">
                        <div class="col-md-12 panelnoticias">
                            <a class="btn-link" href="#">
                                <h3 class="titulonoticia">ABC COMPETENCIAS S.A.S.</h3>
                            </a>
                            <p class="redaccionoticia-index">
                                Somos una empresa privada, constituida el 28 de marzo de 2016, cuyo objeto es “la Evaluación y Certificación de personas, a fines de demostrar objetivamente una conformidad, acorde con determinadas normas que permite establecer que una persona cumple con los requisitos de competencia para desarrollar un trabajo o labor determinada”. Trabajamos aplicando los principios de mejoramiento continuo, propiciando a nuestros clientes seguridad y confianza.
                            </p>

                            <p class="redaccionoticia-index">
                                Contamos con un equipo de trabajo competente y calificado, que conscientes de la importancia que representa la prestación de un excelente servicio, impulsamos una cultura de atención y satisfacción al cliente.
                            </p>

                            <p class="redaccionoticia-index">
                                El Manual de Sistema de Gestión ha sido desarrollado de acuerdo a los requerimientos normativos establecidos en la NTC-ISO-IEC 17024:2013 Evaluación de la Conformidad. Requisitos Generales para los Organismos que Realizan Certificación de Personas, así mismo teniendo en cuenta la normativa técnica emitida por los Organismos de Control y las respectivas Normas Técnicas Colombianas relacionadas con las competencias establecidas en el Alcance.
                            </p>

                            <a class="btn btn-warning rounded btn-xs pull-right" href="{{ route('outside.quienes_somos') }}">Ver más</a>
                            {{-- <em class="text-muted small pull-right">{{\Carbon::now()}}</em> --}}
                        </div>
                     </div>
                    <div class="row">    
                        <div class="col-sm-12">
                            <div id="line">
                                <hr>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos" data-aos="zoom-in-left">
                    <div class="headpaneles text-center">
                        <a class="btn-link" href="#">
                            <h3>
                                <i aria-hidden="true" class="fa fa-file">
                                </i>
                                Noticias
                            </h3>
                        </a>
                    </div>

                    <section style="height: 800px; overflow-y: scroll; overflow-x: hidden;">
                        @if(count($noticias) > 0)
                            @foreach ($noticias as $noticia)
                                <div class="contenidopaneles intro">
                                    <div class="col-md-12 panelnoticias">
                                        <a class="btn-link" target="_blank" href="{{ route('outside.vernoticia', [$noticia->slug]) }}">
                                            <h3 class="titulonoticia">{{$noticia->titulo}}</h3>
                                        </a>
                                         <br>
                                        <div class="text-center">
                                            <img class="shadow_abs" width="400" height="350" src="{{$noticia->rutaimagenpublica}}">
                                        </div>
                                        <br>
                                        <p class="redaccionoticia-index">
                                            {{$noticia->descripcion}}
                                        </p>

                                        <br>
                                        <a class="btn btn-warning rounded btn-xs" target="_blank" href="{{ route('outside.vernoticia', [$noticia->slug]) }}">Ver más</a>
                                        <em class="text-muted small pull-right">{{$noticia->updated_at}}</em>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="col-sm-12">
                                        <div id="line">
                                            <hr>
                                        </div>   
                                    </div>
                                </div>
                            @endforeach
                            @else
                                <div class="contenidopaneles intro">
                                    <div class="col-md-12 panelnoticias">
                                        <a class="btn-link" href="#">
                                            <h3 class="titulonoticia">SIN NOTICIAS</h3>
                                        </a>
                                        <p class="redaccionoticia-index">
                                            Aun no se han publicado noticias...
                                        </p>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="col-sm-12">
                                        <div id="line">
                                            <hr>
                                        </div>   
                                    </div>
                                </div>
                            @endif                     
                    </section>
                </div>
            </div>
        </div>
@endsection

@section('modales')

@endsection