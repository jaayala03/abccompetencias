@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/outside/js/pqrs.js') }}">
</script>

@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
.bold {
    font-weight: bold !important;
}
.g-recaptcha{
    margin: 0 auto;
    max-width: fit-content;
}
</style>
@endsection

@section('content')
<div class="container" style="background: white" data-aos="zoom-in" id="app">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-question-circle">
                        </i>
                        QUEJAS Y APELACIONES
                    </h3>
                </a>
            </div>    
        </div>    
    </div>    

    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panelcontenidos">
            <!-- Start body content -->
            <div class="body-content animated fadeIn">

                <!-- Start mail page -->
                <div class="row compose-mail-wrapper">
                    <div class="col-sm-8 col-sm-offset-2">
                        <!-- Star form compose mail -->
                        <form method="POST" id="frm_pqrs" class="form-horizontal">
                            <div class="panel rounded shadow panel-danger">
                                <div class="panel-heading text-center">
                                
                                        <h3 class="panel-title">Formulario de contacto</h3>
                                 
                                    <div class="clearfix"></div>
                                </div><!-- /.panel-heading -->
                                <div class="panel-title text-center">
                                    <h3 style="font-weight: bold;">Quejas - Apelaciones</h3>
                                    <div class="clearfix"></div>
                                </div><!-- /.panel-sub-heading -->
                                <div class="panel-body">
                                    <div class="compose-mail">
                                        <div class="form-group">
                                            <label for="nombre" class="bold col-sm-2 control-label">Nombre:</label>
                                            <div class="col-sm-10">
                                                <input style="text-transform: capitalize;" name="nombre" type="text" id="nombre" class="form-control input-md">
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="telefono" class="bold col-sm-2 control-label">Telefono:</label>
                                            <div class="col-sm-10">
                                                <input name="telefono" type="text" id="telefono" class="form-control input-md">
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="email" class="bold col-sm-2 control-label">Email:</label>
                                            <div class="col-sm-10">
                                                <input name="email" type="email" id="email" class="form-control input-md">
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="tipo" class="bold col-sm-2 control-label">Tipo de Solicitud:</label>
                                            <div class="col-sm-10">
                                                {{ Form::select('tipo', Config::get('dominios.PQRS.TXT'), null, ['placeholder' => 'Seleccione', 'class'=>'form-control input-md']) }}
                                            </div>
                                        </div><!-- /.form-group -->
                                        <div class="form-group">
                                            <label for="mensaje" class="bold col-sm-2 control-label">Descripción:</label>
                                            <div class="col-sm-10">
                                                <textarea name="mensaje" id="mensaje" class="form-control" rows="10" placeholder="Escriba aquí..."></textarea>
                                            </div>
                                        </div><!-- /.form-group -->

                                        {!! htmlFormSnippet() !!}
                                    </div><!-- /.compose-mail -->
                                </div><!-- /.panel-body -->
                                <div class="panel-footer">
                                    <div class="text-center">
                                        <button id="registrar_pqrs" type="button" class="btn btn-primary btn-md"><i class="fa fa-send"></i> Enviar</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div><!-- /.panel-footer -->
                            </div><!-- /.panel -->
                        </form>
                        <!--/ End form compose mail -->
                    </div>
                </div><!-- /.row -->
                <!--/ End mail page -->

            </div><!-- /.body-content -->
            <!--/ End body content -->
        </div>        

    </div>    

    <div class="row secciones"></div>
</div>
@endsection

@section('modales')

@endsection
