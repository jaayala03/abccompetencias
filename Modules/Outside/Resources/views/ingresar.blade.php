@extends('outside::layouts.master')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->

<script src="{{ asset('modules/outside/js/ingresar/registrarme.js') }}">
</script>

@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link href="{{ asset('modules/outside/css/ingresar.css') }}" rel="stylesheet" >
<style type="text/css">
    .bold {
        font-weight: bold !important;
    }
    .tittle-acord {
    color: #516FB4;
    font-weight: bold;
    }
</style>
@endsection

@section('content')
<div class="container" style="background: white" data-aos="zoom-in">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-sign-in">
                        </i>
                        Ingresar
                    </h3>
                </a>
            </div>    
        </div>    
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- Start accordion menu -->
            <div class="panel-group rounded shadow" id="accordion2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#F0F0F1;">
                        <h4 class="tittle-acord panel-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-1">
                                <span class="fa fa-sign-in" style="color: #516FB4;"></span> 
                                Inicio de Sesión
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div id="collapse2-1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="panel rounded shadow">
                                <div class="panel-body no-padding">
                                    <form class="form-horizontal mt-10" role="form" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                        @endif
                                        
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('layouts.partials.errors')
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-body">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="bold col-sm-3 control-label">Email</label>
                                                <div class="input-group rounded col-sm-7">
                                                    <input id="email" type="text" class="form-control" placeholder="Email" name="email"
                                                        value="{{ old('email') }}">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label for="password" class="bold col-sm-3 control-label">Password</label>
                                                <div class="input-group rounded col-sm-7">
                                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-7">
                                                    <div class="ckbox ckbox-theme">
                                                        <input id="rememberme" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                        <label for="rememberme" class="rounded">Recordarme</label>
                                                    </div>
                                                </div>
                                            </div><!-- /.form-group -->
                                        </div><!-- /.form-body -->
                                        <div class="form-footer text-center">
                                            <button type="submit" class="btn btn-primary rounded"><i class="fa fa-sign-in"></i> Ingresar</button>
                                        </div><!-- /.form-footer -->
                                    </form>
                                </div><!-- /.panel-body -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#F0F0F1;">
                        <h4 class="tittle-acord panel-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse2-2">
                                <span class="fa fa-users" style="color: #516FB4;"></span> 
                                Registrarme
                            </a>
                        </h4>
                    </div><!-- /.panel-heading -->
                    <div id="collapse2-2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel rounded shadow">
                                <div class="panel-body no-padding">
                                    <form method="POST" id="frm_registrarme" class="form-horizontal mt-10">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="tipo_usuario" class="bold col-sm-3 control-label">Tipo de Usuario</label>
                                                <div class="col-sm-7">
                                                    {{ Form::select('tipo_usuario', Config::get('dominios.TIPO_USUARIO.TXT'), 1, ['placeholder' => 'Seleccione tipo de usuario', 'class'=>'form-control']) }}
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="nro_documento" class="bold col-sm-3 control-label">Nro. Documento</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="documento" id="nro_documento"
                                                        placeholder="Nro. Documento">
                                                </div>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="nombres" class="bold col-sm-3 control-label">Nombres</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombres">
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label for="primerapellido" class="bold col-sm-3 control-label">Primer Apellido</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="primerapellido" id="primerapellido"
                                                        placeholder="Primer Apellido">
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label for="segundoapellido" class="bold col-sm-3 control-label">Segundo Apellido</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" name="segundoapellido" id="segundoapellido"
                                                        placeholder="Segundo Apellido">
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group form-group-divider" style="margin-bottom: 10px;">
                                                <div class="form-inner">
                                                    <h4 class="no-margin">Información de Ingreso</h4>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="email" class="bold col-sm-3 control-label">E-mail</label>
                                                <div class="col-sm-7">
                                                    <input style="text-transform:lowercase;" type="email" class="form-control" name="email" id="emailr"
                                                        placeholder="Correo electronico">
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label for="password" class="bold col-sm-3 control-label">Contraseña</label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" name="password" id="password"
                                                        placeholder="Password">
                                                </div>
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <label for="password2" class="bold col-sm-3 control-label">Confirmar Contraseña</label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" name="password2" id="password2"
                                                        placeholder="Re-password">
                                                </div>
                                            </div><!-- /.form-group -->

                                        </div><!-- /.form-body -->
                                        <div class="form-footer text-center">
                                            <button type="button" id="registrarme" class="btn btn-primary rounded"><i class="fa fa-check"></i> Registrarme</button>
                                        </div><!-- /.form-footer -->
                                    </form>
                                </div><!-- /.panel-body -->
                            </div><!-- /.panel -->
                        </div>
                    </div>
                </div><!-- /.panel -->
            </div><!-- /.panel-group -->
            <!-- End accordion menu -->
        </div>
    </div>
</div>
@endsection

@section('modales')

@endsection
