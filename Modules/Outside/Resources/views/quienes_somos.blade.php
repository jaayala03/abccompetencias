@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
{{--
<script src="{{ asset('modules/krauff/js/perfiles/perfiles.js') }}">
</script>
--}}
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="container" style="background: white" data-aos="zoom-in">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-briefcase">
                        </i>
                        ¿Quiénes Somos?
                    </h3>
                </a>
            </div>    
        </div>    
    </div>    

    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias intro">
                    <a class="btn-link text-center">
                        <h3 class="">ABC COMPETENCIAS S.A.S.</h3>
                    </a>
                    <p class="redaccionoticia-index">
                        Somos una empresa privada, constituida el 28 de marzo de 2016, cuyo objeto es “la Evaluación y Certificación de personas, a fines de demostrar objetivamente una conformidad, acorde con determinadas normas que permite establecer que una persona cumple con los requisitos de competencia para desarrollar un trabajo o labor determinada”. Trabajamos aplicando los principios de mejoramiento continuo, propiciando a nuestros clientes seguridad y confianza.
                    </p>

                    <p class="redaccionoticia-index">
                        Contamos con un equipo de trabajo competente y calificado, que conscientes de la importancia que representa la prestación de un excelente servicio, impulsamos una cultura de atención y satisfacción al cliente.
                    </p>
 
                    <p class="redaccionoticia-index">
                        El Manual de Sistema de Gestión ha sido desarrollado de acuerdo a los requerimientos normativos establecidos en la NTC-ISO-IEC 17024:2013 Evaluación de la Conformidad. Requisitos Generales para los Organismos que Realizan Certificación de Personas, así mismo teniendo en cuenta la normativa técnica emitida por los Organismos de Control y las respectivas Normas Técnicas Colombianas relacionadas con las competencias establecidas en el Alcance.
                    </p>
                </div>
            </div>
        </div>        

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 panelcontenidos">
            <div class="contenidopaneles">
                <div class="col-md-12 panelnoticias text-center">
                    <img src="{{ asset('images/pages/qs.jpg') }}">
                </div>
            </div>
        </div>
    </div>    

    <div class="row secciones"></div>
</div>
@endsection

@section('modales')

@endsection
