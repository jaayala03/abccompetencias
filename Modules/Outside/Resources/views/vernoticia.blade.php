@extends('outside::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
{{--
<script src="{{ asset('modules/krauff/js/perfiles/perfiles.js') }}">
</script>
--}}
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="container" style="background: white">
    <div class="row secciones">
        <div class="col-xs-12 col-sm-12 panelcontenidos">
            <div class="headpaneles text-center">
                <a class="btn-link" href="#">
                    <h3>
                        <i aria-hidden="true" class="fa fa-briefcase">
                        </i>
                        Detalle de Noticia
                    </h3>
                </a>
            </div>
            <div class="contenidopaneles">
                <div class="col-md-offset-2 col-md-8 panelnoticias">
                    <a class="btn-link">
                        <h3 class="titulonoticia" style="text-align: center;">{{$noticia->titulo}}</h3>
                    </a>
                    <br>
                    <p class="redaccionoticia-index">
                        {{$noticia->descripcion}}
                    </p>
                    <br>
                    <div class="text-center">
                        <img class="shadow_abs" width="600" height="350" src="{{$noticia->rutaimagenpublica}}">
                    </div>
                    <br>
                    <p class="redaccionoticia-index">
                       {!!$noticia->contenido!!}
                    </p>
                
                    <br>
                    <em class="text-muted small pull-left">Publicada el: {{$noticia->updated_at}}</em>
                </div>
                <div class="col-md-12 text-center mt-5">
                    <div class="addthis_inline_share_toolbox"></div>
                </div>
                
                <div class="col-md-12 text-center">
                    <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="" data-numposts="5"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modales')

@endsection
