@extends('krauff::layouts.modal_master')
<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/auditoria/js/auditoria/detalles.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->
<!-- Start body content -->

<div class="row" id="app">
    <div class="col-xs-12">
        <!-- Start comment form -->
        <div class="panel rounded">                                        
            <div class="form-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-accent panel-shadow">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title"><i class="fa fa-database"></i> Datos nuevos </h3>
                                </div>
                                <div class="pull-right">
                                   
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body">
                                <ul class="list-group">
                                  <li v-for="(item, index) in propiedades.attributes" :class="{'list-group-item': true , 'text-danger': propiedades.hasOwnProperty('old') && propiedades['old'][index] != propiedades.attributes[index] }"><b>@{{ index }}</b>: @{{ item }} </li>
                                </ul>
                            </div><!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-sm-6" v-if="propiedades.hasOwnProperty('old')">
                        <div class="panel panel-success panel-shadow">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h3 class="panel-title"><i class="fa fa-database"></i> Datos anteriores</h3>
                                </div>
                                <div class="pull-right">
                                   
                                </div>
                                <div class="clearfix"></div>
                            </div><!-- /.panel-heading -->
                            <div class="panel-body">
                                <ul class="list-group">
                                  <li v-for="(item, index) in propiedades.old" :class="{'list-group-item': true , 'text-danger': propiedades['old'][index] != propiedades.attributes[index] }"><b>@{{ index }}</b>: @{{ item }} </li>
                                </ul>
                            </div><!-- /.panel-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.panel -->
        </div><!-- /.row -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection