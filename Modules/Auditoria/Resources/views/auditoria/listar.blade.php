@extends('krauff::layouts.ajaxmaster')

@section('js')
<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset(mix('modules/auditoria/js/auditoria/listar.js')) }}">
</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
  #tbl_auditoria > tbody >  tr { height: 45px } 
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->
<!-- Start page header -->
<div class="header-content">
    <h2>
        <i class="fa fa-tasks">
        </i>
        Auditoría del sistema
    </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">
            Usted esta en:
        </span>
        @include('krauff::layouts.partials.breadcrumbs')
    </div>
    <!-- /.breadcrumb-wrapper -->
</div>
<!-- /.header-content -->
<!--/ End page header -->
<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">

                <div class="panel-heading no-padding">
                    <div class="pull-left">
                        <h3 class="panel-title">Auditoria del sistema</h3>
                    </div>
                    <div class="pull-right">
                        @if(array_key_exists("AUDIT_SIS_EXP", session("permisos")))
                            <button @click.prevent="exportar('xlsx')" class="btn btn-sm btn-success rounded" >Xlsx &nbsp;<i class="fa fa-file-excel-o"></i></button>
                        @endif    

                    </div>
                    <div class="clearfix"></div>
                </div> 
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row text-center">
                        <form class="form-inline">
                            <div class="form-body">
                                <div class="form-group">
                                    {{ Form::label('modulo', 'Modulo:') }}
                                    <el-select clearable v-model="modulo" placeholder="Seleccione">
                                        <el-option
                                          v-for="item in modulos"
                                          :key="item.value"
                                          :label="item.label"
                                          :value="item.value">
                                        </el-option>
                                      </el-select>
                                </div>
                                @if(array_key_exists("AUDIT_SIS_TIPO_TODO", session("permisos")))
                                    <div class="form-group">
                                        {{ Form::label('codusuario', 'Usuario:') }}
                                        <el-select clearable v-model="codusuario" placeholder="Seleccione">
                                            <el-option
                                              v-for="item in usuarios"
                                              :key="item.value"
                                              :label="item.label"
                                              :value="item.value">
                                              <span style="float: left">@{{ item.label }}</span>
                                              <img :src="item.img" style="float: right;"/>
                                            </el-option>
                                         </el-select>
                                    </div>
                                @endif
                                <div class="form-group">
                                    {{ Form::label('fecha_desde', 'Desde:') }}
                                    <date-picker :required="true" class="rounded" input-class="form-control" name="fecha_desde" placeholder="dd/mm/yyyy" v-model="fecha_desde">
                                    </date-picker>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    {{ Form::label('fecha_hasta', 'Hasta:') }}
                                    <date-picker :required="true" class="rounded" input-class="form-control" name="fecha_hasta" placeholder="dd/mm/yyyy" v-model="fecha_hasta">
                                    </date-picker>
                                </div>
                                
                            </div>
                            <div class="form-body">
                                <button @click.prevent="updateData()" class="btn btn-success rounded">
                                    <i aria-hidden="true" class="fa fa-search" >
                                    </i>
                                    Consultar
                                </button>
                                <button @click.prevent="resetData()" class="btn btn-danger rounded">
                                    <i aria-hidden="true" class="fa fa-times" >
                                    </i>
                                    Limpiar
                                </button>
                            </div>    
                            <!-- /.form-body -->
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <!-- Start datatable -->
                                <table class="table table-bordered table-hover table-striped table-danger rounded" id="tbl_auditoria" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th data-hide="phone">
                                                Descripción
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Usuario
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Modulo
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Fecha
                                            </th>
                                            <th class="text-center" data-hide="phone,tablet">
                                                <em class="fa fa-cog"></em>
                                            </th>
                                        </tr>
                                    </thead>
                                    <!--tbody section is required-->
                                    <tbody>
                                    </tbody>
                                </table>
                                <!--/ End datatable -->
                            </div>
                            <!-- /.responsive -->
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.body-content -->
<!--/ End body content -->
@endsection

@section('modales')
<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA LAS FUNCIONALIDADES DE LOS PERFILES-->
<!--****************************************************************************************************************-->
<div role="dialog" tabindex="-1" id="detalleAuditoria" class="modal fade modal-iframe" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span></button>
                <h4 id="titulo-modal" class="modal-title">Funcionalidades</h4>
            </div>
            <div class="modal-body">
                <iframe scrolling="yes" width="100%" height="460" src="" data-iframe-src="{{ url('auditoria/verdetalles') }}" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<!--****************************************************************************************************************-->
@endsection
