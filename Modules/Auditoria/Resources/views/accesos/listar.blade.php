@extends('krauff::layouts.ajaxmaster')

@section('js')
<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/auditoria/js/accesos/listar.js') }}">
</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<style>
  #tbl_accesos > tbody >  tr { height: 45px } 
</style>
@endsection

@section('content')
<!-- START @PAGE CONTENT -->
<!-- Start page header -->
<div class="header-content">
    <h2>
        <i class="fa fa-tasks">
        </i>
        Accesos al sistema
    </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">
            Usted esta en:
        </span>
        @include('krauff::layouts.partials.breadcrumbs')
    </div>
    <!-- /.breadcrumb-wrapper -->
</div>
<!-- /.header-content -->
<!--/ End page header -->
<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row text-center">
                        <form class="form-inline">
                            <div class="form-body">
                                
                                <div class="form-group">
                                    {{ Form::label('codusuario', 'Usuario:') }}
                                    <el-select clearable v-model="codusuario" placeholder="Seleccione">
                                        <el-option
                                          v-for="item in usuarios"
                                          :key="item.value"
                                          :label="item.label"
                                          :value="item.value">
                                          <span style="float: left">@{{ item.label }}</span>
                                          <img :src="item.img" style="float: right;"/>
                                        </el-option>
                                     </el-select>
                                </div>
                                <div class="form-group">
                                    {{ Form::label('fecha_desde', 'Desde:') }}
                                    <date-picker :required="true" class="rounded" input-class="form-control" name="fecha_desde" placeholder="dd/mm/yyyy" v-model="fecha_desde">
                                    </date-picker>
                                </div>
                                <!-- /.form-group -->
                                <div class="form-group">
                                    {{ Form::label('fecha_hasta', 'Hasta:') }}
                                    <date-picker :required="true" class="rounded" input-class="form-control" name="fecha_hasta" placeholder="dd/mm/yyyy" v-model="fecha_hasta">
                                    </date-picker>
                                </div>
                                
                            </div>
                            <div class="form-body">
                                <button  @click.prevent="updateData()" class="btn btn-success rounded">
                                    <i aria-hidden="true" class="fa fa-search" >
                                    </i>
                                    Consultar
                                </button>
                                <button @click.prevent="resetData()" class="btn btn-danger rounded">
                                    <i aria-hidden="true" class="fa fa-times" >
                                    </i>
                                    Limpiar
                                </button>
                            </div>    
                            <!-- /.form-body -->
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <!-- Start datatable -->
                                <table class="table table-bordered table-hover table-striped table-danger rounded" id="tbl_accesos" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th data-hide="phone">
                                                Usuario
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Fecha
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Ip
                                            </th>
                                            <th data-hide="phone,tablet">
                                                so
                                            </th>
                                            <th data-hide="phone,tablet">
                                                Navegador
                                            </th>
                                        </tr>
                                    </thead>
                                    <!--tbody section is required-->
                                    <tbody>
                                    </tbody>
                                </table>
                                <!--/ End datatable -->
                            </div>
                            <!-- /.responsive -->
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.body-content -->
<!--/ End body content -->
@endsection

@section('modales')

@endsection
