// =========================================================================
// Instancia Vue
// =========================================================================
var vm = new Vue({
    el: "#app",
    data: {
        usuarios: window.usuarios,
        fecha_desde: '',
        fecha_hasta: '',
        codusuario: '',
        modulo: '',
        modulos: [{
            label: 'Comparendos',
            value: 'comparendos'
        }, {
            label: 'Tramites',
            value: 'tramites'
        }, {
            label: 'Parqueaderos',
            value: 'parqueaderos'
        },{
            label: 'Parametros',
            value: 'parametros'
        },{
            label: 'Facturas',
            value: 'facturas'
        },{
            label: 'Sistema',
            value: 'sistema'
        }]
    },
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        updateData() {
            //this.reload()
            tabla.ajax.reload();
        },
        resetData() {
            this.fecha_desde = '';
            this.fecha_hasta = '';
            this.codusuario = '';
            this.modulo = '';
            this.updateData();
        },
        exportar(ext) {
            self = this;
            var url = baseurl + laroute.route('auditoria.exportar', {
                fecha_desde: self.fecha_desde,
                fecha_hasta: self.fecha_hasta,
                codusuario: self.codusuario,
                modulo: self.modulo,
                ext: ext
            });
            var win = window.open(url, '_blank');
            win.focus();
        },
    }
});
//******************************************************************************
//Datatable conceptos
//******************************************************************************
$(document).ready(function() {
    tabla = $('#tbl_auditoria').DataTable({
        order: [
            [3, "desc"]
        ],
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/auditoria/getAll',
            "type": "POST",
            "data": function(d) {
                d.fecha_desde = vm.fecha_desde;
                d.fecha_hasta = vm.fecha_hasta;
                d.codusuario = vm.codusuario;
                d.modulo = vm.modulo;
            },
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
        },
        columns: [{
            data: 'description',
            name: 'activity_log.description'
        }, {
            data: 'usuario',
            name: 'usuario',
            searchable: false,
            orderable: false
        },{
            data: 'log_name',
            name: 'activity_log.log_name',
            searchable: false,
            orderable: false
        }, {
            data: 'created_at',
            name: 'activity_log.created_at',
        }, {
            data: 'opciones',
            name: 'opciones',
            searchable: false,
            orderable: false
        }]
    });
});