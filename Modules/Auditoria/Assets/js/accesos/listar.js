// =========================================================================
// Instancia Vue
// =========================================================================
var vm = new Vue({
    el: "#app",
    data: {
        usuarios: window.usuarios,
        fecha_desde: '',
        fecha_hasta: '',
        codusuario: '',
    },
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        updateData() {
            //this.reload()
            tabla.ajax.reload();
        },
        resetData() {
            this.fecha_desde = '';
            this.fecha_hasta = '';
            this.codusuario = '';
            this.updateData();
        }
    }
});
//******************************************************************************
//Datatable conceptos
//******************************************************************************
$(document).ready(function() {
    tabla = $('#tbl_accesos').DataTable({
        order: [
            [1, "desc"]
        ],
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            "url": baseurl + '/auditoria/accesos/getAll',
            "type": "POST",
            "data": function(d) {
                d.fecha_desde = vm.fecha_desde;
                d.fecha_hasta = vm.fecha_hasta;
                d.codusuario = vm.codusuario;
            },
        },
        "fnDrawCallback": function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
        },
        columns: [{
            data: 'usuario',
            name: 'usuario',
            searchable: false,
            orderable: false
        }, {
            data: 'created_at',
            name: 'created_at',
        }, {
            data: 'ipoculta',
            name: 'ipoculta',
            searchable: false,
            orderable: false
        }, {
            data: 'so',
            name: 'so',
        }, {
            data: 'navegador',
            name: 'navegador',
        }]
    });
});