<?php

namespace Modules\Auditoria\Entities;

use Illuminate\Database\Eloquent\Model;

class Acceso extends Model
{
    protected $table      = 'accesos';
    protected $primaryKey = 'codacceso';
    protected $fillable   = [
        'codacceso',
        'codusuario',
        'fechaingreso',
        'horaingreso',
        'ipoculta',
        'ipvisible',
    ];

    //relacion con usuarios
    public function usuario()
    {
        return $this->belongsTo('Modules\Auditoria\Entities\Usuario', 'codusuario');
    }
}
