<?php

Route::group(['middleware' => 'web', 'prefix' => 'auditoria', 'namespace' => 'Modules\Auditoria\Http\Controllers'], function () {

    Route::get('listar', 'AuditoriaController@index')->middleware('funcionalidades:AUDIT_SIS');

    //Ruta traer datos
    Route::post('getAll', 'AuditoriaController@getAll')
        ->middleware('funcionalidades:AUDIT_SIS');

    Route::get('verdetalles/{id}', 'AuditoriaController@show')->middleware('funcionalidades:AUDIT_SIS');

    Route::get('verdetalles/{id}', 'AuditoriaController@show')->middleware('funcionalidades:AUDIT_SIS');

    Route::get('exportar', 'AuditoriaController@exportar')->name('auditoria.exportar')->middleware('funcionalidades:AUDIT_SIS_EXP');

    //**********************************************************************
    //Grupo de rutas para accesos
    //**********************************************************************
    Route::group(['prefix' => 'accesos'], function () {
        //ruta para abrir la vista de la hoja de vida del vehiculo
        Route::get('listar', [
            'uses' => 'AccesosController@index',
            'as'   => 'auditoria.accesos.listar',
        ])->middleware('funcionalidades:AUDIT_SIS');;

        //Ruta traer datos
        Route::post('getAll', 'AccesosController@getAll')->middleware('funcionalidades:AUDIT_SIS');
    });

});
