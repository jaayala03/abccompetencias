<?php

namespace Modules\Auditoria\Http\Controllers;

//Paquetes
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Auditoria\Entities\Usuario;
use ABCcomptencias\Traits\Permisos;

//permisos
use Spatie\Activitylog\Models\Activity;

class AuditoriaController extends Controller
{

    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $usuarios = Usuario::select(\DB::raw("concat(nombres,' ',primerapellido,' ',segundoapellido) as label"), 'codusuario as value')->estado(\Config::get('dominios.ESTADOUSU.VALORES.Act'))->orderBy('nombres')->get();

        $usuarios->map(function ($obj) {
            $obj->img = route('krauff.getimage', ['id' => \Crypt::encrypt($obj->value), 'option' => \Crypt::encrypt(1), 'size' => 25]);
            return $obj;
        });

        //Pasar parametros a JavaScript
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Auditoria del sistema',
                'componente' => $this->componente('AUDIT_SIS'),
            ],
            'usuarios'   => $usuarios,
        ]);

        //Pasar Parametros al breadcrumbs
        View::share('breadcrumbs', [
            ['name' => 'Auditoria del sistema'],
        ]);

        return view('auditoria::auditoria.listar');
    }

    public function getAll(Request $request)
    {
        $fecha_desde = $request->fecha_desde;
        $fecha_hasta = $request->fecha_hasta;
        $modulo      = $request->modulo;
        if (array_key_exists("AUDIT_SIS_TIPO_TODO", session("permisos"))) {
            $codusuario = $request->codusuario;
        } else {
            $codusuario = \Auth::id();
        }
        $query = Activity::query();
        if (!empty($fecha_desde) && !empty($fecha_hasta)) {
            $query->whereBetween('created_at', [\Carbon::createFromFormat('d/m/Y', $fecha_desde)->format('Y-m-d 00:00:00'),
                \Carbon::createFromFormat('d/m/Y', $fecha_hasta)->format('Y-m-d 23:59:00'),
            ]);
        }
        if (!empty($codusuario)) {
            $query->where('causer_id', $codusuario);
        }
        if (!empty($modulo)) {
            $query->inLog($modulo);
        }
        return Datatables::of($query)
        ->addColumn('usuario', function (Activity $activity) {
                $codusuario = $activity->causer_id;
                $usuario    = Usuario::find($codusuario);
                if (!empty($usuario)) {
                    return '<img class="img-circle" src="' . $usuario->foto . '" alt="...">' . $usuario->nombres;
                }
                return 'No Registra';
        })->addColumn('opciones', function (Activity $activity) {
            $html = '' . ($this->verificarpermiso("AUDIT_SIS_DET") ? '<a href="javascript:void(0)" data-id="' . \Crypt::encrypt($activity->id) . '" data-toggle="modal" data-target="#detalleAuditoria" data-title="Detalles de auditoria" class="btn btn-primary btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Ver detalles"><i class="fa fa-eye"></i></a> ' : '');
            return $html;
        })->editColumn('created_at', function (Activity $activity) {
            return \Carbon::parse($activity->created_at)->format('d/m/Y h:i A');
        })->rawColumns(['usuario', 'opciones'])->make(true);
    }

    public function show($id)
    {
        $id          = \Crypt::decrypt($id);
        $activity    = Activity::find($id);
        $propiedades = json_decode($activity->properties, true);
        //Pasar parametros a JavaScript
        JavaScript::put([
            'activity'    => $activity,
            'propiedades' => $propiedades,
        ]);
        return view('auditoria::auditoria.detalles')->with('activity', $activity);
    }

    public function exportar(Request $request)
    {
        $fecha_desde = $request->fecha_desde;
        $fecha_hasta = $request->fecha_hasta;
        $modulo      = $request->modulo;
        $ext         = $request->ext;
        if (array_key_exists("AUDIT_SIS_TIPO_TODO", session("permisos"))) {
            $codusuario = $request->codusuario;
        } else {
            $codusuario = \Auth::id();
        }
        $query = Activity::query();
        if (!empty($fecha_desde) && !empty($fecha_hasta)) {
            $query->whereBetween('created_at', [\Carbon::createFromFormat('d/m/Y', $fecha_desde)->format('Y-m-d 00:00:00'),
                \Carbon::createFromFormat('d/m/Y', $fecha_hasta)->format('Y-m-d 23:59:00'),
            ]);
        }
        if (!empty($codusuario)) {
            $query->where('causer_id', $codusuario);
        }
        if (!empty($modulo)) {
            $query->inLog($modulo);
        }
        $results = $query->orderBy('created_at')->get();

        $name = "rep_audit.";
        switch ($ext) {
            case 'xlsx':
                $Arr   = [];
                $Arr[] = ['Modulo', 'Descripcion', 'Fecha'];
                foreach ($results as $value) {
                    $data = array_values(json_decode(json_encode($value), true));
                    unset($data[0]);
                    unset($data[7]);
                    unset($data[9]);
                    unset($data[3]);
                    unset($data[4]);
                    unset($data[5]);
                    unset($data[6]);
                    $Arr[] = $data;
                }
                ob_end_clean();
                ob_start();
                \Excel::create($name, function ($excel) use ($Arr) {
                    $excel->setTitle('Auditoria del sistema');
                    $excel->setCreator(config('app.name'))
                        ->setCompany(config('app.name'));
                    $excel->sheet('hoja1', function ($sheet) use ($Arr) {
                        $sheet->fromArray($Arr, null, 'A1', false, false);
                    });
                })->download($ext);
                break;
            default:
                abort(404);
                break;
        }
    }

}
