<?php

namespace Modules\Auditoria\Http\Controllers;

//Paquetes
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use JavaScript;
use Modules\Auditoria\Entities\Acceso;
use Modules\Auditoria\Entities\Usuario;
use ABCcomptencias\Traits\Permisos;

class AccesosController extends Controller
{

    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $usuarios = Usuario::select(\DB::raw("concat(nombres,' ',primerapellido,' ',segundoapellido) as label"), 'codusuario as value')->estado(\Config::get('dominios.ESTADOUSU.VALORES.Act'))->orderBy('nombres')->get();

        $usuarios->map(function ($obj) {
            $obj->img = route('krauff.getimage', ['id' => \Crypt::encrypt($obj->value), 'option' => \Crypt::encrypt(1), 'size' => 25]);
            return $obj;
        });

        //Pasar parametros a JavaScript
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Accesos al sistema',
                'componente' => $this->componente('AUDIT_SIS'),
            ],
            'usuarios'   => $usuarios,
        ]);

        //Pasar Parametros al breadcrumbs
        View::share('breadcrumbs', [
            ['name' => 'Accesos al sistema'],
        ]);

        return view('auditoria::accesos.listar');
    }

    public function getAll(Request $request)
    {
        $fecha_desde = $request->fecha_desde;
        $fecha_hasta = $request->fecha_hasta;
        $codusuario  = $request->codusuario;

        $query = Acceso::query();
        if (!empty($fecha_desde) && !empty($fecha_hasta)) {
            $query->whereBetween('created_at', [\Carbon::createFromFormat('d/m/Y', $fecha_desde)->format('Y-m-d 00:00:00'),
                \Carbon::createFromFormat('d/m/Y', $fecha_hasta)->format('Y-m-d 23:59:00'),
            ]);
        }

        if (!empty($codusuario)) {
            $query->where('codusuario', $codusuario);
        }

        return Datatables::of($query)
            ->addColumn('usuario', function (Acceso $row) {
                $codusuario = $row->codusuario;
                $usuario    = Usuario::find($codusuario);
                if (!empty($usuario)) {
                    return '<img class="img-circle" src="' . $usuario->foto . '" alt="...">' . $usuario->nombres;
                }
                return 'No Registra';
            })->editColumn('created_at', function (Acceso $row) {
            return \Carbon::parse($row->created_at)->format('d/m/Y h:i A');
        })->rawColumns(['usuario'])->make(true);
    }
}
