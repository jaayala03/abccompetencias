
$(document).ready(function () {
// Default datepicker (options)
    $('#fechanacimiento').flatpickr({
        "locale": "es",
    });


});

//******************************************************************************
//******************************************************************************
//EVENTO AJAX PARA EDITAR USUARIO
//******************************************************************************
$("#btn-editar").click(function (e) {
    e.preventDefault();
    var $formeditar = $("#frm_editar_perfil");
    if ($formeditar.valid()) {
        procesar_imagen({
            image: $(".image-crop > img"),
            form: $formeditar,
            hiddenfield: $("#imagenAdjunta")
        });
        dialogo_confirmacion_ajax({
            title: "Editar perfil",
            message: "¿Está seguro que desea editar?",
            labelconfirm: "Si / Editar",
            callbackfunction: function (data) {
                editar_miperfil(data);

            },
            data: $("#frm_editar_perfil").serialize()
        });
    }
});

function editar_miperfil(data) {
    var url = baseurl + '/krauff/updateperfil';
    call_ajax({
        url: url,
        data: data,
        callbackfunctionsuccess: function (data) {
            notifier.show(data.title, data.message, data.type, 5000);
            routie('/krauff/verperfil');
        }
    });
}


$(document).ready(function () {

    var $image = $(".image-crop > img");
    $($image).cropper({
        aspectRatio: '1',
        preview: ".img-preview",
        done: function (data) {
            // Output the result data for cropping image.
        }
    });

    var $inputImage = $("#FileInput");
    if (window.FileReader) {
        $inputImage.change(function () {
            var fileReader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function () {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }

    $("#zoomIn").click(function () {
        $image.cropper("zoom", 0.1);
    });

    $("#zoomOut").click(function () {
        $image.cropper("zoom", -0.1);
    });

    $("#rotateLeft").click(function () {
        $image.cropper("rotate", 45);
    });

    $("#rotateRight").click(function () {
        $image.cropper("rotate", -45);
    });

    $("#setDrag").click(function () {
        $image.cropper("setDragMode", "crop");
    });

});

//==========================================================
//validation
//==========================================================
var BlankonFormWizard = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormWizard.twitterBootstrapWizard();
        },
        // =========================================================================
        // TWITTER BOOTSTRAP WIZARD
        // =========================================================================
        twitterBootstrapWizard: function () {


            if ($('#validacionusuario').length) {
                var $validator = $("#frm_editar_perfil").validate({
                    rules: {
                        tipodoc: {
                            required: true
                        },
                        documento: {
                            required: true
                        },
                        nombres: {
                            required: true
                        },
                        primerapellido: {
                            required: true
                        },
                        correo: {
                            required: true,
                            email: true
                        },
                        codperfil: {
                            required: true
                        },
                        nombreusuario: {
                            required: true
                        },
                        password: {
                            required: false,
                            minlength: 8,
                            maxlength: 20
                        },
                        password2: {
                            required: false,
                            equalTo: "#password"
                        },
                        passwordantigua: {
                            required: false,
                            remote: {
                                url: baseurl + '/krauff/validarcontrasena',
                                type: 'POST',
                                data: {
                                    pass: function ()
                                    {
                                        return  $('#passwordantigua').val();
                                    }
                                },
                                delay: 2000,
                                message: 'La nueva clave no debe ser igual a la clave actual.'
                            }
                        }
                    },
                    messages: {
                        tipodoc: {
                            required: "Seleccione un tipo de documento."
                        },
                        documento: {
                            required: "Este campo es requerido."
                        },
                        nombres: {
                            required: "Este campo es requerido."
                        },
                        primerapellido: {
                            required: "Este campo es requerido."
                        },
                        correo: {
                            required: "Este campo es requerido.",
                            email: "Ingrese un correo valido."
                        },
                        codperfil: {
                            required: "Seleccione un perfil."
                        },
                        nombreusuario: {
                            required: "Este campo es requerido."
                        },
                        password: {
                            required: "Este campo es requerido.",
                            minlength: "La contraseña debe tener minimo 8 carcteres.",
                            maxlength: "La contraseña debe tener maximo 20 carcteres."
                        },
                        password2: {
                            required: "Este campo es requerido.",
                            equalTo: "Las contraseñas no coinciden."
                        },
                        passwordantigua: {
                            required: "Este campo es requerido.",
                        }
                    },
                    highlight: function (element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function (element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }

                });

                $('#validacionusuario').bootstrapWizard({
                    'nextSelector': '.next',
                    'previousSelector': '.previous',
                    'onNext': function (tab, navigation, index) {

                        var $valid = $("#frm_editar_perfil").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                        var $active = navigation.find('li');
                        $active.addClass('success-step');
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacionusuario').find('.progress-bar').css('width', $percent + '%');
                    },
                    onPrevious: function (tab, navigation, index) {

                        if (index == -1) {
                            return false;
                        }

                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacionusuario').find('.progress-bar').css('width', $percent + '%');
                    },
                    onTabClick: function (tab, navigation, index) {
                        var $valid = $("#frm_editar_perfil").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    },
                    onTabShow: function (tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index + 1;
                        var $percent = ($current / $total) * 100;
                        jQuery('#validacionusuario').find('.progress-bar').css('width', $percent + '%');
                    }
                });
            }
        }

    };

}();

// Call main app init
BlankonFormWizard.init();

//******************************************************************************
//Busqueda de ubicaciones residencia
//******************************************************************************
$(document).ready(function() {
    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('nombre'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: baseurl + '/krauff/ubicaciones/buscarUbicacion?q=%QUERY'
        }
    });
    engine.initialize();
    $('#codubicacion').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        items: 5
    }, {
        name: 'codubicacion',
        display: function(item) {
            return item.nombre + ' - ' + item.padre.nombre
        },
        source: engine.ttAdapter(),
        templates: {
            empty: ['<div class="list-group search-results-dropdown"><div class="list-group-item">sin resultados</div></div>'],
            suggestion: function(data) {
                return '<div>' + data.nombre + ' - ' + data.padre.nombre + '</a>';
            }
        }
    }).on('typeahead:selected', function(event, data) {
        $("#codubicacion_value").val(data.codubicacion);
        $("#codubicacion").prop("disabled", "disabled");
    });
    $(".remove-ubicacion").click(function(e) {
        $('#codubicacion').typeahead('val', '');
        $("#codubicacion_value").val('');
        $("#codubicacion").removeAttr("disabled");
    });
});
//******************************************************************************


//******************************************************************************
//Busqueda de ubicaciones nacimiento
//******************************************************************************
$(document).ready(function() {
    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('nombre'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: baseurl + '/krauff/ubicaciones/buscarUbicacion?q=%QUERY'
        }
    });
    engine.initialize();
    $('#codubicacionnacimiento').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        items: 5
    }, {
        name: 'codubicacion',
        display: function(item) {
            return item.nombre + ' - ' + item.padre.nombre
        },
        source: engine.ttAdapter(),
        templates: {
            empty: ['<div class="list-group search-results-dropdown"><div class="list-group-item">sin resultados</div></div>'],
            suggestion: function(data) {
                return '<div>' + data.nombre + ' - ' + data.padre.nombre + '</a>';
            }
        }
    }).on('typeahead:selected', function(event, data) {
        $("#codubicacionnacimiento_value").val(data.codubicacion);
        $("#codubicacionnacimiento").prop("disabled", "disabled");
    });
    $(".remove-nacimiento").click(function(e) {
        $('#codubicacionnacimiento').typeahead('val', '');
        $("#codubicacionnacimiento_value").val('');
        $("#codubicacionnacimiento").removeAttr("disabled");
    });
});
//******************************************************************************


