var verperfil = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            verperfil.additionalXeditable();
            verperfil.nombres();
            verperfil.primerapellido();
            verperfil.segundoapellido();
            verperfil.email();
            verperfil.direccion();
            verperfil.telefono();
            verperfil.celular();
            verperfil.documento();
        },
      
        // =========================================================================
        // ADDITIONAL X-EDITABLE
        // =========================================================================
        additionalXeditable: function () {
            // Defaults
            $.fn.editable.defaults.url = '/post';
            // Enable or Disable
            $('#enable').click(function () {
                $('#user .editable').editable('toggleDisabled');
            });
        },
        // =========================================================================
        // USERNAME EDITABLE
        // =========================================================================
        nombres: function () {
            $('#nombres').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                name: 'nombres',
                title: 'Nombres',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        primerapellido: function () {
            $('#primerapellido').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'primerapellido',
                title: 'Primer apellido',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        segundoapellido: function () {
            $('#segundoapellido').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'segundoapellido',
                title: 'Primer apellido',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        documento: function () {
            $('#documento').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'documento',
                title: 'Documento',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        email: function () {
            $('#email').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'email',
                title: 'Email',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        direccion: function () {
            $('#direccion').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'direccion',
                title: 'Direccion',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        telefono: function () {
            $('#telefono').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'telefono',
                title: 'Telefono',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        },
        celular: function () {
            $('#celular').editable({
                url: baseurl + "/krauff/editarperfilajax",
                type: 'text',
                emptytext: 'No registra',
                name: 'celular',
                title: 'Celular',
                success: function (response, newValue) {
                    notifier.show("Exito!", "Operacion realizada correctamente", 'success', 5000);
                },
                validate: function (value) {
                    if ($.trim(value) == '')
                        return 'Requerido';
                }
            });
        }

    };

}();

// Call main app init
verperfil.init();