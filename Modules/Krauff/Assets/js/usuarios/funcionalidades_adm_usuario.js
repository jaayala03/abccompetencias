var selected = null;

function cargar(usuario) {
    var url = baseurl + '/krauff/usuarios/cargarfunc';
    
    var parametros = {
        id: usuario
    };

    $("#jstree").jstree({
        "plugins": ['wholerow', "types", "checkbox", "themes", "json_data", "ui"],
        'core': {
            'data': {
                "type": 'post',
                "url": url,
                "data": parametros,
                "plugins": ["wholerow", "checkbox"],
                "dataType": "json"
            }
        },
        "types": {
            "default": {
                "icon": "glyphicon glyphicon-user"
            },
            "demo": {
                "icon": "glyphicon glyphicon-ok"
            }
        },
        "checkbox": {
            "keep_selected_style": false
        }
    });

    $('#jstree').on("changed.jstree", function (e, data) {
        //console.log(data.selected);
    });

    $('#save').on('click', function () {
        //$('#jstree').jstree(true).select_node('child_node_1');
        //$('#jstree').jstree('select_node', 'child_node_1');
        //$.jstree.reference('#jstree').select_node('child_node_1');

        selected = $("#jstree").jstree('get_selected');

    });
}



function guardar(codusuario) {
    var urlguardar = baseurl + '/krauff/usuarios/guardarfunc';
    var parametros = {
        id: codusuario,
        codfunc: selected
    };
    
    $.ajax({
        data: parametros,
        url: urlguardar,
        type: 'post',
        beforeSend: function () {
            $("#save").html("Procesando...");
        },
        success: function (response) {
            var filas = response;
            if (filas.resp === "1f") {
                notifier.show('Error', 'No Existen Funcionalidades Seleccionadas!', 'error', '', 5000);
            } else {               
                window.parent.$('#VentanaFlotanteFunc').modal('toggle');
                window.parent.notifier.show('Exito', 'Operación Realizada Correctamente!', 'success', '', 5000);
            }
            $("#save").html("Guardar");
            $("#save").removeAttr("disabled");
        },
        error: function (response) {
            console.log("Error en el proceso " + response);
        }
    });
}




