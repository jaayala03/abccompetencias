//******************************************************************************
//Funcion para iframes responsives
//******************************************************************************
function adjustIframes() {
    $('iframe').each(function() {
        var $this = $(this),
            proportion = $this.data('proportion'),
            w = $this.attr('width'),
            actual_w = $this.width();
        if (!proportion) {
            proportion = $this.attr('height') / w;
            $this.data('proportion', proportion);
        }
        if (actual_w != w) {
            $this.css('height', Math.round(actual_w * proportion) + 'px');
        }
    });
}
$(window).on('resize load', adjustIframes);
//******************************************************************************
//******************************************************************************
//Funcion para mostrar notifier
//******************************************************************************
function show_message(data) {
    notifier.show(data.title, data.message, data.type, 5000);
}
//******************************************************************************
//******************************************************************************
//Funcion para peticiones ajax
//******************************************************************************
function call_ajax(options) {
    var type = (typeof options.type !== 'undefined') ? options.type : 'POST';
    $.ajax({
        type: type,
        url: options.url,
        data: options.data,
        success: function(data) {
            options.callbackfunctionsuccess(data);
        }
    });
}
//******************************************************************************
//******************************************************************************
//Funcion para procesar una imagen antes de enviar el formulario
//******************************************************************************
function procesar_imagen(options) {
    //$(".image-crop > img")
    var $image = options.image;
    var obj = $("#" + options.form.id);
    $(options.hiddenfield).val($image.cropper("getDataURL"));
    if (obj.submit()) {
        return true;
    }
    return false;
}
//******************************************************************************
//******************************************************************************
//Funcion para mensaje de confirmacion antes de enviar un formulario
//******************************************************************************
function dialogo_confirmacion(options) {
    var labelconfirm = (typeof options.labelconfirm !== 'undefined') ? options.labelconfirm : 'Si';
    var labelcancel = (typeof options.labelcancel !== 'undefined') ? options.labelcancel : 'No';
    var className = (typeof options.className !== 'undefined') ? options.className : 'modal-danger modal-center';
    bootbox.confirm({
        title: options.title,
        message: options.message,
        className: className,
        buttons: {
            confirm: {
                label: labelconfirm,
                className: 'btn-success'
            },
            cancel: {
                label: labelcancel,
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            if (result) {
                $(options.form).submit();
            }
        }
    });
}
//******************************************************************************
//******************************************************************************
//Funcion para mensaje de confirmacion antes de enviar una peticion ajax
//******************************************************************************
function dialogo_confirmacion_ajax(options) {
    var labelconfirm = (typeof options.labelconfirm !== 'undefined') ? options.labelconfirm : 'Si';
    var labelcancel = (typeof options.labelcancel !== 'undefined') ? options.labelcancel : 'No';
    var className = (typeof options.className !== 'undefined') ? options.className : 'modal-danger modal-center';
    bootbox.confirm({
        title: options.title,
        message: options.message,
        className: className,
        buttons: {
            confirm: {
                label: labelconfirm,
                className: 'btn-success'
            },
            cancel: {
                label: labelcancel,
                className: 'btn-danger'
            }
        },
        callback: function(result) {
            (result) ? options.callbackfunction(options.data): (typeof options.callbackfunctioncancel !== 'undefined') ? options.callbackfunctioncancel(options.data) : "";
        }
    });
}
//******************************************************************************
//******************************************************************************
//Carga de contenido por ajax
//******************************************************************************
$(document).ready(function() {
    if(!window.nomenu) {
        routie('*', function() {
            var url = window.location.href;
            var p = url.indexOf('#');
            if (p > -1) {
                var controllerAction = url.substr(url.indexOf('#') + 1);
                var pos = controllerAction.indexOf('*');
                var menu = controllerAction;
                if (pos > -1) menu = controllerAction.substr(0, pos);
                //activeMenu("nav_" + menu.replace('/', '_'));
                if (controllerAction.replace('*', '/') !== "") {
                    ajaxLoad(controllerAction.replace('*', '/'));
                }
            } else {
                //activeMenu("Inicio");
                //ajaxLoad('/home');
            }
        });
    }
});

function procesarComponente(componente) {
    $('ul.sidebar-menu li').removeClass('active');
    if (componente.indexOf("-") >= 0) {
        Arrcomponenets = componente.split('-');
        for (var i = 0; i < Arrcomponenets.length; i++) {
            activeMenu(Arrcomponenets[i]);
        }
    } else {
        activeMenu(componente);
    }
}

function activeMenu(componente) {
    var $results = $('.sidebar-menu').find('*').filter(function() {
        return $(this).data("func") === componente;
    });
    $results.each(function(i, obj) {
        if (this.tagName != 'LI') {
            $(this).closest('.submenu').addClass('active');
            $(this).closest('li').addClass('active');
        } else {
            $(this).addClass('active');
        }
    });
}

function ajaxLoad(filename, content) {
    content = typeof content !== 'undefined' ? content : 'page-content';
    $('.loading').show();
    $("#" + content).hide();
    $.ajax({
        type: "GET",
        url: baseurl + filename,
        contentType: false,
        success: function(data) {
            if (typeof data.url !== 'undefined') {
                routie(data.url);
            } else if (typeof data.data !== 'undefined' && typeof data.data.url !== 'undefined') {
                routie(data.data.url);
            } else {
                $("#" + content).show();
                $("#" + content).html(data);
                procesarComponente(parametros.componente);
                document.title = parametros.meta_title;
                $('.loading').hide();
            }
            if (typeof data.message !== 'undefined') {
                notifier.show(data.title, data.message, data.type, 5000);
            } else if (typeof data.data !== 'undefined' && typeof data.data.message !== 'undefined') {
                notifier.show(data.data.title, data.data.message, data.data.type, 5000);
            }
        },
        error: function(xhr, status, error) {
            //alert(xhr.responseText);
            //notifier.show("Ups! Algo anda mal", error, 'danger', 5000);
            $("#" + content).show();
            $("#" + content).html(xhr.responseText);
            $('.loading').hide();
        }
    });
}
//******************************************************************************
//******************************************************************************
//******************************************************************************
//Paginacion por ajax
//******************************************************************************
$(document).ready(function() {
    $(document).on('click', '.pagination-ajax a', function(e) {
        e.preventDefault();
        ajaxLoadPaginator($(this).attr('href').split('page=')[1]);
    });
});

function ajaxLoadPaginator(page) {
    var url = window.location.hash.replace('#', '');
    $.ajax({
        type: "GET",
        url: baseurl + url + '?page=' + page,
        contentType: false,
        success: function(data) {
            $("#page-content").html(data);
        },
        error: function(xhr, status, error) {
            //alert(xhr.responseText);
            notifier.show("Ups! Algo anda mal", error, 'danger', 5000);
        }
    });
}
//******************************************************************************
//Configuracion del token para peticiones ajax
//******************************************************************************
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//******************************************************************************
//******************************************************************************
//Funcion para cargar modales con iframe
//******************************************************************************
$(document).on('shown.bs.modal', '.modal-iframe', function(e) {
    var modal = $(this);
    var iframe = modal.find('iframe');
    var title = e.relatedTarget.dataset['title'];
    var param = e.relatedTarget.dataset['id'];
    var src = modal.find('iframe').attr('data-iframe-src');
    var ruta = src;
    if (param) {
        ruta += "/" + param;
    }
    iframe.attr('src', ruta);
    modal.find(".modal-title").html(title);
});
$('.modal-iframe').on('hidden.bs.modal', function(e) {
    $(this).find('iframe').attr('src', '');
});
//******************************************************************************
//Warnings
//******************************************************************************
$(document).ready(function() {
    var warnings = window.session.warnings;
    var messages = [];
    if (warnings && warnings.length > 0) {
        warnings.forEach(function(entry) {
            messages.push({
                title: entry.title,
                text: entry.text,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#ff0000",
                confirmButtonText: "Entendido",
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                focusConfirm: false,
                showCloseButton: false
            });
        });
    }
    setTimeout(function() {
        swal.queue(messages);
    }, 2000);
});