<?php

//**********************************************************************
//Grupo de rutas principal para acceder al modulo
//Middleware funcionalidades:IDENTIFICACOR (tabla funcionalidades)
//**********************************************************************

Route::group(['middleware' => 'web', 'prefix' => 'krauff', 'namespace' => 'Modules\Krauff\Http\Controllers'], function () {

    //Ruta para mostrar la informacion del usuario
    Route::get('verperfil', 'UsuariosController@verperfil');

    //Ruta para mostrar una imagen
    Route::get('getimage/{id}', [
        'uses' => 'KrauffController@getimage',
        'as'   => 'krauff.getimage',
    ]);

    //Ruta para mostrar un archivo
    Route::get('getfile/{id}', [
        'uses' => 'KrauffController@getfile',
        'as'   => 'krauff.getfile',
    ]);

    //Ruta para mostrar un archivo
    Route::get('verpdf', [
        'uses' => 'KrauffController@verpdf',
        'as'   => 'krauff.verpdf',
    ]);

    //Ruta para Editar la informacion del usuario
    Route::get('editarperfil', [
        'uses' => 'UsuariosController@editarperfil',
        'as'   => 'krauff.editarperfil',
    ]);

    //Ruta leer una notificacion
    Route::get('leernotificacion/{id}', [
        'uses' => 'UsuariosController@leernotificacion',
        'as'   => 'krauff.leernotificacion',
    ]);

    //Ruta para Editar la informacion del perfil
    Route::post('updateperfil', [
        'uses' => 'UsuariosController@updateperfil',
        'as'   => 'krauff.updateperfil',
    ]);

    //Ruta para Eliminar notificaciones de un usuario
    Route::post('eliminarnotificaciones', 'UsuariosController@eliminarnotificaciones');

    //Ruta para validar contraseñas
    Route::post('validarcontrasena', 'UsuariosController@validarcontrasena');

    //Ruta para editar el perfil por ajax
    Route::post('editarperfilajax', 'UsuariosController@editarperfilajax');

    //**********************************************************************
    //Grupo de rutas para usuarios
    //**********************************************************************
    Route::group(['middleware' => ['funcionalidades:MNTO_USU'], 'prefix' => 'usuarios'], function () {
        //Ruta para listar
        Route::get('/', 'UsuariosController@listar');
        //Ruta para traer datos datatables
        Route::post('TodosDatos', 'UsuariosController@TodosDatos');
        //Ruta para crear
        Route::post('crear', 'UsuariosController@store');
        //Ruta para eliminar
        Route::post('eliminar', 'UsuariosController@destroy');
        //Ruta para editar
        Route::get('editar/{id}', [
            'uses' => 'UsuariosController@editar',
            'as'   => 'krauff.usuarios.editar',
        ]);
        Route::post('update', [
            'uses' => 'UsuariosController@update',
            'as'   => 'krauff.usuarios.update',
        ]);
        //Ruta para funcionalidades
        Route::get('funcionalidades/{id}', 'UsuariosController@ShowFunc');
        //Ruta para cargar las funcionalidades de un perfil
        Route::post('cargarfunc', 'UsuariosController@LoadFunc');
        //Ruta para guardar las funcionalidades de un usuario
        Route::post('guardarfunc', 'UsuariosController@SaveFunc');
    });

    //**********************************************************************
    //Grupo de rutas para perfiles
    //**********************************************************************
    Route::group(['middleware' => ['funcionalidades:MNTO_PER'], 'prefix' => 'perfiles'], function () {
        //Ruta para listar
        Route::get('/', 'PerfilesController@listar');
        //Ruta para traer datos datatables
        Route::post('TodosDatos', 'PerfilesController@TodosDatos');
        //Ruta para crear
        Route::post('crear', 'PerfilesController@store');
        //Ruta para eliminar
        Route::post('eliminar', 'PerfilesController@destroy');
        //Ruta para editar
        Route::post('editar', 'PerfilesController@update');
        //Ruta para funcionalidades
        Route::get('funcionalidades/{id}', 'PerfilesController@ShowFunc');
        //Ruta para cargar las funcionalidades de un perfil
        Route::post('cargarfunc', 'PerfilesController@LoadFunc');
        //Ruta para guardar las funcionalidades de un perfil
        Route::post('guardarfunc', 'PerfilesController@SaveFunc');
        //para cargar la vista de usuarios por perfil
        Route::get('cargarusuariosperfil/{codperfilcifrado}', [
            'uses' => 'PerfilesController@cargarusuariosperfil',
            'as'   => 'krauff.perfiles.cargarusuariosperfil',
        ]);
    });

//**********************************************************************
    //Grupo de rutas para ubicaciones
    //**********************************************************************
    Route::group(['middleware' => ['funcionalidades:MNTO_USU'], 'prefix' => 'ubicaciones'], function () {
        //Ruta para traer datos datatables
        Route::get('buscarUbicacion', 'UbicacionesController@buscarUbicacion');

    });

});
