<?php

namespace Modules\Krauff\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use ABCcomptencias\User;

class ValidarFuncionalidades
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$params)
    {
        if ($this->user->tiene_func($params)) {
            if ($request->ajax()) {
                return response()->view('errors.403', [], 403);
            } else {
                abort(403, 'Accion no autorizada');
            }
        }
        return $next($request);
    }

}
