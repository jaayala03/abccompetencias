<?php

namespace Modules\Krauff\Http\ViewComposers;

use Auth;
use Illuminate\View\View;
use Modules\Krauff\Entities\Notificacion;

class NotificacionesComposer
{

    public function compose(View $view)
    {
        $notificaciones = Notificacion::WhereUsuario(Auth::user()->codusuario)->get();
        $noleidas       = Notificacion::WhereUsuario(Auth::user()->codusuario)->Noleidas()->count();

        $view->with('notificaciones', $notificaciones)->with('cantNotNoleidas', $noleidas);
    }

}
