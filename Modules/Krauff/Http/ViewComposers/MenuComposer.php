<?php

namespace Modules\Krauff\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class MenuComposer
{

    public function compose(View $view)
    {
        if (isset($view['parametros']['componente'])) {
            $componente = $view['parametros']['componente'];
        } else {
            $componente = "";
        }
        $html = $this->buildMenu(1, url('/home'), $componente);
        $view->with('menu', $html);
    }

    public function buildMenu($parent, $mainPage, $components)
    {
        $nombreusuario = Auth::user()->nombres;
        $perfil        = session('perfil');
        $img_usuario   = session('imagen_perfil');

        $active     = "";
        $components = explode('-', $components);

        if (in_array('Inicio', $components)) {
            $active = " class=\"active\"";
        }
        $html = "<aside id=\"sidebar-left\" class=\"sidebar-rounded sidebar-light\">";
        $html .= "<div class=\"sidebar-content\">
                    <div class=\"media\">
                            <a class=\"pull-left has-notif avatar\" href=\"#/krauff/verperfil\">
                                <img src=\"{$img_usuario}\" alt=\"admin\">
                            </a>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\"><span>{$nombreusuario}</span></h4>
                            <small>{$perfil}</small>
                        </div>
                    </div>
                  </div>";
        $html .= "<ul class=\"sidebar-menu\">";

        $html .= "<li{$active} >\n";
        $html .= "<a href=\"{$mainPage}\">
                    <span class=\"icon\"><i class=\"fa fa-home\"></i></span>
                    <span class=\"text\">Inicio</span>
                  </a>\n";
        $html .= "</li>\n";

        $arrayData = session('funcionalidades');
        foreach ($arrayData as $arr) {
            if ($arr->tipo == 'MENU') {
                if ($arr->codpadre == $parent) {
                    $html .= "<li class=\"sidebar-category\">";
                    $html .= "<span>{$arr->nombre}</span>";
                    $html .= "<span class=\"pull-right\"><i class=\"$arr->icono\"></i></span>";
                    $html .= "</li>\n";
                    $html .= $this->MenuLevel($arrayData, $arr->codfunc, $components);
                }
            }
        }

        $html .= "</ul>";

        $html .= '<!-- Start left navigation - footer -->
                <div class="sidebar-footer hidden-xs hidden-sm hidden-md">
                    <!--<a id="setting" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>-->
                    <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Pantalla Completa"><i class="fa fa-desktop"></i></a>
                    <!--<a id="lock-screen" data-url="page-signin.html" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Bloquear"><i class="fa fa-lock"></i></a>-->
                    <a id="logout" data-url="#" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Cerrar Sesion"><i class="fa fa-power-off"></i></a>
                </div><!-- /.sidebar-footer -->
                <!--/ End left navigation - footer -->';

        $html .= "</aside>";
        return $html;
    }

    private function MenuLevel($arrayData, $codfunc, $components)
    {
        $urlhome = url('/home');
        $html    = "";
        foreach ($arrayData as $arr) {
            if ($arr->tipo == 'MENU') {
                if ($arr->codpadre === $codfunc) {
                    if ($arr->hijostipomenu > 0) {

                        $active = " class=\"submenu\"";
                        if (in_array($arr->nombre, $components)) {
                            $active = " class=\"submenu active\"";
                        }
                        $html .= "<li {$active}>\n";
                        $html .= "<a href=\"javascript:void(0);\">";
                        $html .= "<span class=\"icon\"><i class=\"{$arr->icono}\"></i></span>";
                        $html .= "<span class=\"text\">{$arr->nombre}</span>";
                        $html .= "<span class=\"plus\"></span>";
                        $html .= "</a>";
                        $html .= $this->secondLevel($arrayData, $arr->codfunc, $components);
                    } else {
                        $active = "";
                        if (in_array($arr->nombre, $components)) {
                            $active = " class=\"active\"";
                        }
                        $html .= "<li {$active}>\n";
                        $html .= "<a data-func=\"{$arr->identificador}\" href=\"{$urlhome}#{$arr->urlpagina}\">
                    <span class=\"icon\"><i class=\"{$arr->icono}\"></i></span>
                    <span class=\"text\">{$arr->nombre}</span>
                    </a>\n";
                    }
                    $html .= "</li>\n";
                }
            }
        }
        return $html;
    }

    private function secondLevel($arrayData, $codfunc, $components)
    {
        $urlhome = url('/home');
        $html    = "";
        $html .= "<ul>\n";
        foreach ($arrayData as $arr) {
            if ($arr->tipo == 'MENU') {
                if ($arr->codpadre === $codfunc) {
                    if ($arr->hijostipomenu > 0) {
                        $active = " class=\"submenu\"";
                        if (in_array($arr->nombre, $components)) {
                            $active = " class=\"submenu active\"";
                        }
                        $html .= "<li {$active}>\n";
                        $html .= "<a href=\"javascript:void(0);\">";
                        $html .= "<span class=\"text\" data-func=\"{$arr->identificador}\">{$arr->nombre}</span>";
                        $html .= "<span class=\"arrow\"></span>";
                        $html .= "</a>";
                        $html .= $this->secondLevel($arrayData, $arr->codfunc, $components);
                    } else {
                        $active = "";
                        if (in_array($arr->nombre, $components)) {
                            $active = " class=\"active\"";
                        }
                        $html .= "<li {$active}>";
                        $html .= "<a data-func=\"{$arr->identificador}\" href=\"{$urlhome}#{$arr->urlpagina}\">{$arr->nombre}</a>";
                        $html .= "</li>\n";
                    }
                }
            }
        }
        $html .= "</ul>\n";
        return $html;
    }

}
