<?php

namespace Modules\Krauff\Http\Controllers;

#**********************************************************************
#Use Necesarios
#**********************************************************************

use Crypt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

#Modelos
use Illuminate\Routing\Controller;

#Paquetess
use Intervention\Image\Facades\Image;
use Modules\Krauff\Entities\Usuario;
use Modules\Outside\Entities\Imagen_fachada;
use Modules\Outside\Entities\Noticia;
use Modules\Krauff\Entities\Seguimientocomparendo;
use Modules\Krauff\Entities\Norma;

#**********************************************************************

class KrauffController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //**********************************************************************
    //Funcion para obtener imagenes
    //**********************************************************************
    public function getimage(Request $request)
    {
        $option = Crypt::decrypt($request->get('option'));
        $size   = $request->get('size');
        switch ($option) {
            //Para obtener la imagen de perfil de un usuario
            case 1:
                $codusuario  = Crypt::decrypt($request->id);
                $usuario     = Usuario::find($codusuario);
                $img         = (!empty($usuario->imagencodificada)) ? $usuario->imagencodificada : \Config::get('dominios.DEFAULT_IMG_USER');
                $subruta     = \Config::get('paths.IMAGENES_PERFIL') . $img;
                $ruta_nombre = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;

            //Para obtener la imagen de la fachada
            case 2:
                $codimagenfachada  = Crypt::decrypt($request->id);
                $imagen_fachada     = Imagen_fachada::find($codimagenfachada);
                $img         = (!empty($imagen_fachada->nombre)) ? $imagen_fachada->nombre : \Config::get('dominios.DEFAULT_IMG_FACHADA');
                $subruta     = \Config::get('paths.IMAGENES_FACHADA') . $img;
                $ruta_nombre = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;

                            //Para obtener la imagen de la noticia
            case 3:
                $codnoticia  = Crypt::decrypt($request->id);
                $noticia     = Noticia::find($codnoticia);
                $img         = (!empty($noticia->imagen)) ? $noticia->imagen : \Config::get('dominios.DEFAULT_IMG_NOTICIA');
                $subruta     = \Config::get('paths.IMAGENES_NOTICIA') . $img;
                $ruta_nombre = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;
        }

        if (!empty($size)) {
            
            return Image::make($ruta_nombre)->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            })->response();
        }

        $response = Image::make($ruta_nombre)->response();
        
        return $response;
    }

    //**********************************************************************
    //Funcion para obtener archivos
    //**********************************************************************
    public function getfile(Request $request)
    {
        $option = \Crypt::decrypt($request->get('option'));
        $type   = \Crypt::decrypt($request->get('type'));

        switch ($option) {
            //Para obtener los archivos adjuntos del seguimiento de un comparendo
            case 1:
                $id             = \Crypt::decrypt($request->id);
                $archivoadjunto = Norma::findOrFail($id);
                $nombrearchivo  = $archivoadjunto->archivo;
                $subruta        = \Config::get('paths.DOCUMENTOS_NORMAS') . $archivoadjunto->archivo;
                $ruta_nombre    = \Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                break;
        }

        //Ver
        if ($type == 1) {
            return response()->file($ruta_nombre, [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $nombrearchivo . '"',
                'Cache-Control'       => 'private, max-age=0, must-revalidate',
            ]);
        }else{
            //Descargar
            return response()->download($ruta_nombre);
        }
    }

    public function verpdf()
    {

        return view('krauff::krauff.verpdf');

    }

}
