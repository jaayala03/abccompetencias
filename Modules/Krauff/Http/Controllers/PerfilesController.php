<?php

namespace Modules\Krauff\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

//Modelos
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use JavaScript;
//Paquetes
use Modules\Krauff\Entities\Funcionalidad;
use Modules\Krauff\Entities\Paginainicio;
use Modules\Krauff\Entities\Perfil;
//permisos trait
use ABCcomptencias\Traits\Permisos;

//**********************************************************************

class PerfilesController extends Controller
{

    //permisos trait
    use Permisos;

    protected $funcionalidades;

    public function __construct(Funcionalidad $obj)
    {
        $this->funcionalidades = $obj;
        $this->middleware('auth');

    }

    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Perfiles',
                'componente' => $this->componente('MNTO_PER'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Perfiles'],
        ]);
        $paginasinicio = Paginainicio::orderby('nombre', 'ASC')->pluck('nombre', 'codpaginainicio')->all();

        return view('krauff::perfiles.listar')->with('paginasinicio', $paginasinicio);
    }

    public function cargarusuariosperfil($codperfilcifrado)
    {
        $perfil = Perfil::findOrFail(\Crypt::decrypt($codperfilcifrado));

        JavaScript::put([
            'parametros'       => [
                'meta_title' => 'Usuarios: ' . $perfil->nombreperfil,
                'componente' => $this->componente('MNTO_PER'),
            ],
            'codperfilcifrado' => $codperfilcifrado,
        ]);

        View::share('breadcrumbs', [
            ['url' => url('/krauff/perfiles'), 'name' => 'Perfiles'],
            ['name' => 'Usuarios'],
        ]);

        $perfiles = Perfil::orderby('nombreperfil', 'ASC')->pluck('nombreperfil', 'codperfil')->all();

        return view('krauff::perfiles.usuariosperfil')
            ->with('perfiles', $perfiles)
            ->with('perfil', $perfil);
    }

    public function TodosDatos()
    {
        $model = Perfil::query();

        return Datatables::eloquent($model)->addColumn('idcifrado', function (Perfil $perfil) {
            return Crypt::encrypt($perfil->codperfil);
        })->addColumn('horario', function (Perfil $perfil) {

            $horario = $perfil->hinicio . ' - ' . $perfil->hfin;

            return !empty(trim($horario, ' - ')) ? trim($horario, ' - ') : 'Sin Restricción';
        })->editColumn('restriccion_ip', function (Perfil $perfil) {
            $badge = $perfil->restriccion_ip == 1 ? "badge-success" : "badge-danger";
            return !empty($perfil->restriccion_ip) ? '<span class="badge rounded ' . $badge . '">' . \Config::get('dominios.DOMINIO_SI_NO.TXT.' . $perfil->restriccion_ip) . '</span>' : 'No Registra';
        })->addColumn('usuarios', function (Perfil $perfil) {

            $url = \route('krauff.perfiles.cargarusuariosperfil', [\Crypt::encrypt($perfil->codperfil)], false);

            return '<a target="_blank" href="#' . $url . '"><span style="cursor: pointer;" class="badge circle badge-primary">' . $perfil->usuarios()->count() . '</span></a>';
        })->addColumn('opciones', function (Perfil $perfil) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($this->verificarpermiso("MNTO_PER_FUNC") ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#VentanaFlotanteFunc" class="btn btn-primary btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Funcionalidades" data-id="' . \Crypt::encrypt($perfil->codperfil) . '" data-title="Funcionalidades del perfil: ' . $perfil->nombreperfil . '"><i class="fa fa-users"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("MNTO_PER_EDI") ? '<a href="javascript:void(0)" data-id="' . \Crypt::encrypt($perfil->codperfil) . '" data-perfil="' . $perfil->nombreperfil . '" data-hinicio="' . $perfil->hinicio . '" data-hfin="' . $perfil->hfin . '" data-restriccion_ip="' . $perfil->restriccion_ip . '" data-codpaginainicio="' . $perfil->codpaginainicio . '" data-toggle="modal" data-target="#PerfilesEditar" class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar" href="' . url("/") . '"/krauff/perfiles/"' . $perfil->codperfil . '"><i class="fa fa-pencil"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("MNTO_PER_ELI") && $perfil->usuarios()->count() == 0 ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminarperfil tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . $perfil->codperfil . '"><i class="fa fa-times"></i></a>' : '');

            return $html;
        })->rawColumns(['opciones', 'horario', 'restriccion_ip', 'usuarios'])->make(true);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('krauff::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $perfiles = new Perfil($request->all());
        $status   = $perfiles->save();
        return response()->status($status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('krauff::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('krauff::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $perfil = Perfil::find(Crypt::decrypt($request->codperfil));
        $perfil->fill($request->all());
        $status = $perfil->save();
        return response()->status($status);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $perfiles = Perfil::findOrFail($request->id);
        $perfiles->delete();

        if ($request->ajax()) {
            return response()->json([
                'message' => 'Perfil eliminado',
                'title'   => 'Exito',
            ]);
        }
    }

    public function ShowFunc($id)
    {
        $codperfil = Crypt::decrypt($id);
        $perfil    = Perfil::find($codperfil);
        return view('krauff::perfiles.funcionalidades')->with('perfil', $perfil);
    }

    public function LoadFunc(Request $request)
    {
        //**********************************************************************
        //Manejo de datos
        //**********************************************************************
        $codperfil = $request->id;
        $nodo      = 1; //Este es el primer nodo del arbol de funcionalidades
        //Este arreglo se usa para seleccionar casillas por defecto
        $seleccionados = Perfil::find($codperfil)->funcionalidades;

        $i   = 0;
        $arr = array();
        //**********************************************************************
        //Obtener las funcionalidades del perfil
        //**********************************************************************
        foreach ($seleccionados as $campo) {
            if (!$this->funcionalidades->verificar_hijos($campo->codfunc)) {
                $i++;
                $arr[$i] = $campo->codfunc;
            }
        }

        $arreglo = $this->funcionalidades->obtener_nodo_perfiles($nodo, $arr);
        //**********************************************************************
        return response()->json($arreglo);
    }

    public function SaveFunc(Request $request)
    {
        //**********************************************************************
        //Manejo de datos
        //**********************************************************************
        $cod_perfil       = $request->id;
        $cod_funcionalida = $request->codfunc;

        if ($cod_funcionalida != 0) {
            $perfil = Perfil::findOrFail($cod_perfil);
            //Eliminar todas las funcionalidades
            $perfil->funcionalidades()->detach();
            //Inserta la funcionalidad principal
            $perfil->funcionalidades()->attach(1);

            foreach ($cod_funcionalida as $valor) {
                //Busqueda del padre
                $padre_func = Funcionalidad::findOrFail($valor)->codpadre;
                //Verificar si el padre existe de lo contrario insertarlo
                $this->funcionalidades->verificar_padre($cod_perfil, $padre_func);

                if (!$this->funcionalidades->verificar_hijos($valor)) {
                    //Insertar la funcionalidad
                    $perfil->funcionalidades()->attach($valor);
                }
            }
        } else {
            return response()->json([
                'resp' => '1f',
            ]);
        }
    }

}
