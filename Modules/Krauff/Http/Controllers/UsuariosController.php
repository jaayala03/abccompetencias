<?php

namespace Modules\Krauff\Http\Controllers;

//**********************************************************************
//Use Necesarios
//**********************************************************************
use Crypt;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
//Modelos
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use JavaScript;
use Modules\Krauff\Entities\Acceso;
//Paquetes
use Modules\Krauff\Entities\Funcionalidad;
use Modules\Krauff\Entities\Notificacion;
use Modules\Krauff\Entities\Perfil;
use Modules\Krauff\Entities\Usuario;
//Traits
use ABCcomptencias\Traits\Permisos;
use Spatie\Activitylog\Models\Activity;

//**********************************************************************

class UsuariosController extends Controller
{

    //permisos trait
    use Permisos;

    protected $funcionalidades;

    public function __construct(Funcionalidad $obj)
    {
        $this->funcionalidades = $obj;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function listar()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Listado de Usuarios',
                'componente' => $this->componente('MNTO_USU'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Usuarios'],
        ]);

        $perfiles = Perfil::orderby('nombreperfil', 'ASC')->pluck('nombreperfil', 'codperfil')->all();
        return view('krauff::usuarios.listar')->with('perfiles', $perfiles);
    }

    public function editar($id)
    {
        $codusuario = Crypt::decrypt($id);
        $usuario    = Usuario::find($codusuario);

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar de Usuarios',
                'componente' => 'General-Usuarios',
            ],
            'usuario'    => [
                'ubicacion'           => $usuario->ubicacion->nombre ?? '',
                'ubicacionnacimiento' => $usuario->ubicacionnacimiento->nombre ?? '',
            ],
        ]);

        View::share('breadcrumbs', [
            ['url' => url('/krauff/usuarios'), 'name' => 'Usuarios'],
            ['name' => 'Editar'],
        ]);

        $perfiles = Perfil::orderby('nombreperfil', 'ASC')->pluck('nombreperfil', 'codperfil')->all();
        return view('krauff::usuarios.editar')->with('perfiles', $perfiles)->with('usuario', $usuario)->with('idcifrado', $id);

    }

    public function TodosDatos(Request $request)
    {
        if (isset($request->codperfilcifrado)) {
            $model = Usuario::query()->where('estado', $request->estado)->where('codperfil', \Crypt::decrypt($request->codperfilcifrado))->orderby('created_at', 'DESC');
        } else {
            $model = Usuario::query()->where('estado', $request->estado)->orderby('created_at', 'DESC');
        }
        return Datatables::eloquent($model)
        ->addColumn('idcifrado', function (Usuario $user) {
            return Crypt::encrypt($user->codusuario);
        })->addColumn('foto', function (Usuario $user) {
            return '<a class="image-lightbox" href="' . route('krauff.getimage', ['codusuario' => Crypt::encrypt($user->codusuario), 'option' => Crypt::encrypt(1)]) . '">'
            . '<img width="50" height="50" src="' . route('krauff.getimage', ['codusuario' => Crypt::encrypt($user->codusuario), 'option' => Crypt::encrypt(1)]) . '" class="text-center scale-img img-circle" alt="img">'
                . '</a>';
        })->editColumn('created_at', function (Usuario $user) {
            return !empty($user->created_at) ? \Carbon::parse($user->created_at)->format('d-m-Y') : 'No Registra';
        })->editColumn('nombres', function (Usuario $user) {
            return $user->nombrecompleto;
        })->editColumn('codperfil', function (Usuario $user) {
            $perfil = Perfil::findOrFail($user->codperfil);
            return $perfil->nombreperfil;
        })->editColumn('telefono', function (Usuario $user) {
            $telefonos = $user->telefono . ' - ' . $user->celular;
            return !empty(trim($telefonos, ' - ')) ? trim($telefonos, ' - ') : 'No Registra';
        })->addColumn('opciones', function (Usuario $user) {
            //******************************************************************************
            //VALIDACION DE PERMISOS
            //******************************************************************************
            $html = '' . ($this->verificarpermiso("MNTO_USU_ELI") ? '<a href="javascript:void(0)" class="btn btn-danger btn-xs rounded btn-eliminar-usuario tooltip-opcion" data-placement="top" data-original-title="Eliminar" data-id="' . \Crypt::encrypt($user->codusuario) . '"><i class="fa fa-times"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("MNTO_USU_EDI") ? '<a href="#/krauff/usuarios/editar/' . \Crypt::encrypt($user->codusuario) . '"  class="btn btn-success btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Editar"><i class="fa fa-pencil"></i></a>' : '');
            $html = $html . ($this->verificarpermiso("MNTO_USU_FUNC") ? '<a href="javascript:void(0)" data-toggle="modal" data-target="#VentanaFlotanteFunc" class="btn btn-primary btn-xs rounded tooltip-opcion" data-placement="top" data-original-title="Funcionalidades" data-id="' . \Crypt::encrypt($user->codusuario) . '" data-title="Funcionalidades del usuario: ' . $user->nombres . '"><i class="fa fa-users"></i></a>' : '');
            return $html;
        })->rawColumns(['opciones', 'foto', 'codperfil'])->make(true);
    }

    /**
     * Muestra la informacion de un usuario especifico segun du id.
     * @return Response
     */
    public function verperfil()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Datos perfil',
                'componente' => 'Inicio',
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Mi perfil'],
        ]);

        $accesos = Acceso::where("codusuario", Auth::user()->codusuario)->groupBy('fechaingreso')->orderby('fechaingreso', 'desc')->select('fechaingreso')->paginate(3);
        $ultimo  = Acceso::where("codusuario", Auth::user()->codusuario)->orderby('fechaingreso', 'desc')->first();

        $totalaccesos = Acceso::where("codusuario", Auth::user()->codusuario)->count();
        $actividades  = Activity::query()->where("causer_id", Auth::user()->codusuario)->orderBy('created_at', 'desc')->get();

        $accesos->map(function ($obj) use ($actividades) {
            $obj["actividades"] = $actividades->filter(function ($actividad, $key) use ($obj) {
                return \Carbon::parse($actividad->created_at)->format('m/d/Y') === \Carbon::parse($obj["fechaingreso"])->format('m/d/Y');
            });
        });
        return view('krauff::miperfil.verperfil')->with("accesos", $accesos)->with("ultimo", $ultimo)->with("totalaccesos", $totalaccesos)->with("actividades", $actividades);
    }

    /**
     * Muestra la informacion de un usuario para editarla.
     * @return Response
     */
    public function editarperfil()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Editar perfil',
                'componente' => 'Inicio',
            ],
        ]);
        View::share('breadcrumbs', [
            ['url' => '#/krauff/verperfil', 'name' => 'Ver perfil'],
            ['name' => 'Editar perfil'],
        ]);
        return view('krauff::miperfil.editarperfil');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('krauff::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $usuario = new Usuario($request->all());
        $foto    = $request->imagenAdjunta;
        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".jpg";
                $subruta                  = Config::get('paths.IMAGENES_PERFIL') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('jpg'));

                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();
                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $usuario->imagencodificada = $nombre_imagen_codificado;
                $usuario->mime             = $mime;
                $usuario->tamanno          = $tamanno;
            }
        }

        //******************************************************************
        //Modificar el objeto
        //******************************************************************
        $password        = $request->password;
        $passwordCifrada = bcrypt($password);

        $usuario->password               = $passwordCifrada;
        $usuario->codubicacion           = $request->codubicacion_value;
        $usuario->codubicacionnacimiento = $request->codubicacionnacimiento_value;

        //******************************************************************
        //Guardar
        //******************************************************************
        $status = $usuario->save();
        return response()->status($status);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('krauff::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('krauff::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $codusuario = Crypt::decrypt($request->codusuario);
        $usuario    = Usuario::findOrFail($codusuario);
        $usuario->fill($request->all());

        $foto = $request->imagenAdjunta;
        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Eliminar Imagen Anterior
                //******************************************************************
                $imagen_anterior = Config::get('paths.IMAGENES_PERFIL') . $usuario->imagencodificada;
                if (!empty($usuario->imagencodificada)) {
                    Storage::disk('web_files')->delete($imagen_anterior);
                }
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".jpg";
                $subruta                  = Config::get('paths.IMAGENES_PERFIL') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('jpg'));
                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();
                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $usuario->imagencodificada = $nombre_imagen_codificado;
                $usuario->mime             = $mime;
                $usuario->tamanno          = $tamanno;
            }
        }

        $usuario->codubicacion           = $request->codubicacion_value;
        $usuario->codubicacionnacimiento = $request->codubicacionnacimiento_value;

        $status = $usuario->save();

        return response()->status($status);
    }

    public function updateperfil(Request $request)
    {

        $codusuario = Auth::user()->codusuario;
        $usuario    = Usuario::findOrFail($codusuario);
        $usuario->fill($request->all());
        $foto = $request->imagenAdjunta;

        if ($request->imagenAdjunta !== null) {
            $foto = str_replace('data:image/png;base64,', '', $foto);
            $foto = str_replace(' ', '+', $foto);
            $foto = base64_decode($foto);

            $im = imagecreatefromstring($foto);
            if ($im !== false) {
                //******************************************************************
                //Eliminar Imagen Anterior
                //******************************************************************
                $imagen_anterior = Config::get('paths.IMAGENES_PERFIL') . $usuario->imagencodificada;
                if (!empty($usuario->imagencodificada)) {
                    Storage::disk('web_files')->delete($imagen_anterior);
                }
                //******************************************************************
                //Crear Nueva Imagen
                //******************************************************************
                $img = Image::make($im);
                //******************************************************************
                //Redimensionar imagen
                //******************************************************************
                $img->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                //******************************************************************
                //Guardar Imagen
                //******************************************************************
                $nombre_aleatorio         = uniqid(rand(), true) . str_replace(" ", "", microtime());
                $nombre_imagen_codificado = $nombre_aleatorio . ".jpg";
                $subruta                  = Config::get('paths.IMAGENES_PERFIL') . $nombre_imagen_codificado;
                $ruta_nombre              = Storage::disk('web_files')->getDriver()->getAdapter()->getPathPrefix() . $subruta;
                Storage::disk('web_files')->put($subruta, (string) $img->encode('jpg'));
                //******************************************************************
                //datos de la imagen
                //******************************************************************
                $tamanno = filesize($ruta_nombre) / 1024;
                $mime    = $img->mime();

                //******************************************************************
                //Modificar el objeto
                //******************************************************************
                $usuario->imagencodificada = $nombre_imagen_codificado;
                $usuario->mime             = $mime;
                $usuario->tamanno          = $tamanno;
            }
        }

        $usuario->codubicacion           = $request->codubicacion_value;
        $usuario->codubicacionnacimiento = $request->codubicacionnacimiento_value;
        $usuario->password               = bcrypt($request->password);

        if ($request->password == "") {
            $usuario->password = Auth::user()->password;
        }

        $status = $usuario->save();
        return response()->status($status);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $codusuario = Crypt::decrypt($request->id);
        $usuario    = Usuario::findOrFail($codusuario);
        $status     = $usuario->delete();
        return response()->status($status);
    }

    public function ShowFunc($id)
    {
        $codusuario = Crypt::decrypt($id);
        $usuario    = Usuario::find($codusuario);
        return view('krauff::usuarios.funcionalidades')->with('usuario', $usuario);
    }

    public function validarcontrasena(Request $request)
    {
        $guardada = Auth::user()->password;

        $arreglo = false;
        if (Hash::check($request->pass, $guardada)) {
            $arreglo = true;
        }

        return response()->json($arreglo);
    }

    public function LoadFunc(Request $request)
    {
        //**********************************************************************
        //manejo de los datos
        //**********************************************************************
        $codusuario = $request->id;
        $nodo       = 1; //Este es el primer nodo del arbol de funcionalidades

        $usuario              = Usuario::find($codusuario);
        $seleccionados        = $usuario->funcionalidades;
        $seleccionados_perfil = Perfil::find($usuario->codperfil)->funcionalidades;

        //**********************************************************************
        //Cargar funcionalidades que tiene el perfil y adicionales del usuario
        //**********************************************************************
        $i   = 0;
        $arr = array();
        foreach ($seleccionados as $campo) {
            if (!$this->funcionalidades->verificar_hijos($campo->codfunc)) {
                $i++;
                $arr[$i] = $campo->codfunc;
            }
        }

        $i2   = 0;
        $arr2 = array();
        foreach ($seleccionados_perfil as $campo2) {
            $i2++;
            $arr2[$i2] = $campo2->codfunc;
        }

        $arreglo1 = $this->funcionalidades->obtener_nodo_disabled($nodo, $arr2, $arr);
        //**********************************************************************

        return response()->json($arreglo1);
    }

    public function SaveFunc(Request $request)
    {

        //**********************************************************************
        //Manejo de datos
        //**********************************************************************
        $cod_usuario      = $request->id;
        $cod_funcionalida = $request->codfunc;

        if ($cod_funcionalida != 0) {
            $usuario = Usuario::findOrFail($cod_usuario);

            $funcPerfil = Perfil::findOrFail($usuario->codperfil)->funcionalidades;
            $i          = 0;
            $arr        = array();
            foreach ($funcPerfil as $campo) {
                $i++;
                $arr[$i] = $campo->codfunc;
            }

            $keys = array_values($arr);
            //Eliminar todas las funcionalidades
            $usuario->funcionalidades()->detach();
            //Inserta la funcionalidad principal
            $usuario->funcionalidades()->attach(1);

            foreach ($cod_funcionalida as $valor) {

                //Busqueda del padre
                $padre_func = Funcionalidad::findOrFail($valor)->codpadre;

                if (!in_array($valor, $keys)) {

                    //Verificar si el padre existe de lo contrario insertarlo
                    $this->funcionalidades->verificar_padre_funcusuarios($cod_usuario, $padre_func);

                    if (!$this->funcionalidades->verificar_hijos($valor)) {
                        //Insertar la funcionalidad
                        $usuario->funcionalidades()->attach($valor);
                    }
                }
            }
        } else {
            return response()->json([
                'resp' => '1f',
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function eliminarnotificaciones(Request $request)
    {
        $codusuario      = Auth::user()->id;
        $usuario         = Usuario::findOrFail($codusuario);
        $cantidad_actual = Notificacion::WhereUsuario(Auth::user()->codusuario)->Noleidas()->count();

        $success = false;
        if ($usuario->notificaciones()->detach()) {
            $success = true;
        }

        return response()->json([
            'success'         => $success,
            'cantidad_actual' => 0,
        ]);
    }

    public function leernotificacion($id)
    {
        //**********************************************************************
        //Manejo de datos
        //**********************************************************************
        $codnotificacion = Crypt::decrypt($id);
        $notificacion    = Notificacion::find($codnotificacion);
        //**********************************************************************
        //Actualizar la tabla intermedia
        //**********************************************************************
        $notificacion->Usuario()->sync([Auth::user()->codusuario => ['leida' => Config::get('dominios.NOTIFICACION_LEIDA.VALORES.SI')]]);
        //**********************************************************************
        //Redireccionar a la pagina de la notificacion
        //**********************************************************************
        return \Redirect::to($notificacion->url);
    }

    public function editarperfilajax(Request $request)
    {
        $usuario         = Usuario::find($request->pk);
        $campo           = $request->name;
        $usuario->$campo = $request->value;
        if ($usuario->save()) {
            return response('Exito!', 202);
        }
        return response('Error!', 500);
    }

    public function ActivityData()
    {
        $activity = Activity::query()->limit(2)->get();
        $activity->map(function ($obj) {
            $obj->user = Usuario::find($obj->causer_id)->nombres;
        });
        return $activity;
    }

}
