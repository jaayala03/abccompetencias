<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Perfil extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'perfiles';
    protected $primaryKey = 'codperfil';
    protected $fillable   = [
        'nombreperfil',
        'codpaginainicio',
        'hinicio',
        'hfin',
        'restriccion_ip'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'sistema';

    protected static $logAttributes = [
        'codperfil',
        'nombreperfil',
        'codpaginainicio',
        'hinicio',
        'hfin',
        'restriccion_ip'
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó el Perfil: ' . $this->nombreperfil;
                break;
            case 'updated':
                return 'Editó el Perfil: ' . $this->nombreperfil;
                break;
            case 'deleted':
                return 'Eliminó el Perfil: ' . $this->nombreperfil;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'sistema';
    }
    //**********************************************************************

    //Relacion uno a muchos con usuarios
    public function usuarios()
    {
        return $this->hasMany('Modules\Krauff\Entities\Usuario', 'codperfil', 'codperfil');
    }

    //Relacion muchos a muchos con funcionalidades
    public function funcionalidades()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Funcionalidad', 'relfuncperfil', 'codperfil', 'codfunc')->withTimestamps();
    }

}
