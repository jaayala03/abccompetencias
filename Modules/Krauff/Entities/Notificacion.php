<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table      = 'notificaciones';
    protected $primaryKey = 'codnotificacion';
    protected $fillable   = [
        'codnotificacion',
        'titulo',
        'mensaje',
        'icono',
        'url',
        'tipo',
        'fecha',
        'created_at',
        'created_at',
    ];

    //Relacion muchos a muchos con usuarios
    public function usuarios()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Usuario', 'notificacionesusuarios', 'codnotificacion', 'codusuario')->withTimestamps();
    }

    public function scopeWhereUsuario($query, $id)
    {
        $query
            ->join('notificacionesusuarios', 'notificaciones.codnotificacion', '=', 'notificacionesusuarios.codnotificacion')
            ->where('notificacionesusuarios.codusuario', '=', $id);
        return $query;
    }

    public function scopeNoleidas($query)
    {
        return $query->where('leida', '=', 1);
    }
}
