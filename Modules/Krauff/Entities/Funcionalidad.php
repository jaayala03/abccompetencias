<?php

namespace Modules\Krauff\Entities;

use DB;
use Illuminate\Database\Eloquent\Model;
use Modules\Krauff\Entities\Funcionalidad;
use Modules\Krauff\Entities\Perfil;
use Modules\Krauff\Entities\Usuario;

class Funcionalidad extends Model
{

    protected $table      = 'funcionalidades';
    protected $primaryKey = 'codfunc';
    protected $fillable   = [
        'codfunc',
        'nombre',
    ];

    //Relacion muchos a muchos con perfiles
    public function perfiles()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Perfil', 'relfuncperfil', 'codfunc', 'codperfil')->withTimestamps();
    }

    //Relacion muchos a muchos con usuarios
    public function usuarios()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Usuario', 'relfuncusuarios', 'codfunc', 'codusuario')->withTimestamps();
    }

    public function verificar_hijos($codfunc)
    {
        $cantidad_hijos = DB::table('funcionalidades')
            ->select('codfunc')
            ->where('codpadre', $codfunc)
            ->count();

        return ($cantidad_hijos > 0) ? true : false;
    }

    public function verificar_padre($codigo, $codfunc)
    {
        $cantidad = DB::table('relfuncperfil')
            ->select('codfunc')
            ->where('codperfil', $codigo)
            ->where('codfunc', $codfunc)
            ->count();

        if ($cantidad == 0) {
            $perfil = Perfil::findOrFail($codigo);
            $perfil->funcionalidades()->attach($codfunc);
            $padre_func = Funcionalidad::findOrFail($codfunc)->codpadre;
            $this->verificar_padre($codigo, $padre_func);
        }
    }

    public function verificar_padre_funcusuarios($codigo, $codfunc)
    {
        $cantidad = DB::table('relfuncusuarios')
            ->select('codfunc')
            ->where('codusuario', $codigo)
            ->where('codfunc', $codfunc)
            ->count();

        if ($cantidad == 0) {
            $usuario = Usuario::findOrFail($codigo);
            $usuario->funcionalidades()->attach($codfunc);
            $padre_func = Funcionalidad::findOrFail($codfunc)->codpadre;
            $this->verificar_padre_funcusuarios($codigo, $padre_func);
        }
    }

    public function obtener_nodo_perfiles($indicednode, &$seleccionados)
    {
        $subquery1 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc");

        $arr = DB::table('funcionalidades as f')
            ->select('f.codfunc', 'f.codpadre', 'f.nombre', 'f.identificador', 'f.orden', 'f.urlpagina', 'f.target', 'f.icono', 'f.tipo', DB::raw("({$subquery1->toSql()}) as hijos"))
            ->where('f.codpadre', $indicednode)
            ->orderBy('f.orden')
            ->get();

        $indice  = 0;
        $arrData = array();

        foreach ($arr as $campo) {
            $hijos                     = $campo->hijos;
            $arrData[$indice]["title"] = $campo->nombre;
            $arrData[$indice]["text"]  = $campo->nombre;
            $arrData[$indice]["key"]   = $campo->codfunc;
            $arrData[$indice]["id"]    = $campo->codfunc;
            $arrData[$indice]["icon"]  = $campo->icono;

            $keys = array_values($seleccionados);
            if (in_array($campo->codfunc, $keys)) {
                $arrData[$indice]["select"]            = true;
                $arrData[$indice]["state"]["selected"] = true;
                $arrData[$indice]["state"]["disabled"] = false;
                $arrData[$indice]["state"]["opened"]   = false;
            }
            if ($hijos > 0) {
                $arrData[$indice]["isFolder"] = true;
                $arrData[$indice]["children"] = $this->obtener_nodo_perfiles($campo->codfunc, $seleccionados);
            }
            $indice++;
        }
        return $arrData;
    }

    public function obtener_nodo_disabled($indicednode, &$seleccionados, &$seleccionados2)
    {
        $subquery1 = DB::TABLE('funcionalidades')->SELECT(DB::raw('count(*)'))->whereRaw("codpadre = f.codfunc");

        $arr = DB::table('funcionalidades as f')
            ->select('f.codfunc', 'f.codpadre', 'f.nombre', 'f.identificador', 'f.orden', 'f.urlpagina', 'f.target', 'f.icono', 'f.tipo', DB::raw("({$subquery1->toSql()}) as cant"))
            ->where('f.codpadre', $indicednode)
            ->orderBy('f.orden')
            ->get();

        $indice  = 0;
        $arrData = array();

        foreach ($arr as $campo) {
            $hijos                     = $campo->cant;
            $arrData[$indice]["title"] = $campo->nombre;
            $arrData[$indice]["text"]  = $campo->nombre;
            $arrData[$indice]["key"]   = $campo->codfunc;
            $arrData[$indice]["id"]    = $campo->codfunc;
            $arrData[$indice]["icon"]  = $campo->icono;

            $keys  = array_values($seleccionados);
            $keys2 = array_values($seleccionados2);
            if (in_array($campo->codfunc, $keys)) {

                $arrData[$indice]["select"]            = true;
                $arrData[$indice]["state"]["disabled"] = true;
                $arrData[$indice]["state"]["opened"]   = false;

                if ($this->verificar_hijos($campo->codfunc)) {
                    $arrData[$indice]["state"]["selected"] = false;
                } else {
                    $arrData[$indice]["state"]["selected"] = true;
                }
            }

            if (in_array($campo->codfunc, $keys2)) {
                $arrData[$indice]["select"]            = true;
                $arrData[$indice]["state"]["selected"] = true;
                $arrData[$indice]["state"]["disabled"] = false;
                $arrData[$indice]["state"]["opened"]   = false;
            }
            if ($hijos > 0) {
                $arrData[$indice]["isFolder"] = true;
                $arrData[$indice]["children"] = $this->obtener_nodo_disabled($campo->codfunc, $seleccionados, $seleccionados2);
            }
            $indice++;
        }
        return $arrData;
    }

    public function scopeIdentificador($query, $identificador)
    {
        return $query->where('funcionalidades.identificador', '=', $identificador);
    }

}
