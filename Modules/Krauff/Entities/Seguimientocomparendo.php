<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Seguimientocomparendo extends Model
{
    use LogsActivity;
    use SearchableTrait;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table = 'seguimientocomparendos';

    protected $primaryKey = 'codseguimiento';

    protected $fillable = [
        'codusuario',
        'codcomparendo',
        'observaciones',
        'fecha',
        'nombrearchivo',
        'mime',
        'tamanno',
        'extension',
    ];

    //**********************************************************************
    protected $appends = ['usuario'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty         = true;
    protected static $logName       = 'comparendos';
    protected static $logAttributes = [
        'codseguimiento',
        'codusuario',
        'codcomparendo',
        'observaciones',
        'fecha',
        'nombrearchivo',
        'mime',
        'tamanno',
        'extension',
    ];

    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'comparendos';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó un Seguimiento al comparendo: ' . $this->codcomparendo;
                break;
            case 'updated':
                return 'Editó un Seguimiento al comparendo: ' . $this->codcomparendo;
                break;
            case 'deleted':
                return 'Eliminó un Seguimiento al comparendo: ' . $this->codcomparendo;
                break;
        }
        return '';
    }

    public function getUsuarioAttribute($value)
    {
        $key = 'usuario';
        if (array_key_exists($key, $this->relations)) {
            $value = $this->relations[$key];
        } elseif (method_exists($this, $key)) {
            $value = $this->getRelationshipFromMethod($key);
        }
        $value = $value ?: new Usuario(['nombres' => '']);
        $this->setRelation($key, $value);
        return $value;
    }

    //Relacion muchos a uno con usuario
    public function usuario()
    {
        return $this->belongsTo('Modules\Krauff\Entities\Usuario', 'codusuario', 'codusuario');
    }
}
