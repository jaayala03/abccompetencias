<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Usuarios extends Model
{

    use LogsActivity;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table = 'usuarios';

    protected $primaryKey = 'codusuario';

    protected $fillable = [
        'id',
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'sistema';

    protected static $logAttributes = [
        'id',
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];


    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'sistema';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                 return 'Creó el Usuario: '.$this->email;
                break;
            case 'updated':
                return 'Editó el Usuario: '.$this->email;
                break;
            case 'deleted':
                return 'Eliminó el Usuario: '.$this->email;
                break;
        }
        return '';
    }

    //**********************************************************************

    //relacion con perfiles
    public function perfil()
    {
        return $this->belongsTo('Modules\Krauff\Entities\Usuarios', 'codperfil');
    }

    //Relacion muchos a muchos con funcionalidades
    public function funcionalidades()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Funcionalidades', 'relfuncusuarios', 'codusuario', 'codfunc')->withTimestamps();
    }

    //Relacion muchos a uno con ubicaciones
    public function ubicacion()
    {
        return $this->hasOne('Modules\Krauff\Entities\Ubicaciones', 'codubicacion', 'codubicacion');
    }

    //Relacion muchos a uno con ubicaciones
    public function ubicacionnacimiento()
    {
        return $this->hasOne('Modules\Krauff\Entities\Ubicaciones', 'codubicacion', 'codubicacionnacimiento');
    }

    //Relacion muchos a muchos con notificaciones
    public function notificaciones()
    {
        return $this->belongsToMany('Modules\Krauff\Entities\Notificaciones', 'notificacionesusuarios', 'codusuario', 'codnotificacion')->withTimestamps();
    }

    //Relacion uno a muchos con accesos
    public function accesos()
    {
        return $this->hasMany('Modules\Krauff\Entities\Accesos', 'codusuario', 'codusuario');
    }

    public function scopeWithFuncUsu($query, $identificador)
    {
        return $query
            ->join('relfuncusuarios', 'usuarios.id', '=', 'relfuncusuarios.codusuario')
            ->join('funcionalidades', 'relfuncusuarios.codfunc', '=', 'funcionalidades.codfunc')
            ->where('funcionalidades.identificador', '=', $identificador);
    }

    public function scopeWithFuncPerfil($query, $identificador)
    {
        return $query
            ->join('perfiles', 'usuarios.codperfil', '=', 'perfiles.codperfil')
            ->join('relfuncperfil', 'perfiles.codperfil', '=', 'relfuncperfil.codperfil')
            ->join('funcionalidades', 'relfuncperfil.codfunc', '=', 'funcionalidades.codfunc')
            ->where('funcionalidades.identificador', '=', $identificador);
    }

}
