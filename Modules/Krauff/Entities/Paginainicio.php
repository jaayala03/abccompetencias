<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;

class Paginainicio extends Model
{
    protected $table = 'paginasinicio';
    protected $primaryKey = 'codpaginainicio';
    protected $fillable = [
        'codpaginainicio',
        'nombre',
        'nombrearchivo'
    ];

    function perfiles() {
        return $this->hasMany('Modules\Krauff\Entities\Perfil', 'codpaginainicio', 'codpaginainicio');
    }
}
