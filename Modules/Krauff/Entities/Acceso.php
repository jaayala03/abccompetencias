<?php

namespace Modules\Krauff\Entities;

use Illuminate\Database\Eloquent\Model;

class Acceso extends Model
{
    protected $table      = 'accesos';
    protected $primaryKey = 'codacceso';
    protected $fillable   = [
        'codacceso',
        'codusuario',
        'fechaingreso',
        'horaingreso',
        'ipoculta',
        'ipvisible',
        'so',
        'navegador'
    ];

    //relacion con usuarios  
    public function usuario()
    {
        return $this->belongsTo('Modules\Krauff\Entities\Usuario', 'codusuario');
    }
}
