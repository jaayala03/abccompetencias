@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/perfiles/perfiles.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Perfiles </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">

            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Perfiles</h3>
                    </div>
                    <div class="pull-right">                       
                        @if(array_key_exists("MNTO_PER_ADD", session("permisos")))
                        <a  title='Crear Nuevo Perfil'  data-toggle="modal" data-target="#PerfilesCrear">
                            <button class='btn btn-primary' type="button"><i class='fa fa-plus'></i> Crear Perfil</button></a>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <!-- Start datatable -->
                        <table id="perfiles" class="table table-striped table-bordered table-warning" width="100%">
                            <thead>
                                <tr>
                                    <th data-class="expand">Nombre</th>
                                    <th data-hide="phone">Horario Permitido</th>
                                    <th data-hide="phone">Restricción IP</th>
                                    <th data-hide="phone">Usuarios</th>
                                    <th data-hide="phone,tablet">Opciones</th>
                                </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>                            
                        </table>
                        <!--/ End datatable -->
                    </div><!-- /.responsive -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->


        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->

</div><!-- /.body-content -->
<!--/ End body content -->



<!--/ END PAGE CONTENT -->
@endsection

@section('modales')
<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA AGREGRAR Perfiles-->
<!--****************************************************************************************************************-->
<!-- Start tabs in modal element -->
<div id="PerfilesCrear" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Crear Perfil</h4>
            </div>
            <!-- Start comment form -->
            <div class="panel rounded">                                        
                <div class="panel-body no-padding">
                    <div class="form-body">
                        <div class="form-group">
                            <form method="post" id="frm_crear_perfil">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label">Perfil:</label>
                                    <input placeholder="perfil..." class="form-control" name="nombreperfil" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Pagina inicio:</label>
                                    {{ Form::select('codpaginainicio', $paginasinicio, null, ['placeholder' => 'Seleccione', 'class'=>'form-control'] ) }}
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Hora inicio:</label>
                                    <input placeholder="Hora inicio..." class="form-control inputmask_hora" name="hinicio" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Hora fin:</label>
                                    <input placeholder="Hora  fin..." class="form-control inputmask_hora" name="hfin" type="text">
                                </div>
                            </form>
                        </div>
                    </div><!-- /.form-body -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End comment form -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-theme" id="btn-guardar">Guardar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/ End tabs in modal element -->
<!--****************************************************************************************************************-->

<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA AGREGRAR Perfiles-->
<!--****************************************************************************************************************-->
<!-- Start tabs in modal element -->
<div id="PerfilesEditar" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Editar Perfil</h4>
            </div>
            <!-- Start comment form -->
            <div class="panel rounded">                                        
                <div class="panel-body no-padding">
                    <div class="form-body">
                        <div class="form-group">
                            <form method="post" id="frm_editar_perfil">
                                {{ csrf_field() }}
                                <input type="hidden" name="codperfil">
                                <div class="form-group">
                                    <label class="control-label">Perfil:</label>
                                    <input placeholder="perfil..." class="form-control" id="nombreperfil" name="nombreperfil" type="text">
                                </div>    
                                <div class="form-group">
                                    <label class="control-label">Pagina inicio:</label>
                                    {{ Form::select('codpaginainicio', $paginasinicio, null, ['id'=>'codpaginainicio_edit','placeholder' => 'Seleccione', 'class'=>'form-control'] ) }}
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Hora inicio:</label>
                                    <input placeholder="Hora inicio..." class="form-control inputmask_hora" name="hinicio" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Hora fin:</label>
                                    <input placeholder="Hora  fin..." class="form-control inputmask_hora" name="hfin" type="text">
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 text-right">Restricción IP:</label>
                                    <div class="col-sm-7">
                                        <input type="checkbox" class="switch_restriccion_ip" name="switch" data-on-text="SI" data-off-text="NO" data-on-color="teal">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.form-body -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End comment form -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-theme" id="btn-guardar-editar">Guardar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/ End tabs in modal element -->
<!--****************************************************************************************************************-->
<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA LAS FUNCIONALIDADES DE LOS PERFILES-->
<!--****************************************************************************************************************-->
<div role="dialog" tabindex="-1" id="VentanaFlotanteFunc" class="modal fade" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span></button>
                <h4 id="titulo-modal" class="modal-title">Funcionalidades</h4>
            </div>
            <div class="modal-body">
                <iframe id="contenedorIframeFunc" scrolling="yes" width="100%" height="460" src="" data-iframe-src="{{url('/')}}" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<!--****************************************************************************************************************-->
@endsection
