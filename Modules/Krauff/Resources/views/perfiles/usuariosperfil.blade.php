@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/usuarios/usuarios.js') }}"></script>
<script src="{{ asset('modules/krauff/js/usuarios/usuarios_crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link href="{{ asset('modules/krauff/css/custom.css') }}" rel="stylesheet" >
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Usuarios </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted esta en:</span>
        @include('krauff::layouts.partials.breadcrumbs')
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">

            <!-- Start datatable using ajax -->
            <div class="panel panel-tab panel-tab-double shadow">
                <div class="panel-heading">
                    <div class="pull-left">

                        <h3 class="panel-title">Lista de Usuarios 
                        </h3>
                    </div>
         
                    <div class="pull-right">
                    
                        @if(array_key_exists("MNTO_USU_ADD", session("permisos")))
                        <a  title='Crear Nuevo Usuario'  data-toggle="modal" data-target="#UsuariosCrear">
                            <button class='btn btn-success' type="button"><i class='fa fa-plus'></i> Crear Usuario</button></a>
                        @endif
                   

                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-heading no-padding">
                    <ul class="nav nav-tabs">                       
                        <li class="active nav-border nav-border-top-success">
                            <a href="#tab-usuariosactivos" data-toggle="tab">
                                <i class="icon-check icons fg-success"></i>
                                <div>
                                    <h5 class="text-strong">
                                        Activos
                                    </h5>
                                </div>
                                <div>
                                    <span class="badge rounded bg-success" id="contador-usuariosactivos">
                                        0
                                    </span>
                                </div>
                            </a>
                        </li>                            
                        <li class="nav-border nav-border-top-danger">
                            <a href="#tab-usuariosinactivos" data-toggle="tab">
                                <i class="icon-close icons fg-danger"></i>
                                <div>
                                    <h5 class="text-strong">
                                        Inactivos
                                    </h5>
                                </div>
                                <div>
                                    <span class="badge rounded bg-danger" id="contador-usuariosinactivos">
                                        0
                                    </span>
                                </div>
                            </a>
                        </li>                            
                    </ul>
                </div>
                
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab-usuariosactivos">
                            <div class="table-responsive">
                            <!-- Start datatable -->
                                <table id="tbl_usuariosactivos" class="table table-success table-bordered table-striped table-hover" style="width: 100%;" >
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Perfil</th>
                                            <th>Foto</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Telefonos</th>
                                            <th></th>
                                            <th>Fecha Creación</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <!--tbody section is required-->
                                    <tbody></tbody>                            
                                </table>
                            </div><!-- /.responsive -->
                        </div>                        
                        <div class="tab-pane fade in" id="tab-usuariosinactivos">
                            <div class="table-responsive">
                            <!-- Start datatable -->
                                <table id="tbl_usuariosinactivos" class="table table-danger table-bordered table-striped table-hover" style="width: 100%;" >
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>Perfil</th>
                                            <th>Foto</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Telefonos</th>
                                            <th></th>
                                            <th>Fecha Creación</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <!--tbody section is required-->
                                    <tbody></tbody>                            
                                </table>
                            </div><!-- /.responsive -->
                        </div>
                    </div>
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')
<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA AGREGRAR USUARIOS-->
<!--****************************************************************************************************************-->
<!-- Start tabs in modal element -->
<div id="UsuariosCrear" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">           
            <div class="row">
                <div class="col-md-12">

                    <!-- Start form validation wizard -->
                    <div id="validacion">                        
                        <div class="panel panel-tab rounded shadow">
                            <div class="pull-right" style="padding: 15px;">
                                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <!-- Start tabs heading -->
                            <div class="panel-heading no-padding">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab4-1" data-toggle="tab">
                                            <i class="fa fa-user"></i>
                                            <div>
                                                <span class="text-strong">Paso 1</span>
                                                <span>Datos Basicos</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-2" data-toggle="tab">
                                            <i class="fa fa-desktop"></i>
                                            <div>
                                                <span class="text-strong">Paso 2</span>
                                                <span>Sistema</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-3" data-toggle="tab">
                                            <i class="fa fa-picture-o"></i>
                                            <div>
                                                <span class="text-strong">Paso 3</span>
                                                <span>Foto</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4-4" data-toggle="tab">
                                            <i class="fa fa-key"></i>
                                            <div>
                                                <span class="text-strong">Paso 4</span>
                                                <span>Contraseña</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div><!-- /.panel-heading -->
                            <!--/ End tabs heading -->
                            <!--/ End tabs heading -->
                            <div class="panel-sub-heading">
                                <div class="inner-all">
                                    <div class="progress progress-striped active no-margin progress-xs">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div><!-- /.panel-sub-heading -->
                            <!-- Start tabs content -->
                            <div class="panel-body">
                                <form method="post" id="frm_crear_usuario" class="tab-content form-horizontal">
                                    <input type="hidden" name="imagenAdjunta" id="imagenAdjunta" value=""> 
                                    {{Form::hidden('codubicacion_value', null, ['id' => 'codubicacion_value'] )}}
                                    {{Form::hidden('codubicacionnacimiento_value', null, ['id' => 'codubicacionnacimiento_value'] )}}
                                    {{ csrf_field() }}
                                    <div class="tab-pane fade in active inner-all" id="tab4-1">
                                        <div class="row">
                                            <div class="col-sm-4 b-r">                                                

                                                <div class="form-group">
                                                    {{ Form::label('tipodoc', 'Tipo Documento') }}
                                                    {{ Form::select('tipodoc', Config::get('dominios.TIPODOC.TXT'), null, ['placeholder' => 'Seleccione tipo documento', 'class'=>'form-control']) }}

                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('documento', 'Documento') }}
                                                    {{Form::text('documento', null, ['class' => 'form-control'] )}}

                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('nombres', 'Nombres') }}
                                                    {{Form::text('nombres', null, ['class' => 'form-control'] )}}
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('primerapellido', 'Primer Apellido') }}
                                                    {{Form::text('primerapellido', null, ['class' => 'form-control'] )}}
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('segundoapellido', 'Segundo Apellido') }}
                                                    {{Form::text('segundoapellido', null, ['class' => 'form-control'] )}}
                                                </div>
                                            </div>

                                            <div class="col-sm-4 b-r">                                                

                                                <div class="form-group">
                                                    {{ Form::label('genero', 'Género') }}
                                                    {{ Form::select('genero', Config::get('dominios.GENERO.TXT'), null, ['placeholder' => 'Seleccione género', 'class'=>'form-control'] ) }}

                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('telefono', 'Teléfono Fijo') }}
                                                    {{Form::text('telefono', null, ['class' => 'form-control'] )}}

                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('celular', 'Celular') }}
                                                    {{Form::text('celular', null, ['class' => 'form-control'] )}}
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('email', 'Correo Electrónico') }}
                                                    {{Form::email('email', null, ['class' => 'form-control'] )}}
                                                </div>
                                            </div>

                                            <div class="col-sm-4 b-r">                                                
                                                <div class="form-group">
                                                    {{ Form::label('fechanacimiento', 'Fecha Nacimiento') }}
                                                    {{ Form::text('fechanacimiento', null, ['class' => 'form-control', 'placeholder' => 'aaaa/mm/dd']) }}
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('codubicacionnacimiento', 'Lugar de nacimiento') }}
                                                    <div class="input-group">
                                                        {{Form::text('codubicacionnacimiento', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger remove-nacimiento" type="button">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    {{ Form::label('codubicacion', 'Lugar de residencia') }}
                                                    <div class="input-group">
                                                        {{Form::text('codubicacion', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger remove-ubicacion" type="button">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('direccion', 'Dirección') }}
                                                    {{Form::text('direccion', null, ['class' => 'form-control'] )}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade inner-all" id="tab4-2">                                           
                                        <div class="form-group">
                                            {{ Form::label('codperfil', 'Perfil', ['class'=>'col-sm-2']) }}
                                            <div class="col-sm-4">
                                                {{ Form::select('codperfil', $perfiles, null, ['placeholder' => 'Seleccione perfil', 'class'=>'form-control'] ) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('estado', 'Estado', ['class'=>'col-sm-2']) }}
                                            <div class="col-sm-4">
                                                {{ Form::select('estado', Config::get('dominios.ESTADOUSU.TXT'), null, ['placeholder' => 'Seleccione estado', 'class'=>'form-control'] ) }}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade inner-all" id="tab4-3">

                                        <div class="form-group">

                                            <div class="col-md-5">
                                                <div class="image-crop">
                                                    <img src=""/>
                                                </div>

                                            </div>
                                            <label title="Adjuntar Imagen" for="FileInput" class="btn btn-success col-sm-2">
                                                <input type="file" accept="image/*" name="file" id="FileInput" class="hide">
                                                Examinar
                                            </label>
                                            <div class="col-sm-4">
                                                <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                                                <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                                                <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
                                                <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha</button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade inner-all" id="tab4-4">
                                        <div class="row">

                                            <div class="col-sm-6">    
                                                <div class="form-group">
                                                    {{ Form::label('password', 'Contraseña', ['class'=>'col-sm-6']) }}
                                                    <div class="col-sm-6">
                                                        {{ Form::password('password', ['class' => 'form-control']) }}
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    {{ Form::label('password2', 'Confirmar contraseña', ['class'=>'col-sm-6']) }}
                                                    <div class="col-sm-6">
                                                        {{ Form::password('password2', ['class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>                                           

                                            <button type="button" class="btn btn-success btn-push center-block" id="btn-guardar">Guardar</button>

                                        </div>

                                    </div>
                                </form>
                            </div><!-- /.panel-body -->
                            <!--/ End tabs content -->

                            <!-- Start pager -->
                            <div class="panel-footer">
                                <ul class="pager wizard no-margin">
                                    <button type="button" class="btn btn-danger previous pull-left">Atras</button>
                                    <button type="button" class="btn btn-success next pull-right">Siguiente</button>
                                </ul>
                            </div><!-- /.panel-footer -->
                            <!--/ End pager -->

                        </div><!-- /.panel -->
                    </div>
                    <!--/ End form validation wizard-->

                </div>
            </div><!-- /.row -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--/ End tabs in modal element -->
<!--****************************************************************************************************************-->

<!--****************************************************************************************************************-->
<!--VENTANA FLOTANTE PARA LAS FUNCIONALIDADES DE LOS PERFILES-->
<!--****************************************************************************************************************-->
<div role="dialog" tabindex="-1" id="VentanaFlotanteFunc" class="modal fade" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span></button>
                <h4 id="titulo-modal" class="modal-title">Funcionalidades</h4>
            </div>
            <div class="modal-body">
                <iframe id="contenedorIframeFunc" scrolling="yes" width="550" height="460" src="" data-iframe-src="{{url('/')}}" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
<!--****************************************************************************************************************-->
@endsection