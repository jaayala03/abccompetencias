<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Aplicacion ABCcomptencias">
        <meta name="keywords" content="ABCcomptencias">
        <meta name="author" content="jaayala03">
        <title>{{$parametros['meta_title']}}</title>

        <!-- CSS Files -->
        @yield('css')
        
        <!-- Variable Global JS -->
        <script>
            var baseurl = @json(url('/'));
            var session = {warnings: @json(session('warnings'))};
        </script>

    </head>

    <body>
        @yield('content') 
    </body>

    @yield('js')       


</html>
