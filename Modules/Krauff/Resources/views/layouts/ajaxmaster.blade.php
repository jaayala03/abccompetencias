<!-- INICIO CSS ADICIONAL -->
@yield('css')
<!--/ FIN DE CSS ADICIONAL -->
<!-- INICIO CONTENIDO -->
@yield('content')
<!-- FIN CONTENIDO -->
<!-- START @ALL  MODALS -->
@yield('modales')
<!--/ END ALL MODALS -->
<!-- INICIO JS ADICIONAL -->
@yield('js')
<!--/ FIN DE JS ADICIONAL -->