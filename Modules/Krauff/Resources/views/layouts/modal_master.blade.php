<!-- START @PAGE  CONTENT -->
<!--/ END PAGE CONTENT -->

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="es"> <!--<![endif]-->

    <!-- START @HEAD  -->
    <head>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- START @GLOBAL  MANDATORY STYLES -->
        <link href="{{ asset(mix('css/app.css'), Request::secure()) }}" rel="stylesheet">
        <link href="{{ asset(mix('css/all.css'), Request::secure()) }}" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->


        <!-- INICIO CSS ADICIONAL -->        
        @yield('css')         
        <!--/ FIN DE CSS ADICIONAL -->

        <!-- Variable Global JS -->
        <script>
            var baseurl = @json(url('/'));
            var session = {warnings: @json(session('warnings'))};
        </script>

    </head>
    <body class="page-session page-sound page-header-fixed page-sidebar-fixed">

        <!--[if lt IE 9]>
    <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

        <!-- START @WRAPPER  -->


        <!-- START @PAGE  CONTENT -->
        <!-- Aqui es donde ustedes trabajan perros  -->
        @yield('content')             

        <!--/ END WRAPPER -->
        <!-- START @ALL  MODALS --> 
        @yield('modales')            
        <!--/ END ALL MODALS -->

    
        <!--<div id="back-top" class="animated pulse circle">-->
        <!--    <i class="fa fa-angle-up"></i>-->
        <!--</div>-->     
        
        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE  JS -->
        <script src="{{ asset(mix('/js/app.js'), Request::secure()) }}"></script>
        <script src="{{ asset(mix('/js/all.js'), Request::secure()) }}"></script>
         <!-- END @CORE  JS -->


        <!-- INICIO JS ADICIONAL -->        
        @yield('js')         
        <!--/ FIN DE JS ADICIONAL -->

    </body>
</html>