<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="es"> <!--<![endif]-->

    <!-- START @HEAD  -->
    <head>
        
        <!-- START @META  SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Aplicacion ABCcomptencias">
        <meta name="keywords" content="ABCcomptencias">
        <meta name="author" content="jaayala03">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico', Request::secure()) }}">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{$parametros['meta_title']}}</title>
        <!--/ END META SECTION -->

        <!-- START @GLOBAL  MANDATORY STYLES -->
        <link href="{{ asset(mix('/css/app.css'), Request::secure()) }}" rel="stylesheet">
        <link href="{{ asset(mix('/css/all.css'), Request::secure()) }}" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START CSS ADITIONAL -->        
        @yield('css')
        <!--/ END DE CSS ADITIONAL -->

        <!-- VAR GLOBAL JS -->
        <script>
            var baseurl = @json(url("/"));
            var session = {warnings: @json(session('warnings'))};
            var config_epayco = @json(['key'=> config('pay.public_key'), 'test' => config('pay.test')]);
        </script>
    </head>
    <body class="page-session page-sound page-header-fixed page-sidebar-fixed">
        <section id="wrapper">
            <!-- START @WRAPPER  -->

            <!-- START @HEADER  -->
            @include('krauff::layouts.menu.menu_top')
            <!--/ END HEADER -->

            @include('krauff::layouts.menu.menu_left')
            <!--/ END SIDEBAR LEFT -->
            <!--/ END SIDEBAR LEFT -->

            <!-- START @PAGE  CONTENT -->
            <!-- Aqui es donde ustedes trabajan perros  -->
            @yield('content')

            <!-- START @SIDEBAR  RIGHT -->
            @include('krauff::layouts.menu.menu_right')
            <!--/ END SIDEBAR RIGHT -->

            <!-- START @ALL  MODALS --> 
            @yield('modales')            
            <!--/ END ALL MODALS -->
            
            <!-- LOADING AJAX --> 
            <div class="loading"></div>
            <!-- LOADING AJAX --> 

            <!--/ END WRAPPER -->
        </section>
        
        <!-- START @CORE  JS -->
        <script src="{{ asset(mix('/js/app.js'), Request::secure()) }}"></script>
        <script src="{{ asset(mix('/js/all.js'), Request::secure()) }}"></script>
        <script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
         <!-- END @CORE  JS -->
    
        <!-- SOCKET IO-->
        @if(Config::get('notificaciones.ENABLED'))
        <script type="text/javascript" src="{{Config::get('notificaciones.SERVER_NODE')}}/socket.io/socket.io.js"></script>
        <script type="text/javascript">
                var pathsounds = baseurl+ "public/js/ion.sound/sounds/";
                var socket = io.connect('{{Config::get('notificaciones.SERVER_NODE')}}');
                socket.on('connect', function () {
                    localStorage.setItem('uUID', {{Auth::user()->codusuario}});
                    socket.emit('subscribe_notifications', localStorage.getItem('uUID'));
                });
        </script>
        <!-- SOCKET IO-->

        <!-- NOTIFICATIONS-->
        <script type="text/javascript" src="{{ asset('js/notificaciones/notificaciones.js', Request::secure())}}"></script>
        <!-- NOTIFICATIONS-->
        @endif

        <!-- START JS ADITIONAL -->        
        @yield('js')         
        <!--/ END JS ADITIONAL -->
        
        @include('krauff::layouts.partials.msg')
    </body>
</html>