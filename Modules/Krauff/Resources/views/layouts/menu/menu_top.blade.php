<!-- START @HEADER  -->
<!-- START @HEADER  -->
<header id="header">

    <style type="text/css" >
        #sidebar-left .sidebar-menu ul li.active:after {
            background-color: #00B1E1 !important;
        }
    </style>

    <!-- Start header left -->
    <div class="header-left">
        <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
        <div class="navbar-minimize-mobile left">
            <i class="fa fa-bars"></i>
        </div>
        <!--/ End offcanvas left -->

        <!-- Start navbar header -->
        <div class="navbar-header">

            <!-- Start brand -->
            <a class="navbar-brand" href="{{url('/home')}}">
                <img class="logo" src="{{ asset('images/logo.png', Request::secure()) }}" alt="brand logo"/>
            </a><!-- /.navbar-brand -->
            <!--/ End brand -->

        </div><!-- /.navbar-header -->
        <!--/ End navbar header -->

        <!-- Start offcanvas right: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
         <!--<div class="navbar-minimize-mobile right">
            <i class="fa fa-cog"></i>
        </div>-->
        <!--/ End offcanvas right -->

        <div class="clearfix"></div>
    </div><!-- /.header-left -->
    <!--/ End header left -->

    <!-- Start header right -->
    <div class="header-right">
        <!-- Start navbar toolbar -->
        <div class="navbar navbar-toolbar navbar-primary">

            <!-- Start left navigation -->
            <ul class="nav navbar-nav navbar-left">

                <!-- Start sidebar shrink -->
                <li class="navbar-minimize">
                    <a href="javascript:void(0);" title="Minimize sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <!--/ End sidebar shrink -->
                <!-- Start sidebar shrink -->
                <li >
                    <div id="date-time" style="padding-left: 10px;padding-top: 16px;padding-bottom: 13px;"></div>
                </li>
                <!--/ End sidebar shrink -->

                <!-- Start form search -->
                <li class="navbar-search">
                    <!-- Just view on mobile screen
                    <a href="javascript:void(0)" class="trigger-search"><i class="fa fa-search"></i></a>
                    <form class="navbar-form">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control typeahead rounded" placeholder="Search for people, places and things">
                            <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                        </div>
                    </form>
                </li>-->
                <!--/ End form search -->

            </ul><!-- /.nav navbar-nav navbar-left -->
            <!--/ End left navigation -->

            <!-- Start right navigation -->
            <ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->


                <!-- Start notifications -->
                {{-- <li class="dropdown navbar-notification">

                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="count label label-danger rounded cantNotificaciones">0</span></a>

                    <!-- Start dropdown menu -->
                    <div class="dropdown-menu animated flipInX">
                        <div class="dropdown-header">
                            <span class="title">Notificaciones <strong>(<span class="cantNotificaciones">0</span>)</strong></span>
                            <span class="option text-right"><a id="delete-all-notifications" href="javascript:void(0)"><i class="fa fa-trash"></i></a></span>
                        </div>
                        <div class="dropdown-body niceScroll">

                            <!-- Start notification list -->
                            <div class="media-list small media-notificaciones">
                            @foreach ($notificaciones as $notificacion)
                                <a href="{{ route('krauff.leernotificacion', Crypt::encrypt($notificacion->codnotificacion)) }}" class="media media-{{$notificacion->tipo}} {{ ($notificacion->leida == 2) ? "note-leida" : "note-noleida" }}">
                                    <div class="media-object pull-left"><i class="{{$notificacion->icono}} fg-success"></i></div>
                                    <div class="media-body">
                                        <span class="media-text">{{$notificacion->mensaje}}</span>
                                        <span class="media-meta">{{ Carbon::createFromFormat('Y-m-d H:i:s', $notificacion->created_at)->diffForHumans() }}</span>
                             
                                    </div>
                                </a>
                            @endforeach

                            

                            </div>
                            <!--/ End notification list -->

                        </div>
                        <!--
                        <div class="dropdown-footer">
                            <a href="javascript:void(0)">Ver todas</a>
                        </div>-->
                    </div>
                    <!--/ End dropdown menu -->

                </li><!-- /.dropdown navbar-notification --> --}}
                <!--/ End notifications -->

                <!-- Start profile -->
                <li class="dropdown navbar-profile">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="meta">
                            <span class="avatar"><img src="{{session('imagen_perfil')}}" class="img-circle" alt="admin"></span>
                            <span class="text hidden-xs hidden-sm text-muted">{{ Auth::user()->nombres }}</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <!-- Start dropdown menu -->
                    <ul class="dropdown-menu animated flipInX">
                        <li class="dropdown-header">Cuenta</li>
                        <li><a href="#/krauff/verperfil"><i class="fa fa-user"></i>Ver perfil</a></li>
                        <!--<li><a href="http://localhost:8090/mail/inbox"><i class="fa fa-envelope-square"></i>Inbox <span class="label label-info pull-right">30</span></a></li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-share-square"></i>Invite a friend</a></li>-->
                        <!--<li class="dropdown-header">Product</li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-upload"></i>Upload</a></li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-dollar"></i>Earning</a></li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-download"></i>Withdrawals</a></li>-->
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Salir</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                    <!--/ End dropdown menu -->
                </li><!-- /.dropdown navbar-profile -->
                <!--/ End profile -->

                <!-- 
                <li class="navbar-setting pull-right">
                    <a href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a>
                </li>
                 -->

            </ul>
            <!--/ End right navigation -->

        </div><!-- /.navbar-toolbar -->
        <!--/ End navbar toolbar -->
    </div><!-- /.header-right -->
    <!--/ End header left -->

</header> <!-- /#header -->
<!--/ END HEADER -->
<!--/ END HEADER -->
