<aside id="sidebar-right">
    <div class="panel panel-tab">
        <div class="panel-heading no-padding">
            <div class="pull-right">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#sidebar-profile" data-toggle="tab">
                            <i class="fa fa-user"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#sidebar-layout" data-toggle="tab">
                            <i class="fa fa-cogs"></i>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#sidebar-setting" data-toggle="tab">
                            <i class="fa fa-paint-brush"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#sidebar-chat" data-toggle="tab">
                            <i class="fa fa-comments"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body no-padding">
            <div class="tab-content">
                <div class="tab-pane" id="sidebar-profile">
                    <div class="sidebar-profile">

                        <!-- Start right navigation - menu -->
                        <ul class="sidebar-menu niceScroll">

                        </ul>
                        <!-- Start right navigation - menu -->
                    </div>
                </div><!-- /#sidebar-profile -->
                <div class="tab-pane" id="sidebar-layout">
                    <div class="sidebar-layout">

                        <!-- Start right navigation - menu -->
                        <ul class="sidebar-menu niceScroll">

                        </ul>
                        <!-- Start right navigation - menu -->
                    </div>
                </div><!-- /#sidebar-layout -->
                <div class="tab-pane in active" id="sidebar-setting">
                    <div class="sidebar-setting">
                        <!-- Start right navigation - menu -->
                        <ul class="sidebar-menu">


                        </ul>
                        <!-- Start right navigation - menu -->
                    </div>
                </div><!-- /#sidebar-setting -->
                <div class="tab-pane" id="sidebar-chat">
                    <div class="sidebar-chat">

                    </div><!-- /.sidebar-setting -->
                </div><!-- /#sidebar-chat -->
            </div>
        </div>
    </div>

</aside><!-- /#sidebar-right -->