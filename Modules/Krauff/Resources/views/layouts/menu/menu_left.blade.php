    <!--

    START @SIDEBAR  LEFT
    |=========================================================================================================================|
    |  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
    |=========================================================================================================================|
    |  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
    |  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
    |  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
    |=========================================================================================================================|

    -->
    <!--
START @SIDEBAR  LEFT
|=========================================================================================================================|
|  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
|=========================================================================================================================|
|  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
|  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
|  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
|=========================================================================================================================|

    -->
 {!! $menu !!}
        
    <!--/ END SIDEBAR LEFT -->
    <!--/ END SIDEBAR LEFT -->

  