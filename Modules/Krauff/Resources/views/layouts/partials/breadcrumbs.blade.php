<ol class="breadcrumb">
    <li >
        <i class="fa fa-home"></i>
        <a href="{{url('/home')}}">Inicio</a>
        <i class="fa fa-angle-right"></i>
    </li>
    @if(!empty($breadcrumbs))
    @foreach($breadcrumbs as $bread)
    @if(isset($bread['url']))
    <li>{!! link_to($bread['url'], $bread['name']) !!}
        <i class="fa fa-angle-right"></i>
    </li>
    @else
    <li class='active'>{!! $bread['name'] !!}</li>
    @endif
    @endforeach
    @endif
</ol>