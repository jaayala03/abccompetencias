<script>
@foreach (['msg'] as $msg)
    @if(Session::has($msg))
        notifier.show('{{ Session::get($msg)["title"] }}', '{{ Session::get($msg)["msj"] }}','{{ Session::get($msg)["type"] }}', 5000);
    @endif
@endforeach

@if(Session::has('welcome'))     
setTimeout(function () {
    var unique_id_msg_wel = $.gritter.add({
        title: 'Bienvenido(a)',
        text: '{{ Session::get('welcome') }}',
        image: '{{ session('imagen_perfil') }}',
        sticky: false,
        time: ''
    });

    setTimeout(function () {
        $.gritter.remove(unique_id_msg_wel, {
            fade: true,
            speed: 'slow'
        });
    }, 15000);
}, 5000); 
@endif
</script>