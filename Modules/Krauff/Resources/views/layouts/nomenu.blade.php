<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Aplicacion ABCcomptencias">
        <meta name="keywords" content="ABCcomptencias">
        <meta name="author" content="jaayala03">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{$parametros['meta_title']}}</title>

        <!-- START @GLOBAL  MANDATORY STYLES -->
        <link href="{{ asset(mix('/css/app.css'), Request::secure()) }}" rel="stylesheet">
        <link href="{{ asset(mix('/css/all.css'), Request::secure()) }}" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- CSS Files -->
        @yield('css')
        
        <!-- Variable Global JS -->
        <script>
            var baseurl = @json(url('/'));
            var session = {warnings: @json(session('warnings'))};
            window.nomenu = true;
        </script>

    </head>

    <body>
        @yield('content') 
    </body>

    <!-- START @CORE  JS -->
    <script src="{{ asset(mix('/js/app.js'), Request::secure()) }}"></script>
    <script src="{{ asset(mix('/js/all.js'), Request::secure()) }}"></script>
    <!-- END @CORE  JS -->

    @yield('js')       
    

</html>
