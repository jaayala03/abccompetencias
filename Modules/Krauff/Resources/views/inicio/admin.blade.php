@extends('krauff::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/inicio/admin.js') }}" type="text/javascript"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-home"></i>Admin <span>Inicio</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">Usted esta en:</span>
            @include('krauff::layouts.partials.breadcrumbs')
        </div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn" id="dashboard" v-cloak>

        <div class="row top17">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-5 border-facebook shadow">
                    <h3>Trámites <span class="label label-inverse label-circle tooltips" data-toggle="tooltip" data-placement="top" data-title="Total tramites hoy" data-original-title="" title=""><i class="fa fa-question"></i></span></h3>
                    <h1>
                        <span class="status">
                            <span :class="{'percent': true, 'fg-danger': dashboard['section1']['tramites']['decrease'] <= 0, 'fg-success': dashboard['section1']['tramites']['decrease'] > 0}"><span class="counter">@{{dashboard['section1']['tramites']['decrease']}}</span>%</span>

                            <i :class="{'fa': true, 'fa-angle-down fg-danger': dashboard['section1']['tramites']['decrease'] <= 0, 'fa-angle-up fg-success': dashboard['section1']['tramites']['decrease'] > 0}"></i>
                        </span>
                        <span class="number counter">@{{dashboard['section1']['tramites']['count']}}</span>
                    </h1>
                    <p class="text-muted">Comparado con el día anterior.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-5 border-facebook shadow">
                    <h3>Impuestos <span class="label label-inverse label-circle tooltips" data-toggle="tooltip" data-placement="top" data-title="Total impuestos hoy" data-original-title="" title=""><i class="fa fa-question"></i></span></h3>
                    <h1>
                        <span class="status">
                            <span :class="{'percent': true, 'fg-danger': dashboard['section1']['impuestos']['decrease'] <= 0, 'fg-success': dashboard['section1']['impuestos']['decrease'] > 0}"><span class="counter">@{{dashboard['section1']['impuestos']['decrease']}}</span>%</span>

                            <i :class="{'fa': true, 'fa-angle-down fg-danger': dashboard['section1']['impuestos']['decrease'] <= 0, 'fa-angle-up fg-success': dashboard['section1']['impuestos']['decrease'] > 0}"></i>
                        </span>
                        <span class="number counter">@{{dashboard['section1']['impuestos']['count']}}</span>
                    </h1>
                    <p class="text-muted">Comparado con el día anterior.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-5 border-facebook shadow">
                    <h3>Parqueaderos <span class="label label-inverse label-circle tooltips" data-toggle="tooltip" data-placement="top" data-title="Total parqueaderos hoy" data-original-title="" title=""><i class="fa fa-question"></i></span></h3>
                    <h1>
                        <span class="status">
                            <span :class="{'percent': true, 'fg-danger': dashboard['section1']['parqueaderos']['decrease'] <= 0, 'fg-success': dashboard['section1']['parqueaderos']['decrease'] > 0}"><span class="counter">@{{dashboard['section1']['parqueaderos']['decrease']}}</span>%</span>

                            <i :class="{'fa': true, 'fa-angle-down fg-danger': dashboard['section1']['parqueaderos']['decrease'] <= 0, 'fa-angle-up fg-success': dashboard['section1']['parqueaderos']['decrease'] > 0}"></i>
                        </span>
                        <span class="number counter">@{{dashboard['section1']['parqueaderos']['count']}}</span>
                    </h1>
                    <p class="text-muted">Comparado con el día anterior.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-5 border-facebook shadow">
                    <h3>Comparendos <span class="label label-inverse label-circle tooltips" data-toggle="tooltip" data-placement="top" data-title="Total comparendos hoy" data-original-title="" title=""><i class="fa fa-question"></i></span></h3>
                    <h1>
                        <span class="status">
                            <span :class="{'percent': true, 'fg-danger': dashboard['section1']['comparendos']['decrease'] <= 0, 'fg-success': dashboard['section1']['comparendos']['decrease'] > 0}"><span class="counter">@{{dashboard['section1']['comparendos']['decrease']}}</span>%</span>

                            <i :class="{'fa': true, 'fa-angle-down fg-danger': dashboard['section1']['comparendos']['decrease'] <= 0, 'fa-angle-up fg-success': dashboard['section1']['comparendos']['decrease'] > 0}"></i>
                        </span>
                        <span class="number counter">@{{dashboard['section1']['comparendos']['count']}}</span>
                    </h1>
                    <p class="text-muted">Comparado con el día anterior.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div :class="{'mini-stat-type-2 shadow': true, 'border-danger':dashboard['section2']['diario']['decrease'] <= 0, 'border-success': dashboard['section2']['diario']['decrease'] > 0 }">
                    <h3 class="text-center text-thin">Diario</h3>
                    <p class="text-center">
                        <span :class="{'overview-icon': true , 'bg-danger': dashboard['section2']['diario']['decrease'] <= 0,  'bg-success': dashboard['section2']['diario']['decrease'] > 0}"><i :class="{'icon-arrow-down': dashboard['section2']['diario']['decrease'] <= 0, 'icon-arrow-up': dashboard['section2']['diario']['decrease'] > 0 }"></i></span>
                    </p>
                    <p>
                        <b>$<span class="counter">@{{dashboard['section2']['diario']['count']}}</span></b> <span :class="{'fg-danger': dashboard['section2']['diario']['decrease'] <= 0, 'fg-success': dashboard['section2']['diario']['decrease'] > 0}"><span class="counter">@{{ dashboard['section2']['diario']['decrease'] }}</span>%</span>
                    </p>
                    <p class="text-muted">
                        último día: $<span class="counter">@{{dashboard['section2']['diario_old']['count']}}</span>
                    </p>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div :class="{'mini-stat-type-2 shadow': true, 'border-danger':dashboard['section2']['semanal']['decrease'] <= 0, 'border-success': dashboard['section2']['semanal']['decrease'] > 0 }">
                    <h3 class="text-center text-thin">Semanal</h3>
                    <p class="text-center">
                        <span :class="{'overview-icon': true , 'bg-danger': dashboard['section2']['semanal']['decrease'] <= 0,  'bg-success': dashboard['section2']['semanal']['decrease'] > 0}"><i :class="{'icon-arrow-down': dashboard['section2']['semanal']['decrease'] <= 0, 'icon-arrow-up': dashboard['section2']['semanal']['decrease'] > 0 }"></i></span>
                    </p>
                    <p>
                        <b>$<span class="counter">@{{dashboard['section2']['semanal']['count']}}</span></b> <span :class="{'fg-danger': dashboard['section2']['semanal']['decrease'] <= 0, 'fg-success': dashboard['section2']['semanal']['decrease'] > 0}"><span class="counter">@{{ dashboard['section2']['semanal']['decrease'] }}</span>%</span>
                    </p>
                    <p class="text-muted">
                        última semana: $<span class="counter">@{{dashboard['section2']['semanal_old']['count']}}</span>
                    </p>
                </div>
            </div>
            <div class="col-md-6">

                <!-- Start list bank table -->
                <div class="panel panel-sh">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Banco - <b>Ingresos</b></h3>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body no-padding">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="pull-left text-capitalize">Ingresos tramites</span>
                                        <span class="pull-right text-strong fg-teals">$<span class="counter">@{{dashboard['section2']['tramites']['count']}}</span></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="pull-left text-capitalize">Ingresos comparendos:</span>
                                        <span class="pull-right text-strong fg-teals">$<span class="counter">@{{dashboard['section2']['multas']['count']}}</span></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="pull-left text-capitalize">Ingresos otros pagos:</span>
                                        <span class="pull-right text-strong">$<span class="counter">@{{dashboard['section2']['otros_pagos']['count']}}</span></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.panel-body -->
                </div>
                <!--/ End list bank table -->

            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-4 bg-success shadow">
                    <h4>Fact. Emitidas</h4>
                    <h3 class="counter">@{{ dashboard['section3']['emitidas']['count'] }}</h3>
                    <a href="#/facturas/historico" class="btn btn-success">VER</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-4 bg-danger shadow">
                    <h4>Fact. Pagadas</h4>
                    <h3 class="counter">@{{ dashboard['section3']['pagadas']['count'] }}</h3>
                    <a href="#/facturas/historico" class="btn btn-danger">VER</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-4 bg-warning shadow">
                    <h4>Fact. Utilizadas</h4>
                    <h3 class="counter">@{{ dashboard['section3']['utilizadas']['count'] }}</h3>
                    <a href="#/facturas/historico" class="btn btn-warning">VER</a>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat-type-4 bg-lilac shadow">
                    <h4>Fact. Anuladas</h4>
                    <h3 class="counter">@{{ dashboard['section3']['anuladas']['count'] }}</h3>
                    <a href="#/facturas/historico" class="btn btn-lilac">VER</a>
                </div>
            </div>

            
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-facebook rounded">
                    <span class="mini-stat-icon"><i class="fa fa-car fg-facebook"></i></span><!-- /.mini-stat-icon -->
                    <div class="mini-stat-info">
                        <span class="counter">@{{ dashboard['section4']['particular']['count'] }}</span>
                        Vehículos particulares
                    </div><!-- /.mini-stat-info -->
                </div><!-- /.mini-stat -->
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-twitter rounded">
                    <span class="mini-stat-icon"><i class="fa fa-car fg-twitter"></i></span><!-- /.mini-stat-icon -->
                    <div class="mini-stat-info">
                        <span class="counter">@{{ dashboard['section4']['publico']['count'] }}</span>
                        Vehículos públicos
                    </div><!-- /.mini-stat-info -->
                </div><!-- /.mini-stat -->
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-googleplus rounded">
                    <span class="mini-stat-icon"><i class="fa fa-car fg-googleplus"></i></span><!-- /.mini-stat-icon -->
                    <div class="mini-stat-info">
                        <span class="counter">@{{ dashboard['section4']['oficial']['count'] }}</span>
                        Vehículos oficiales
                    </div><!-- /.mini-stat-info -->
                </div><!-- /.mini-stat -->
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="mini-stat clearfix bg-bitbucket rounded">
                    <span class="mini-stat-icon"><i class="fa fa-car fg-bitbucket"></i></span><!-- /.mini-stat-icon -->
                    <div class="mini-stat-info">
                        <span class="counter">@{{ dashboard['section4']['otro']['count'] }}</span>
                        Vehículos otro servicio
                    </div>
                </div><!-- /.mini-stat-info -->
            </div><!-- /.mini-stat -->
        </div>





    </div><!-- /.body-content -->
    <!--/ End body content -->



</section><!-- /#page-content -->
<!--/ END PAGE CONTENT -->
@endsection
