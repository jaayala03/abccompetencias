@extends('krauff::layouts.master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-home"></i>Contruccion <span>Inicio</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">Usted esta en:</span>
            @include('krauff::layouts.partials.breadcrumbs')            
        </div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn" style="min-height: 600px;">

        <div class="row">
            <div class="col-md-12">

                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <h3 class="panel-title">Página en construcción</h3>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body">
                        Esta página se encuentra actualmente en construcción.
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->

            </div>
        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->



</section><!-- /#page-content -->
<!--/ END PAGE CONTENT -->
@endsection
