<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

<script>
    // If absolute URL from the remote server is provided, configure the CORS
    // header on that server.
    var url = 'http://localhost:8000/krauff/getfile/eyJpdiI6IjNKRW9PcTFNUlptaVRRYWY5U1pVS3c9PSIsInZhbHVlIjoiU1p6THFjN0grTmNnSXAwWEFpejJkdz09IiwibWFjIjoiZjU0MjUxNDk2NzhmMThjZDZlZjBjYTg4ZWZjYjkwMDk1Y2E0M2M0NGY3OGEzZmFmMWZlZTcxNzI2ZDI4N2IxNyJ9?option=eyJpdiI6IjQrZFdUZ0ZBS1wvY2sxaFMrcDRLcHdnPT0iLCJ2YWx1ZSI6InV0aG5oWU5ZVlwvaHp4YVFkckdUWFdnPT0iLCJtYWMiOiI2NTg3MDAxODRkYTk4ZTI0MmU2NmUwNDBmZGZiN2NlNmM3YjA4MTcwZWZjZjRjMjM4NTJmNTRmMmEyZmY4YzViIn0%3D&type=eyJpdiI6IjhPVGxzT2QxdWNOZVF1b3BPQ2drTlE9PSIsInZhbHVlIjoiSW01aldVeG1XcEdXamNEWjkyK3g1QT09IiwibWFjIjoiZTdiM2VkMzRmMTJiYzdhNzg5NGU4OTE3YjE3MGY1ZmI1ODE3YTFhNDNhMTNiODVmYTRkZTZhNzU5YTQ3NWZlMCJ9';

    // Disable workers to avoid yet another cross-origin issue (workers need
    // the URL of the script to be loaded, and dynamically loading a cross-origin
    // script does not work).
    // PDFJS.disableWorker = true;

    // The workerSrc property shall be specified.
    PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

    // Asynchronous download of PDF
    var loadingTask = PDFJS.getDocument(url);
    loadingTask.promise.then(function (pdf) {
        console.log('PDF loaded');

        // Fetch the first page
        var pageNumber = 1;
        pdf.getPage(pageNumber).then(function (page) {
            console.log('Page loaded');

            var scale = 1.5;
            var viewport = page.getViewport(scale);

            // Prepare canvas using PDF page dimensions
            var canvas = document.getElementById('the-canvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.then(function () {
                console.log('Page rendered');
            });
        });
    }, function (reason) {
        // PDF loading error
        console.error(reason);
    });


</script> 

<canvas id="the-canvas"></canvas>