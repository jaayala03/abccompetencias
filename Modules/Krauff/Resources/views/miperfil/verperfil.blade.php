
@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- START @PAGE LEVEL PLUGINS -->

<!--/ END PAGE LEVEL PLUGINS -->
<!-- START @PAGE LEVEL SCRIPTS -->
<script src="{{ asset('modules/krauff/js/miperfil/verperfil.js') }}"></script>
<!--/ END PAGE LEVEL SCRIPTS -->
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->

@endsection


@section('content')
<!-- START @PAGE CONTENT -->
    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-child"></i> Perfil <span>Informacion del usuario</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">Usted esta en:</span>
            @include('krauff::layouts.partials.breadcrumbs') 
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4">

                <div class="panel rounded shadow">
                    <div class="panel-body">
                        <div class="inner-all">
                            <ul class="list-unstyled">
                                <li class="text-center">
                                    <img class="img-circle img-bordered-primary" src="{{session("imagen_perfil")}}" height="100" width="100">

                                </li>
                                <li class="text-center">
                                    <h4 class="text-capitalize">{{ Auth::user()->nombres }}</h4>
                                    <p class="text-muted text-capitalize"> {{session('perfil')}}</p>
                                </li>
                                <li>
                                    <a href="{{url("/")}}" class="btn btn-success text-center btn-block">Inicio</a>
                                </li>
                                <li><br/></li>
                                <li>
                                    <div class="btn-group-vertical btn-block">
                                        <a href="#/krauff/editarperfil" class="btn btn-default"><i class="fa fa-cog pull-right"></i>Editar Perfil</a>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();" class="btn btn-default"><i class="fa fa-sign-out pull-right"></i>Salir</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.panel -->

                <div class="panel panel-theme rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">Informacion Básica</h3>
                        </div>
                        <div class="pull-right">
<!--                                    <a href="javascript:void(0)" class="btn btn-sm btn-success"><i class="fa fa-facebook"></i></a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-success"><i class="fa fa-twitter"></i></a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-success"><i class="fa fa-google-plus"></i></a>-->
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body no-padding rounded">
                        <ul class="list-group no-margin">
                            <li class="list-group-item"><i class="fa fa-envelope mr-5"></i>{{Auth::user()->email}}</li>
                            <li class="list-group-item"><i class="fa fa fa-bars mr-5"></i>Tipo documento: {{ (Auth::user()->tipodoc) ? Auth::user()->tipodoc : "No registra Tipo documento"}}</li>
                            <li class="list-group-item"><i class="fa fa-file mr-5"></i>Documento: {{ (Auth::user()->documento) ? Auth::user()->documento : "No registra documento"}}</li>
                            <li class="list-group-item"><i class="fa fa-phone mr-5"></i> {{ (Auth::user()->telefono) ? Auth::user()->telefono : "No registra telefono"}}</li>
                            <li class="list-group-item"><i class="fa fa-phone mr-5"></i> {{ (Auth::user()->celular) ? Auth::user()->celular : "No registra celular"}}</li>
                        </ul>
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->

            </div>
            <div class="col-lg-9 col-md-9 col-sm-8">

                <div class="profile-cover">
             
                    <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-facebook rounded">
                                <span class="mini-stat-icon"><i class="fa fa-calendar fg-facebook"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span style="font-size: 25px">Ingresos al sistema</span>
                                    {{$totalaccesos}} Accesos
                                </div><!-- /.mini-stat-info -->
                            </div><!-- /.mini-stat -->
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-googleplus rounded">
                                <span class="mini-stat-icon"><i class="fa fa-clock-o fg-googleplus"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span style="font-size: 25px">Último Acceso</span>
                                    {{ Carbon::parse($ultimo->created_at)->format('d/m/Y h:i A')}}
                                </div><!-- /.mini-stat-info -->
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-tab panel-tab-double shadow">
                        <!-- Start tabs heading -->
                        <div class="panel-heading hidden-sm no-padding">
                            <ul class="nav nav-tabs">
                                <li class="active nav-border nav-border-top-success">
                                    <a href="#tab6-1" data-toggle="tab"><i class="fa fa-fw fa-clock-o"></i> 
                                        Mi actividad
                                    </a>
                                </li>
                                <li class="nav-border nav-border-top-danger"><a href="#tab6-2" data-toggle="tab"><i class="fa fa-fw fa-user fg-danger"></i> Datos Personales</a></li>
                            </ul>

                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab6-1">
                                <div class="profile-timeline">
                                    <!-- Start timeline -->

                                    <div class="timeline" >
                                        @foreach ($accesos->all() as $acceso)
                                        <div class="timeline-item alt">
                                            <div class="text-right">
                                                <div class="time-show">
                                                    <a href="javascript:void(0)" class="btn btn-danger">{{Carbon::parse($acceso->fechaingreso)->format('d/m/Y')}}</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="timeline-item alt">
                                            <div class="timeline-desk">
                                                <div class="panel panel-success shadow">
                                                    <div class="panel-body">
                                                        <span class="arrow-alt"></span>
                                                        <span class="timeline-icon bg-success circle"><i class="fa fa-user"></i></span>
                                                        <span class="timeline-date">10:00 am</span>
                                                        <h1 class="fg-success">Inicio de sesión</h1>
                                                        <p>Acceso al sistema</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <div class="timeline-desk">
                                                <div class="panel panel-info shadow">
                                                    <div class="panel-heading">
                                                        <div class="panel-title">Actividad</div>
                                                        <span class="arrow"></span>
                                                        <span class="timeline-icon bg-info"><i class="fa fa-history"></i></span>
                                                    </div>
                                                    <div class="panel-body rounded">
                                                        <ul class="list-unstyled no-margin">
                                                            @if($acceso->actividades->count() > 0)
                                                                @foreach ($acceso->actividades as $actividad)
                                                                    <li><i class="fa fa-book"></i> {{$actividad->description}} </li>
                                                                @endforeach
                                                            @else
                                                               <li> 
                                                               <div class="alert alert-danger" role="alert">
                                                                    <strong>No se encontraron datos</strong>
                                                                </div>
                                                               </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--<div class="timeline-item alt">
                                            <div class="text-right">
                                                <div class="time-show">
                                                    <a href="javascript:void(0)" class="btn btn-primary">Load More</a>
                                                </div>
                                            </div>
                                        </div>-->

                                        @endforeach

                                    </div>
                                    <!--/ End timeline -->
                                    <div class="text-center">
                                        {{ $accesos->links() }}
                                    </div>
                                </div>
                            </div><!-- /.profile-body -->
                            <!-- /.profile-cover -->
                            <div class="tab-pane fade" id="tab6-2">
                                <!-- Start X-Editable -->
                                <div class="panel panel-danger rounded shadow">
                                    <div class="panel-heading">
                                        <div class="pull-left">
                                            <h3 class="panel-title">Editar Datos personales</h3>
                                        </div><!-- /.pull-left -->
                                        <div class="pull-right">
                                    
                                        </div><!-- /.pull-right -->
                                        <div class="clearfix"></div>
                                    </div><!-- /.panel-heading -->
                                    <div class="panel-sub-heading inner-all">
                                        <div class="pull-right">
                                            <button id="enable" class="btn btn-primary">Activar / Desactivar Edición</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body">
                                        <table id="user" class="table table-bordered table-striped" style="clear: both">
                                            <tbody>
                                                <tr>
                                                    <td width="35%">Nombres</td>
                                                    <td width="65%"><a href="javascript:void(0)" id="nombres" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-title="Editar">{{Auth::user()->nombres}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Primer Apellido</td>
                                                    <td><a href="javascript:void(0)" id="primerapellido" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->primerapellido}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Segundo Apellido</td>
                                                    <td><a href="javascript:void(0)" id="segundoapellido" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->segundoapellido}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Documento</td>
                                                    <td><a href="javascript:void(0)" id="documento" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->documento}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><a href="javascript:void(0)" id="email" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->email}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Direccion</td>
                                                    <td><a href="javascript:void(0)" id="direccion" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->direccion}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Telefono</td>
                                                    <td><a href="javascript:void(0)" id="telefono" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->telefono}}</a></td>
                                                </tr>
                                                <tr>
                                                    <td>Celular</td>
                                                    <td><a href="javascript:void(0)" id="celular" data-type="text" data-pk="{{Auth::user()->codusuario}}" data-placement="right" data-placeholder="Requerido" data-title="Editar">{{Auth::user()->celular}}</a></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel -->
                                <!--/ End X-Editable -->
                            </div>

                        </div>
                    </div>
                </div><!-- /.row -->

            </div><!-- /.body-content -->
            <!--/ End body content -->
        </div>
    </div>
<!--/ END PAGE CONTENT -->
@endsection
