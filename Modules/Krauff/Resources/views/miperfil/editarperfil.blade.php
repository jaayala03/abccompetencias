@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/miperfil/miperfil.js') }}"></script>
<script>
$(document).ready(function() {
    $('#codubicacion').typeahead('val', '{{ (isset(Auth::user()->ubicacion->nombre) ? Auth::user()->ubicacion->nombre : null)}}');
    $("#codubicacion").prop("disabled", "{{ (isset(Auth::user()->ubicacion->nombre) ? "disabled" : null)}}");
    $('#codubicacionnacimiento').typeahead('val', '{{ isset(Auth::user()->ubicacionnacimiento->nombre) ? Auth::user()->ubicacionnacimiento->nombre : null}}');
    $("#codubicacionnacimiento").prop("disabled", "{{ (isset(Auth::user()->ubicacionnacimiento->nombre) ? "disabled" : null)}}");
});
</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link href="{{ asset('modules/krauff/css/custom.css') }}" rel="stylesheet" >
@endsection


@section('content')

 
    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-home"></i>Editar perfil <span>{{Auth::user()->nombres}}</span></h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">Usted esta en:</span>
            @include('krauff::layouts.partials.breadcrumbs') 
        </div>
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                <!-- Start basic wizard horizontal -->
                <div id="validacionusuario">
                    <div class="panel panel-tab rounded shadow">
                        <!-- Start tabs heading -->
                        <div class="panel-heading no-padding">
                            <ul class="nav nav-tabs">
                                <li class="success-step active">
                                    <a href="#tab1-1" data-toggle="tab">
                                        <i class="fa fa-user"></i>
                                        <div>
                                            <span class="text-strong">Datos Basicos</span>

                                        </div>
                                    </a>
                                </li>
                                <li class="success-step">

                                    <a href="#tab1-2" data-toggle="tab">
                                        <i class="fa fa-fw fa-photo"></i>
                                        <div>
                                            <span class="text-strong">Foto - Perfil</span>

                                        </div>
                                    </a>
                                </li>
                                <li class="success-step">
                                    <a href="#tab1-3" data-toggle="tab">
                                        <i class="fa fa-credit-card"></i>
                                        <div>
                                            <span class="text-strong">Usuario - Contraseña</span>

                                        </div>
                                    </a>
                                </li>

                            </ul>


                            <div class="pull-right" style="padding: 15px;">
                                {!! Form::submit('Editar',['class'=>'btn btn-primary btn-push', 'id'=>'btn-editar']) !!}

                            </div>

                        </div><!-- /.panel-heading -->
                        <!--/ End tabs heading -->

                        <!-- Start tabs content -->
                        <div class="panel-body">
                            {!! Form::open(array('id'=>'frm_editar_perfil', 'class'=>'tab-content form-horizontal', 'route' => array('krauff.updateperfil'))) !!}
                            {{Form::hidden('imagenAdjunta', null, ['id' => 'imagenAdjunta'])}}
                            {{Form::hidden('codubicacion_value', isset(Auth::user()->ubicacion->codubicacion) ? Auth::user()->ubicacion->codubicacion : null, ['id' => 'codubicacion_value'] )}}
                            {{Form::hidden('codubicacionnacimiento_value', isset(Auth::user()->ubicacionnacimiento->codubicacion) ? Auth::user()->ubicacionnacimiento->codubicacion : null, ['id' => 'codubicacionnacimiento_value'] )}}
                            <div class="tab-pane fade in active inner-all" id="tab1-1">
                                <div class="row">
                                    <div class="col-sm-4 b-r">                                                

                                        <div class="form-group">
                                            {{ Form::label('tipodoc', 'Tipo Documento') }}
                                            {{ Form::select('tipodoc', Config::get('dominios.TIPODOC.TXT'),Auth::user()->tipodoc, ['placeholder' => 'Seleccione tipo documento', 'class'=>'form-control']) }}

                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('documento', 'Documento') }}
                                            {{Form::text('documento',  Auth::user()->documento, ['class' => 'form-control'] )}}

                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('nombres', 'Nombres') }}
                                            {{Form::text('nombres', Auth::user()->nombres, ['class' => 'form-control'] )}}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('primerapellido', 'Primer Apellido') }}
                                            {{Form::text('primerapellido', Auth::user()->primerapellido, ['class' => 'form-control'] )}}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('segundoapellido', 'Segundo Apellido') }}
                                            {{Form::text('segundoapellido', Auth::user()->segundoapellido, ['class' => 'form-control'] )}}
                                        </div>
                                    </div>

                                    <div class="col-sm-4 b-r">                                                

                                        <div class="form-group">
                                            {{ Form::label('genero', 'Género') }}
                                            {{ Form::select('genero', Config::get('dominios.GENERO.TXT'), Auth::user()->genero, ['placeholder' => 'Seleccione género', 'class'=>'form-control'] ) }}

                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('telefono', 'Teléfono Fijo') }}
                                            {{Form::text('telefono', Auth::user()->telefono, ['class' => 'form-control'] )}}

                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('celular', 'Celular') }}
                                            {{Form::text('celular', Auth::user()->celular, ['class' => 'form-control'] )}}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('correo', 'Correo Electrónico') }}
                                            {{Form::email('correo', Auth::user()->email, ['class' => 'form-control'] )}}
                                        </div>
                                    </div>

                                    <div class="col-sm-4 b-r">                                                
                                        <div class="form-group">
                                            {{ Form::label('fechanacimiento', 'Fecha Nacimiento') }}
                                            {{ Form::text('fechanacimiento', Auth::user()->fechanacimiento, ['class' => 'form-control', 'placeholder' => 'aaaa/mm/dd']) }}

                                        </div>
                                        <div class="form-group">
                                                    {{ Form::label('codubicacionnacimiento', 'Lugar de nacimiento') }}
                                                    <div class="input-group">
                                                        {{Form::text('codubicacionnacimiento', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger remove-nacimiento" type="button">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                        </div>

                                        <div class="form-group">
                                                    {{ Form::label('codubicacion', 'Lugar de residencia') }}
                                                    <div class="input-group">
                                                        {{Form::text('codubicacion', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger remove-ubicacion" type="button">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </button>
                                                        </span>
                                                    </div>
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('direccion', 'Dirección') }}
                                            {{Form::text('direccion', Auth::user()->direccion, ['class' => 'form-control'] )}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade inner-all" id="tab1-2">
                                <div class="form-group">

                                    <div class="col-md-5">
                                        <div class="image-crop">
                                            <img src=""/>
                                        </div>

                                    </div>
                                    <label title="Adjuntar Imagen" for="FileInput" class="btn btn-success col-sm-2">
                                        <input type="file" accept="image/*" name="file" id="FileInput" class="hide">
                                        Examinar
                                    </label>
                                    <div class="col-sm-4">
                                        <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                                        <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                                        <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
                                        <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha</button>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="center-block">                                            
                                            <img class="img-circle center-block" src="{{session("imagen_perfil")}}" height="300" width="300"/>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade inner-all" id="tab1-3">
                                <div class="row">
                                    <div class="col-sm-6">    
                                        <div class="form-group">
                                            {{ Form::label('passwordantigua', 'Contraseña Anterior', ['class'=>'col-sm-6']) }}
                                            <div class="col-sm-6">
                                                {{ Form::password('passwordantigua', ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('password', 'Contraseña', ['class'=>'col-sm-6']) }}
                                            <div class="col-sm-6">
                                                {{ Form::password('password', ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('password2', 'Confirmar contraseña', ['class'=>'col-sm-6']) }}
                                            <div class="col-sm-6">
                                                {{ Form::password('password2', ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <img class="img-circle center-block" src="{{session("imagen_perfil")}}" height="300" width="300"/> </div>
                                    </div>                                           



                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div><!-- /.panel-body -->
                        <!--/ End tabs content -->

                        <!-- Start pager -->
                        <div class="panel-footer">
                            <ul class="pager wizard no-margin">
                                <button type="button" class="btn btn-danger previous pull-left">Atras</button>                                
                                <button type="button" class="btn btn-success next pull-right">Siguiente</button> 
                            </ul>
                        </div><!-- /.panel-footer -->
                        <!--/ End pager -->

                    </div><!-- /.panel -->
                </div><!-- /#basic-wizard-horizontal -->
                <!--/ End basic wizard horizontal-->
            </div>
        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->


<!--/ END PAGE CONTENT -->
@endsection
