@extends('krauff::layouts.modal_master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/usuarios/funcionalidades_adm_usuario.js') }}"></script>

<script>
    $(document).ready(function () {
        cargar(<?php echo $usuario->codusuario; ?>);

        $('#save').on('click', function () {
            $(this).prop("disabled","disabled");
            selected = $("#jstree").jstree('get_selected');
            
            guardar(<?php echo $usuario->codusuario; ?>);

        });
    });
    
    $('#reset').on('click', function () {
         $('#jstree').jstree(true).deselect_all();
    });
</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <p class="text-center">Arbol de funcionalidades</p>
                            </div>
                            <div class="panel-body">
                                <div id="jstree">

                                </div>
                                <hr>
                                <button id="save" class="btn btn-success btn-md">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

