@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/usuarios/usuarios.js') }}"></script>
<script src="{{ asset('modules/krauff/js/usuarios/usuarios_editar.js') }}"></script>
<script>

</script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
<link href="{{ asset('modules/krauff/css/custom.css') }}" rel="stylesheet" >
@endsection

@section('content')
<!-- START @PAGE CONTENT -->


<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-pencil"></i> Editar Usuario </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted esta en:</span>
        @include('krauff::layouts.partials.breadcrumbs')
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <!-- Start form validation wizard -->
            <div id="validacion">                        
                <div class="panel panel-tab rounded shadow">                         
                    <!-- Start tabs heading -->
                    <div class="panel-heading no-padding">                            
                        <ul class="nav nav-tabs">
                            <li class="success-step active">
                                <a href="#tab4-1" data-toggle="tab">
                                    <i class="fa fa-user"></i>
                                    <div>
                                        <span class="text-strong">Datos Basicos</span>
                                    </div>
                                </a>
                            </li>
                            <li class="success-step">
                                <a href="#tab4-2" data-toggle="tab">
                                    <i class="fa fa-desktop"></i>
                                    <div>
                                        <span class="text-strong">Sistema</span>
                                    </div>
                                </a>
                            </li>
                            <li class="success-step">
                                <a href="#tab4-3" data-toggle="tab">
                                    <i class="fa fa-picture-o"></i>
                                    <div>
                                        <span class="text-strong">Foto</span>
                                    </div>
                                </a>
                            </li>
                            <li class="success-step">
                                <a href="#tab4-4" data-toggle="tab">
                                    <i class="fa fa-key"></i>
                                    <div>
                                        <span class="text-strong">Contraseña</span>
                                    </div>
                                </a>
                            </li>  
                        </ul>
                        <div class="pull-right" style="padding: 15px;">
                            <a href="#/krauff/usuarios/" class="btn btn-default">Cerrar</a>
                            {!! Form::submit('Guardar',['class'=>'btn btn-danger', 'id'=>'btn-editar']) !!}

                        </div>
                    </div><!-- /.panel-heading -->
                    <!--/ End tabs heading -->
                    <!-- Start tabs content -->
                    <div class="panel-body">
                        {!! Form::open(array('id'=>'frm_editar_usuario', 'class'=>'tab-content form-horizontal', 'route' => array('krauff.usuarios.update', $idcifrado))) !!}
                        {{Form::hidden('imagenAdjunta', null, ['id' => 'imagenAdjunta'])}}
                        {{Form::hidden('codusuario', $idcifrado)}}
                        {{Form::hidden('codubicacion_value', isset($usuario->ubicacion->codubicacion) ? $usuario->ubicacion->codubicacion : null, ['id' => 'codubicacion_value'] )}}
                        {{Form::hidden('codubicacionnacimiento_value', isset($usuario->ubicacionnacimiento->codubicacion) ? $usuario->ubicacionnacimiento->codubicacion : null, ['id' => 'codubicacionnacimiento_value'] )}}
                        <div class="tab-pane fade in active inner-all" id="tab4-1">
                            <div class="row">
                                <div class="col-sm-4 b-r">                                                

                                    <div class="form-group">
                                        {{ Form::label('tipodoc', 'Tipo Documento') }}
                                        {{ Form::select('tipodoc', Config::get('dominios.TIPODOC.TXT'), $usuario->tipodoc, ['placeholder' => 'Seleccione tipo documento', 'class'=>'form-control']) }}

                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('documento', 'Documento') }}
                                        {{Form::text('documento', $usuario->documento, ['class' => 'form-control'] )}}

                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('nombres', 'Nombres') }}
                                        {{Form::text('nombres', $usuario->nombres, ['class' => 'form-control'] )}}
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('primerapellido', 'Primer Apellido') }}
                                        {{Form::text('primerapellido', $usuario->primerapellido, ['class' => 'form-control'] )}}
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('segundoapellido', 'Segundo Apellido') }}
                                        {{Form::text('segundoapellido', $usuario->segundoapellido, ['class' => 'form-control'] )}}
                                    </div>
                                </div>

                                <div class="col-sm-4 b-r">                                                

                                    <div class="form-group">
                                        {{ Form::label('genero', 'Género') }}
                                        {{ Form::select('genero', Config::get('dominios.GENERO.TXT'), $usuario->genero, ['placeholder' => 'Seleccione género', 'class'=>'form-control'] ) }}

                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('telefono', 'Teléfono Fijo') }}
                                        {{Form::text('telefono', $usuario->telefono, ['class' => 'form-control'] )}}

                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('celular', 'Celular') }}
                                        {{Form::text('celular', $usuario->celular, ['class' => 'form-control'] )}}
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('email', 'Correo Electrónico') }}
                                        {{Form::email('email', $usuario->email, ['class' => 'form-control'] )}}
                                    </div>
                                </div>

                                <div class="col-sm-4 b-r">                                                

                                    <div class="form-group">
                                        {{ Form::label('fechanacimiento', 'Fecha Nacimiento') }}
                                        {{ Form::text('fechanacimiento', $usuario->fechanacimiento, ['class' => 'form-control', 'placeholder' => 'aaaa-mm-dd']) }}

                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('codubicacionnacimiento', 'Lugar de nacimiento') }}
                                        <div class="input-group">
                                            {{Form::text('codubicacionnacimiento', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger remove-nacimiento" type="button">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('codubicacion', 'Lugar de residencia') }}
                                        <div class="input-group">
                                            {{Form::text('codubicacion', null, ['class' => 'form-control','autocomplete'=>'off', 'placeholder'=>'Ingrese una ubicacion'] )}}
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger remove-ubicacion" type="button">
                                                    <span class="glyphicon glyphicon-remove"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        {{ Form::label('direccion', 'Dirección') }}
                                        {{Form::text('direccion', $usuario->direccion, ['class' => 'form-control'] )}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade inner-all" id="tab4-2">                                           
                            <div class="form-group">
                                {{ Form::label('codperfil', 'Perfil', ['class'=>'col-sm-2']) }}
                                <div class="col-sm-4">
                                    {{ Form::select('codperfil', $perfiles, $usuario->codperfil, ['placeholder' => 'Seleccione perfil', 'class'=>'form-control'] ) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('estado', 'Estado', ['class'=>'col-sm-2']) }}
                                <div class="col-sm-4">
                                    {{ Form::select('estado', Config::get('dominios.ESTADOUSU.TXT'), $usuario->estado, ['placeholder' => 'Seleccione estado', 'class'=>'form-control'] ) }}
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade inner-all" id="tab4-3">

                            <div class="form-group">

                                <div class="col-md-5">
                                    <div class="image-crop center-block">                                            
                                        <img src=""/>
                                    </div>

                                </div>
                                <label title="Adjuntar Imagen" for="FileInput" class="btn btn-success col-sm-2">
                                    <input type="file" accept="image/*" name="file" id="FileInput" class="hide">
                                    Examinar
                                </label>
                                <div class="col-sm-4">
                                    <button class="btn btn-white" id="zoomIn" type="button">Zoom +</button>
                                    <button class="btn btn-white" id="zoomOut" type="button">Zoom -</button>
                                    <button class="btn btn-white" id="rotateLeft" type="button">Rotar Izquierda</button>
                                    <button class="btn btn-white" id="rotateRight" type="button">Rotar Derecha</button>
                                </div>
                                <div class="col-md-5">
                                    <div class="center-block">                                            
                                        <img class="img-circle center-block" src="{{route('krauff.getimage',['id'=>$idcifrado,'option'=>Crypt::encrypt(1)])}}" height="300" width="300"/>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade inner-all" id="tab4-4">
                            <div class="row">
                                <div class="col-sm-6">    
                                    <div class="form-group">
                                        {{ Form::label('reestablecer', 'Reestablecer contraseña', ['class'=>'col-sm-6']) }}
                                        <div class="col-sm-6">
                                            {{ Form::button('Restablecer', ['id'=>'btn-resetclave','class'=>'btn btn-success center-block']) }}
                                        </div>
                                    </div>                                            
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">                                            

                                    </div>
                                </div>  
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div><!-- /.panel-body -->
                    <!--/ End tabs content -->

                    <!-- Start pager -->
                    <div class="panel-footer">
                        <ul class="pager wizard no-margin">
                            <button type="button" class="btn btn-danger previous pull-left">Atras</button>                                
                            <button type="button" class="btn btn-success next pull-right">Siguiente</button> 
                        </ul>
                    </div><!-- /.panel-footer -->
                    <!--/ End pager -->

                </div><!-- /.panel -->
            </div>
            <!--/ End form validation wizard-->           
        </div>
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->


<!-- Lost password form -->
<form id="formresetpassword" class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
    {{ csrf_field() }}
    <input type="hidden" name="email" value="{{$usuario->email}}" >
</form>
<!--/ Lost password form -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection