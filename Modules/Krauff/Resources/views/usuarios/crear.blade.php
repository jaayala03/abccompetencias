@extends('krauff::layouts.modal_master')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/krauff/js/usuarios_crear.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">            
            <!-- Start default tabs -->
            <div class="panel panel-tab rounded shadow">
                <!-- Start tabs heading -->
                <div class="panel-heading no-padding">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab1-1" data-toggle="tab">
                                <i class="fa fa-user"></i>
                                <span>Datos Basicós</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab1-2" data-toggle="tab">
                                <i class="fa fa-file-text"></i>
                                <span>Product</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab1-3" data-toggle="tab">
                                <i class="fa fa-credit-card"></i>
                                <span>Payment</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab1-4" data-toggle="tab">
                                <i class="fa fa-check-circle"></i>
                                <span>Confirmation</span>
                            </a>
                        </li>
                    </ul>
                </div><!-- /.panel-heading -->
                <!--/ End tabs heading -->

                <!-- Start tabs content -->
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1-1">
                            <h4>Personal details content</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="tab-pane fade" id="tab1-2">
                            <h4>Product details content</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="tab-pane fade" id="tab1-3">
                            <h4>Payment content</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="tab-pane fade" id="tab1-4">
                            <h4>Confirmation content</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div><!-- /.panel-body -->
                <!--/ End tabs content -->
            </div><!-- /.panel -->
            <!--/ End default tabs -->
        </div>
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->



@endsection
