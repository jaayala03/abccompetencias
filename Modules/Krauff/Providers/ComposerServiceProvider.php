<?php

namespace Modules\Krauff\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot() {
        View::composer(
                ['krauff::layouts.menu.menu_left'], 'Modules\Krauff\Http\ViewComposers\MenuComposer'
        );
        // View::composer(
        //         ['krauff::layouts.menu.menu_top'], 'Modules\Krauff\Http\ViewComposers\NotificacionesComposer'
        // );
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return [];
    }

}
