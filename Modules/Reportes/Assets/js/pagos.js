$(document).ready(function() {
    tbl_pagos = $("#tbl_pagos").DataTable({
        orderClasses: false,
        processing: true,
        responsive: true,
        serverSide: true,
        ajax: {
            url: baseurl + "/reportes/todosdatosPagos",
            type: "POST",
            data: function(d) {
                d.fecha_inicio = app.fecha_inicio;
                d.fecha_fin = app.fecha_fin;
                d.codnorma = app.codnorma;
                d.codusuario = app.codusuario;
            }
        },
        fnDrawCallback: function(oSettings, json) {
            $(".tooltip-opcion").tooltip();
            $(".image-lightbox").featherlight({
                type: "image"
            });
        },
        columns: [{
                data: "transaction_id",
                name: "pagos_solicitudes.transaction_id"
            },
            {
                data: "documento",
                name: "usuarios.documento"
            },
            {
                data: "candidato",
                name: "usuarios.nombres"
            },
            {
                data: "candidato",
                name: "usuarios.primerapellido",
                visible: false
            },
            {
                data: "candidato",
                name: "usuarios.segundoapellido",
                visible: false
            },
            {
                data: "codigonorma",
                name: "normas.codigo"
            },
            {
                className: "text-center",
                data: "fechapago",
                name: "pagos_solicitudes.created_at"
            },
            {
                data: "value",
                name: "pagos_solicitudes.value"
            },
            {
                data: "state",
                name: "pagos_solicitudes.state"
            },
            {
                data: "reference",
                name: "pagos_solicitudes.reference"
            },
        ]
    });
});

// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
    el: "#app",
    data: {
        fecha_inicio: "",
        fecha_fin: "",
        normas: [],
        codnorma: [],
        codusuario: [],
        usuarios: [],
        loading: false
    },
    components: {},
    computed: {},
    mounted: function() {},
    watch: {},
    methods: {
        buscarNormas(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;

                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/reportes/buscarNorma", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.normas = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.normas = [];
            }
        },
        buscarUsuario(query) {
            var self = this;
            if (query !== "") {
                this.loading = true;

                var data = {
                    query: query
                };
                axios
                    .post(baseurl + "/reportes/buscarUsuario", data)
                    .then(function(response) {
                        var data = response.data;
                        self.loading = false;
                        self.usuarios = data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            } else {
                this.loading = false;
                this.usuarios = [];
            }
        },
        actualizardatatable() {
            tbl_pagos.ajax.reload(null, false);
        },
        limpiardatatable() {
            this.fecha_inicio = "";
            this.fecha_fin = "";
            this.usuarios = [];
            this.codusuario = [];
            this.codnorma = [];
            this.normas = [];
            this.actualizardatatable();
        },
        exportar() {
            var route = "reportes.exportar_pagos";
            if (route === "") {
                swal("Error!", "No se ha podido exportar!", "warning");
                return false;
            }
            var url =
                baseurl +
                laroute.route(route, {
                    fecha_inicio: this.fecha_inicio,
                    fecha_fin: this.fecha_fin,
                    codnorma: this.codnorma,
                    codusuario: this.codusuario
                });
            var win = window.open(url, "_blank");
            win.focus();
        }
    }
});