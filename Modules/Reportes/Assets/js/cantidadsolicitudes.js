$(document).ready(function() {
  tbl_etapas = $("#tbl_etapas").DataTable({
    orderClasses: false,
    processing: true,
    responsive: true,
    serverSide: true,
    ajax: {
      url: baseurl + "/reportes/todosdatosCantidadsolicitudes",
      type: "POST",
      data: function(d) {
        d.fecha_inicio = app.fecha_inicio;
        d.fecha_fin = app.fecha_fin;
      }
    },
    fnDrawCallback: function(oSettings, json) {
      $(".tooltip-opcion").tooltip();
      $(".image-lightbox").featherlight({
        type: "image"
      });
    },
    columns: [
      {
        data: "etapa",
        searchable: false
      },
      {
        className: "text-center",
        data: "solicitudes",
        searchable: false
      }
      //   {
      //     className: "text-center",
      //     data: "opciones",
      //     searchable: false,
      //     orderable: false
      //   }
    ]
  });
});

// =========================================================================
// Instancia Vue
// =========================================================================
var app = new Vue({
  el: "#app",
  data: {
    fecha_inicio: "",
    fecha_fin: ""
  },
  components: {},
  computed: {},
  mounted: function() {},
  watch: {},
  methods: {
    actualizardatatable() {
      tbl_etapas.ajax.reload(null, false);
    },
    limpiardatatable() {
      this.fecha_inicio = "";
      this.fecha_fin = "";
      this.actualizardatatable();
    },
    exportar(){
        var route = "reportes.exportar_cantidadsolicitudes";
      if (route === "") {
        swal("Error!", "No se ha podido exportar!", "warning");
        return false;
      }
      var url =
        baseurl +
        laroute.route(route, {
          fecha_inicio: this.fecha_inicio,
          fecha_fin: this.fecha_fin,
        });
      var win = window.open(url, "_blank");
      win.focus();
    }
  }
});
