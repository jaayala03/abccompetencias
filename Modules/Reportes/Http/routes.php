<?php

Route::group(['middleware' => 'web', 'prefix' => 'reportes', 'namespace' => 'Modules\Reportes\Http\Controllers'], function()
{

    Route::post('/buscarNorma', [
        'uses' => 'ReportesController@buscarNorma',
        'as'   => 'reportes.buscarNorma',
    ]);
    Route::post('/buscarUsuario', [
        'uses' => 'ReportesController@buscarUsuario',
        'as'   => 'reportes.buscarUsuario',
    ]);
    // --------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------
    // cargar vista historialsolicitudes
    Route::get('/historialsolicitudes', [
        'uses' => 'ReportesController@historialsolicitudes',
        'as'   => 'reportes.historialsolicitudes',
    ])->middleware('funcionalidades:REP_HIST_SOL');

    Route::post('/todosdatosHistorialsolicitudes', [
        'uses' => 'ReportesController@todosdatosHistorialsolicitudes',
        'as'   => 'reportes.todosdatosHistorialsolicitudes',
    ])->middleware('funcionalidades:REP_HIST_SOL');

    Route::get('/exportar_historialsolicitudes', [
        'uses' => 'ReportesController@exportar_historialsolicitudes',
        'as'   => 'reportes.exportar_historialsolicitudes',
    ])->middleware('funcionalidades:REP_HIST_SOL');

    // --------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------
    // cargar vista cantidadsolicitudes
    Route::get('/cantidadsolicitudes', [
        'uses' => 'ReportesController@cantidadsolicitudes',
        'as'   => 'reportes.cantidadsolicitudes',
    ])->middleware('funcionalidades:REP_CANT_SOL');

    Route::post('/todosdatosCantidadsolicitudes', [
        'uses' => 'ReportesController@todosdatosCantidadsolicitudes',
        'as'   => 'reportes.todosdatosCantidadsolicitudes',
    ])->middleware('funcionalidades:REP_CANT_SOL');

    Route::get('/exportar_cantidadsolicitudes', [
        'uses' => 'ReportesController@exportar_cantidadsolicitudes',
        'as'   => 'reportes.exportar_cantidadsolicitudes',
    ])->middleware('funcionalidades:REP_CANT_SOL');
    // --------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------

    // cargar vista pagos
    Route::get('/pagos', [
        'uses' => 'ReportesController@pagos',
        'as'   => 'reportes.pagos',
    ])->middleware('funcionalidades:REP_PAG');

    Route::post('/todosdatosPagos', [
        'uses' => 'ReportesController@todosdatosPagos',
        'as'   => 'reportes.todosdatosPagos',
    ])->middleware('funcionalidades:REP_PAG');

    Route::get('/exportar_pagos', [
        'uses' => 'ReportesController@exportar_pagos',
        'as'   => 'reportes.exportar_pagos',
    ])->middleware('funcionalidades:REP_PAG');
    // --------------------------------------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------------------------------------

    // cargar vista calificaciones
    Route::get('/calificaciones', [
        'uses' => 'ReportesController@calificaciones',
        'as'   => 'reportes.calificaciones',
    ])->middleware('funcionalidades:REP_CALIF');
});
