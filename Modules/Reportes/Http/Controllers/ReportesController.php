<?php

namespace Modules\Reportes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Crypt;
use Datatables;
use JavaScript;
use ABCcomptencias\Traits\Permisos;
use Modules\Reportes\Entities\EtapaSolicitud;
use Modules\Reportes\Entities\Norma;
use Modules\Reportes\Entities\Usuario;
use Modules\Reportes\Entities\PagoSolicitud;
use Amenadiel\JpGraph\Graph\Graph;
use Amenadiel\JpGraph\Plot\BarPlot;
use Amenadiel\JpGraph\Plot\GroupBarPlot;
use Amenadiel\JpGraph\Themes\UniversalTheme;
use Carbon;
use ABCcomptencias\Libraries\Fpdf_formato;

class ReportesController extends Controller
{
    //permisos trait
    use Permisos;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function historialsolicitudes()
    {
        $etapas = \Config::get('dominios.ETAPA_SOLICITUD.TXT');

        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Hist. Solicitudes',
                'componente' => $this->componente('REP_HIST_SOL'),
            ],
            'etapas' => $etapas,
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Reportes'],
            ['name' => 'Hist. Solicitudes'],
        ]);

        return view('reportes::historialsolicitudes');
    }

    public function todosdatosHistorialsolicitudes(Request $request){
        $datos = EtapaSolicitud::historialSolicitudes($request->all());

        return Datatables::of($datos)
        ->editColumn('fechaetapa', function (EtapaSolicitud $etapa) {
            return \Carbon::parse($etapa->fechaetapa)->format('d/m/Y h:m A');
        })
        ->editColumn('etapa', function (EtapaSolicitud $etapa) {
            return '<span class="label rounded" style="background-color: '.$etapa->etapacolor.'">'.$etapa->etapatxt.'</span>';
        })->addColumn('opciones', function (EtapaSolicitud $etapa) {
            $html = '';

            $html .=  $etapa->ultima ? '<span class="label rounded label-success">Actual</span>' : '';

            return $html;
        })->rawColumns(['fechaetapa', 'etapa', 'opciones'])->make(true);
    }

    public function todosdatosPagos(Request $request){
        $datos = PagoSolicitud::pagosrealizados($request->all());

        return Datatables::of($datos)
        ->editColumn('fechapago', function (PagoSolicitud $pago) {
            return \Carbon::parse($pago->fechapago)->format('d/m/Y h:m A');
        })
        ->editColumn('value', function (PagoSolicitud $pago) {
            return '$ '. number_format($pago->value, 2, ',', '.');
        })
        ->editColumn('state', function (PagoSolicitud $pago) {
            return \Config::get('dominios.RESPUESTA_EPAYCO.TXT.'.$pago->state);
        })
        ->rawColumns(['fechapago', 'value', 'state'])
        ->make(true);
    }

    public function buscarNorma(Request $request){
        return Norma::search($request['query'])->limit(20)->where('normas.estado', 1)->get();
    }

    public function buscarUsuario(Request $request){
        return Usuario::search($request['query'])->limit(20)->where('usuarios.estado', 1)->get();
    }

    public function cantidadsolicitudes()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Cant. Solicitudes',
                'componente' => $this->componente('REP_CANT_SOL'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Reportes'],
            ['name' => 'Cant. Solicitudes'],
        ]);

        return view('reportes::cantidadsolicitudes');
    }

    public function todosdatosCantidadsolicitudes(Request $request){
        $datos = EtapaSolicitud::cantidadSolicitudes($request->all());

        return Datatables::of($datos)->editColumn('etapa', function (EtapaSolicitud $etapa) {
            return $etapa->etapatxt;
        })->rawColumns(['etapa'])->make(true);
    }

    public function exportar_pagos(Request $request){
        dd($request->all());
    }

    public function exportar_historialsolicitudes(Request $request){
        
        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode('Historial de solicitudes')));

        $pdf->setCodigo(utf8_decode(''));
        $pdf->setVersion(utf8_decode(''));
        $pdf->setFecha(utf8_decode(''));
        
        $pdf->SetTitle(utf8_decode('Reporte historial de solicitudes'));
        $pdf->AliasNbPages();

        $pdf->AddPage();
    

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);

        $pdf->CellFitScale(25, 6, utf8_decode('DOCUMENTO'), 1, 0, 'L', true);
        $pdf->CellFitScale(65, 6, utf8_decode('NOMBRES'), 1, 0, 'L', true);
        $pdf->CellFitScale(20, 6, utf8_decode('NORMA'), 1, 0, 'L', true);
        $pdf->CellFitScale(30, 6, utf8_decode('ETAPA'), 1, 0, 'L', true);
        $pdf->CellFitScale(40, 6, utf8_decode('FECHA'), 1, 1, 'C', true);

        
        if(!empty($request->codusuario)){
            $request['codusuario'] = (explode(",", $request->codusuario));
        }
        if (!empty($request->codnorma)) {
            $request['codnorma'] = (explode(",", $request->codnorma));
        }

        $datos = EtapaSolicitud::historialSolicitudes($request->all())->get();
        
        if($datos->count()){
            foreach ($datos as $key => $fila) {
                $pdf->SetFont('Arial', '', 10);
                $pdf->CellFitScale(25, 6, utf8_decode($fila->documento), 1, 0, 'L', false);
                $pdf->CellFitScale(65, 6, utf8_decode($fila->candidato), 1, 0, 'L', false);
                $pdf->CellFitScale(20, 6, utf8_decode($fila->codigonorma), 1, 0, 'L', false);
                $pdf->CellFitScale(30, 6, utf8_decode($fila->etapatxt), 1, 0, 'L', false);
                $pdf->CellFitScale(40, 6, utf8_decode(\Carbon::parse($fila->fechaetapa)->format('d/m/Y h:m A')), 1, 1, 'C', false);
            }
        }else{
            $pdf->SetFont('Arial', '', 10);
            $pdf->CellFitScale(140, 6, utf8_decode('No se han encontrado registros'), 1, 0, 'C', false);
        }

        // ob_end_clean();
        $pdf->Output("rep_hist_solicitudes.pdf", 'I');
    }

    public function exportar_cantidadsolicitudes(Request $request){
        
        $pdf          = new Fpdf_formato();
        $paramsheader = [
            "imagen1" => ["x" => 20, "y" => 10, "ancho" => 50, "alto" => 20],
            "celda1"  => ["ancho" => 50, "alto" => 20],
            "celda2"  => ["ancho" => 95, "alto" => 20],
            "celda3"  => ["ancho" => 35, "alto" => 20],
            "codigo"  => ["x" => 4, "y" => 6],
            "version"  => ["x" => 10, "y" => 12],
            "fecha"  => ["x" => 10, "y" => 18],
        ];
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->setParamsheader($paramsheader);
        $pdf->setTitulo(strtoupper(utf8_decode('Cantidad solicitudes por etapa')));

        $pdf->setCodigo(utf8_decode(''));
        $pdf->setVersion(utf8_decode(''));
        $pdf->setFecha(utf8_decode(''));
        
        $pdf->SetTitle(utf8_decode('Reporte solicitudes por etapa'));
        $pdf->AliasNbPages();

        $pdf->AddPage();
    

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFillColor(238, 238, 238);

        $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
        $pdf->CellFitScale(70, 6, utf8_decode('ETAPA'), 1, 0, 'L', true);
        $pdf->CellFitScale(70, 6, utf8_decode('SOLICITUDES'), 1, 1, 'C', true);

        $datos = EtapaSolicitud::cantidadSolicitudes($request->all())->orderBy('etapa')->get();

        $totalsolicitudes = 0;

        $labels = array();
        $values = array();
        $colors = array();
        
        if($datos->count()){
            foreach ($datos as $key => $fila) {

                $totalsolicitudes += $fila->solicitudes;
                array_push($labels, $fila->etapatxt);
                array_push($values, $fila->solicitudes);
                array_push($colors, $fila->etapacolor);

                $pdf->SetFont('Arial', '', 12);
                $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
                $pdf->CellFitScale(70, 6, utf8_decode($fila->etapatxt), 1, 0, 'L', false);
                $pdf->CellFitScale(70, 6, utf8_decode($fila->solicitudes), 1, 1, 'C', false);
            }
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
            $pdf->CellFitScale(70, 6, utf8_decode('TOTAL SOLICITUDES'), 1, 0, 'R', false);
            $pdf->CellFitScale(70, 6, utf8_decode($totalsolicitudes), 1, 1, 'C', TRUE);

        }else{
            $pdf->SetFont('Arial', '', 12);
            $pdf->CellFitScale(20, 6, utf8_decode(''), 0, 0, 'L', false);
            $pdf->CellFitScale(140, 6, utf8_decode('No se han encontrado registros'), 1, 0, 'C', false);
        }

        // Create the graph. These two calls are always required
        $graph = new Graph(500,300,'auto');
        $graph->SetScale("textlin");

        $theme_class=new UniversalTheme;
        $graph->SetTheme($theme_class);

        // $graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels($labels);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);

        // Create the bar plots
        $b1plot = new BarPlot($values);

        // Create the grouped bar plot
        $gbplot = new GroupBarPlot(array($b1plot));
        // ...and add it to the graPH
        $graph->Add($gbplot);


        $b1plot->SetColor("white");
        $b1plot->SetFillColor($colors);


        $graph->title->Set("Solicitudes");

        // Display the graph
        $img = $graph->Stroke(_IMG_HANDLER);
        ob_start();
        imagepng($img);
        $imageData = ob_get_contents();
        $pic = 'data://text/plain;base64,' . base64_encode($imageData);
        // ob_end_clean();

        $pdf->Image($pic, $pdf->GetX() + 25, $pdf->GetY() + 6, 0, 0, 'png');


        // ob_end_clean();
        $pdf->Output("rep_cant_solicitudes_etapas.pdf", 'I');
    }

    public function pagos()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Pagos',
                'componente' => $this->componente('REP_PAG'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Reportes'],
            ['name' => 'Pagos'],
        ]);

        return view('reportes::pagos');
    }

    public function calificaciones()
    {
        JavaScript::put([
            'parametros' => [
                'meta_title' => 'Calificaciones',
                'componente' => $this->componente('REP_CALIF'),
            ],
        ]);

        View::share('breadcrumbs', [
            ['name' => 'Reportes'],
            ['name' => 'Calificaciones'],
        ]);

        return view('reportes::calificaciones');
    }
    
}
