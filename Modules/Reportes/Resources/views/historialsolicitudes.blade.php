@extends('krauff::layouts.ajaxmaster')

<!-- Se incluyen las variables js pasadas desde el controlador -->
@include('krauff::layouts.partials.jsvars')

@section('js')
<!-- Aqui incluye los js a utilizar en la vista actual -->
<script src="{{ asset('modules/reportes/js/historialsolicitudes.js') }}"></script>
@endsection

@section('css')
<!-- Aqui incluye los css a utilizar en la vista actual-->
@endsection

@section('content')
<!-- START @PAGE CONTENT -->

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-file-pdf-o"></i> Historial de solicitudes </h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted está en:</span>
        @include('krauff::layouts.partials.breadcrumbs') 
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn" id="app">
    <div class="row">
        <div class="col-md-12">
            <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading text-center">
                    <form class="form-inline">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="fecha_inicio">Desde</label>
                                <input v-model="fecha_inicio" type="date" class="form-control" id="fecha_inicio">
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="fecha_fin">Hasta</label>
                                <input v-model="fecha_fin" type="date" class="form-control" id="fecha_fin">
                            </div><!-- /.form-group -->

                            <button @click.prevent="actualizardatatable" type="button" class="btn btn-success"><i class="fa fa-search"></i> Buscar</button>
                            <button @click.prevent="limpiardatatable" type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>

                            <button @click.prevent="exportar" type="button" class="btn btn-warning pull-right"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                        </div><!-- /.form-body -->
                    </form>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-heading text-center">
                    <form class="form-inline">
                        <div class="form-body">
                            <div class="form-group">
                                <label for="etapa">Etapa</label>
                                <el-select @change="actualizardatatable" v-model="etapa" clearable placeholder="--seleccione--">
                                    <el-option
                                        v-for="(item, index) in etapas"
                                        :key="index"
                                        :label="item"
                                        :value="index">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="etapa">Norma</label>
                                <el-select @change="actualizardatatable" v-model="codnorma" multiple filterable remote reserve-keyword placeholder="Ingrese codigo"
                                    :remote-method="buscarNormas" :loading="loading">
                                    <el-option v-for="item in normas" :key="item.codnorma" :label="item.codigo" :value="item.codnorma">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="etapa">Usuario</label>
                                <el-select @change="actualizardatatable" v-model="codusuario" multiple filterable remote reserve-keyword placeholder="Ingrese documento"
                                    :remote-method="buscarUsuario" :loading="loading">
                                    <el-option v-for="item in usuarios" :key="item.codusuario" :label="item.nombrecompleto" :value="item.codusuario">
                                    </el-option>
                                </el-select>
                            </div><!-- /.form-group -->
                        </div><!-- /.form-body -->
                    </form>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                    <!-- Start datatable -->
                        <table id="tbl_historial" class="table table-danger table-bordered table-striped table-hover" style="width: 100%;" >
                            <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombres</th>
                                    <th></th>
                                    <th></th>
                                    <th>Norma</th>
                                    <th>Etapa</th>
                                    <th>Fecha</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <!--tbody section is required-->
                            <tbody></tbody>                            
                        </table>
                    </div><!-- /.responsive -->                    
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->

<!--/ END PAGE CONTENT -->
@endsection

@section('modales')

@endsection
