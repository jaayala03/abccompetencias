<?php

namespace Modules\Reportes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use DB;

class EtapaSolicitud extends Model
{

    use LogsActivity;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'etapas_solicitudes';
    protected $primaryKey = 'codetapasolicitud';
    protected $fillable   = [
        "etapa",
        "ultima",
        "codsolicitud"
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'solicitudes';

    protected static $logAttributes = [
        "etapa",
        "ultima",
        "codsolicitud",
        "created_at",
        "updated_at",
    ];

    protected $appends = ['etapatxt', 'etapacolor'];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
            case 'updated':
                return 'Editó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
            case 'deleted':
                return 'Eliminó la etapa de solicitud: ' . $this->codetapasolicitud;
                break;
        }
        return '';
    }


    public function getEtapatxtAttribute($value)
    {
        return \Config::get('dominios.ETAPA_SOLICITUD.TXT.'.$this->etapa);
    }

    public function getEtapacolorAttribute($value)
    {
        return \Config::get('dominios.ETAPA_SOLICITUD.ALL.'.$this->etapa)['color'];
    }

    public function ScopeCantidadSolicitudes($query, $datos){

        $query->select('etapas_solicitudes.etapa', DB::raw("COUNT (etapas_solicitudes.codsolicitud) as solicitudes"))
        ->where('etapas_solicitudes.ultima', true)
        ->groupBy('etapas_solicitudes.etapa');

        if(!empty($datos['fecha_inicio']) && !empty($datos['fecha_fin'])){
            $query->whereBetween('etapas_solicitudes.created_at',array(\Carbon::createFromFormat('Y-m-d', $datos['fecha_inicio'])->format('Y-m-d 00:00:00'), \Carbon::createFromFormat('Y-m-d', $datos['fecha_fin'])->format('Y-m-d 23:59:59')));
        }

        return $query;
    }

    public function ScopeHistorialSolicitudes($query, $datos){

        $query->select('usuarios.documento', DB::raw("concat (usuarios.nombres,' ', usuarios.primerapellido,' ', usuarios.segundoapellido) AS candidato"),'normas.codigo AS codigonorma','normas.nombre AS norma','etapas_solicitudes.etapa', 'etapas_solicitudes.created_at AS fechaetapa','etapas_solicitudes.ultima')
        ->JOIN('solicitudes', 'etapas_solicitudes.codsolicitud', '=', 'solicitudes.codsolicitud')
        ->JOIN('usuarios', 'solicitudes.codusuario', '=', 'usuarios.codusuario')
        ->JOIN('normas', 'solicitudes.codnorma', '=', 'normas.codnorma')
        ->orderBy('usuarios.documento', 'ASC')
	    ->orderBy('etapas_solicitudes.etapa', 'DESC')
        ->orderBy('etapas_solicitudes.ultima');
        
        if(!empty($datos['fecha_inicio']) && !empty($datos['fecha_fin'])){
            $query->whereBetween('etapas_solicitudes.created_at',array(\Carbon::createFromFormat('Y-m-d', $datos['fecha_inicio'])->format('Y-m-d 00:00:00'), \Carbon::createFromFormat('Y-m-d', $datos['fecha_fin'])->format('Y-m-d 23:59:59')));
        }

        if(isset($datos['etapa']) && !empty($datos['etapa'])){
            $query->where('etapas_solicitudes.etapa', $datos['etapa']);
        }

        if(isset($datos['codnorma'])){
            if (count($datos['codnorma']) > 0) {
                $query->whereIn('solicitudes.codnorma', $datos['codnorma']);
            }
        }

        if(isset($datos['codusuario'])){
            if (count($datos['codusuario']) > 0) {
                $query->whereIn('solicitudes.codusuario', $datos['codusuario']);
            }
        }

        return $query;
    }

    public function ScopeCalificaciones($query){
        $query->select('usuarios.documento',
        DB::raw("concat (
            usuarios.nombres,
            ' ',
            usuarios.primerapellido,
            ' ',
            usuarios.segundoapellido
        ) AS candidato"),
        'normas.codigo AS codigonorma',
        'normas.nombre AS norma',
        'etapas_solicitudes.etapa',
        'etapas_solicitudes.created_at AS fechaetapa',
        'cuestionarios_solicitud.hora_inicio',
        'cuestionarios_solicitud.hora_fin',
        DB::raw("extract(epoch from age(cuestionarios_solicitud.hora_fin, cuestionarios_solicitud.hora_inicio)) / 60 as minutostotales"),
        'cuestionarios_solicitud.hora_fin_estimado',
        DB::raw("extract(epoch from age(cuestionarios_solicitud.hora_fin_estimado, cuestionarios_solicitud.hora_inicio)) / 60 as minutosestimados"),
        'cuestionarios.nombre as cuestionario')
        ->JOIN('solicitudes', 'etapas_solicitudes.codsolicitud', '=', 'solicitudes.codsolicitud')
        ->JOIN('usuarios', 'solicitudes.codusuario', '=', 'usuarios.codusuario')
        ->JOIN('normas', 'solicitudes.codnorma', '=', 'normas.codnorma')
        ->JOIN('cuestionarios_solicitud', 'solicitudes.codsolicitud', '=', 'cuestionarios_solicitud.codsolicitud')
        ->JOIN('cuestionarios', 'cuestionarios_solicitud.codcuestionario', '=', 'cuestionarios.codcuestionario')
        ->where('etapas_solicitudes.ultima', TRUE)
        ->where('cuestionarios_solicitud.terminado', TRUE)
        ->orderBy('etapas_solicitudes.created_at', 'DESC');

        return $query;
    }
}
