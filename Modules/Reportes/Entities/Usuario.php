<?php

namespace Modules\Reportes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Nicolaslopezj\Searchable\SearchableTrait;

class Usuario extends Model
{

    use LogsActivity;
    use SearchableTrait;

    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table = 'usuarios';

    protected $primaryKey = 'codusuario';

    protected $fillable = [
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];
    //**********************************************************************
    protected $appends    = ['nombrecompleto'];
    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'reportes';

    protected static $logAttributes = [
        'codusuario',
        'codperfil',
        'codubicacion',
        'codubicacionnacimiento',
        'tipodoc',
        'documento',
        'nombres',
        'primerapellido',
        'segundoapellido',
        'email',
        'direccion',
        'telefono',
        'celular',
        'fechanacimiento',
        'genero',
        'nombreusuario',
        'password',
        'imagencodificada',
        'mime',
        'tamano',
        'cargo',
        'estado',
    ];

    protected $searchable = [
        'columns' => [
            'usuarios.nombres'       => 5,
            'usuarios.primerapellido'       => 5,
            'usuarios.segundoapellido'       => 5,
            'usuarios.documento'       => 10,
        ],
    ];
    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'reportes';
    }

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                 return 'Creó el Usuario: '.$this->email;
                break;
            case 'updated':
                return 'Editó el Usuario: '.$this->email;
                break;
            case 'deleted':
                return 'Eliminó el Usuario: '.$this->email;
                break;
        }
        return '';
    }

    //**********************************************************************
    public function getNombrecompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primerapellido. ' '. $this->segundoapellido;
    }
}
