<?php

namespace Modules\Reportes\Entities;

use Illuminate\Database\Eloquent\Model;
use DB;

class PagoSolicitud extends Model
{

    protected $table      = 'pagos_solicitudes';
    protected $primaryKey = 'codpagosolicitud';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference', 
        'payu_order_id',  
        'transaction_id', 
        'state', 
        'value', 
        'user_id', 
        'codsolicitud', 
        'respuesta',
        'created_at'
    ];


    public function ScopePagosrealizados($query, $datos){

        $query->select('pagos_solicitudes.reference', 
        'pagos_solicitudes.transaction_id',
        'pagos_solicitudes.state',
        'pagos_solicitudes.value',
        'pagos_solicitudes.created_at as fechapago',
        'normas.codigo as codigonorma',
        DB::raw("concat (usuarios.nombres,' ', usuarios.primerapellido,' ', usuarios.segundoapellido) AS candidato"),
        'usuarios.documento')
        ->join('solicitudes', 'pagos_solicitudes.codsolicitud', '=', 'solicitudes.codsolicitud')
        ->join('usuarios', 'solicitudes.codusuario', '=', 'usuarios.codusuario')
        ->join('normas', 'solicitudes.codnorma', '=', 'normas.codnorma');

        if (!empty($datos['fecha_inicio']) && !empty($datos['fecha_fin'])) {
            $query->whereBetween('pagos_solicitudes.created_at', array(\Carbon::createFromFormat('Y-m-d', $datos['fecha_inicio'])->format('Y-m-d 00:00:00'), \Carbon::createFromFormat('Y-m-d', $datos['fecha_fin'])->format('Y-m-d 23:59:59')));
        }
        if (isset($datos['codnorma'])) {
            if (count($datos['codnorma']) > 0) {
                $query->whereIn('solicitudes.codnorma', $datos['codnorma']);
            }
        }

        if (isset($datos['codusuario'])) {
            if (count($datos['codusuario']) > 0) {
                $query->whereIn('solicitudes.codusuario', $datos['codusuario']);
            }
        }

        return $query;
    }
}
