<?php

namespace Modules\Reportes\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Nicolaslopezj\Searchable\SearchableTrait;

class Norma extends Model
{

    use LogsActivity;
    use SearchableTrait;
    //**********************************************************************
    //Propiedades de la tabla
    //**********************************************************************
    protected $table      = 'normas';
    protected $primaryKey = 'codnorma';
    protected $fillable   = [
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion'
    ];
    //**********************************************************************

    //**********************************************************************
    //Propiedades para auditoria
    //**********************************************************************
    protected $logOnlyDirty = true;
    protected static $logName = 'reportes';

    protected static $logAttributes = [
        'codnorma',
        'nombre',
        'descripcion',
        'codigo',
        'archivo',
        'estado',
        'codtipo_titulacion'
    ];

    protected $searchable = [
        'columns' => [
            'normas.nombre'       => 5,
            'normas.codigo'       => 10,
        ],
    ];

    //Descripcion para auditoria
    public function getDescriptionForEvent(string $eventName): string
    {
        switch ($eventName) {
            case 'created':
                return 'Creó la norma: ' . $this->codigo;
                break;
            case 'updated':
                return 'Editó la norma: ' . $this->codigo;
                break;
            case 'deleted':
                return 'Eliminó la norma: ' . $this->codigo;
                break;
        }
        return '';
    }

    //Tipo de evento auditoria
    public function getLogNameToUse(string $eventName = ''): string
    {
        return 'reportes';
    }

}
