const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/js/app.js')
        .version()
        .sass('resources/assets/sass/app.scss', 'public/css/app.css')
        .styles([
            //CSS de plantilla y Plugins
            './public/plantilla/global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            './public/plantilla/global/plugins/bower_components/animate.css/animate.min.css',
            './public/plantilla/global/plugins/bower_components/dropzone/downloads/css/dropzone.css',
            './public/plantilla/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css',
            './public/plantilla/global/plugins/bower_components/bootstrap-datepicker-vitalets/css/datepicker.css',
            './public/plantilla/global/plugins/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
            './public/plantilla/global/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css',
            './public/plantilla/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css',
            './public/plantilla/global/plugins/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
            //'./public/plantilla/global/plugins/bower_components/bootstrap-calendar/css/calendar.min.css',
            './public/plugins/bc/css/calendar.css',

            './public/plantilla/admin/css/reset.css',
            './public/plantilla/admin/css/layout.css',
            './public/plantilla/admin/css/components.css',
            './public/plantilla/admin/css/plugins.css',
            './public/plantilla/admin/css/themes/blue.theme.css',
            './public/plantilla/admin/css/pages/timeline.css',
            './public/plantilla/admin/css/pages/search.css',
            './public/plantilla/admin/css/pages/search-basic.css',
            './public/plantilla/admin/css/custom.css',

            //Pages
            './public/plantilla/admin/css/pages/project-team-board.css',
            //CSS propios
            './public/modules/krauff/css/plantilla/abstract.css',
            './public/js/notificaciones/notificaciones.css',

            //PLUGINS ADICIONALES
            //Datatable
            './public/plugins/Datatable/datatables.min.css',
            './public/plugins/Datatable/Responsive-2.1.1/css/responsive.bootstrap.min.css',
            './public/plugins/Datatable/checkboxes-1.2.4/css/dataTables.checkboxes.css',
            './public/plugins/Datatable/Buttons-1.2.4/css/buttons.dataTables.min.css',
            //simple-line-icons
            './public/plantilla/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css',
            //Notifier
            './public/plugins/notifier/css/notifier.css',
            //jstree
            './public/plugins/jstree/style.min.css',
            //cropper
            './public/plugins/cropper/cropper.min.css',
            //amaran
            './public/plugins/AmaranJS/dist/css/amaran.min.css',
            //cubeportfolio
            './public/plantilla/commercial/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css',
            //select2
            './public/plugins/select2/dist/css/select2.min.css',
            //x-editable/
            './public/plantilla/global/plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
            './public/plantilla/global/plugins/bower_components/x-editable/dist/inputs-ext/typeaheadjs/lib/typeahead.js-bootstrap.css',
            './public/plantilla/global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.css',
            //featherlight
            './public/plugins/featherlight/featherlight.min.css',
            //flatpickr
            './public/plugins/flatpickr/flatpickr.min.css',
            //ion.rangeSlider
            './public/plugins/ion.rangeSlider/css/ion.rangeSlider.css',
            './public/plugins/ion.rangeSlider/css/ion.rangeSlider.skinModern.css',
            //anijs
            './public/plugins/anijs/anicollection.css',
            //ganttView
            './public/plugins/ganttView/jquery.ganttView.css',
            //fileinput
            './public/plugins/bootstrap-fileinput/css/fileinput.css',
            './public/plugins/bootstrap-fileinput/css/fileinput-rtl.css',
            //Gritter
            './public/plantilla/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css',
            //glyphicons-pro
            './public/plantilla/commercial/plugins/glyphicons-pro/glyphicons/web/html_css/css/glyphicons.css',
            //izimodal
            './public/plugins/izimodal/css/iziModal.min.css',

            //camera
            './public/plugins/camera/camera.css',

            //summernote
            './public/plantilla/global/plugins/bower_components/summernote/dist/summernote.css',

            //chart js
            './public/plugins/chart.js/Chart.css',

            //aos
            './public/plugins/aos/aos.css',

            //wysihtml5
            './public/plantilla/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css',

        ], 'public/css/all.css').scripts([
            //JS de Plantilla y Plugins 

            './public/plantilla/global/plugins/bower_components/jquery-cookie/jquery.cookie.js',
            './public/plantilla/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.js',
            './public/plantilla/global/plugins/bower_components/typehead.js/dist/handlebars.js',
            './public/plantilla/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js',

            './public/plantilla/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js',
            './public/plantilla/global/plugins/bower_components/ionsound/js/ion.sound.min.js',
            './public/plantilla/global/plugins/bower_components/bootbox/bootbox.js',
            './public/plantilla/admin/js/pages/blankon.timeline.js',
            './public/plantilla/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.spline.min.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.categories.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.tooltip.min.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.resize.js',
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.pie.js',
            './public/plantilla/global/plugins/bower_components/dropzone/downloads/dropzone.min.js',
            './public/plantilla/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js',
            './public/plantilla/global/plugins/bower_components/skycons-html5/skycons.js',
            './public/plantilla/global/plugins/bower_components/jquery.inputmask/dist/jquery.inputmask.bundle.min.js',
            './public/plantilla/global/plugins/bower_components/bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js',
            './public/plantilla/global/plugins/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
            './public/plantilla/global/plugins/bower_components/moment/min/moment.min.js',
            './public/plantilla/global/plugins/bower_components/moment/locale/es.js',
            './public/plantilla/global/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js',
            './public/plantilla/global/plugins/bower_components/bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js',
            './public/plantilla/global/plugins/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js',
            //'./public/plantilla/global/plugins/bower_components/bootstrap-calendar/js/calendar.js',
            './public/plugins/bc/js/calendar.js',
            //'./public/plantilla/global/plugins/bower_components/bootstrap-calendar/js/language/es-CO.js',
            './public/plugins/bc/js/language/es-CO.js',

            './public/plantilla/admin/js/apps.js',
            //'./public/plantilla/admin/js/pages/blankon.dashboard.js',
            './public/plantilla/admin/js/demo.js',

            './public/plantilla/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js',
            './public/plantilla/global/plugins/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
            './public/plantilla/admin/js/pages/blankon.form.wizard.js',

            './public/plantilla/global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js',
            './public/plantilla/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js',

            './public/modules/krauff/js/plantilla/abstract.js',
            './public/modules/krauff/js/plantilla/material.js',

            './public/js/laroute.js',

            //PLUGINS ADICIONALES
            //Datatable
            './public/plugins/Datatable/datatables.js',
            './public/plugins/Datatable/Responsive-2.1.1/js/dataTables.responsive.min.js',
            './public/plugins/Datatable/checkboxes-1.2.4/js/dataTables.checkboxes.min.js',
            './public/plugins/Datatable/Buttons-1.2.4/js/dataTables.buttons.min.js',
            //notifier
            './public/plugins/notifier/js/notifier.js',
            //jstree
            './public/plugins/jstree/jstree.min.js',
            //cropper
            './public/plugins/cropper/cropper.min.js',
            //bootbox
            './public/plugins/bootbox/bootbox.min.js',
            //AmaranJS
            './public/plugins/AmaranJS/dist/js/jquery.amaran.min.js',
            //cube-portfolio
            './public/plantilla/commercial/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js',
            //select2
            './public/plugins/select2/dist/js/select2.full.min.js',
            './public/plugins/select2/dist/js/i18n/es.js',
            //validate
            './public/plugins/validate/es.js',
            //x-editable
            './public/plantilla/global/plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js',
            './public/plantilla/global/plugins/bower_components/x-editable/dist/inputs-ext/address/address.js',
            //featherlight
            './public/plugins/featherlight/featherlight.min.js',
            //Jquery-maskMoney
            './public/plugins/Jquery-maskMoney/jquery.maskMoney.min.js',
            //routie
            './public/plugins/routie/routie.min.js',
            //flatpickr
            './public/plugins/flatpickr/flatpickr.min.js',
            './public/plugins/flatpickr/l10n/es.js',
            //ion.rangeSlider
            './public/plugins/ion.rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',
            //anijs
            './public/plugins/anijs/anijs-min.js',
            //ganttView
            //'./public/plugins/ganttView/jquery.ganttView.js',
            //'./public/plugins/ganttView/date.js',
            //fileinput
            './public/plugins/bootstrap-fileinput/js/fileinput.min.js',
            './public/plugins/bootstrap-fileinput/js/locales/es.js',
            //jquery.serializeJSON
            './public/plugins/jquery.serializejson/jquery.serializejson.min.js',
            //pdfobject
            './public/plugins/pdfobject/pdfobject.min.js',
            //raphael
            './public/plantilla/global/plugins/bower_components/raphael/raphael-min.js',
            //jquery.flot
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.js',
            //jquery.flot.spline
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.spline.min.js',
            //jquery.flot.categories
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.categories.js',
            //jquery.flot.tooltip
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.tooltip.min.js',
            //jquery.flot.resize
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.resize.js',
            //jquery.flot.pie
            './public/plantilla/global/plugins/bower_components/flot/jquery.flot.pie.js',
            //morris
            './public/plantilla/global/plugins/bower_components/morrisjs/morris.min.js',
            //jquery.gritter
            './public/plantilla/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js',
            //query.waypoints
            './public/plantilla/global/plugins/bower_components/waypoints/lib/jquery.waypoints.min.js',
            //jquery.counterup
            './public/plantilla/global/plugins/bower_components/counter-up/jquery.counterup.js',
            //jquery.sparkline
            './public/plantilla/global/plugins/bower_components/jquery.sparkline.min/index.js',
            //jquery.easy-pie-chart
            './public/plantilla/global/plugins/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
            //izimodal
            './public/plugins/izimodal/js/iziModal.min.js',

            //modernizr
            './public/plugins/modernizr/modernizr.min.js',

            //camera
            './public/plugins/camera/camera.min.js',

            //pace
            './public/plugins/pace/pace.min.js',

            //pace
            './public/plugins/aos/aos.js',

            //summernote
            './public/plantilla/global/plugins/bower_components/summernote/dist/summernote.min.js',

            //chart js
            './public/plugins/chart.js/Chart.js',

             //wysihtml5
             './public/plantilla/global/plugins/bower_components/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js',
             './public/plantilla/global/plugins/bower_components/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js',

        ], 'public/js/all.js');

//JS Modulos
mix.version([
      'public/modules/*/*/*/*.js',
      'public/modules/*/*/*/*.css',
]);

mix.autoload({
    jquery: ['$', 'window.jQuery'],
});

mix.disableNotifications();

mix.browserSync('ABCcomptencias.test');