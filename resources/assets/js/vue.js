/* Se importa Vue js */
window.Vue = require('vue');

Vue.config.productionTip = false
Vue.config.devtools = false

//VeeValidate
import VeeValidateES from 'vee-validate/dist/locale/es';
import VeeValidate, { Validator } from 'vee-validate';
import VeeValidateLaravel from 'vee-validate-laravel';
Validator.localize('es', VeeValidateES);
Vue.use(VeeValidate, {
  locale: 'es',
  fieldsBagName: 'formFields'
});
Vue.use(VeeValidateLaravel);

//VueEvents
import VueEvents from 'vue-events'
Vue.use(VueEvents)

//Vmodal
import vmodal from 'vue-js-modal';
Vue.use(vmodal);

//AnimatedVue
import AnimatedVue from 'animated-vue';
Vue.use(AnimatedVue);

//Vue-resource
import vueResource from 'vue-resource';
Vue.use(vueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

//Vue-tooltip
import Tooltip from 'vue-directive-tooltip';
Vue.use(Tooltip);

//filters
import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)

import money from 'v-money'
Vue.use(money, {precision: 4})

//ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/es'
Vue.use(ElementUI, { locale })

//VueTheMask
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

//Vform
import { Form, HasError, AlertError, AlertSuccess } from 'vform'
window.Mform = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertSuccess.name, AlertSuccess)

//VueStrap
window.VueStrap = require('vue-strap');
import { SweetModal, SweetModalTab } from 'sweet-modal-vue'

/* SE DEFINEN COMPONENTES GLOBALES */
//Select2
Vue.component('select2', require('./components/Select2.vue'));
//Modal
Vue.component('vue-modal', require('./components/Modal.vue'));
//v-paginator
Vue.component('v-paginator', require('./components/paginator/paginator'));
//SweetModal
Vue.component('sweet-modal', SweetModal);
//SweetModalTab
Vue.component('sweet-modal-tab', SweetModalTab);
//v-select
Vue.component('v-select', require('vue-strap').select);
//Datepicker
Vue.component('date-picker', require('./components/Datepicker.vue'));
//Typeahead
Vue.component('el-typeahead', require('./components/Typeahead.vue'));
//Confirm button
Vue.component('confirm-button', require('./components/Confirm.vue'));
//Animacion zoom-in-zoom-out
//Vue.component('custom-group-zoom-in-zoom-out', CustomGroupZoomInZoomOut);
//vue-upload-component
Vue.component('file-upload', require('vue-upload-component'))
//avaluos-table
Vue.component('tabla-avaluos', require('./components/tables/Avaluos.vue'));
//direcciones-table
Vue.component('tabla-direcciones', require('./components/tables/Direcciones.vue'));

import Vuetable from 'vuetable-2/src/components/Vuetable'
import VuetablePagination from 'vuetable-2/src/components/VuetablePagination'
import VuetablePaginationInfo from 'vuetable-2/src/components/VuetablePaginationInfo'
Vue.component('vuetable', Vuetable)
Vue.component('vuetable-pagination', VuetablePagination)
Vue.component('vuetable-pagination-info', VuetablePaginationInfo)


/* FILTROS */
//formatSize
import formatSize from './filters/filesize.js'
Vue.filter('formatSize', formatSize)