/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//Bootstrap
require('./bootstrap');
//Vue Js
require('./vue');
//Echo
require('./echo');
//Otras dependencias
require('./other');

//
//const app = new Vue({
//    el: '#app'
//});