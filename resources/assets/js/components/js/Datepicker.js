import Vue from 'vue'
import VueMask from 'v-mask'
Vue.use(VueMask);

import flatpickr from 'flatpickr'
import 'flatpickr/dist/flatpickr.min.css'
//import 'flatpickr/dist/themes/material_green.css'
const es = require("flatpickr/dist/l10n/es.js").default.es;

export default {
  name: 'flat-pickr',
  props: {
    value: {
      default: null,
      required: true,
      validator(value) {
        return value === null || value instanceof Date || typeof value === 'string' || value instanceof String || value instanceof Array
      }
    },
    config: {
      type: Object,
      default: () => ({ 
        dateFormat: "d/m/Y",
        allowInput: true,
        wrap: false
      })
    },
    masked: {
      type: Boolean,
      default: true
    },
    placeholder: {
      type: String,
      default: ''
    },
    inputClass: {
      type: [String, Object],
      default: 'form-control input'
    },
    name: {
      type: String,
      default: 'date-time'
    },
    required: {
      type: Boolean,
      default: false
    },
    id: {
      type: String,
    },
  },
  data() {
    return {
      /**
       * Props can not be mutated directly so lets copy to a local property
       */
      mutableValue: this.value,
      /**
       * flatpickr instance
       */
      fp: null,
      /**
       * onChange method
       */
      oldOnChange: null,
    };
  },
  mounted() {

    if (!this.fp) {

      this.oldOnChange = this.config.onChange;

      this.config.onChange = this.onChange;
      this.config.locale = es;

      let elem = this.config.wrap ? this.$el.parentNode : this.$el;
      this.fp = new Flatpickr(elem, this.config);

    }
  },
  beforeDestroy() {
    if (this.fp) {
      this.fp.destroy();
      this.fp = null;
      this.oldOnChange = null;
      this.config.onChange = null
    }
  },
  methods: {
    /**
     * Emit on-change event
     */
    onChange(...args) {
      if (typeof this.oldOnChange === 'function') {
        this.oldOnChange(...args);
      }
      this.$emit('on-change', ...args);
    }
  },
  watch: {
    /**
     * @param newConfig Object
     */
    config(newConfig) {
      this.fp.config = Object.assign(this.fp.config, newConfig);
      this.fp.redraw();
      this.fp.setDate(this.value, true);
    },

    /**
     * @param newValue
     */
    mutableValue(newValue) {
      this.$emit('input', newValue);
    },

    /**
     * @param newValue
     */
    value(newValue) {
      if (newValue === this.mutableValue) return;
      this.fp && this.fp.setDate(newValue, true);
    }
  },
};
