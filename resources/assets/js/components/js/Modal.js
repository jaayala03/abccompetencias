const MODAL_WIDTH = 656
export default {
    props: ['modalname'],
    data() {
        return {
            modalWidth: MODAL_WIDTH,
        }
    },
    created() {
        this.modalWidth = window.innerWidth < MODAL_WIDTH ? MODAL_WIDTH / 2 : MODAL_WIDTH;
    },
    methods: {
        open(type) {
            console.log(type);
        }
    }
}