import _ from 'lodash'

export default {
  props: {
        url: {
            type: String,
            required: true
        },
        cache: {
            type: Boolean,
            required: false,
            default () {
                return false
            }
        },
        classes: {
            type: String,
            required: false,
            default () {
                return 'form-control'
            }
        },
        name: {
            type: String,
            required: false,
            default () {
                return ''
            }
        },
        placeholder: {
            type: String,
            required: false,
            default () {
                return ''
            }
        },
        displayProperty: {
            type: String | Array,
            required: true
        },
        suggestion: {
            type: Array,
            required: true
        },
        prefix: {
            type: String,
            required: false,
            default () {
                return '-'
            }
        },
        prefixed: {
          type: Boolean,
          required: false,
          default () {
              return false
          }
        },
        onRemove: {
          type: Function,
          required: true
        },
        onSelected: {
          type: Function,
          required: true
        },
        value: {
          type: String,
          required: false
        },
        disabled: {
          type: Boolean,
          required: false,
          default () {
              return false
          }
        }
  },
  data() {
      return {
          loading: false,
          selectedText: '',
          selectedObject: {},
          engine: new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                  wildcard: '%QUERY',
                  url: this.url + '?q=%QUERY',
                  cache: this.cache,
                  prepare: function(query, settings) {
                      return $.extend(settings, {
                          url: settings.url.replace('%QUERY', encodeURIComponent(query)),
                          error: function(jqxhr, textStatus, errorThrown) {
                              swal({
                                type: 'error',
                                title: 'Oops...',
                                text: 'Ha ocurrido un error inesperado!',
                              })
                          }
                      });
                 }
            }
          })
      }
  },
  mounted() {
    var vm = this;
    this.engine.initialize();
    $(this.$el.querySelector("input")).typeahead({
        hint: false,
        highlight: true,
        minLength: 2,
        items: 10
    },{
        limit: 10,
        display: function(item) {
          if (vm.displayProperty instanceof Array) {
              var arr = [];
              _.forEach(vm.displayProperty, function(value) {
                arr.push(_.get(item, value))
              });
              return arr.join(' '+vm.prefix+' ');
          } 
          
          return _.get(item, vm.displayProperty);
        },
        name: vm.name,
        source: this.engine.ttAdapter(),
        templates: {
            empty: ['<div class="list-group search-results-dropdown"><div class="list-group-item">sin resultados</div></div>'],
            suggestion: function(data) {
              var arr = [];
              _.forEach(vm.suggestion, function(value) {
                arr.push(_.get(data, value))
              });
              if(vm.prefixed){
                return '<div>' + arr.join(' '+vm.prefix+' ') + '</div>';
              }
              return '<div>' + arr[0] + '</div>';
            }
        }
    }).on('typeahead:selected', function(event, data) {
        
        if (vm.displayProperty instanceof Array) {
          var arr = [];
          _.forEach(vm.displayProperty, function(value) {
            arr.push(_.get(data, value))
          });
          vm.selectedText = arr.join(' '+vm.prefix+' ');
        } else{
          vm.selectedText = _.get(data, vm.displayProperty);
        }

        vm.onSelected(event, data);
    }).on('typeahead:asyncrequest', function() {
       $(vm.$el.querySelector("input")).addClass('loading-th')
    }).on('typeahead:asynccancel typeahead:asyncreceive', function() {
        $(vm.$el.querySelector("input")).removeClass('loading-th')
    });

  },
  destroyed() {
    $(this.$el.querySelector("input")).typeahead('destroy');
  },
  methods: {
    remove(){
      this.selectedText = '';
      this.onRemove();
    }
  },
  watch: {
    value: function(val) {
        this.selectedText = val;
    },
    url: function(val) {
        this.engine.remote.url = val + '&q=%QUERY';
    }
  }
}