import Vue from 'vue'
import { Button } from 'element-ui'
Vue.use(Button)

export default {
    props: {
        busy: {
          type: Boolean,
          required: false,
          default () {
            return false
          }
        },
        onConfirm: {
          type: Function,
          required: true,
        },
        onCancel: {
          type: Function,
          required: false,
        },
        confirmTitle: {
          type: String,
          required: false,
          default () {
            return 'Confirmación'
          }
        },
        confirmText: {
          type: String,
          required: false,
          default () {
            return '¿Está seguro que desea continuar?'
          }
        },
        confirmButtonText: {
          type: String,
          required: false,
          default () {
            return 'Si'
          }
        },
        cancelButtonText: {
          type: String,
          required: false,
          default () {
            return 'Cancelar'
          }
        },
        typeAlert: {
          type: String,
          required: false,
          default () {
            return 'warning'
          }
        },
        type: {
          type: String,
          required: false,
          default () {
            return 'primary'
          }
        },
        size: {
          type: String,
          required: false,
          default () {
            return ''
          }
        },
        disabled: {
          type: Boolean,
          required: false,
          default () {
            return false
          }
        }
    },
    methods: {
        onClick() {
            this.$confirm(this.confirmText, this.confirmTitle, {
                confirmButtonText: this.confirmButtonText,
                cancelButtonText: this.cancelButtonText,
                type: this.typeAlert,
                center: true
            }).then(() => {
                this.onConfirm();
            }).catch(() => {});
        }
    }
}