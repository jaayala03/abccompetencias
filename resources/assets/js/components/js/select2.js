export default {
    props: {
        moreParams: {
            required: false,
            type: Object,
            default: function() {
                return {}
            }
        },
        cache: {
            required: false,
            type: Boolean,
            default: function() {
                return false
            }
        },
        disabled: {
            required: false,
            type: Boolean,
            default: function() {
                return false
            }
        },
        url: {
            required: true,
            type: String
        },
        selected: {
            required: false,
            type: Array | Object,
            default: function() {
                return []
            }
        },
        config: {
            required: false,
            type: Object,
            default: function() {
                var self = this;
                return {
                    ajax: {
                        url: '',
                        type: "GET",
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            return {...{
                                q: params.term,
                                page: params.page,
                            }, ...self.moreParams}
                        },
                        processResults: function(data, page) {
                            var select2data = $.map(data, function(obj) {
                                obj.id = obj.id;
                                obj.text = obj.nombres || obj.nombre;
                                return obj;
                            });
                            return {
                                results: select2data
                            }
                        },
                        cache: this.cache
                    },
                    minimumInputLength: 2,
                }
            }
        }
    },
    mounted: function() {
        this.config.ajax.url = baseurl + this.url;
        var vm = this;
        $(this.$el)
            // init select2
            .select2(this.config).val(this.value).trigger('change')
            // emit event on change.
            .on('change', function() {
                vm.$emit('input', $(this).val());

                if($(vm.$el).select2("data")[0]) {
                    vm.$emit('update:seleccionado', $(vm.$el).select2("data")[0]);
                }
            });
        this.handlerselected();
        this.handleDisabled();
    },
    methods: {
        handlerselected(newValue) {
            var self = this;
            $(self.$el).children('option').remove();
            if (self.selected instanceof Array) {
                $(self.$el).find('option').remove().end();
                self.selected.forEach(function(obj, index) {
                    var $option = $('<option selected>' + obj.text + '</option>').val(obj.id);
                    $(self.$el).append($option).trigger('change');
                });
                $(self.$el).trigger('change');
            } else {
                var $option = $('<option selected>' + self.selected.text + '</option>').val(self.selected.id);
                $(self.$el).append($option).trigger('change');
            }
        },
        handleDisabled(){
            if(this.disabled){
                $(this.$el).attr("disabled", "disabled");
            }else{
                $(this.$el).removeAttr('disabled');
            }
        }
    },
    watch: {
        disabled: function(value) {
            //var newValue = !value;
            this.handleDisabled();
        },
        value: function(value) {
            // check to see if the arrays contain the same values
            if (value) {
                if ([...value].sort().join(",") !== [...$(this.$el).val()].sort().join(",")) $(this.$el).val(value).trigger('change');
            }
        },
        'selected': {
            handler: function(newValue) {
                this.handlerselected(newValue);
            },
            deep: true
        },
    },
    destroyed: function() {
        $(this.$el).off().select2('destroy')
    }
}