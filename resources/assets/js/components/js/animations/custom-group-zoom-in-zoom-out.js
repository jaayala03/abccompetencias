import GenericTransition from 'animated-vue/src/common/generic-transition';

/**
 * The first parameter is the animation's name, the second is the "enter" animation,
 * the third one is the "leave" animation, while the last parameter marks if the transition
 * is a group transition or not (**false** for "single" and **true** for "group")
 * @type {Object}
 */
export default new GenericTransition('custom-zoom-in-zoom-out', 'zoomIn', 'zoomOut', true);