<?php

return [
    'close_sweet'                  => 'Cerrar',
    //Auth
    'sent_email_title'             => 'Verifique su email',
    'email_reset_password_success' => 'Hemos enviado un email de restablecimiento de contraseña.',
    'reset_password_success'       => 'La contraseña se ha cambiado exitosamente.',
    'token_activation_error'       => 'Email de verificacion no encontrado o ya ha sido verificado.',
    'user_activation_error'        => 'Se necesita activar tu cuenta.',
    'email_activation_sent'        => 'Hemos enviado un codigo de activacion.',
    //outside
    'title_last_articles'          => 'Ultimos articulos',
    'title_last_articles_category' => 'Ultimos articulos de :category',
    'title_categories'             => 'Categorias',
    'title_tags'                   => 'Tags',
    //outside
    'title_last_articles'          => 'Ultimos articulos',
    'title_last_articles_category' => 'Ultimos articulos de :category',
    'title_categories'             => 'Categorias',
    'warning_title'                => 'Advertencia',
    'tarifa_warning'               => 'No se ha encontrado tarifas para el año actual, por favor comuníquese con el administrador del sistema.',
    'intereses_warning'            => 'No se ha encontrado tasa intereses para el mes actual, por favor comuníquese con el administrador del sistema.',
    'contra_warning'               => 'No se han encontrado tramites para el periodo actual, por favor comuníquese con el administrador del sistema.',
];
