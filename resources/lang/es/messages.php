<?php

// resources/lang/en/messages.php

return [
    'error' => 'Ha ocurrido un error inesperado',
    'success' => 'Operación realizada correctamente',
    'warning'  => 'Operación no valida'
];