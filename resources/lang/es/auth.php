<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed'         => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle'       => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
    'user_hour'      => 'El sistema ha detectado que no te encuentras en horario laboral, por favor comunícate con el administrador del sistema.',
    'user_day'       => 'El sistema ha detectado que hoy no es un día habil, por favor comunícate con el administrador del sistema.',
    'ip_not_allowed' => 'El sistema ha detectado que intentas acceder desde una ubicación no permitida, por favor comunícate con el administrador del sistema.',

];
