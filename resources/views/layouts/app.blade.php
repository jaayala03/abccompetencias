<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Aplicacion ABCcomptencias">
        <meta name="keywords" content="ABCcomptencias">
        <meta name="author" content="jaayala03">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!--/ END META SECTION -->

        <!-- START @FONT STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700')}}" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="{{ asset('plantilla/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="{{ asset('plantilla/global/plugins/bower_components/fontawesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/global/plugins/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="{{ asset('plantilla/admin/css/reset.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/layout.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/components.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/plugins.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/themes/blue-sea.theme.css') }}" rel="stylesheet" id="theme">
        <link href="{{ asset('plantilla/admin/css/pages/sign.css')}}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/custom.css')}}" rel="stylesheet">
        <!--/ END THEME STYLES -->


        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>
        <!-- Variable Global JS -->
        <script>
            var baseurl = '<?php echo url("/"); ?>';
        </script>
    </head>
    <body>

        @yield('content')

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-cookie/jquery.cookie.js')}}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js')}}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/ionsound/js/ion.sound.min.js')}}"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('plantilla/admin/js/pages/blankon.sign.js')}}"></script>
        <script src="{{ asset('plantilla/admin/js/demo.js')}}"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

    </body>
</html>
