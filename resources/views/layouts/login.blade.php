<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
        <!-- INICIO @META SECCION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Aplicacion ABCcomptencias">
        <meta name="keywords" content="ABCcomptencias">
        <meta name="author" content="jaayala03">
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--/ FIN META SECCION -->

        <!-- INICIO @FONT STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ FIN FONT STYLES -->

        <!-- INICIO @GLOBAL  MANDATORY STYLES -->
        <link href="{{ asset('plantilla/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!--/ FIN GLOBAL MANDATORY STYLES -->

        <!-- INICIO @PAGE  LEVEL STYLES -->
        <link href="{{ asset('plantilla/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
        <!--/ FIN PAGE LEVEL STYLES -->

        <!-- INICIO @THEME  STYLES -->
        <link href="{{ asset('plantilla/admin/css/reset.css') }}" rel="stylesheet">            
        <link href="{{ asset('plantilla/admin/css/layout.css') }}" rel="stylesheet">             
        <link href="{{ asset('plantilla/admin/css/components.css') }}" rel="stylesheet">                
        <link href="{{ asset('plantilla/admin/css/plugins.css') }}" rel="stylesheet">                
        <link href="{{ asset('plantilla/admin/css/themes/blue-sea.theme.css') }}" rel="stylesheet" id="theme">                 
        <link href="{{ asset('plantilla/admin/css/pages/sign.css') }}" rel="stylesheet">                    
        <link href="{{ asset('plantilla/admin/css/custom.css') }}" rel="stylesheet">
        <!-- Preloader -->
        <link href="{{ asset('plantilla/global/plugins/bower_components/spinkit/css/spinners/7-three-bounce.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/global/plugins/bower_components/spinkit/css/spinners/9-cube-grid.css')  }}" rel="stylesheet">

        <!--/ FIN THEME STYLES -->

        <!-- INICIO @IE  SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                            <script src="http://localhost:8090/assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js"></script>
                    <script src="http://localhost:8090/assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js"></script>
                    <![endif]-->
        <!--/ FIN IE SUPPORT -->


        <!-- Styles -->
        <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
        @yield('css')

        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>
        <!-- Variable Global JS -->
        <script>
            var baseurl = '<?php echo url("/"); ?>';
        </script>
    </head>
    <body class="laravel">

        @yield('content')

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        <!-- INICIO JAVASCRIPT SECCION (Load javascripts at bottom to reduce load time) -->
        <!-- INICIO @CORE  PLUGINS -->
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-cookie/jquery.cookie.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/typehead.js/dist/handlebars.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery.sparkline.min/index.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/ionsound/js/ion.sound.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/bootbox/bootbox.js') }}"></script>
        <!--/ FIN CORE PLUGINS -->

        <!-- INICIO @PAGE  LEVEL PLUGINS -->
        <script src="{{ asset('plantilla/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
        <!--/ FIN PAGE LEVEL PLUGINS -->

        <!-- INICIO @PAGE  LEVEL SCRIPTS -->
        <script src="{{ asset('plantilla/admin/js/pages/blankon.sign.js') }}"></script>
        <script src="{{ asset('plantilla/admin/js/demo.js') }}"></script>
        <!--/ FIN PAGE LEVEL SCRIPTS -->
        <!--/ FIN JAVASCRIPT SECCION -->

        @if(Config::get('notificaciones.ENABLED'))
        <!-- SOCKET IO-->
        <script type="text/javascript" src="{{ Config::get('notificaciones.SERVER_NODE') }}/socket.io/socket.io.js"></script>
        <script type="text/javascript">
            var socket = io.connect('{{ Config::get("notificaciones.SERVER_NODE") }}');
            if (localStorage.getItem('uUID') !== null) {
                //Math.random().toString(24) + new Date()
                socket.emit('unsubscribe_notifications', localStorage.getItem('uUID'));
                localStorage.removeItem('uUID');
            }
        </script>
        <!-- SOCKET IO-->
        @endif
        

        @yield('js')

    </body>
</html>
