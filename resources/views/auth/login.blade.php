@extends('layouts.login')

@section('js')
<!-- Preloader -->
<script src="{{ asset('js/pages/login/loader.js') }}" type="text/javascript"></script>
@endsection

@section('css')
<link href="{{ asset('css/pages/login/loader.css') }}" rel="stylesheet"> 
@endsection

@section('content')

{{-- <div id="loader-wrapper">
    <div class="loader-brand">
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
        <div class="loader-text">
            {{ Config::get('app.name') }}
        </div>
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div> --}}

<!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<!-- START @SIGN  WRAPPER -->
<div id="sign-wrapper">

    <!-- Brand -->
    <div class="brand">
        <img src="{{ asset('images/logo2.jpeg') }}" alt="brand logo"/>
    </div>
    <!--/ Brand -->

    <!-- Login form -->
    <form class="sign-in form-horizontal shadow rounded no-overflow" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="sign-header">
            <div class="form-group">
                <div class="sign-text">
                    <span>Inicio de Sesion</span>
                </div>
            </div><!-- /.form-group -->
        </div><!-- /.sign-header -->

        <div class="sign-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.partials.errors')

                    </div>
                </div>               
            </div>

            <div class="form-group{{ $errors->has('documento') ? ' has-error' : '' }}">
                <div class="input-group input-group-lg rounded no-overflow">
                    <input id="documento" type="text" class="form-control input-sm" placeholder="Documento" name="documento" value="{{ old('documento') }}">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>

                </div>
            </div><!-- /.form-group -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="input-group input-group-lg rounded no-overflow">
                    <input type="password" class="form-control input-sm" placeholder="Clave" name="password">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                </div>
            </div><!-- /.form-group -->
        </div><!-- /.sign-body -->
        <div class="sign-footer">
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="ckbox ckbox-theme">
                            <input  id="rememberme" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="rememberme" class="rounded">Recordarme</label>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="{{ route('password.request') }}" title="lost password">Recuperar Clave</a>
                    </div>
                </div>
            </div><!-- /.form-group -->
            <div class="form-group">
                <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Entrar</button>
            </div><!-- /.form-group -->
        </div><!-- /.sign-footer -->


    </form><!-- /.form-horizontal -->
    <!--/ Login form -->

    <!-- Content text -->
    <p class="text-muted text-center sign-link">¿Necesitas una cuenta? <a href="{{ route('register') }}"> Regístrate aquí</a></p>
    <!--/ Content text -->

</div><!-- /#sign-wrapper -->
<!--/ END SIGN WRAPPER -->
@endsection
