@extends('layouts.login')

@section('js')


@endsection

@section('css')

@endsection

@section('content')

<!-- START @SIGN WRAPPER -->
        <div id="sign-wrapper">

            <!-- Brand -->
            <div class="brand">
                <img src="{{ asset('images/logo2.jpeg') }}" alt="brand logo"/>
            </div>
            <!--/ Brand -->

            <!-- Register form -->
            <form class="form-horizontal rounded shadow rounded no-overflow" role="form" method="POST" action="{{ route('register.create') }}">
                {{ csrf_field() }}
                <div class="sign-header">
                    <div class="form-group">
                        <div class="sign-text">
                            <span>Crear una nueva cuenta</span>
                        </div>
                    </div>
                </div>
                <div class="sign-body">
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input name="name" id="name" type="text" class="form-control" placeholder="Nombre de Usuario">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input name="password" id="password" type="password" class="form-control" placeholder="Clave">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input name="password_confirmation" id="password_confirmation" type="password" class="form-control" placeholder="Confirme la Clave">
                            <span class="input-group-addon"><i class="fa fa-check"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input name="email" id="email" type="email" class="form-control" placeholder="Tu Email">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
                <div class="sign-footer">
                    <div class="form-group">
                        <div class="callout callout-info no-margin">
                            <p class="text-muted">Para confirmar y activar su nueva cuenta, deberemos enviar el código de activación a su correo electrónico.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="ckbox ckbox-theme">
                            <input id="term-of-service" value="1" type="checkbox">
                            <label for="term-of-service" class="rounded">Estoy de acuerdo con <a href="#">Terminos y Condiciones</a></label>
                        </div>
                        <div class="ckbox ckbox-theme">
                            <input id="newsletter" value="1" type="checkbox">
                            <label for="newsletter" class="rounded">Mandame boletin</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Regístrate</button>
                    </div>
                </div>
            </form>
            <!--/ Register form -->

            <!-- Content text -->
            <p class="text-muted text-center sign-link">¿Ya tienes una cuenta? <a href="{{url('/login')}}"> Ingrese aquí</a></p>
            <!--/ Content text -->

        </div><!-- /#sign-wrapper -->
        <!--/ END SIGN WRAPPER -->
@endsection
