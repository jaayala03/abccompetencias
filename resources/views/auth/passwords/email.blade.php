@extends('layouts.app')

@section('content')
<!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<!-- START @SIGN WRAPPER -->
<div id="sign-wrapper">

    <!-- Brand -->
    <div class="brand">
        <img src="{{ asset('images/logo2.jpeg') }}" alt="brand logo"/>
    </div>
    <!--/ Brand -->

    <!-- Lost password form -->
    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class="sign-header">
            <div class="form-group">
                <div class="sign-text">
                    <span>Restablecer Contraseña</span>
                </div>
            </div>
        </div>
        <div class="sign-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif

            @include('layouts.partials.errors')


            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group input-group-lg rounded">
                    <input type="text" name="email" class="form-control" value="{{ old('email') }}" required placeholder="Email">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                </div>
            </div>
        </div>
        <div class="sign-footer">
            <div class="form-group">
                <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Enviar</button>
            </div>
        </div>
    </form>
    <!--/ Lost password form -->

    <!-- Content text -->
    <p class="text-muted text-center sign-link">Regresar al<a href="{{url('/login')}}"> Login</a></p>
    <!--/ Content text -->

</div><!-- /#sign-wrapper -->
<!--/ END SIGN WRAPPER -->

@endsection
