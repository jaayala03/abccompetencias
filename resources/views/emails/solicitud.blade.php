@component('mail::message')
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" id="bodyTable" width="100%">
    <tbody>
        <tr>
            <td align="center" id="bodyCell" valign="top">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" class="wrapper-t">
                    <tbody>
                        
                        <tr>
                            <td align="center" valign="top">
                                <!-- BEGIN BODY // -->
                                <table border="0" cellpadding="0" cellspacing="0" id="templateBody" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="bodyContent" mc:edit="body_content" valign="top">
                                                <h1>
                                                    Señor(a): {{ $data['usuario']->nombrecompleto }}
                                                </h1>
                                                <h3>
                                                    Su solicitud se ha realizado con éxito en nuestro sistema.
                                                </h3>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- // END BODY -->
                            </td>
                        </tr>
                        <tr>
                            <td class="bodyContent" mc:edit="body_content" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table class="invoice">
                                                    <thead>
                                                        <tr>
                                                            <th align="left" width="40%">
                                                                Codigo
                                                            </th>
                                                            
                                                            <th align="left" width="60%">
                                                                Nombre
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($data['solicitud'] as $solicitud)
                                                        <tr>
                                                            <td align="justify">
                                                                {{ $solicitud['codigo'] }}
                                                            </td>

                                                            <td align="justify">
                                                                {{ $solicitud['nombre'] }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr style="margin-top: 30px">
                            <td align="center" valign="top">
                                <!-- BEGIN FOOTER // -->
                                <table style="margin-top: 30px" border="0" cellpadding="0" cellspacing="0" id="templateFooter" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="footerContent" colspan="2" mc:edit="footer_content00" valign="top">
                                                Lo invitamos a que se dirija al link de <a href="{{ $data['ruta_descargas'] }}" target="_blank">
                                                    <u>DESCARGAS</u>
                                                </a> para que conozca los formatos y requisitos para llevar a cabo esta certificación.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="footerContent" mc:edit="footer_content01" style="padding-top:5px;" valign="top">
                                                <em>
                                                    Gracias por elegirnos.
                                                </em>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- // END FOOTER -->
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </tbody>
</table>
@endcomponent