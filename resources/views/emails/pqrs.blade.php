@component('mail::message')

<div class="row">
        <div class="col-md-12">

            <!-- Start basic color table -->
            <div class="panel">
                <div class="panel-body no-padding">
                    <div class="table-responsive" style="margin-top: -1px;">
                        <table class="table table-striped table-primary">
                            <thead>
                                <tr>
                                    <th style="font-weight: bold; font-size: xx-large;" colspan="2" class="text-center">
                                        {{ \Config::get('dominios.PQRS.TXT.'.$data['tipo']) }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="font-weight: bold; width: 20%;" class="text-center border-right">Nombre:</td>
                                <td>
                                    {{ ucwords($data['nombre']) }}
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 20%;" class="text-center border-right">Telefono:</td>
                                <td>
                                    {{ ($data['telefono']) ? $data['telefono'] : 'N/R'}}
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 20%;" class="text-center border-right">Email:</td>
                                <td>
                                    {{ $data['email'] }}
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 20%;" class="text-center border-right">Mensaje:</td>
                                <td class="justify" style="text-align: justify;">
                                    {{ $data['mensaje'] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- /.table-responsive -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End basic color table -->

        </div><!-- /.col-md-12 -->
    </div>

@endcomponent
