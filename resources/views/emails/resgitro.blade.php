@component('mail::message')

<table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
    <tr style="border-collapse:collapse;">
      <td align="center" style="padding:0;Margin:0;">
        <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;">
          <tr style="border-collapse:collapse;">
            <td align="center" style="padding:0;Margin:0;padding-left:35px;padding-right:35px;padding-top:40px;background-position:left top;">
              <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                  <td width="530" valign="top" align="center" style="padding:0;Margin:0;">
                    <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                      <tr style="border-collapse:collapse;">
                        <td class="es-m-txt-l" align="center" style="padding:0;Margin:0;padding-top:15px;">
                          <h3 style="Margin:0;line-height:22px;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;font-style:normal;font-weight:bold;color:#333333;">Hola {{ $data['usuario']->nombrecompleto }}</h3>
                        </td>
                      </tr>
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;Margin:0;padding-bottom:10px;padding-top:15px;">
                          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#777777;">
                            Bienvenido a ABC Competencias, solo queda un paso m&aacute;s.
                        </p>
                        </td>
                      </tr>
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;Margin:0;padding-bottom:15px;padding-top:20px;">
                          <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                              <td style="padding:0;Margin:0px;border-bottom:3px solid #EEEEEE;background:rgba(0, 0, 0, 0) none repeat scroll 0% 0%;height:1px;width:100%;margin:0px;"></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr style="border-collapse:collapse;">
            <td align="left" style="Margin:0;padding-top:30px;padding-bottom:35px;padding-left:35px;padding-right:35px;">
              <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                  <td width="530" valign="top" align="center" style="padding:0;Margin:0;">
                    <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;Margin:0;">
                          <h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;font-style:normal;font-weight:bold;color:#333333;">SIGUIENTE PASO</h2>
                        </td>
                      </tr>
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;Margin:0;padding-top:15px;">
                          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#777777;">Por favor confirme su correo</p>
                          <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#777777;">para ingresar al sistema.<br></p>
                        </td>
                      </tr>
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;Margin:0;padding-bottom:15px;padding-top:30px;"> <span class="es-button-border" style="border-style:solid;border-color:transparent;background:#ED8E20 none repeat scroll 0% 0%;border-width:0px;display:inline-block;border-radius:5px;width:auto;">
                            <a href="{{ $data['ruta_activar'] }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#ED8E20;border-width:15px 30px;display:inline-block;background:#ED8E20 none repeat scroll 0% 0%;border-radius:5px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">Confirmar cuenta</a> </span> </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>


{{-- <p style="text-align: justify;">
    <strong>
        Señor(a): {{ $data['usuario']->nombrecompleto }}
    </strong>
</p>
<p style="text-align: justify;">
    Su registro se ha realizado con éxito en nuestro sistema.
</p>
<p style="text-align: justify;">
    Para poder disfrutar de los beneficios que
    <strong>
        <u>
            ABC Competencias
        </u>
    </strong>
    tiene para usted, lea el siguiente contrato de uso de sus datos.
</p>
<p style="text-align: justify;">
    Con la expedición de la ley 1581 de 2012 y el Decreto 1377 de 2013, se desarrolla el principio constitucional que tienen todas las personas a conocer, actualizar y rectificar todo tipo de información recogida o, que haya sido objeto de tratamiento de datos personales en bancos o bases de datos y, en general en archivos de entidades públicas y/o privadas
</p>
<p style="text-align: justify;">
    <strong>
        ABC Competencias S.A.S
    </strong>
    como organismo que almacena, y recolecta datos personales requiere obtener su autorización para que de manera libre, previa, expresa, voluntaria, y debidamente informada, nos permita recolectar, recaudar, almacenar, usar, circular, suprimir, procesar, compilar, intercambiar, dar tratamiento, actualizar y disponer de los datos que han sido suministrados y que se han incorporado en distintas bases o bancos de datos, o en repositorios electrónicos de todo tipo con que cuenta
    <strong>
        ABC Competencias
    </strong>
    . Esta información es, y será utilizada en el desarrollo de las funciones propias de
    <strong>
        ABC Competencias
    </strong>
    en su condición de organismo de certificación de personas, de forma directa o a través de terceros.
</p>
<p style="text-align: justify;">
    <strong>
        ABC Competencias S.A.S
    </strong>
    en los términos dispuestos por el artículo 10 del decreto 1377 de 2013 queda autorizada de manera expresa e inequívoca para mantener y manejar toda su información, a no ser que usted le manifieste lo contrario de manera directa, expresa, inequívoca y por escrito dentro de los treinta (30) días hábiles contados a partir de la recepción de la presente comunicación a la cuenta de correo electrónico dispuesta para tal efecto:
    <strong>
        director.certificaciones@abccompetencias.com
    </strong>
</p>
<p style="text-align: justify;">
    Consiento y autorizo de manera expresa e inequívoca que mis datos personales sean tratados conforme a lo previsto en el presente documento.
</p>
<p style="text-align: justify;">
    En el evento en que usted considere que
    <strong>
        ABC Competencias
    </strong>
    dio un uso contrario al autorizado y a las leyes aplicables, podrá contactarnos a través de una comunicación motivada dirigida a
    <strong>
        director.certificaciones@abccompetencias.com
    </strong>
</p>
<p style="text-align: justify;">
    Si está conforme con lo anterior descrito de clic
    <a href="{{ $data['ruta_activar'] }}" target="_blank">
        <u>
            AQUÍ
        </u>
    </a>
    para aceptar las condiciones de uso de sus datos.
</p>
<p style="text-align: justify;">
    <strong>
        Gracias por elegirnos
    </strong>
</p> --}}
@endcomponent
