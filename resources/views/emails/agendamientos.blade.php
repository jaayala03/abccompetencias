@component('mail::message')
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" id="bodyTable" width="100%" class="wrapper-t">
        <tbody>
            <tr>
                <td align="center" id="bodyCell" valign="top">
                    <!-- BEGIN TEMPLATE // -->
                    <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" width="100%" style="width: 100%">
                        <tbody>

                            @if(isset($data['evaluador']))
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" id="templateBody" width="100%" style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td class="bodyContent" mc:edit="body_content" valign="top">
                                                    <h1>
                                                        Señor(a): {{ $data['evaluador']->nombrecompleto }}
                                                    </h1>
                                                    <h3>
                                                        Se le han agendado nuevas solicitudes para el, {{ $data['agendamiento']->fecha_agendada}} a las, {{ $data['agendamiento']->hora}}.
                                                    </h3>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent" mc:edit="body_content" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table class="invoice" width="100%" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th align="left" width="20%">
                                                                    Documento
                                                                </th>
                            
                                                                <th align="left" width="20%">
                                                                    Usuario
                                                                </th>
                            
                                                                <th align="left" width="30%">
                                                                    Codigo
                                                                </th>

                                                                <th align="left" width="30%">
                                                                    Norma
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data['solicitudes_agendadas'] as $solicitud)
                                                                <tr>
                                                                    <td align="justify">
                                                                        {{ $solicitud->usuario->documento }}
                                                                    </td>
                                                                
                                                                    <td align="justify">
                                                                        {{ $solicitud->usuario->nombrecompleto }}
                                                                    </td>
                                                                
                                                                    <td align="justify">
                                                                        {{ $solicitud->norma->codigo}}
                                                                    </td>

                                                                    <td align="justify">
                                                                        {{ $solicitud->norma->nombre}}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @else
                            <tr>
                                <td align="center" valign="top">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" id="templateBody" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="bodyContent" mc:edit="body_content" valign="top">
                                                    <h1>
                                                        Señor(a): {{ $data['solicitud']->usuario->nombrecompleto }}
                                                    </h1>
                                                    <h3>
                                                        Usted ha sido agendado para presentar la prueba de la solicitud de la norma {{ $data['solicitud']->norma->codigo}} - {{ $data['solicitud']->norma->nombre}}.
                                                    </h3>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyContent" mc:edit="body_content" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table class="invoice" width="100%" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th align="left" width="30%">
                                                                    Fecha
                                                                </th>
                                                                
                                                                <th align="left" width="30%">
                                                                    Hora
                                                                </th>

                                                                <th align="left" width="40%">
                                                                    Evaluador
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td align="justify">
                                                                    {{ $data['agendamiento']->fecha_agendada }}
                                                                </td>

                                                                <td align="justify">
                                                                    {{ $data['agendamiento']->hora }}
                                                                </td>

                                                                <td align="justify">
                                                                    {{ $data['agendamiento']->evaluador->nombrecompleto }}
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            @endif
                            <tr style="margin-top: 30px">
                                <td align="center" valign="top">
                                    <!-- BEGIN FOOTER // -->
                                    <table style="margin-top: 30px" border="0" cellpadding="0" cellspacing="0" id="templateFooter" width="100%">
                                        <tbody>
                                            <tr>
                                                <td class="footerContent" mc:edit="footer_content01" style="padding-top:5px;" valign="top">
                                                    <em>
                                                        Gracias por elegirnos.
                                                    </em>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- // END TEMPLATE -->
                </td>
            </tr>
        </tbody>
    </table>
</center>
@endcomponent