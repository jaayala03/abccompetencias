@extends('errors.master')

@section('content')

<div class="error-wrapper">
    <h1>403!</h1>
    <h3>Acceso denegado.</h3>
    <h4>El sistema ha detectado que te encuentras inactivo, por favor Comunicate con el administrador del sistema.</h4>
    <a href="{{ url('/') }}" class="btn btn-sm btn-theme">Volver a Inicio</a>
</div>
@endsection

