@extends('errors.master')

@section('css')
<link href="{{ asset('plantilla/admin/css/pages/error-page.css') }}" rel="stylesheet">
@endsection

@section('content')


{{-- <!-- START @ERROR PAGE --> --}}
<div class="error-wrapper">
    <h1>404!</h1>
    <h3>La página que busca no ha sido encontrada!</h3>
    <h4>La página que está buscando podría haberse eliminado o no estar disponible. <br> <br/></h4>
    <!--<form action="#" class="form-horizontal">
        <div class="form-group has-feedback no-padding">
            <input type="text" class="form-control typeahead" placeholder="Buscar una página ">
            <button type="submit" class="btn btn-theme fa fa-search form-control-feedback"></button>
        </div>
    </form>-->
    <a href="{{ url('/') }}" class="btn btn-sm btn-theme">Volver a Inicio</a>
</div>
<!--/ END ERROR PAGE -->
        
<!--/ End body content -->
@endsection
