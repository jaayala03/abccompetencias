@extends('errors.master')

@section('css')
<link href="{{ asset('plantilla/admin/css/pages/error-page.css') }}" rel="stylesheet">
@endsection

@section('content')


<!-- START @ERROR PAGE -->
<div class="error-wrapper">
    <h1>403!</h1>
    <h3>Acceso Denegado.</h3>
    <h4>{{ (isset($exception)) ? $exception->getMessage() : 'Usted no tiene permiso para acceder a la ruta especificada.' }} </h4>
    <a href="{{ url('/') }}" class="btn btn-sm btn-theme">Volver a Inicio</a>
</div>
<!--/ END ERROR PAGE -->

<!--/ End body content -->
@endsection
