@extends('errors.master')

@section('content')

<div class="error-wrapper">
    <h1>404!</h1>
    <h3>La página que busca no ha sido encontrada!</h3>
    <h4>La página que está buscando podría haberse eliminado o no estar disponible. <br> <br/></h4>
    <a href="{{ url('/') }}" class="btn btn-sm btn-theme">Volver a Inicio</a>
</div>
@endsection
