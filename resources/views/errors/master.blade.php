<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    {{-- <!-- START @HEAD --> --}}
    <head>
        {{-- <!-- START @META SECTION --> --}}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
        @if(isset($title))
            <title>{{$title}} - {{ config('app.name', 'Laravel') }}</title>
        @endif
        <!--/ END META SECTION -->


        {{-- <!-- START @FONT STYLES --> --}}
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ END FONT STYLES -->

        {{-- <!-- START @GLOBAL MANDATORY STYLES --> --}}
        <link href="{{ asset('plantilla/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        {{-- <!-- START @PAGE LEVEL STYLES --> --}}
        <link href="{{ asset('plantilla/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        {{-- <!-- START @THEME STYLES --> --}}
        <link href="{{ asset('plantilla/admin/css/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/layout.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/components.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/plugins.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/themes/blue-gray.theme.css') }}" rel="stylesheet" id="theme">
        <link href="{{ asset('plantilla/admin/css/pages/error-page.css') }}" rel="stylesheet">
        <link href="{{ asset('plantilla/admin/css/custom.css') }}" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <script src="{{ asset('plantilla/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js') }}"></script>
        <script src="{{ asset('plantilla/global/plugins/bower_components/respond-minmax/dest/respond.min.js') }}"></script>

    </head>

    <body>

        {{-- <!-- START @ERROR PAGE --> --}}
        @yield('content')
        <!--/ END ERROR PAGE -->

    </body>


</html>