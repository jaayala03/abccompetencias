@extends('errors.master')

@section('css')
<link href="{{ asset('plantilla/admin/css/pages/error-page.css') }}" rel="stylesheet">
@endsection

@section('content')

<!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-ban"></i>Error 500 <span>Error Interno del Servidor</span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Usted Está en:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="{{ url('/') }}">Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Error 500</li>
        </ol>
    </div>
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="error-wrapper">
                <h1>500</h1>
                <h3>Error Interno del Servidor.</h3>
                <h4>Algo salió mal.</h4>
                <a href="{{ url('/') }}" class="btn btn-sm btn-theme">Volver a Inicio</a>
                <!--<a href="dashboard.html" class="btn btn-sm btn-theme">Go to Dashboard</a>-->
            </div>
            <!--/ END ERROR PAGE -->

        </div>
    </div><!-- /.row -->
</div><!-- /.body-content -->
<!--/ End body content -->
@endsection