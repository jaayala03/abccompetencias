<?php

//**********************************************************************
//Para acceder a un dominio
//Config::get('dominios.key');
//**********************************************************************

return [
    //******************************************************************************
    //DOMINIO PARA IMAGEN POR DEFECTO DE UN USUARIO
    //******************************************************************************
    'DEFAULT_IMG_USER'                       => 'default.png',
    //******************************************************************************
    //DOMINIO PARA IMAGEN POR DEFECTO DE UN banner fachada
    //******************************************************************************
    'DEFAULT_IMG_FACHADA'                   => 'default.png',
    //******************************************************************************
    //DOMINIO PARA IMAGEN POR DEFECTO DE una noticia
    //******************************************************************************
    'DEFAULT_IMG_NOTICIA'                   => 'default.png',
    //******************************************************************************
    //DOMINIO TIPO DOCUMENTO
    //******************************************************************************
    'TIPODOC'                                => [
        'TXT_SHORT' => [
            '1' => 'C.C',
            '2' => 'NIT',
        ],
        'TXT'       => [
            '1' => 'Cédula Ciudadanía',
            '2' => 'NIT',
        ],
        'VALORES'   => [
            'CC'  => '1',
            'NIT'  => '2',
        ],
    ],
    //******************************************************************************
    //DOMINIO GENERO
    //******************************************************************************
    'GENERO'                                 => [
        'TXT'     => [
            '1' => 'Masculino',
            '2' => 'Femenino',
        ],
        'VALORES' => [
            'M' => '1',
            'F' => '2',
        ],
    ],
    //******************************************************************************
    //DOMINIO tipo de cuestionario
    //******************************************************************************
    'TIPO_CUESTIONARIO'                                 => [
        'TXT'     => [
            '1' => 'PRIMARIO',
            '2' => 'SECUNDARIO',
        ],
        'VALORES' => [
            'P' => '1',
            'S' => '2',
        ],
    ],

    //******************************************************************************
    //DOMINIO ETAPA DE SOLICITUD
    //******************************************************************************
    'ETAPA_SOLICITUD'                                 => [
        'TXT'     => [
            '1' => 'Inscrito',
            '2' => 'Pago Registrado',
            '3' => 'Agendado',
            '4' => 'Habilitado',
            '5' => 'Iniciado',
            '6' => 'Procesado',
            '7' => 'Certificado',
            '8' => 'No Aprobado',
            '9' => 'Anulado',
        ],
        'VALORES' => [
            'inscrito' => '1',
            'pago_registrado' => '2',
            'agendado' => '3',
            'habilitado' => '4',
            'iniciado' => '5',
            'procesado' => '6',
            'certificado' => '7',
            'no_aprobado' => '8',
            'anulado' => '9',
        ],
        'ALL'     => [
            '1' => ['color' => '#F5BB42', 'class' => 'warning', 'value' => 'Inscrito', 'key' => '1', 'orden' => '1'],
            '2' => ['color' => '#8CC152', 'class' => 'success', 'value' => 'Pago Registrado', 'key' => '2', 'orden' => '2'],
            '3' => ['color' => '#63D3E9', 'class' => 'info', 'value' => 'Agendado', 'key' => '3', 'orden' => '3'],
            '4' => ['color' => '#63D3E9', 'class' => 'info', 'value' => 'Habilitado', 'key' => '4', 'orden' => '4'],
            '5' => ['color' => '#37BC9B', 'class' => 'teal', 'value' => 'Iniciado', 'key' => '5', 'orden' => '5'],
            '6' => ['color' => '#00B1E1', 'class' => 'primary', 'value' => 'Procesado', 'key' => '6', 'orden' => '6'],
            '7' => ['color' => '#2A2A2A', 'class' => 'inverse', 'value' => 'Certificado', 'key' => '7', 'orden' => '7'],
            '8' => ['color' => '#906094', 'class' => 'lilac', 'value' => 'No Aprobado', 'key' => '8', 'orden' => '8'],
            '9' => ['color' => 'red', 'class' => 'danger', 'value' => 'Anulado', 'key' => '9', 'orden' => '9'],
        ],
    ],

    //******************************************************************************
    //DOMINIO TIPO DE USUARIO
    //******************************************************************************
    'TIPO_USUARIO'                                 => [
        'TXT'     => [
            '1' => 'Persona Natural',
            // '2' => 'Empresa',
        ],
        'VALORES' => [
            'PN' => '1',
            // 'EMP' => '2',
        ],
    ],

    //******************************************************************************
    //DOMINIO ESTADO DEL USUARIO EN EL SISTEMA
    //******************************************************************************
    'ESTADOUSU'                              => [
        'TXT'     => [
            '1' => 'Activo',
            '2' => 'Inactivo',
        ],
        'VALORES' => [
            'Act' => '1',
            'In'  => '2',
        ],
    ],
    //******************************************************************************
    //DOMINIO TIPO DE EVALUACION O INDICADOR LISTA DE CHECKEO
    //******************************************************************************
    'TIPO_EVALUACION_INDICADOR'                              => [
        'TXT'     => [
            '1' => 'Desempeño',
            '2' => 'Producto',
        ],
        'VALORES' => [
            'DES' => '1',
            'PROD'  => '2',
        ],
        'ALL'     => [
            '1' => ['class' => 'primary', 'value' => 'Desempeño', 'key' => '1', 'icon' => 'fa fa-male'],
            '2' => ['class' => 'default', 'value' => 'Producto', 'key' => '2', 'icon' => 'fa  fa-wrench'],
        ],
    ],
    //******************************************************************************
    //Dominio para lectura de notificaciones
    //******************************************************************************
    'NOTIFICACION_LEIDA'                     => [
        'TXT'     => [
            '1' => 'NO',
            '2' => 'SI',
        ],
        'VALORES' => [
            'NO' => '1',
            'SI' => '2',
        ],
    ],
    //******************************************************************************
    //Dominio para tipo pqrs
    //******************************************************************************
    'PQRS'                     => [
        'TXT'     => [
            '1' => 'Queja',
            '2' => 'Apelación',
        ],
        'VALORES' => [
            'Q' => '1',
            'A' => '2',
        ],
    ],
    //******************************************************************************
    //Dominio para si o no
    //******************************************************************************
    'DOMINIO_SI_NO'                          => [
        'TXT'        => [
            '1' => 'SI',
            '2' => 'NO',
        ],
        'VALORES'    => [
            'SI' => '1',
            'NO' => '2',
        ],
        'VALORES_AB' => [
            'S' => '1',
            'N' => '2',
        ],
    ],


    //DOMINIO TIPO DE UBICACION
    'TIPOS_UBICACIONES'                      => [
        'TXT'     => [
            'PAIS'  => 'Pais',
            'DEPTO' => 'DEPARTAMENTO',
            'CM'    => 'MUNICIPIO',
            'ANM'   => 'CASERIO',
            'CP'    => 'CORREGIMIENTO',
            'C'     => 'Centro Poblado tipo Corregimiento',
            'CAS'   => 'Centro Poblado tipo Caserío',
            'IP'    => 'Centro Poblado tipo Inspección de Policía',
            'IPM'   => 'Centro Poblado tipo Inspección de Policía Municipal',
            'IPD'   => 'Centro Poblado tipo Inspección de Policía Departamental',
            'ZONA'  => 'ZONA',
            'BARR'  => 'BARRIO',
            'VER'   => 'VEREDA',
        ],
        'VALORES' => [
            'PAIS'  => 'PAIS',
            'DEPTO' => 'DEPTO',
            'CM'    => 'CM',
            'ANM'   => 'ANM',
            'CP'    => 'CP',
            'C'     => 'C',
            'CAS'   => 'CAS',
            'IP'    => 'IP',
            'IPM'   => 'IPM',
            'IPD'   => 'IPD',
            'ZONA'  => 'ZONA',
            'BARR'  => 'BARR',
            'VER'   => 'VER',
        ],
    ],

    'RESPUESTA_EPAYCO'                          => [
        'TXT'        => [
            '1' =>    'Aceptada',
            '2' =>    'Rechazada',
            '3' =>    'Pendiente',
            '4' =>    'Fallida',
            '6' =>    'Reversada',
            '7' =>    'Retenida',
            '8' =>    'Iniciada',
            '9' =>    'Expirada',
            '10' =>    'Abandonada',
            '11' =>    'Cancelada',
            '12' =>    'Antifraude',
        ],
    ],

    'FRANQUICIAS_EPAYCO'                          => [
        'TXT'        => [
            'AM' =>    'Amex',
            'BA' =>    'Baloto',
            'CR' =>    'Credencial',
            'DC' =>    'Diners Club',
            'EF' =>    'Efecty',
            'GA' =>    'Gana',
            'PR' =>    'Punto Red',
            'RS' =>    'Red Servi',
            'MC' =>    'Mastercard',
            'PSE' =>    'PSE',
            'SP' =>    'SafetyPay',
            'VS' =>    'Visa',
        ],
    ],

];
