<?php

return [
    'p_cust_id_cliente' => env('PAY_P_CUST_ID_CLIENTE', ''),
    'p_key' => env('PAY_P_KEY', ''),
    'public_key' => env('PAY_PUBLIC_KEY', ''),
    'private_key' => env('PAY_PRIVATE_KEY', ''),
    'test' => env('PAY_TEST', true)
];
