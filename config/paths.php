    <?php

//**********************************************************************
//Para acceder a un path
//Config::get('paths.key');
//**********************************************************************

return [

    //**********************************************************************
    //RUTA DEL PROYECTO
    //**********************************************************************
    'HOME'                  => '/var/www/html/ABCcomptencias/',
    //'HOME'                    => 'D:/wamp64/www/ABCcomptencias/',

    //**********************************************************************
    //RUTA DE IMAGENES DE USUARIOS
    //**********************************************************************
    'IMAGENES_PERFIL'         => '/usuarios/images_perfil/',    

    //**********************************************************************
    //RUTA DE IMAGENES DE FACHADA
    //**********************************************************************
    'IMAGENES_FACHADA'         => '/fachada/images_fachada/',    

    //**********************************************************************
    //RUTA DE IMAGENES DE NOTICIA
    //**********************************************************************
    'IMAGENES_NOTICIA'         => '/fachada/noticias/',    

    //**********************************************************************
    //RUTA DE IMAGENES DE NOTICIA
    //**********************************************************************
    'DOCUMENTOS_NORMAS'         => '/normas/documentos/',

    //**********************************************************************
    //RUTA DE LA CARPETA CON LAS PLANTILLAS DEL PLUGIN DE CALENDARIO
    //**********************************************************************
    //'TEMPLATES_CALENDAR'    => '/plantilla/global/plugins/bower_components/bootstrap-calendar/tmpls/',
    'TEMPLATES_CALENDAR'      => '/plugins/bc/tmpls/',
];
